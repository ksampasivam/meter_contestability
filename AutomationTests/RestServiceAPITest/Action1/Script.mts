﻿Option Explicit

Dim lvxXML, lvfRestBody, lvxRequestXML, url, ContentType, lvxResponseXML, oWinHTTP

lvxXML = Parameter("RequestXML")
If instr(1,lvxXML,"xml") > 0 Then
	

Set lvfRestBody = XMLUtil.CreateXMLFromFile(lvxXML)
lvxRequestXML = lvfRestBody.ToString

url = Parameter("URL")
ContentType = "text/xml"

Set oWinHTTP = CreateObject("WinHttp.WinHttpRequest.5.1")
'oWinHTTP.Option(WinHttpRequestOption_SecureProtocols) = SecureProtocol_TLS1_1 'WinHttpReq.Option(9) = 512
oWinHttp.Open "Post",url,False
oWinHttp.SetRequestHeader "Content-Type", ContentType
oWinHttp.SetRequestHeader "SoapAction",url

oWinHttp.SetTimeouts 60000, 60000, 60000, 60000
oWinHttp.Send lvxRequestXML
lvxResponseXML = oWinHttp.ResponseText

Set oWinHttp = Nothing

Parameter("ResponseXML") = lvxResponseXML

End If