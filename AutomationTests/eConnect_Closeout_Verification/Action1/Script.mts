﻿Dim strSQL_TemplateName_Current, strSQL_TemplateName_Previous, intScratch, cellScratch

Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong

gfwint_DiagnosticLevel = 0 ' CHanging diagnostic level for debugging a particular row

gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

Dim bln_Scratch :  bln_Scratch = CBool(True)

Dim intColNr_InScope, intColNr_RowCompletionStatus
intColNr_InScope = CInt(-1) : intColNr_RowCompletionStatus = CInt(-1)

Dim str_rowScenarioID, intColNR_ScenarioID, temp_Next_ScenarioFunction_canStart_atOrAfter_TS

Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = True

Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
Dim cStr_BarText_Format

BASE_XML_Template_DIR = cBASE_XML_Template_DIR
cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^RequestID`{4}^NMI`{5} ."

Dim strFolderNameWithSlashSuffix_Scratch
strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"

strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciMetaData_Type_and_VersionNr	) = "is_FieldSetVerification_vn01" ' is_ is InformationSet	( 0)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldName						) = "<fn_UnInitialized>"								'	( 1)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldValue						) = "<fv_UnInitialized>"								'	( 2)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciAsOfTimestamp					) = "<aoTS_UnInitialized>"							'	( 3)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldType							) = "<ft_UnInitialized>"								'	( 4)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldVerificationPoint			) =  1													'	( 5)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldSource						) = "gMTS`<tbd_qq>"									'	( 6)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare01					) = "<mt>"												'	( 7)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare02					) = "<mt>"												'	( 8)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare03					) = "<mt>"												'	( 9)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare04					) = "<mt>"												'	(10)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_NameIndexList				) = _
",ciMetaData_Type_and_VersionNr`0,ciFieldName`1,ciFieldValue`2,ciAsOfTimestamp`3,ciFieldType`4,ciFieldVerificationPoint`5,ciFieldSource`6" & _
",ciField_spare01`7,ciField_spare02`8,ciField_spare03`9,ciField_spare04`10,ciField_NameIndexList`11,"						' 	(11)


'		On error resume next
'			strAr_Single_VerifyKey_Fields_local_Defaults	= scratchVariant
'		On error goto 0
strAr_Single_VerifyKey_Fields_local_Defaults	= strAr_Single_VerifyKey_Fields_testCase_Defaults	 
strAr_Single_VerifyKey_Fields 					= strAr_Single_VerifyKey_Fields_local_Defaults ' set the work-area strAr from the local defaults-strAr
strAr_Single_VerifyKey_Fields_logXML			= strAr_Single_VerifyKey_Fields_local_Defaults 

Set hFWobj_SB = DotNetFactory.CreateInstance( "System.Text.StringBuilder" )
Set gFWobj_SystemDate = DotNetFactory.CreateInstance( "System.DateTime" )

' push the RunStats to the DriverWS report-area
Dim cellTopLeft, iTS, iTS_Max, rowTL, colTL

Dim tsOfRun : tsOfRun = DateAdd("d", -0, Now()) ' qq

gDt_Run_TS = tsOfRun

Dim BASE_GIT_DIR : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"
Dim strCaseBaseAutomationDir 
' strCaseBaseAutomationDir = BASE_AUTOMATION_DIR
strCaseBaseAutomationDir  = BASE_GIT_DIR
'strCaseBaseAutomationDir = "r:\"

' ####################################################################################  UFT configurations at the start of test  - START ####################################################################################  

Dim Temp_Execution_Results_Location  : Temp_Execution_Results_Location = "C:\_data\Test_Execution_Results\"        'Share drive for AMI Energisation End to End project
Dim strRunLog_Folder

Dim qtTest, qtResultsOpt, qtApp


Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
If qtApp.launched <> True Then
	qtApp.Launch
End If 'end of If qtApp.launched <> True Then

qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
qtApp.Options.Run.RunMode = "Fast"
qtApp.Options.Run.ViewResults = False


Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539

Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
	str_AnFw_FocusArea = ""
Else
	str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
End If 'end of If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 

strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", Array(fnAttachStr(Temp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
gQtTest.Name                                 , _
Parameter.Item("Environment")                       , _
Parameter.Item("Company")                           , _
fnTimeStamp(gDt_Run_TS, "YYYY`MM`DD_HH`mm`ss")))
' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))

Path_MultiLevel_CreateComplete strRunLog_Folder
gFWstr_RunFolder = strRunLog_Folder

qtResultsOpt.ResultsLocation = gFWstr_RunFolder
gQtResultsOpt.ResultsLocation = strRunLog_Folder


'                                        
'                                        qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
'                                        qtApp.Options.Run.MovieCaptureForTestResults = "Never"
'                                        qtApp.Options.Run.MovieSegmentSize = 2048
qtApp.Options.Run.RunMode = "Fast"
'                                        qtApp.Options.Run.SaveMovieOfEntireRun = False
'                                        qtApp.Options.Run.StepExecutionDelay = 0
'                                        qtApp.Options.Run.ViewResults = False
qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
qtApp.Options.Run.ScreenRecorder.RecordSound = False
qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True

'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html

gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
gQtApp.Options.Run.ViewResults                = False



' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  

' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  

initialize_scratch_Dictionary
clear_scratch_Dictionary gdict_scratch

' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  


If 1 = 1  Then		
	
	
	gFwInt_DataParameterRow_ErrorsTotal = 0 : gFwInt_AbandonIteration_ReasonCount = 0
	gFWbln_ExitIteration = False
	
	Set gFWoDnf_SysDiags = Dotnetfactory.CreateInstance("System.Diagnostics.StackFrame")
	
	Dim MethodName
	On Error Resume Next
	' MethodName = new oDnf_SysDiags.StackTrace().GetFrame(0).GetMet hod().Name
	MethodName = oDnf_SysDiags.GetMethod().Name
	On Error Goto 0
	
	Dim micFinalStatus, intStageSuccessCount, intWantedSuccessCount : intStageSuccessCount = 0
	Dim ctSucc, intStageSuccessAr : intStageSuccessAr = Split(",0,0,0,0", ",") : intWantedSuccessCount = UBound(intStageSuccessAr)
	Dim BASE_XML_Template_DIR
	
	Dim intNrOfEventsToTrack    : intNrOfEventsToTrack      = 10
	Dim tsAr_EventStartedAtTS     : tsAr_EventStartedAtTS     = Split(String(intNrOfEventsToTrack-1, ","), ",")
	Dim tsAr_EventEndedAtTS       : tsAr_EventEndedAtTS       = Split(String(intNrOfEventsToTrack-1, ","), ",")
	Dim tsAr_EventPermutationsCount : tsAr_EventPermutationsCount = Split(Replace(String(intNrOfEventsToTrack-1, ","), ",", "0,")&"0", ",")
	
	Const iTS_Stage_0 = 0
	Const iTS_Stage_I = 1
	Const iTS_Stage_II = 2
	Const iTS_Stage_III = 3
	
	
	tsAr_EventStartedAtTS(iTS_Stage_0) = Now()
	
	Environment.Value("COMPANY")=Parameter.Item("Company")
	Environment.Value("ENV")=Parameter.Item("Environment")
	
	
	Dim intColNames_RowNr, intColNames_ColNr_Start, intColNames_ColNr_End
	Dim r_rtTemplate, intColNr_RowType, intColNr_DataCol_First, intColNr_DataCol_Last,  intColNr_ScenarioStatus
	Dim intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
	
	' Load the MasterWorkBook objWB_Master
	Dim strWB_noFolderContext_onlyFileName  
	strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
	
	Dim wbScratch
	
	Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
	Dim  int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr
	
	
	
	fnExcel_CreateAppInstance  objXLapp, True
	
	'Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"
	LoadData_RunContext_V4  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
	
	If LCase(Environment.Value("COMPANY")) = "sapn" Then
		Environment.Value("COMPANY_CODE")="ETSA"
	Else
		Environment.Value("COMPANY_CODE")=Environment.Value("COMPANY")
	End If 'end of If LCase(Environment.Value("COMPANY")) = "sapn" Then
	
	' ####################################################################################################################################################################################
	' #####################################################                 LOGGING STEP 1 - SETUP      #################################################################################################
	' ####################################################################################################################################################################################
	fn_setup_Test_Execution_Log Parameter.Item("Company"), Parameter.Item("Environment"), "N", "mcB2B_SO_01", GstrRunFileName, "ws_B2B_SO", strRunLog_Folder, gObjNet.ComputerName
	Printmessage "i", "Start of Execution", "Starting execution of mcB2B_SO_01 scripts"
	
	
	' qq 2017`01Jan`24Tue_16`33`22 BrianM Workaround
	Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
	Set wbScratch = Nothing
	
	
	objXLapp.Calculation = xlManual
	objXLapp.screenUpdating=False
	
	Dim objWS_DataParameter 						 
	Set objWS_DataParameter           					= objWB_Master.worksheets(Parameter("Worksheet")) ' wsMsDT_mrCntestbty_Objectn") ' qq this must become a parm
	objWS_DataParameter.activate
	
	Dim objWS_templateXML_TagDataSrc_Tables 	
	Set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
	Set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
	' Dim objWS_ScenarioOutComesValidations		: set objWS_ScenarioOutComeValidations 		= objWB_Master.worksheets("wsSpec_ScenarioOutcomeVerify") ' reference the Scenario ValidationRules WS
	
	Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
	Dim tempInt_RoleIdentificationCounter
	' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
	'	Get headers of table in a dictionary
	Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	
	
	gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
	Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
	gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
	' Load the first/title row in a dictionary
	int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
	
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	
	Dim intLogCol_Start, intLogCol_End,  intLogRow_Start, intLogRow_End 
	
	'	if logging is being done to an array, get the size of the range to be loaded to the array, and load it
	If gcFw_intLogMethod    =     gcFw_intLogMethod_RangeToWS Then	
		
		intLogRow_Start        =    gfwobjWS_LogSheet.range("rgWS_RowNr_Data_FirstMinusOne").row        +    1
		intLogRow_End         =    gfwobjWS_LogSheet.range("rgWS_RowNr_Data_LastPlusOne").row        -    1
		intLogCol_Start        =     gfwobjWS_LogSheet.range("rgWS_ColNr_Data_First_MinusOne").column    +    1
		intLogCol_End        =     gfwobjWS_LogSheet.range("rgWS_ColNr_Data_Last_PlusOne").column    -    1
		
		gfwAr_DriverSheet_Log_rangeFor_PullPush = _
		gfwobjWS_LogSheet.range ( gfwobjWS_LogSheet.cells ( intLogRow_Start, intLogCol_Start), gfwobjWS_LogSheet.cells(intLogRow_End, intLogCol_End) )
		intLogRow_End = intLogRow_End         ' qq for debugging    '        else            '    =    gcFw_intLogMethod_WSdirect
		
	End If 'end of If gcFw_intLogMethod    =     gcFw_intLogMethod_RangeToWS Then	
	
	Dim vntNull, mt
	
	MethodName = gFWoDnf_SysDiags.GetMethod().Name		
	objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
	objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
	'objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
	'objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
	objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= "0"
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
	objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = Now 
	
	
	' setAll_RequestIDs_Unique objWS_DataParameter
	
	'objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula = "'" & fnWindows_UserName(false)'
	'objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula = "'" & fnFW_GetComputerName()
	
	
	gDt_Run_TS = Now
	gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 
	
	Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE, cellReportStatus_SingleScenario
	'		set cellReportStatus_FwkProcessStage = objWS_DataParameter.range("rgWS_cellReportStatus_FwkProcessStage")
	'		set cellReportStatus_ROLE            = objWS_DataParameter.range("rgWS_cellReportStatus_ROLE")
	On Error Resume Next
	' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	'Set cellReportStatus_SingleScenario = objWS_DataParameter.range("rgWS_cellReportStatus_SingleScenario")
	On Error Goto 0
	
	
	'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
	Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
	dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
	Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
	intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
	intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
	intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
	fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
	
	'set cellTopLeft = objWS_DataParameter.range("rgWS_RunReport_TopLeftCell_Stage0_StartTS")
	' rowTL = cellTopLeft.row : colTL = cellTopLeft.column
	rowTL = 1 : colTL = 1
	
	Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
	
	Dim r
	Dim objDBConn_TestData_A, objDBRcdSet_TestData_A
	
	Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
	r = 0
	Set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")
	
	
	Dim objDBConn_TestData_B, objDBRcdSet_TestData_B
	Set objDBConn_TestData_B    = CreateObject("ADODB.Connection") : Set objDBRcdSet_TestData_B  = CreateObject("ADODB.Recordset") ': set objDBRcdSet_TestData_C  = CreateObject("ADODB.Recordset")
	Dim	DB_CONNECT_STR_MTS
	DB_CONNECT_STR_MTS = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
	Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
	Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
	Environment.Value("USERNAME") 	& 	";Password=" & _
	Environment.Value("PASSWORD") 	& ";"
	
	Dim objDBConn_TestData_C, objDBRcdSet_TestData_C
	Set objDBConn_TestData_C    = CreateObject("ADODB.Connection")
	Set objDBRcdSet_TestData_C  = CreateObject("ADODB.Recordset")
	
	
	Dim	strEnvCoy
	strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")
	
	Dim	DB_CONNECT_STR_CIS
	DB_CONNECT_STR_CIS = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
	Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
	Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
	Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
	Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"

	Dim objDBConn_TestData_D, objDBRcdSet_TestData_D
	Set objDBConn_TestData_D    = CreateObject("ADODB.Connection")
	Set objDBRcdSet_TestData_D  = CreateObject("ADODB.Recordset")
	
	
'	Dim	DB_CONNECT_STR_USB
'	DB_CONNECT_STR_USB = _
'	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
'	Environment.Value( strEnvCoy & 	"_USB_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
'	Environment.Value( strEnvCoy & 	"_USB_SERVICENAME"		) 	& "))); User ID=" & _
'	Environment.Value( strEnvCoy & 	"_USB_USERNAME"			) 	& ";Password=" & _
'	Environment.Value( strEnvCoy & 	"_USB_PASSWORD"			)  	& ";"
	
	Dim objDBConn_TestData_E, objDBRcdSet_TestData_E
	Set objDBConn_TestData_E    = CreateObject("ADODB.Connection")
	Set objDBRcdSet_TestData_E  = CreateObject("ADODB.Recordset")
	
	
	Dim	DB_CONNECT_STR_TEL
	DB_CONNECT_STR_TEL = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
	Environment.Value( strEnvCoy & 	"_TEL_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
	Environment.Value( strEnvCoy & 	"_TEL_SERVICENAME"		) 	& "))); User ID=" & _
	Environment.Value( strEnvCoy & 	"_TEL_USERNAME"			) 	& ";Password=" & _
	Environment.Value( strEnvCoy & 	"_TEL_PASSWORD"			)  	& ";"
	
	
	
	'IEE DB Connections
	Dim objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE
	Set objDBConn_TestData_IEE    = CreateObject("ADODB.Connection")
    set objDBRcdSet_TestData_IEE  = CreateObject("ADODB.Recordset")
    
    Dim	DB_CONNECT_STR_IEE
	DB_CONNECT_STR_IEE = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
	Environment.Value(strEnvCoy & 	"_IEE_HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
	Environment.Value(strEnvCoy & 	"_IEE_SERVICENAME") 	&	"))); User ID=" & _
	Environment.Value(strEnvCoy & 	"_IEE_USERNAME") 	& 	";Password=" & _
	Environment.Value(strEnvCoy & 	"_IEE_PASSWORD") 	& ";"

	
	
	
	
	'Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL") & "SORD\"
	
	Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
	Dim strQueryTemplate, dtD, Hyph, strNMI
	Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
	Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
	intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
	
	Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
	
	Dim intSQL_Pkg_ColNr, strSQL_RunPackage
	Dim tmp_EBMID, temp_TP_WORK_ORDER, temp_NO_TASK, tmp_AllSO_Check_Flag, tmp_Equip_Check_Flag
	
	
	'					Dim intRowNr_DPs_SmallLarge : intRowNr_DPs_SmallLarge = objWS_DataParameter.range("rgWS_RowNr_DPs_SmallLarge").row
	
	Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
	intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
	intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
	
	intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
	
	
	'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
	intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
	sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
	
	Dim strColName
	
	intScratch = CInt (fnTimeStamp ( Now,  "yW" ) )
	If ((intScratch 	= 710) Or (intScratch 	= 711)) Then
		strColName 	= "InScope_YN"		  			
		'	\Select Case gobjNet.UserName 
		Select Case gobjNet.ComputerName
			Case "CORPVMUFT01z"		:	strColName 	= "qq"
			Case "CORPVMUFT02z" 	:	strColName 	= "qq"
			Case "PCA19323a"		 	:	strColName 	= "InScope_Umesh"
			Case "PCA19323b"		 	:	strColName 	= "InScope_Preeti"
			Case "PCA19323c"		 	:	strColName 	= "InScope_Brian"
			Case Else	'	CORPVMUFT06	CORPVMUFT02 PCA19323
		End Select
		
	End If 'end of If ((intScratch 	= 710) Or (intScratch 	= 711)) Then
	objWS_DataParameter.calculate	'	to ensure the filename is updated
	
	
	
	Dim  str_xlBitNess : str_xlBitNess = objWS_DataParameter.range("rgWS_XL_BitNess_x32x64").value
	
	sb_File_WriteContents  _
	gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , True, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
	
	
	intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
	intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")
	
	Dim blnColumnSet : blnColumnSet = CBool(True)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
	If IsEmpty(intColNr_InScope) Then	
		blnColumnSet =False
	ElseIf intColNr_InScope = -1 Then
		blnColumnSet =False
	End If 'end of If IsEmpty(intColNr_InScope) Then	
	If Not blnColumnSet Then 
		'		qq log this	
		'		objXLapp.Calculation = xlManual
		objXLapp.screenUpdating=True
		exittest
	End If 'end of If Not blnColumnSet Then 
	
	objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
	'objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope
	'objWS_DataParameter.range("rgWS_Stage_Config_LocalOrGlobal") = "Local"			'		<<<===   should this always be the way the run-scope is managed ?
	objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").formula = "'" & intRowNr_LoopWS_RowNr_StartOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).formula = "'" & intRowNr_LoopWS_RowNr_FinishOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
	'		objWS_DataParameter.cells ( intRowNr_LoopWS_RowNr_StartOn  , intColNr_InScope ).select   '   qq reInstate ?? 
	objWS_DataParameter.calculate
	objxlapp.screenupdating = True
	'	objWS_DataParameter.Calculate
	
	objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
	
	tsAr_EventEndedAtTS(iTS_Stage_0) = Now()
	
	Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
	Dim  intMasterZoomLevel 
	
	Dim rowStart, rowEnd, colStart, colEnd
	Dim RangeAr 
	
	'objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
	
	
End If 'end of If 1 = 1  Then	

objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope


fnLog_Iteration  cFW_Exactly, 100 '  intRowNr_CurrentScenario ' cFW_Plus cFW_Minus 




Const gcInt_NrOf_OneBased_FlowFunctions = 10									'	"OneBased" means that q(0,n) will never be used operationally
Const gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction = 10		'	"OneBased" means that q(n,0) will never be used operationally
Dim	   gcInt_NrOf_Flows
Const cstrDelimiter = "," : Const cAllElements = -1
Const cStr_flowFnName_NoneProvidedYet = "flowFnName_NoneProvidedYet"
Const cStr_Template_dictionaryKey_ScenarioRow_FlowNrName_FlowStepNrName_ParameterNrName_BeforeAfter = "row`^flow`^step`^parm`^befAft`"

Dim q ()	'	this is the 2-D FlowFunction_ParameterArray; its name is "q" to keep the name as short as possible
Const c_StaticValue = "§"	'	this value : 	a) indicates that a flowFunction parameter is static, and 
'											b) is ASSUMED to be  value that will never be passed as a parameter to a FlowFunction
Dim qNm()				'	this is the parameterName array
Dim qBL()				'	this is the parameterBaseLine array
Dim fParmsCt()			'	this is the FunctionParmsCount array
Dim strAr_Flow_FunctionNames()		'	this is the FlowNr / FunctionList array
Dim intAr_Flow_nrOf_Functions_inThisFlow()		

Dim strParameterNameList_for_aSingle_FlowFunction

'	make the parameterArray large enough to handle all the FlowFunctions and all the parms for whichever FlowFunction has the most parms
ReDim 	q 				( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )
ReDim 	qNm			( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )
ReDim	qBL				( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )			
ReDim	fParmsCt		( gcInt_NrOf_OneBased_FlowFunctions                                                                                                                       )	
gcInt_NrOf_Flows = 16	      
ReDim	strAr_Flow_fNames						( gcInt_NrOf_Flows )	 	
ReDim intAr_Flow_nrOf_Functions_inThisFlow	( gcInt_NrOf_Flows )


Dim f, p: f= CInt(0) : p = CInt(0)

For f = 0 To gcInt_NrOf_OneBased_FlowFunctions 									'	iterate over the functionRows
	For p = 1 To gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction	'		iterate over the ParameterColumns
		If f = 0 Then 
			q(f , 0) = "!! It is a design constraint, that none of the cells, or columns, of the array, in THIS row-zero, are used, for any purpose, specifically, none of row zero is used, and " & _
			"the first column/element of rows one-and-later, contain the FlowFunction-Name for the row  !! "
		Else
			q(f , p) = c_StaticValue		'	parameters that are not static will have their c_StaticValue replaced with a variant that contains the variable value to be used
			'	this means that those cells must never be static in themselves, as they will be passed to FlowFunctions as ByRef parameters,
			' 		so that if the function changes the value, we can detect and log the fact, then later desk-debug what happened, without having to do a rerun
		End If 'end of If f = 0 Then 
	Next ' For p = 1 To gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction	'		iterate over the ParameterColumns
	'	set the FlowFunctionName equal to none for all function-parameter rows (as these will align with the case statement that processes the functions, below
	'		i.e. q(f,0) will contain the FlowFunctionName
	q(f , 0) = cStr_flowFnName_NoneProvidedYet
Next ' For f = 0 To gcInt_NrOf_OneBased_FlowFunctions 									'	iterate over the functionRows

Dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = CInt(0)	: intFirst= CInt(0)	: intSecond = CInt(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = CInt(1)	' qq



sb_File_WriteContents   	_
str_Fq_LogFileName, gfsoForAppending , True, _
fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf

Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun

tsStartOfRun = Now()

sb_File_WriteContents   	_
str_Fq_LogFileName, gfsoForAppending , False, _
fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf

Dim strRunScenario_PreScenarioRow_keyTemplate , strRunScenario_PreScenarioRow_Key
strRunScenario_PreScenarioRow_keyTemplate = _
"^RunNr`{0}^TestName`{1}^Envt`{2}^Coy`{3}^ScenarioList`^ScenarioChunk`^DriverSheetName`{4}^" ' 
'	Examples   1 2 3                              DEVMC      CITI                                                                                                              

strRunScenario_PreScenarioRow_Key  = fn_StringFormatReplacement ( _
strRunScenario_PreScenarioRow_keyTemplate , _
Array ( fnTimeStamp( Now(), 	cFWstrRequestID_StandardFormat) , gQtTest.Name  , Parameter.Item("Environment") , Parameter.Item("Company")  , objWS_DataParameter.name ) )

' the keys below must be set from inside the DataSheet-ScenarioRow processing loop
Dim strRunScenario_ScenarioRow_keyTemplate, strRunScenario_ScenarioRow_Key
strRunScenario_ScenarioRow_keyTemplate = "ScenarioRowNr`{0}^InfoType`FieldValue_Actual^FlowName`{1}^"
' examples                                                                                  1001 1100                                                                   Flow02
Dim str_FlowFunction_Seq_Name_templateKey , str_FlowFunction_Seq_Name_Key 
str_FlowFunction_Seq_Name_templateKey  = "FlowFunction_Seq_Name`{0}_{1}^"
'                                                                                                                                 1   DataMine
Dim str_DataContext_FieldName_templateKey,  str_DataContext_FieldName_Key
str_DataContext_FieldName_templateKey = "DataContext_System_AccessMethod_AccessPoint`{0}_{1}_{2}^FieldName`{3}^Field_ActualValue`inItem^"
'                                                                                                                      dcCIS_SQL_ap1  dcMTS_GUI_ServiceOrder                       NMI


Dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow


Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_Scenario : intRowNr_CurrentScenario = CInt(0) ' formerly cLng
Dim cInt_MinutesRequired_toComplete_eachScenario, cDbl_ExtraFactor_ToWait_for_RunToComplete, tsStopRunWhenRunningTooLong
Dim cInt_MinutesPerDay : cInt_MinutesPerDay = 24 * 60
Dim cInt_SecondsPerDay : cInt_SecondsPerDay = 60 * cInt_MinutesPerDay 
Dim int_rgWS_NumberOfIncompleteScenarios
Dim dt_rgWS_RunWillCeaseAt_TimeStamp : dt_rgWS_RunWillCeaseAt_TimeStamp = Now()-7
cInt_MinutesRequired_toComplete_eachScenario = CInt(5)
cDbl_ExtraFactor_ToWait_for_RunToComplete = CDbl(1.2)


Dim str_ScenarioRow_FlowName 
'Dim int_FlowHasNotStartedYet : int_FlowHasNotStartedYet =  objWS_DataParameter.range("rgWS_TestHasNotStarted").value
Dim cStr_FlowNumbers 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= CInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= CInt(-1)

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = False

Dim intColNr_Request_ID 
' intColNr_Request_ID 				= dictWSDataParameter_KeyName_ColNr("Request_ID")
intColNr_Request_ID 				= dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
intColNr_ScenarioStatus 			= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")
Dim intColNr_Flow_CurrentStepNr : intColNr_Flow_CurrentStepNr = dictWSDataParameter_KeyName_ColNr("Flow_CurrentStepNr") 
Dim strRole, strInitiator_Role, var_ScheduledDate, temp_INITIATOR, temp_INITIATOR_NAME
Dim strInScope, strRowCompletionStatus
Dim strSQL_USB, strSQL_CIS, strSQL_MTS, strSQL_IEE, strSQL_NMIREF, str_Iteration, str_Total_Iterations, str_SOType_Keyword, strINITIATED_DATE, strEFFECTIVE_DATE, str_Total_CATS_OUT, strCRCode

objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=True
objWB_Master.save



gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

' Get headers of table in a dictionary
Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare



' Load the first/title row in a dictionary
int_MaxColumns = UBound(										gvntAr_RuleTable,2)

'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue

'fnMTS_WinClose
'OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")

' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
''	
' Reset error flags
fn_reset_error_flags
Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID

int_ctr_no_of_Eligible_rows = 0
int_ctr_total_Eligible_rows = 0
int_ctr_no_of_passed_rows = 0


' For i  = 1 to 1000 ' this loop would be decided on whether there are any remaining rows to be executed
i = 1
Do
	
	
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart To int_rgWS_cellReportStatus_rowEnd			
		
		strInScope 							= 	UCase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope).value )
		str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
		str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
		temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value
		
		
		' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
		
	If   strInScope = cY  And str_ScenarioRow_FlowName <> "" And LCase(str_Next_Function) <> "fail" And LCase(str_Next_Function) <> "end" Then
			
		If i = 1 Then
			If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 or InStr(1,LCase(temp_Scenario_ID),"step3") > 0 Then
				objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "WaitingForPrevStep"
			Else
				objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start"
				str_Next_Function = "start"
				fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			End If 'end of If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 Then
			
			int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
			int_ctr_total_Eligible_rows = int_ctr_total_Eligible_rows + 1			
			objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows					
		End If ' end of If i = 1 Then
		
		' Generate a filename for screenshot file
		gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", Array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, Left(temp_Scenario_ID,10)))
		
		' ####################################################################################################################################################################################
		' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
		' ####################################################################################################################################################################################
		fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", "", "", "", "", "", ""
		
		Select Case LCase(str_ScenarioRow_FlowName)
			
			Case "flow1":
			Do
				str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
				Select Case str_Next_Function
					Case "start" ' this is datamine
					' First function
					tsNow = Now() 
					tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
					If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
						Exit Do
					End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
					
					
					If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
						
						PrintMessage "p", "Data mining", "Scenario row ("& intRowNr_CurrentScenario &") - Starting data mine"
						
						'--------------------------------Reading the data input from ECONNECT_DATA_ATTRIBUTES Table----------------------------------------' 
						strSQL_TEL = "ECONNECT_DATA_ATTRIBUTES"
						PrintMessage "p", " ECONNECT_DATA_ATTRIBUTES Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_TEL  &")"
						strSQL_TEL = fnRetrieve_SQL(strSQL_TEL)
						
												
						' Perform replacements
						If InStr(1, strSQL_TEL,"<UNIQUE_SCENARIO_ID>") > 0 Then
							strSQL_TEL = Replace ( strSQL_TEL , "<UNIQUE_SCENARIO_ID>" , objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value, 1, -1, vbTextCompare)
						End If 
						

						strScratch = Execute_SQL_Populate_DataSheet (objWS_DataParameter, DB_CONNECT_STR_TEL, objDBConn_TestData_E, objDBRcdSet_TestData_E, strSQL_TEL, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("eConnect_Details_Columns_Names"), dictWSDataParameter_KeyName_ColNr("eConnect_Details_Columns_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
						objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
						
						if ucase(strScratch) = "PASS" Then	
	
								
								'--------------------------------Verifying the All CIS Service Order details----------------------------------------' 
								
						If gFWbln_ExitIteration = False Then
													
							tmp_fn_status = ""	
							tmp_AllSO_Check_Flag = "pass"	
							
							If trim(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("No_Of_Service_Orders") ).value) <> "" Then
								str_Total_Iterations = Cint(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("No_Of_Service_Orders") ).value)
														
								For str_Iteration = 1 To str_Total_Iterations Step 1
									
									Select Case  str_Iteration 
										Case 1  
											str_SOType_Keyword = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("First_Service_Order_Type") ).value
										Case 2  
											str_SOType_Keyword = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Second_Service_Order_Type") ).value
										Case 3 
											str_SOType_Keyword = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Third_Service_Order_Type") ).value
									End Select
									
									If trim(str_SOType_Keyword) <> "" Then
										
										
										strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("SERVICE_ORDER_DETAILS_QUERY")).value 
										If strSQL_CIS <> ""  Then
										
											PrintMessage "p", "CIS SO Details Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_CIS  &")"
											strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
											tmp_NMI = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI") ).value
											tmp_TP_WORK_ORDER = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(str_SOType_Keyword & "_TP_WORK_ORDER")).value
											tmp_NO_TASK = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(str_SOType_Keyword & "_NO_TASK") ).value
											
											If InStr(1, strSQL_CIS,"<NMI>") > 0 Then
												strSQL_CIS = Replace ( strSQL_CIS , "<NMI>" ,tmp_NMI , 1, -1, vbTextCompare)
											End If 									
											If InStr(1, strSQL_CIS,"<TP_WORK_ORDER>") > 0 Then
												strSQL_CIS = Replace ( strSQL_CIS , "<TP_WORK_ORDER>" ,tmp_TP_WORK_ORDER , 1, -1, vbTextCompare)
											End If 
											If InStr(1, strSQL_CIS,"<NO_TASK>") > 0 Then
												strSQL_CIS = Replace ( strSQL_CIS , "<NO_TASK>" ,tmp_NO_TASK , 1, -1, vbTextCompare)
											End If 
											If InStr(1, strSQL_CIS,"<EFFECTIVE_DATE>") > 0 Then
												tmp_EFFECTIVE_DATE = fnTimeStamp(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "eConnect_Details_Columns_Names", "eConnect_Details_Columns_Values", intRowNr_CurrentScenario, "EXECUTION_DATE"),"DD-MmM-YY")
												strSQL_CIS = Replace ( strSQL_CIS , "<EFFECTIVE_DATE>" ,UCASE(tmp_EFFECTIVE_DATE) , 1, -1, vbTextCompare)
											End If 
											
											' Update query in SQL Query - SQL_Populated cell
											strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
											strScratch = strScratch & vbCrLf & "SERVICE_ORDER_DETAILS Query:" & vbCrLf & strSQL_CIS
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
											strScratch = ""
											
											strScratch = Execute_SQL_Populate_DataSheet  (objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A,  objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "3",  "","", "","")
											
											If strScratch = "PASS" Then
												' Write the data in all respective columns for all roles
												strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value
												strScratch = strScratch & ",NMI"& str_Iteration & ",NETWORKSERVICEORDER"& str_Iteration & ",SOTYPE"& str_Iteration & ",SOEFFECTIVEDATE"& str_Iteration & ",SOCREATEDBY"& str_Iteration & ",SODATECREATED" & str_Iteration & ",TASKNUMBER" & str_Iteration & ",TASKTYPE" & str_Iteration & ",SOSTATUSDESC" & str_Iteration & ",REQUESTER" & str_Iteration & _
												",UTILEQUIPNUMBER" & str_Iteration & ",CHARGE" & str_Iteration & ",CHARGEAMOUNT" & str_Iteration & ",SAPNUMBER" & str_Iteration & ",SAPNAME"& str_Iteration & ",PRIMARY_SO" & str_Iteration & ",RETAILERSONAME" & str_Iteration & ",RETAILERSONUMBER" & str_Iteration & ",SOREASONDESCRIPTION" & str_Iteration & ",WORKCOMPLETED" & str_Iteration & ",CANCELREASON" & str_Iteration & ",WAIVEFEE" & str_Iteration & ",WAIVEREASON" & str_Iteration 
												
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = strScratch
												strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
												
												strScratch = strScratch & "," & objDBRcdSet_TestData_A.Fields("NMI").value & "," & objDBRcdSet_TestData_A.Fields("NETWORKSERVICEORDER").value & "," & objDBRcdSet_TestData_A.Fields("SOTYPE").value & "," & objDBRcdSet_TestData_A.Fields("SOEFFECTIVEDATE").value & "," & objDBRcdSet_TestData_A.Fields("SOCREATEDBY").value & "," & objDBRcdSet_TestData_A.Fields("SODATECREATED").value & "," & objDBRcdSet_TestData_A.Fields("TASKNUMBER").value _
												& "," & objDBRcdSet_TestData_A.Fields("TASKTYPE").value & "," & trim(objDBRcdSet_TestData_A.Fields("SOSTATUSDESC").value)  & "," & objDBRcdSet_TestData_A.Fields("REQUESTER").value  & "," & objDBRcdSet_TestData_A.Fields("UTILEQUIPNUMBER").value & "," & objDBRcdSet_TestData_A.Fields("CHARGE").value & "," & objDBRcdSet_TestData_A.Fields("CHARGEAMOUNT").value & "," & objDBRcdSet_TestData_A.Fields("SAPNUMBER").value & "," & objDBRcdSet_TestData_A.Fields("SAPNAME").value _ 
												& "," & objDBRcdSet_TestData_A.Fields("PRIMARY_SO").value & "," & objDBRcdSet_TestData_A.Fields("RETAILERSONAME").value & "," & objDBRcdSet_TestData_A.Fields("RETAILERSONUMBER").value & "," & objDBRcdSet_TestData_A.Fields("SOREASONDESCRIPTION").value & "," & objDBRcdSet_TestData_A.Fields("WORKCOMPLETED").value & "," & objDBRcdSet_TestData_A.Fields("CANCELREASON").value & "," & objDBRcdSet_TestData_A.Fields("WAIVEFEE").value & "," & objDBRcdSet_TestData_A.Fields("WAIVEREASON").value
												
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = strScratch
												
											End if
											
										If strScratch <> "" Then
											strScratch = "PASS"  
								
											If ucase(strScratch) = "PASS" Then											
												tmp_fn_status = fnService_Order_Details_Check(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr,  intRowNr_CurrentScenario, tmp_TP_WORK_ORDER, str_Iteration)
												
												If lcase(tmp_fn_status) = "pass" Then
													PrintMessage "p", tmp_TP_WORK_ORDER & " Service Order Verified Successully" , "Actual Value matches expected Values"
												Else 
													PrintMessage "f", "Issue with " & tmp_TP_WORK_ORDER & " Service Order" , gFWstr_ExitIteration_Error_Reason	
													'gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason &  vbCrLf & "Issue with Service_Order_Type column in the driver sheet"
													tmp_AllSO_Check_Flag = "fail"						
												End If
											Else 
												'gFWbln_ExitIteration = True
												gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason &  vbCrLf & tmp_TP_WORK_ORDER & " Service Order is not created by USB. Test Failed"
												PrintMessage "f", "Service Order Check Failed" , gFWstr_ExitIteration_Error_Reason
											End If
										Else
										 
										 PrintMessage "f", "Service Order Check Failed" , gFWstr_ExitIteration_Error_Reason
										
										End If 'trScratch <> "" Then
	 								End If ' end of If strSQL_CIS <> ""  Then
									
	    								str_SOType_Keyword = ""
									strSQL_CIS = ""
									tmp_TP_WORK_ORDER = ""
									tmp_NO_TASK = ""
									tmp_fn_status = ""
										
								Else 
									gFWbln_ExitIteration = True
									'gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason &  vbCrLf & "Issue with Service_Order_Type column in the driver sheet"
									PrintMessage "f", "Service Order Check Failed" , gFWstr_ExitIteration_Error_Reason
								
								End If ' If trim(str_SOType_Keyword) <> "" Then
							Next							
						
							If lcase(tmp_AllSO_Check_Flag) = "fail" Then
								gFWstr_ExitIteration_Error_Reason =gFWstr_ExitIteration_Error_Reason & vbCrLf & "Issue with  Service Order details. Please refer the 'VerificationComments' column to check expected vs actual"
								'gFWbln_ExitIteration = True
								PrintMessage "f", "Service Order Check Failed" , gFWstr_ExitIteration_Error_Reason	
							End If
							
						Else 
							PrintMessage "f", "No_Of_Service_Orders is blank in driver sheet", "Please specify the No_Of_Service_Orders for every row"
							gFWbln_ExitIteration = True
							gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "No_Of_Service_Orders is blank in driver sheet. Please specify the No_Of_Service_Orders for every row"
						End If 'end of If trim(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("No_Of_Service_Orders") ).value) <> "" Then
								
								
					End If ''If gFWbln_ExitIteration = False Then
								'--------------------------------Verifying the All SP details----------------------------------------' 
								
								If gFWbln_ExitIteration = False Then
									' clear scratch dictionary
									clear_scratch_Dictionary gdict_scratch
									
									'strSQL_CIS = ""
									strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("SERVICE_PROVISION_DETAIL_QUERY")).value 
									If strSQL_CIS <> ""  Then
									
										PrintMessage "p", "SP Details Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_CIS  &")"
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										If InStr(1, strSQL_CIS,"<NMI>") > 0 Then
											strSQL_CIS = Replace ( strSQL_CIS , "<NMI>" ,tmp_NMI , 1, -1, vbTextCompare)
										End If 									
										
										' Update query in SQL Query - SQL_Populated cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "SP Details Query:" & vbCrLf & strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
										strScratch = ""
										
										'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = ""
										'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = ""
										
										strScratch = Execute_SQL_Populate_DataSheet (objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A,  objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),"")
										
										if ucase(strScratch) = "PASS" Then	
										
											tmp_fn_status = fnService_Provision_Details_Check(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, intRowNr_CurrentScenario,Int_CurrentRecordNumber)
											
											If lcase(tmp_fn_status) = "pass" Then
												PrintMessage "p", " NMI/SP Details Verified Successully" , "Please refer the 'VerificationComments' column to check expected vs actual"
											Else 
												PrintMessage "f", "Issue with NMI/SP details" , gFWstr_ExitIteration_Error_Reason
												'gFWbln_ExitIteration = True					
												gFWstr_ExitIteration_Error_Reason =gFWstr_ExitIteration_Error_Reason & vbCrLf & "Issue with NMI/SP details. Please refer the 'VerificationComments' column to check expected vs actual"
											End If
										
										Else 
												gFWbln_ExitIteration = True	
												gFWstr_ExitIteration_Error_Reason =gFWstr_ExitIteration_Error_Reason & vbCrLf & "No record found by SERVICE_PROVISION_DETAIL_QUERY: " & strSQL_CIS
												PrintMessage "f", "Issue with NMI/SP details" , gFWstr_ExitIteration_Error_Reason												
										End if
									End If ' end of If strSQL_CIS <> ""  Then			
									
									strScratch = ""
									tmp_fn_status = ""
									
								End If ' end of if gFWbln_ExitIteration = False
								
								'------------------------------------------------  NMI REF Check ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					
				If gFWbln_ExitIteration = False Then
					
					tmp_fn_status = ""
					
					If trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMIREF_VERIFICATION" ))) <> "" Then
						tsNow = Now() 
						tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
						If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							Exit Do
						 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter
						End If
					
					
						Dim str_NMI
						str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
						tmp_fn_status = fn_Compare_NMIREF_DB_COLUMN_VALUES(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMIREF_VERIFICATION")).value, "^", objWS_DataParameter, intRowNr_CurrentScenario, str_NMI,  "NMIREF_DETAIL_QUERY", dictWSDataParameter_KeyName_ColNr)
						
						If lcase(tmp_fn_status) = "pass" Then
							PrintMessage "p", " NMI Ref Success" , "Please refer the 'VerificationComments' column to check expected vs actual"
						Else 
							gFWbln_ExitIteration = True
							gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "NMI Ref Failed. Please refer the 'VerificationComments' column to check expected vs actual"		
							PrintMessage "f", "NMI Ref Failed" , gFWstr_ExitIteration_Error_Reason								
						End If
						
					End If	'If trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMIREF_VERIFICATION" ))) <> "" Then
					
				End if ' If gFWbln_ExitIteration = False The
							
							
								'--------------------------------Verifying the Equipment details both installed and removed----------------------------------------' 
							
								If gFWbln_ExitIteration = False Then
								
									str_Iteration = ""
									str_Total_Iterations = ""
									tmp_fn_status = ""
									
									If lcase(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Validate_Equipment_Details")).value) = "y"  Then
										
										If TRIM(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("No_Of_Equipments_Required_Valdiation")).value) = "" Then
											gFWbln_ExitIteration = True	
											gFWstr_ExitIteration_Error_Reason =gFWstr_ExitIteration_Error_Reason & vbCrLf & "No_Of_Equipments_Required_Valdiation is not mentioned for row " & intRowNr_CurrentScenario
											PrintMessage "f", "Issue with No_Of_Equipments_Required_Valdiation Column" , gFWstr_ExitIteration_Error_Reason
										Else
											str_Total_Iterations = Cint(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("No_Of_Equipments_Required_Valdiation")).value)											
										End If
										
										
										If str_Total_Iterations > 0  and gFWbln_ExitIteration = False Then
											
											For str_Iteration = 1 To str_Total_Iterations Step 1
											
												' clear scratch dictionary
												clear_scratch_Dictionary gdict_scratch
												
												strSQL_CIS = ""
												strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("EQUIPMENT_DETAILS_QUERY")).value 
												If strSQL_CIS <> ""  Then
												
													PrintMessage "p", "EQUIPMENT_DETAILS Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_CIS  &")"
													strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
		
													If InStr(1, strSQL_CIS,"<NMI>") > 0 Then
														strSQL_CIS = Replace ( strSQL_CIS , "<NMI>" ,tmp_NMI , 1, -1, vbTextCompare)
													End If 	
													
													If TRIM(LCASE(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("EQUIP_STATUS")).value)) = "installed" or _
													 TRIM(LCASE(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("EQUIP_STATUS")).value)) = "installedandremoved"  Then
														If InStr(1, strSQL_CIS,"<EQUIP_INST_STATUS>") > 0 Then
															tmp_EquipStatus = "A" 
															strSQL_CIS = Replace ( strSQL_CIS , "<EQUIP_INST_STATUS>" ,tmp_EquipStatus , 1, -1, vbTextCompare)
														End If 	
														If InStr(1, strSQL_CIS,"<TS_INSTALLATION_or_TS_STATUS>") > 0 Then
															strSQL_CIS = Replace ( strSQL_CIS , "<TS_INSTALLATION_or_TS_STATUS>" ,"TS_INSTALLATION" , 1, -1, vbTextCompare)
														End If 
														
													ElseIf TRIM(LCASE(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("EQUIP_STATUS")).value)) = "removed" or _
													 TRIM(LCASE(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("EQUIP_STATUS")).value)) = "installedandremoved" Then
														
														If InStr(1, strSQL_CIS,"<EQUIP_INST_STATUS>") > 0 Then
															tmp_EquipStatus = "R"
															strSQL_CIS = Replace ( strSQL_CIS , "<EQUIP_INST_STATUS>" ,tmp_EquipStatus , 1, -1, vbTextCompare)
														End If 	
														If InStr(1, strSQL_CIS,"<TS_INSTALLATION_or_TS_STATUS>") > 0 Then
															strSQL_CIS = Replace ( strSQL_CIS , "<TS_INSTALLATION_or_TS_STATUS>" ,"TS_STATUS" , 1, -1, vbTextCompare)
														End If 
														
													Else
														gFWbln_ExitIteration = True	
														gFWstr_ExitIteration_Error_Reason =gFWstr_ExitIteration_Error_Reason & vbCrLf & "EQUIP_STATUS  Column row " & intRowNr_CurrentScenario & " needs correction."
														PrintMessage "f", "Issue with EQUIP_STATUS Column" , gFWstr_ExitIteration_Error_Reason
														
													End If
													
													If InStr(1, strSQL_CIS,"<EFFECTIVE_DATE>") > 0 Then
														tmp_EFFECTIVE_DATE = fnTimeStamp(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "eConnect_Details_Columns_Names", "eConnect_Details_Columns_Values", intRowNr_CurrentScenario, "EFFECTIVE_DATE"),"DD-MmM-YY")
														strSQL_CIS = Replace ( strSQL_CIS , "<EFFECTIVE_DATE>" ,UCASE(tmp_EFFECTIVE_DATE) , 1, -1, vbTextCompare)
													End If 	
													
													' Update query in SQL Query - SQL_Populated cell
													strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
													strScratch = strScratch & vbCrLf & "EQUIPMENT_DETAILS Query:" & vbCrLf & strSQL_CIS
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
													strScratch =""
													
													'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = ""
													'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = ""
													
													strScratch = Execute_SQL_Populate_DataSheet (objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A,  objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),"")
													
													If ucase(strScratch) = "PASS" Then
														
														' Write the data in all respective columns for all roles
														strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value	
														strScratch = strScratch & ",NMI"& str_Iteration & ",EQUIPTYP"& str_Iteration & ",UTILEQUIPNO"& str_Iteration & ",STATUS"& str_Iteration & ",INSTALLDATE"& str_Iteration & ",REMOVALDATE" & str_Iteration & ",MODELCODE" & str_Iteration & ",MANUFACTURER" & str_Iteration & ",READMETHODDESC" & str_Iteration & ",DISPLAYMULTIPLIER" & str_Iteration & _
																",BILLINGMULTIPLIER" & str_Iteration & ",METERSTATUS" & str_Iteration & ",DISPOSALREASON" & str_Iteration & ",DISPOSEDDATE" & str_Iteration & ",DATEAQUIRED"& str_Iteration & ",METERFORM" & str_Iteration & ",METERCLASS" & str_Iteration & ",CONFIG" & str_Iteration 
													
													      objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = strScratch
														strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
														
														strScratch = strScratch & "," & objDBRcdSet_TestData_A.Fields("NMI").value & "," & objDBRcdSet_TestData_A.Fields("EQUIPTYP").value & "," & objDBRcdSet_TestData_A.Fields("UTILEQUIPNO").value & "," & objDBRcdSet_TestData_A.Fields("STATUS").value & "," & objDBRcdSet_TestData_A.Fields("INSTALLDATE").value & "," & objDBRcdSet_TestData_A.Fields("REMOVALDATE").value & "," & objDBRcdSet_TestData_A.Fields("MODELCODE").value _
																	& "," & objDBRcdSet_TestData_A.Fields("MANUFACTURER").value & "," & trim(objDBRcdSet_TestData_A.Fields("READMETHODDESC").value)  & "," & objDBRcdSet_TestData_A.Fields("DISPLAYMULTIPLIER").value  & "," & objDBRcdSet_TestData_A.Fields("BILLINGMULTIPLIER").value & "," & objDBRcdSet_TestData_A.Fields("METERSTATUS").value & "," & objDBRcdSet_TestData_A.Fields("DISPOSALREASON").value & "," & objDBRcdSet_TestData_A.Fields("DISPOSEDDATE").value & "," & objDBRcdSet_TestData_A.Fields("DATEAQUIRED").value _ 
																	& "," & objDBRcdSet_TestData_A.Fields("METERFORM").value & "," & objDBRcdSet_TestData_A.Fields("METERCLASS").value & "," & objDBRcdSet_TestData_A.Fields("CONFIG").value 
														
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = strScratch
														
														tmp_fn_status = fn_Check_Equip_Details(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, intRowNr_CurrentScenario, tmp_EquipStatus, str_Iteration)
														
														tmp_Equip_Check_Flag = "pass"
														
														If lcase(tmp_fn_status) = "pass" Then
															PrintMessage "p", " Equipment Details Verified Successully" , "Please refer the 'VerificationComments' column to check expected vs actual"
														Else 
															PrintMessage "f", "Issue with Equipment Details" , gFWstr_ExitIteration_Error_Reason
															tmp_Equip_Check_Flag = "fail"			
														End If
													Else 
														gFWbln_ExitIteration = True
														gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "No Meter in status " & tmp_EquipStatus & " found on the NMI for date " & tmp_EFFECTIVE_DATE
														PrintMessage "f", "Equipment Details check Failed" , gFWstr_ExitIteration_Error_Reason
													End if 
													
												End If ' end of If strSQL_CIS <> ""  Then	
										
										Next
								
									
								If lcase(tmp_Equip_Check_Flag) = "fail" Then
									gFWbln_ExitIteration = True
									gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "Issue with Equipment Details. Please refer the 'VerificationComments' column to check expected vs actual"
									PrintMessage "f", "Equipment Details check Failed" , gFWstr_ExitIteration_Error_Reason	
								End If
	
										strScratch = ""
										tmp_fn_status =""
							
							End If 'end of if str_Total_Iterations > 0  
										
						End If 'end of if lcase(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Validate_Equipment_Details")).value) = "y"  Then
																								
					End If ' end of if gFWbln_ExitIteration = False

'					'--------------------------------Verifying the Equipment Datastreams both installed meter----------------------------------------' 
'							
					If gFWbln_ExitIteration = False Then
						
						If lcase(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Validate_Equipment_Details")).value) = "y" Then
						
							'str_Iteration = ""
							If objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("No_Of_Datastream") ).value <> "" Then		
								str_Total_Iterations =  Cint(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("No_Of_Datastream") ).value)
							Else 
								gFWbln_ExitIteration = True
								gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "No_Of_Datastream column in the driver sheet is blank. Please mention No_Of_Datastream to be verified."
								PrintMessage "f", "Equipment Details check Failed" , gFWstr_ExitIteration_Error_Reason									
							End If
							
							str_Iteration = 1
							
							For str_Iteration = 1 To str_Total_Iterations Step 1
						
							Select Case  str_Iteration 
									Case 1
										tmp_usage = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("First_Datastream") ).value
									Case 2 
										tmp_usage = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Second_Datastream") ).value
									Case 3
										tmp_usage = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Third_Datastream") ).value
									Case 4
										tmp_usage = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Fourth_Datastream") ).value
									Case Else
										gFWbln_ExitIteration = True
										gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "No of Datastreams specified in Driver sheet are more than what is handled by script"
										PrintMessage "f", "Equipment Datastream Usage check Failed" , gFWstr_ExitIteration_Error_Reason
							End Select
								
							If lcase(trim(tmp_usage)) = "so" then 
								tmp_LogicalMeterNo = 1 
								tmp_datastream = "B"
								
							Elseif lcase(trim(tmp_usage)) = "l+p" then 
								tmp_LogicalMeterNo = 1 
								tmp_datastream = "E"
								
							Elseif lcase(trim(tmp_usage)) = "hw"  or lcase(trim(tmp_usage)) = "cs" Then
								tmp_LogicalMeterNo = 2 
								tmp_datastream = "E"

							Elseif lcase(trim(tmp_usage)) = "nb-q"  then 
								tmp_LogicalMeterNo = 1 
								tmp_datastream = "Q"
								tmp_usage = "NB"
								
							Elseif lcase(trim(tmp_usage)) = "nb-k" then 
								tmp_LogicalMeterNo = 1 
								tmp_datastream = "K"
								tmp_usage = "NB"
								
							Else
								gFWbln_ExitIteration = True
								gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "Datastream Usage is either blank in Driversheet or is not handled by script." 
								PrintMessage "f", "Equipment Datastream Usage check Failed" , gFWstr_ExitIteration_Error_Reason
							End If
							
							' clear scratch dictionary
							clear_scratch_Dictionary gdict_scratch
							
							strSQL_CIS = ""
							strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DATASTREAM_USAGE_DETAILS_QUERY")).value 
							If strSQL_CIS <> ""  Then
							
								PrintMessage "p", "DATASTREAM_USAGE_DETAILS Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_CIS  &")"
								strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)

								
								If InStr(1, strSQL_CIS,"<NMI>") > 0 Then
									strSQL_CIS = Replace ( strSQL_CIS , "<NMI>" ,tmp_NMI , 1, -1, vbTextCompare)
								End If 	
								
								If InStr(1, strSQL_CIS,"<LOGICAL_NUMBER>") > 0 Then
									strSQL_CIS = Replace ( strSQL_CIS , "<LOGICAL_NUMBER>" ,trim(tmp_LogicalMeterNo) , 1, -1, vbTextCompare)
								End If 	
								
								If InStr(1, strSQL_CIS,"<USAGE>") > 0 Then
									strSQL_CIS = Replace ( strSQL_CIS , "<USAGE>" ,Trim(Ucase(tmp_usage)) , 1, -1, vbTextCompare)
								End If 	
								
								' Update query in SQL Query - SQL_Populated cell
								strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
								strScratch = strScratch & vbCrLf & "EQUIPMENT_DETAILS Query:" & vbCrLf & strSQL_CIS
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
								strScratch =""
								
								'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = ""
								'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = ""
								
								strScratch = Execute_SQL_Populate_DataSheet (objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A,  objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),"")
								'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
								
								if ucase(strScratch) = "PASS" Then	
								
									PrintMessage "p", " Datastreams Verified Successully" , "Please refer the 'VerificationComments' column to check expected vs actual"
									tmp_fn_status = "Actual Datastream is: " & trim(ucase(tmp_datastream)) &  trim(tmp_LogicalMeterNo) & " - " & Trim(Ucase(tmp_usage))
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("VerificationComments")).value = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("VerificationComments")).value & vbCrLf & vbCrLf & tmp_fn_status
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("VerificationComments")).WrapText = False
									
									'---------------------------------------------------------------------eConnect Closeout IEE Verification --------------------------------------------------------------------'
									
									if UCASE(TRIM(tmp_datastream )) = "Q" and  tmp_LogicalMeterNo = 1  Then
										tmp_channel_number = 1301
									ElseIf UCASE(TRIM(tmp_datastream)) = "K"  and tmp_LogicalMeterNo = 1 Then
										tmp_channel_number = 1201
									ElseIf UCASE(TRIM(tmp_datastream )) = "B"  and tmp_LogicalMeterNo = 1 Then
										tmp_channel_number = 1001
									ElseIf UCASE(TRIM(tmp_datastream )) = "E"  and tmp_LogicalMeterNo = 1 Then
										tmp_channel_number = 1101
									ElseIf UCASE(TRIM(tmp_datastream )) = "E"  and tmp_LogicalMeterNo = 2 Then
										tmp_channel_number = 1102
									Else
										PrintMessage "F", " Datastreams vs Channel Number Mapping Failed" , "Datastreams vs Channel Number Mapping is not specified"
										gFWbln_ExitIteration = True
										gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "Datastreams vs Channel Number Mapping is not specified"
									End If
									
									fn_Closeout_IEE_Validation_status = "" 
									fn_Closeout_IEE_Validation_status = fn_Closeout_IEE_Validation (objWS_DataParameter,dictWSDataParameter_KeyName_ColNr, intRowNr_CurrentScenario, _
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value, _
														tmp_channel_number, UCASE(TRIM(tmp_datastream )) , tmp_LogicalMeterNo)
																							
									If lcase(fn_Closeout_IEE_Validation_status) = "pass" Then
										PrintMessage "p", " Closeout IEE Validation Successfull" , "Please refer the 'VerificationComments' column to check expected vs actual"
									Else 
										gFWbln_ExitIteration = True
										gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "Closeout IEE Validation Failed. Please refer the 'VerificationComments' column to check expected vs actual"		
										PrintMessage "f", "Closeout IEE Validation Failed" , gFWstr_ExitIteration_Error_Reason								
									End If
								
								
								Else 
									gFWbln_ExitIteration = True
									gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "No datastream retrived from SQL " &  strSQL_CIS 
									PrintMessage "f", "Datastreams check Failed" , gFWstr_ExitIteration_Error_Reason
									Exit For
								End if 
								
							End If ' end of If strSQL_CIS <> ""  Then	
							
						Next
							

								strScratch = ""
								tmp_fn_status = ""
					End If   'end of If lcase(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Validate_Equipment_Details")).value) = "y" 
				End If ' end of if gFWbln_ExitIteration = False
							
			'--------------------------------------------------------------------------CATS OUT Verification----------------------------------------------------------------
			
				If gFWbln_ExitIteration = False Then
					
					PrintMessage "p", "CATS OUT Verification starting", "CATS OUT Verification starting for  row ("& intRowNr_CurrentScenario &") "
					
					strNMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI")).value
					strNMID = MID(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI")).value , 1, 10)
					strINITIATED_DATE = fnTimeStamp(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "eConnect_Details_Columns_Names", "eConnect_Details_Columns_Values", intRowNr_CurrentScenario, "EXECUTION_DATE"), "DD-MmM-YY")
					strEFFECTIVE_DATE = fnTimeStamp(fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "eConnect_Details_Columns_Names", "eConnect_Details_Columns_Values", intRowNr_CurrentScenario, "EFFECTIVE_DATE"), "DD-MmM-YY")
					str_Total_CATS_OUT = Cint(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("No_OF_CATS_OUT")).value)
					
					If str_Total_CATS_OUT <> 0 Then
					
						strScratch = fn_COUNT_CATS_OUT(strNMID,strINITIATED_DATE,str_Total_CATS_OUT,"COUNT_CATS_OUT_Query", objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, intRowNr_CurrentScenario, "VerificationComments")
						
						If lcase(strScratch) = "pass" Then
							PrintMessage "p", "Validation for no. of CATS OUT transactions Passed." , "Actual No. of CATS OUT matches the expected no. of CATS OUT = ." & str_Total_CATS_OUT
						ElseIf lcase(strScratch) = "info" Then
							'gFWbln_ExitIteration = True
							gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "Just Informaion: Actual No. of CATS OUT does not matches the expected no. of CATS OUT"
							PrintMessage "i", "Validation for no. of CATS OUT transactions Failed" , gFWstr_ExitIteration_Error_Reason	
						ElseIf lcase(strScratch) = "fail" Then
							gFWbln_ExitIteration = True
							gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "No CATS transaction was sent out. "
							PrintMessage "f", "Validation of  no. of CATS sent OUT Failed", "No CATS transaction was sent out."
						End If
							
						If gFWbln_ExitIteration = false Then
													
								For str_Iteration = 1 To str_Total_CATS_OUT Step 1
								
									strCRCode = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CATS_" & str_Iteration)).value 	
									PrintMessage "p", "Closeout CATS OUT verification starting", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ")"
									
									strScratch = fn_CATS_OUT_Validation (	objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, intRowNr_CurrentScenario, _
									"CATS_OUT_Validation_Query", "CATS_OUT_NCHANNEL_Query", "CATS_OUT_Register_Validation_Query", "MTS_CATS_Query", "IEE_CATS_Query", "IEE_NCHANNEL_CATS_Query", "VerificationComments" , _
									strNMI, strINITIATED_DATE, strEFFECTIVE_DATE, strCRCode)
												
								 Next
								 
								If lcase(strScratch) = "pass" Then
									PrintMessage "p", "Validation for CATS OUT transactions details Passed." , "Details in the CATS OUT matches the Details in MTS and IEE = ." & str_Total_CATS_OUT
								ElseIf lcase(strScratch) = "info" Then
									'gFWbln_ExitIteration = True
									gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "Details in the CATS OUT does not match the Details in MTS and IEE"
									PrintMessage "i", "Validation for CATS OUT transactions details Failed" , gFWstr_ExitIteration_Error_Reason	
								ElseIf lcase(strScratch) = "fail" Then
									gFWbln_ExitIteration = True
									gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & vbCrLf & "Details in the CATS OUT does not match the Details in MTS and IEE"
									PrintMessage "f", "Validation for CATS OUT transactions details Failed", "Details in the CATS OUT does not match the Details in MTS and IEE"
								End If

						End If 'If gFWbln_ExitIteration = false Then
			
			 		End If 'If str_Total_CATS_OUT <> 0 Then
				
				End If 'If gFWbln_ExitIteration = False Then
				
'			--------------------------------Marking row as Verified in ECONNECT_DATA_ATTRIBUTES Table----------------------------------------' 
'			If gFWbln_ExitIteration = False Then
'				PrintMessage "p", "Updating the row in ECONNECT_DATA_ATTRIBUTES to Verified", "Scenario row ("& intRowNr_CurrentScenario &") - Marking row as Verified"
'				
'				strSQL_TEL = "ECONNECT_DATA_ATTRIBUTES_VERIFIED"
'				PrintMessage "p", " ECONNECT_DATA_ATTRIBUTES_VERIFIED Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_TEL  &")"
'				strSQL_TEL = fnRetrieve_SQL(strSQL_TEL)
'					
'				 Perform replacements
'				tmp_EBMID_PARAM =  fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "eConnect_Details_Columns_Names", "eConnect_Details_Columns_Values", intRowNr_CurrentScenario, "EBMID")
'				
'				If InStr(1, strSQL_TEL,"<EBMID_PARAM>") > 0 Then
'					strSQL_TEL = Replace ( strSQL_TEL , "<EBMID_PARAM>" ,tmp_EBMID_PARAM , 1, -1, vbTextCompare)
'				End If 								
'
'				strScratch = Execute_SQL_Populate_DataSheet (objWS_DataParameter, DB_CONNECT_STR_TEL, objDBConn_TestData_E, objDBRcdSet_TestData_E, strSQL_TEL, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"),"","", "")
'				
'				PrintMessage "p", "Marked row as Verified in ECONNECT_DATA_ATTRIBUTES Table Successully" , "Marked row as Verified in ECONNECT_DATA_ATTRIBUTES Table"
'				 gFWbln_ExitIteration = False
'												
'			End If	

		Else 
				gFWbln_ExitIteration = True
				gFWstr_ExitIteration_Error_Reason = "eConnect request details not availble. No Row found for in ECONNECT_DATA_ATTRIBUTES Table for Testcase - " & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID")).value 
				PrintMessage "f", "eConnect request details not availble" , gFWstr_ExitIteration_Error_Reason
		End If 'objDBRcdSet_TestData_E.RecordCount > 0
							
		' At the end, check if any error was there and write in corresponding column
		If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True or gFWstr_ExitIteration_Error_Reason <> "" Then ' write an error in the results
			objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Closeout Verification Failed. (" & vbCrLf & gFWstr_ExitIteration_Error_Reason & ")"
			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).WrapText = False
			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
			Printmessage "f", "Closeout Verification Failed.",   gFWstr_ExitIteration_Error_Reason
			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
			int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
			Exit Do
		Else
			objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Closeout Verification complete. Please refer the 'VerificationComments' column for more details."
			PrintMessage "p", "Closeout Verification complete", "Closeout Verification complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
			' Function to write timestamp for next process
			fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
			int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
			
			Exit Do
		End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
				
				
									
		strSQL_CIS = ""
		strSQL_MTS = ""
		strSQL_USB = ""
		'strSQL_NMIREF ""
		strSQL_TEL = ""
		strSQL_IEE = ""
		strSQL_MTS = ""
		tmp_NMI = ""
		
	End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter
			
		

	Case "WaitingForPrevStep"
	
	temp_last_row_status = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value
	
	If LCase(temp_last_row_status ) = "pass" Then
		objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start"
		
	ElseIf LCase(temp_last_row_status ) = "fail" Then
		gFWbln_ExitIteration = True
		gFWstr_ExitIteration_Error_Reason =  "Row failed as Previous Step row failed"
		
		If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
			objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed as Previous Step row failed"
			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
			PrintMessage "f", "Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
			objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
			int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
		End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
	Else
		Exit Do
	End If 'end of If LCase(temp_last_row_status ) = "pass" Then
	
	
	Case "FAIL"
	Exit Do
End Select 'end of Select Case str_Next_Function
		
		
		' Loop While  (lcase(gFWbln_ExitIteration) <> "n" or gFWbln_ExitIteration <> false)
	Loop While  (LCase(gFWbln_ExitIteration) = "n" Or gFWbln_ExitIteration = False)
	
	Case Else
		gFWbln_ExitIteration = "Y"
		gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
		PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
		objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
		objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
		int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
End Select ' end of Select Case LCase(str_ScenarioRow_FlowName)
End If ' If   strInScope = cY  And str_ScenarioRow_FlowName <> "" And LCase(str_Next_Function) <> "fail" And LCase(str_Next_Function) <> "end" Then
		
		' ####################################################################################################################################################################################
		' #####################################################                 LOGGING STEP 3 - Clear data before moving to next row #########################################################################
		' ####################################################################################################################################################################################
		fnTestExeLog_ClearRuntimeValues
		
		
		'		' Before resetting the flags, write the last error in comments column
		'		If gFWstr_ExitIteration_Error_Reason <> "" Then
		'			strScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value 
		'			strScratch = strScratch & " : " & gFWstr_ExitIteration_Error_Reason
		'			objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value = strScratch 
		'		End If
		'		
		' Reset error flags
		fn_reset_error_flags
		
		' clear scratch dictionary
		clear_scratch_Dictionary gdict_scratch
		
	Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
	
	objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
	
	fn_reset_error_flags
	
	i = 2
	objWB_Master.save ' Save the workbook 
	
Loop While int_ctr_no_of_Eligible_rows > 0

' Next ' end of For i  = 1 to 1000 

PrintMessage "i", "MILESTONE - Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & " ) - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= Now

objWB_Master.save ' Save the workbook
Printmessage "i", "MILESTONE - End of Execution", "Finished execution of B2BSO scripts"

' Once the test is complete, move the results to network drive
Printmessage "i", "MILESTONE - Copy to network drive", "Copying results to network drive ("& Cstr_B2BSO_Final_Result_Location &")"
copyfolder  gFWstr_RunFolder , Cstr_B2BSO_Final_Result_Location
'Window("z_Retired").Click 173,82 @@ hightlight id_;_65806_;_script infofile_;_ZIP::ssf80.xml_;_
