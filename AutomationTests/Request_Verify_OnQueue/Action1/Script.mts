﻿'Get the input parameters
'Company: CITI, PCOR, SAPN
'ENV: DEV, SYSTEST, UAT
'RunCount: Number of iterations
Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")
Environment.Value("RUNCOUNT")=Parameter.Item("RunCount")
Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"


Dim objExcel, objWorkBook, objWorkSheet, dictKeyNameCol

fnExcel_CreateAppInstance  objExcel, true
LoadData_RunContext objExcel, objWorkBook, objWorkSheet, 1,  BASE_AUTOMATION_DIR & "DataSheets\DataLoader.bj.xls", dictKeyNameCol

Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL")&"SORD\"
'msgbox Environment.Value("TESTFOLDER") 


Dim strQueryTemplate, strDate_dd_mmm_ccyyy, dtD, Hyph, strNMI
dtD = DateAdd("d", -0, now()) : Hyph = "-"
strNMI = "610202712" ' qqqqqqqqqqqqqqqqqqqq  what NMI to use ?
strDate_dd_mmm_ccyyy = right("0" & DatePart("d", dtD), 2) & Hyph & MonthName(DatePart("m", dtD), True) & Hyph & DatePart("yyyy", dtD)
strQueryTemplate = Replace(Environment.Value("DBQuery"), "<SubmittedAfterDt_dd-mmm-ccyy>", strDate_dd_mmm_ccyyy, 1, 1, vbTextCompare)
strQueryTemplate = Replace(strQueryTemplate            , "<NMI>"                          , strNMI                , 1, 1, vbTextCompare)


'Save Datamine query in a variable to use it for all iterations
Environment.Value("FindNMI") = strQueryTemplate ' Environment.Value("DBQuery")

strWorkType = "Re-energisation"
strSOPrefix = "RN"

'Open MTS GUI
'MTS_Open Environment.Value("ENV"), Environment.Value("COMPANY")
' RunAction "A_MTS_OpenMTS [A_MTS_OpenMTS]", oneIteration	

Environment.Value("DBQuery") = Environment.Value("FindNMI")
Dim objConnection_A, objRecordSet_A  
Set objConnection_A = CreateObject("ADODB.Connection")
Set objRecordSet_A  = CreateObject("ADODB.Recordset")
Dim intIterationsRequiredCount : intIterationsRequiredCount = 3
fn_DbQuery_v2 objConnection_A, objRecordSet_A, Environment.Value("DBQuery"), intIterationsRequiredCount  



For i = 1 To intIterationsRequiredCount ' Environment.Value("RUNCOUNT")
' qq why iterations ?
' no .EOF test requried because of the use of intIterationsRequiredCount when querying
	strNMI = objRecordSet_A.Fields("SERVICEPOINT").Value ' qq here the DriverSheet data_out cell would be populated
	objRecordSet_A.MoveNext
	
	'Get the query for fetching FRMP
	GetRole strNMI,"FRMP"
	strFRMP = ExecuteDatabaseQuery
	
	'Get the query for fetching LNSP
	GetRole strNMI,"LNSP"
	strLNSP = ExecuteDatabaseQuery
	
	'Get the prospective retailer that is different to current FRMP
	GetProspectiveRetailer strFRMP
	strProsRetailer = ExecuteDatabaseQuery
	
	strFileName = "so_"&strNMI&"_"&datepart("d",date)&hour(time)&minute(time)&second(time)
	CreateSOXML strSOPrefix,strNMI,strLNSP,strProsRetailer,strWorkType,strFileName
	
	ZipFile strFileName&".xml",Environment.Value("TESTFOLDER")
	CopyFile strFileName&".zip",Environment.Value("TESTFOLDER"),Environment.Value("MTS_SO_OUTBOX")

	'GetServiceOrderAckDestinationFolder "CITI","SYSTEST"
	' check ack file generated
	' wait 20 ' grrrr
	
	If FSO.FileExists(strSoughtFqFileName) Then
	 '   Reporter.ReportEvent micPass, "Ack file generated", "Ack  file generated:" & ackDestinationFolder &expectedACKFileName
	    'check ack file has text 'status="Accept" 'in the file
	    Dim lowPass : lowPass = micPass': lowPass = micDone
	    If (TextExistsInFile(ackDestinationFolder &expectedACKFileName, "status=" & Chr(34) & "Accept" & Chr(34))) AND NOT (TextExistsInFile(ackDestinationFolder &expectedACKFileName, "status=" & Chr(34) & "Reject" & Chr(34)))Then
	        Reporter.ReportEvent lowPass, "Ack file check for text 'status=" & Chr(34) &"Accept" & Chr(34) & "'", "Found text in ack file `" & strSoughtFqFileName & "`."
	    Else
	        Reporter.ReportEvent micFail, "Ack file check for text 'status=" & Chr(34) &"Accept" & Chr(34) & "'", "Text NOT found in ack file `" & strSoughtFqFileName & "`."
	    End If
	Else 
	    Reporter.ReportEvent micWarning, "Ack file generated ?", "Ack  file NOT generated at:" & ackDestinationFolder &expectedACKFileName
	End If
	

' wait 10 ' grrr
'	MTS_SearchSO strNMI
	' RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration
Next

'Close MTS GUI
'MTS_WinClose
set objConnection_A = nothing : set objRecordSet_A = nothing