﻿
' v20b was for missing a) AC-verify log-entries  b) restore umesh's logging2

Dim strSQL_TemplateName_Current, strSQL_TemplateName_Previous, intScratch, cellScratch

' v13  SIT 
Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong

gfwint_DiagnosticLevel = 0 ' CHanging diagnostic level for debugging a particular row

gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

Dim bln_Scratch :  bln_Scratch = cbool(true)

dim intColNr_InScope, intColNr_RowCompletionStatus
intColNr_InScope = cint(-1) : intColNr_RowCompletionStatus = cint(-1)

Dim str_rowScenarioID, intColNR_ScenarioID

Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
Dim thisCell
Dim strPhase, strStage
Dim gFWcell_xferNMI 
Dim strBarText, strBarText_Save, strMsg
Dim cStr_BarText_Format
								Dim temp_DataPrep_CIS_SOType, temp_DataPrep_CIS_SOReason , temp_DataPrep_CIS_TaskType, str_Required_SP_Status_MTS



cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^RequestID`{4}^NMI`{5} ."

'
'strScratch = fnTimeStamp ( now,  "yyyy`wkWW" )
'intScratch = cInt (fnTimeStamp ( now,  "yW" ) )
'If ((intScratch 	= 710) or (intScratch 	= 711)) Then	'										    ==============================================================================				
'									Parameter.Item("Environment")	=	"DEVMC"
'	Select Case gobjNet.ComputerName
''		Case "CORPVMUFT01" 	:	Parameter.Item("Environment")	=	"SITMC"
'		Case else	'	CORPVMUFT06	CORPVMUFT02 PCA19323
'	End Select
'End if


'			
'			
'			' ReDim gFwAr_strRequestID( cFWint_MaxNrOfProcesses_Required , cFWint_MaxNrOfRoles_Required ) ' no redim preserve required as the Ar has no values in it yet
'

strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciMetaData_Type_and_VersionNr	) = "is_FieldSetVerification_vn01" ' is_ is InformationSet	( 0)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldName						) = "<fn_UnInitialized>"								'	( 1)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldValue						) = "<fv_UnInitialized>"								'	( 2)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciAsOfTimestamp					) = "<aoTS_UnInitialized>"							'	( 3)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldType							) = "<ft_UnInitialized>"								'	( 4)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldVerificationPoint			) =  1													'	( 5)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldSource						) = "gMTS`<tbd_qq>"									'	( 6)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare01					) = "<mt>"												'	( 7)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare02					) = "<mt>"												'	( 8)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare03					) = "<mt>"												'	( 9)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare04					) = "<mt>"												'	(10)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_NameIndexList				) = _
	",ciMetaData_Type_and_VersionNr`0,ciFieldName`1,ciFieldValue`2,ciAsOfTimestamp`3,ciFieldType`4,ciFieldVerificationPoint`5,ciFieldSource`6" & _
	",ciField_spare01`7,ciField_spare02`8,ciField_spare03`9,ciField_spare04`10,ciField_NameIndexList`11,"						' 	(11)


'		On error resume next
'			strAr_Single_VerifyKey_Fields_local_Defaults	= scratchVariant
'		On error goto 0
strAr_Single_VerifyKey_Fields_local_Defaults	= strAr_Single_VerifyKey_Fields_testCase_Defaults	 
strAr_Single_VerifyKey_Fields 					= strAr_Single_VerifyKey_Fields_local_Defaults ' set the work-area strAr from the local defaults-strAr
strAr_Single_VerifyKey_Fields_logXML			= strAr_Single_VerifyKey_Fields_local_Defaults 


'fName = "Dani"
'lName = "Vainstein"
Set hFWobj_SB = DotNetFactory.CreateInstance( "System.Text.StringBuilder" )

Set gFWobj_SystemDate = DotNetFactory.CreateInstance( "System.DateTime" )


' push the RunStats to the DriverWS report-area
Dim cellTopLeft, iTS, iTS_Max, rowTL, colTL

Dim strRunLog_Folder

' -----

Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq


' gQtApp, gQtTest, gQtResultsOpt    are all available from in fnLib_AutomationFwk
' gobjNet is available from fnLib_MsWindows to retrieve the computer and username, amongst other things
'dim qtResultsOpt'
'Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")  ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539

' gQtApp in instantiated in fnLib_AutomationFwk.qfl
'If gQtApp.launched <> True then
'  gQtApp.Launch
'End If

'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html

gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
gQtApp.Options.Run.ViewResults                = False


' gQtTest.Settings.Run.OnError = "NextStep" ' Instruct QuickTest to perform next step when error occurs
' gQtTest.Run qtResultsOpt,true ' Run the test
' MsgBox gQtTest.LastRunResults.Status
' gQtTest.Close ' Close the test


Dim strCaseBaseAutomationDir 
strCaseBaseAutomationDir = BASE_AUTOMATION_DIR
  'strCaseBaseAutomationDir = "r:\"


 Dim BASE_GIT_DIR : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"


gDt_Run_TS = tsOfRun

 
	'  Description:-  Function to replace arguments in a string format
	'   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is 
	'						cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^CRCode`{4}^Phase`{5}^Role`{6}^Stage`{7}^RequestID`{8}^NMI`{9} ."
	'						The template should have arguments within {} and the numbering should s
	
	



Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
	str_AnFw_FocusArea = ""
else
	str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
End If


' StringBuilder function replaced with fn_StringFormatReplacement function
'strRunLog_Folder = hFWobj_SB.AppendFormat ("{0}DataSheets\runs\{1}_{2}_{3}_{4}\"          , _
'    fnAttachStr(strCaseBaseAutomationDir,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
'    gQtTest.Name                                 , _
'    Parameter.Item("Environment")                       , _
'    Parameter.Item("Company")                           , _
'    str_AnFw_FocusArea & fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkXX`ddDdD_HH`mm") ).toString
'

'strRunLog_Folder = fn_StringFormatReplacement("{0}DataSheets\runs\{1}_{2}_{3}_{4}\", array(fnAttachStr(strCaseBaseAutomationDir,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
  '  gQtTest.Name                                 , _
 '   Parameter.Item("Environment")                       , _
'    Parameter.Item("Company")                           , _
 '   str_AnFw_FocusArea & fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))
 
 strRunLog_Folder = hFWobj_SB.AppendFormat ("{0}{1}_{2}_{3}_{4}\"          , _
  fnAttachStr(cTemp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
  gQtTest.Name                                 , _
   Parameter.Item("Environment")                       , _
   Parameter.Item("Company")                           , _
   str_AnFw_FocusArea & fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkXX`ddDdD_HH`mm") ).toString



Path_MultiLevel_CreateComplete strRunLog_Folder
gFWstr_RunFolder = strRunLog_Folder




' ----------------------------------

gQtResultsOpt.ResultsLocation = strRunLog_Folder
					
	If 1 = 1  Then		
					
					
					    gFwInt_DataParameterRow_ErrorsTotal = 0 : gFwInt_AbandonIteration_ReasonCount = 0
					    gFWbln_ExitIteration = false
					
					    Set gFWoDnf_SysDiags = Dotnetfactory.CreateInstance("System.Diagnostics.StackFrame")
					
					      dim MethodName
					      On error resume next
					    ' MethodName = new oDnf_SysDiags.StackTrace().GetFrame(0).GetMet hod().Name
					      MethodName = oDnf_SysDiags.GetMethod().Name
					      On error goto 0
					
					
					
					
					
					' =========
					' =========  FrameworkPhase 00   - Setup the Stage-reporting elements =============
					' =========
					
					Dim micFinalStatus, intStageSuccessCount, intWantedSuccessCount : intStageSuccessCount = 0
					Dim ctSucc, intStageSuccessAr : intStageSuccessAr = split(",0,0,0,0", ",") : intWantedSuccessCount = uBound(intStageSuccessAr)
					Dim BASE_XML_Template_DIR
					
					Dim intNrOfEventsToTrack    : intNrOfEventsToTrack      = 10
					Dim tsAr_EventStartedAtTS     : tsAr_EventStartedAtTS     = split(string(intNrOfEventsToTrack-1, ","), ",")
					Dim tsAr_EventEndedAtTS       : tsAr_EventEndedAtTS       = split(string(intNrOfEventsToTrack-1, ","), ",")
					Dim tsAr_EventPermutationsCount : tsAr_EventPermutationsCount = split(replace(string(intNrOfEventsToTrack-1, ","), ",", "0,")&"0", ",")
					
					const iTS_Stage_0 = 0
					const iTS_Stage_I = 1
					const iTS_Stage_II = 2
					const iTS_Stage_III = 3
					
					' =========
					' =========  FrameworkPhase 0a   - Expand the Template Rows into SingleScenario Rows  =============
					' =========
					
					
					
					tsAr_EventStartedAtTS(iTS_Stage_0) = now()
					
					Environment.Value("COMPANY")=Parameter.Item("Company")
					Environment.Value("ENV")=Parameter.Item("Environment")
					
					'   qq  moved to fnLib_MsWindows Dim gObjNet : Set gObjNet = CreateObject("WScript.NetWork") '  gObjNet.UserName gObjNet.ComputerName
					
					' Environment.Value("RUNCOUNT")=Parameter.Item("RunCount") ' not required
					
					
					    Dim intColNames_RowNr, intColNames_ColNr_Start, intColNames_ColNr_End
					    Dim r_rtTemplate, intColNr_RowType, intColNr_DataCol_First, intColNr_DataCol_Last,  intColNr_ScenarioStatus
					    Dim intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
					    
					    
					' Load the MasterWorkBook objWB_Master
					Dim strWB_noFolderContext_onlyFileName  
											strWB_noFolderContext_onlyFileName = "Automation_Plan_Book_B2B_SO.xlsm"
					
					Dim wbScratch
					
					Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
					dim  int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr
					
					
					
					fnExcel_CreateAppInstance  objXLapp, true
					
					Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"
					LoadData_RunContext objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder

					
								' qq 2017`01Jan`24Tue_16`33`22 BrianM Workaround
								Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
								Set wbScratch = nothing
								
								
								objXLapp.Calculation = xlManual
								objXLapp.screenUpdating=False
					
					
					' load the DataParameter worksheet objWS_DataParameter - this contains the DataScenarios to test
					'
					' set objWS_DataParameter         = objWB_Master.worksheets("wsDT_MeterCntestblty_Objections")
					'set objWS_DataParameter            = objWB_Master.worksheets("wsSsDT_mrCntestbty_Objectns")
					
					' Dim objWB_Master , ' objWS_useInEach_ProcessStage <<== moved to automation framework
					
					Dim objWS_DataParameter 						 
					set objWS_DataParameter           					= objWB_Master.worksheets("AAwsFlows_B2B_SO") ' wsMsDT_mrCntestbty_Objectn") ' qq this must become a parm
					
					Dim objWS_templateXML_TagDataSrc_Tables 	
					set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
					set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
					' Dim objWS_ScenarioOutComesValidations		: set objWS_ScenarioOutComeValidations 		= objWB_Master.worksheets("wsSpec_ScenarioOutcomeVerify") ' reference the Scenario ValidationRules WS
					
					Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
					Dim tempInt_RoleIdentificationCounter
					' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
					gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

				'	Get headers of table in a dictionary
					Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
					gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
					
					
					gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
					Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
					gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
					' Load the first/title row in a dictionary
					int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
					fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
					
					
					int_MaxColumns = UBound(gvntAr_RuleTable,2)

					
					' Prepare the runLogAr for processing by loading it from the relevant worksheet
					    Set gfwobjWS_LogSheet = objWB_Master.worksheets("ws_RunLog")
					    Dim intLogCol_Start, intLogCol_End,  intLogRow_Start, intLogRow_End 
					    
					'	if logging is being done to an array, get the size of the range to be loaded to the array, and load it
					    If gcFw_intLogMethod    =     gcFw_intLogMethod_RangeToWS Then	
					    
					        intLogRow_Start        =    gfwobjWS_LogSheet.range("rgWS_RowNr_Data_FirstMinusOne").row        +    1
					        intLogRow_End         =    gfwobjWS_LogSheet.range("rgWS_RowNr_Data_LastPlusOne").row        -    1
					        intLogCol_Start        =     gfwobjWS_LogSheet.range("rgWS_ColNr_Data_First_MinusOne").column    +    1
					        intLogCol_End        =     gfwobjWS_LogSheet.range("rgWS_ColNr_Data_Last_PlusOne").column    -    1
					        
					        gfwAr_DriverSheet_Log_rangeFor_PullPush = _
					            gfwobjWS_LogSheet.range ( gfwobjWS_LogSheet.cells ( intLogRow_Start, intLogCol_Start), gfwobjWS_LogSheet.cells(intLogRow_End, intLogCol_End) )
					        intLogRow_End = intLogRow_End         ' qq for debugging    '        else            '    =    gcFw_intLogMethod_WSdirect
					
					    End If
					
					
					'qq - Temp sheet which need to be removed once verification function is complete
					set objWS_Mts_Value_Dump = objWB_Master.worksheets("MTS_Value_Dump")
					
					Dim vntNull, mt
							
							
					
					MethodName = gFWoDnf_SysDiags.GetMethod().Name
'	believed to be not-required for B2B_SO					
'									gFWrange_NMI_Spec   		= objWS_DataParameter.range("rgWS_dataNMI_Specification")
'									gFWrange_NMI_SpecResult 	= objWS_DataParameter.range("rgWS_dataNMI_SpecificationResult")
'	believed to be not-required for B2B_SO					
					objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
					objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
					objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
					objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
					
					' setAll_RequestIDs_Unique objWS_DataParameter
					
					'objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula = "'" & fnWindows_UserName(false)'
					'objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula = "'" & fnFW_GetComputerName()
					
					
					gDt_Run_TS = now
					gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 
					'setAll_RequestIDs_to     objWS_DataParameter, gFwTsRequestID_Common 


'	believed to be not-required for B2B_SO
'									int_NrOf_InScope_Rows      = objWS_DataParameter.range("rgWS_Count_NrOf_needed_NMIs").value
'									int_NrOf_InScope_Rows      = cInt(int_NrOf_InScope_Rows * 1.5) + 10 ' get some extra rows in case some of those identified are reserved for other cases
'									int_NrOf_NMI_rows_required = int_NrOf_InScope_Rows * (intSize_listOldNew + 1)
'	believed to be not-required for B2B_SO

					Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE, cellReportStatus_SingleScenario
			'		set cellReportStatus_FwkProcessStage = objWS_DataParameter.range("rgWS_cellReportStatus_FwkProcessStage")
			'		set cellReportStatus_ROLE            = objWS_DataParameter.range("rgWS_cellReportStatus_ROLE")
					On error resume next
					' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
					  Set cellReportStatus_SingleScenario = objWS_DataParameter.range("rgWS_cellReportStatus_SingleScenario")
					On error goto 0
					
					
					'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
					Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
					dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
					Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
					intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
					intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
					intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
					fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
					
					set cellTopLeft = objWS_DataParameter.range("rgWS_RunReport_TopLeftCell_Stage0_StartTS")
					' rowTL = cellTopLeft.row : colTL = cellTopLeft.column
					rowTL = 1 : colTL = 1
					
					'  <<<=========   expand the rows (already done)
					'                 expand the rows (already done) - in the objWS_DataParameter in the MasterWB
					'                 expand the rows (already done)
					'                 expand the rows (already done)
					'                 expand the rows (already done)
					' sb_rtTemplate_Expand objWS_DataParameter, dictWSDataParameter_KeyName_ColNr
					
					
					' objExcel.Application.Run "test.xls!sheet1.csi"
					' objXLapp.Run gStrWB_noFolderContext_only_RunVersion_FileName & "!sb_rtTemplate_Expand"     ' qqq <<<====   needs fixing, wouldn't run the macro
					
					' objXLapp.Calculation = xlAutomatic qq later, and ensure that    xlAutomatic     is defined
					
					
					
					' =========
					' =========  FrameworkPhase 0b   - Technical Setup for Running the SingleScenario Rows   =============
					' =========
					
					Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
					
					dim r
					Dim objDBConn_TestData_A, objDBRcdSet_TestData_A
					
'					On error resume next
		'	add more of these as-required, for querying databases
		'	add more of these as-required, for querying databases
					Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
					r = 0
					set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")
		'	add more of these as-required, for querying databases
		
		
					Dim objDBConn_TestData_B, objDBRcdSet_TestData_B
					Set objDBConn_TestData_B    = CreateObject("ADODB.Connection") : set objDBRcdSet_TestData_B  = CreateObject("ADODB.Recordset")


'	believed to be not-required for B2B_SO
	'					Dim strAr_listOldNew, intSize_listOldNew, str_listOldNew
'	believed to be not-required for B2B_SO
					'str_listOldNew = ucase(Parameter.Item("listOldNew")) ' for debugging
					'if left(str_listOldNew,1) <> "," then str_listOldNew = "," & str_listOldNew
					'Environment.Value("listOldNew") = str_listOldNew
					
					'strAr_listOldNew = split(Environment.Value("listOldNew"), ",")  ' this list will be one of :  old  new old,new  so we split the list with a comma to find out what permutations we're going to test
					'intSize_listOldNew = uBound(strAr_listOldNew)
					
					
					'fnWS_LoadKey_ColOffsets_toDictionary objWS_TestRunContext, 1, 1, 50, "", dictWSTestRunContext_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
			Dim	DB_CONNECT_STR_MTS
					DB_CONNECT_STR_MTS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
						Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
						Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
						Environment.Value("USERNAME") 	& 	";Password=" & _
						Environment.Value("PASSWORD") 	& ";"

			Dim	strEnvCoy
					strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")
					
					'	Available <Environment>_<Company>_<DataBaseConnectionParameters> are :
					'		Environment.Value( strEnvCoy & 	"_HOSTNAME"			)     
					'		Environment.Value( strEnvCoy & 	"_SERVICENAME"		)   	
					'		Environment.Value( strEnvCoy & 	"_USERNAME"			)     
					'		Environment.Value( strEnvCoy & 	"_PASSWORD"			)     
					'		Environment.Value( strEnvCoy &	 "_MTS_SO_INBOX"	)  	
					'		Environment.Value( strEnvCoy & 	"_MTS_SO_OUTBOX"	) 	
			Dim	DB_CONNECT_STR_CIS
					DB_CONNECT_STR_CIS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
						Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
						Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
						Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
						Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"

					' dim strTestName, uftapp : Set uftapp = CreateObject("QuickTest.Application")
					' strTestName = uftapp.Test.Name
					' Set uftapp = nothing
					
					Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL") & "SORD\"
					
					Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
					Dim strQueryTemplate, dtD, Hyph, strNMI
					
'	believed to be not-required for B2B_SO
	'								Dim strList_NMIsizes, strAr_NMIsizes, nmiSizes
	'								strList_NMIsizes = ",SMALL,LARGE"
	'								strAr_NMIsizes = split(strList_NMIsizes, ",")
	'								
	'								Dim oldnew, oldnewMax, n
	'								oldnewMax = intSize_listOldNew
'	believed to be not-required for B2B_SO


					Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
					Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
					intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
'	believed to be not-required for B2B_SO
	'							intCatsCR_ColNr                 = dictWSDataParameter_KeyName_ColNr("CATS_CR")
	'							intColNR_BaseDate       = dictWSDataParameter_KeyName_ColNr("BaseDate")
	'							intColNR_DateOffset       = dictWSDataParameter_KeyName_ColNr("DayOffset")
	'							intColNr_sizeOfNMI        = dictWSDataParameter_KeyName_ColNr("list_tNMI")
'	believed to be not-required for B2B_SO
					
					Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
					
					Dim intSQL_Pkg_ColNr, strSQL_RunPackage
					
					
					Dim intRowNr_DPs_SmallLarge : intRowNr_DPs_SmallLarge = objWS_DataParameter.range("rgWS_RowNr_DPs_SmallLarge").row
					
					Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
					
					
					' qq this needs to change so that it is driven from a table which is a sibling to the run log table so that this per run/env/user/machine i.e. context-run config becomes table driven
						
						' Setup the SingleScenario row range to run over - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
						'                                                 - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
						'                                                 - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
						' Setup the SingleScenario row range to run over - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
						'intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
						intRowNr_LoopWS_RowNr_StartOn 	= 1040
						
						'intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
						intRowNr_LoopWS_RowNr_FinishOn	= 1040
						
						intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
					
								
							'	===>>>   Setting the   intColNr_InScope   colulmn - EXECUTION flow
						sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
					
						Dim strColName
					
'						strScratch = fnTimeStamp ( now,  "yyyy`wkWW" )	'											==============================================================================
'						If ((strScratch 	= "2017`wk10") or (strScratch 	= "2017`wk11")) Then				'										    ==============================================================================				
						intScratch = cInt (fnTimeStamp ( now,  "yW" ) )
						If ((intScratch 	= 710) or (intScratch 	= 711)) Then
																strColName 	= "InScope_YN"		  			
						'	Select Case gobjNet.UserName 
							Select Case gobjNet.ComputerName
								Case "CORPVMUFT01z"		:	strColName 	= "qq"
								Case "CORPVMUFT02z" 	:	strColName 	= "qq"
								Case "PCA19323a"		 	:	strColName 	= "InScope_Umesh"
								Case "PCA19323b"		 	:	strColName 	= "InScope_Preeti"
								Case "PCA19323c"		 	:	strColName 	= "InScope_Brian"
								Case else	'	CORPVMUFT06	CORPVMUFT02 PCA19323
							End Select
					
					'		strColName 	= "InScope_WIGS_All"		  			'	RunParm Overrides are here   <<<<<====================================================================================
						 	intColNr_InScope = dictWSDataParameter_KeyName_ColNr(strColName) ' "FrameworkSheetShakeout")
							objWS_DataParameter.Cells(intWsRowNr_ColumNames, intColNr_InScope).Interior.Color = objWS_DataParameter.range("rgWS_status_CaseStarted").Interior.Color
						 	
					'		If 1 = 1 Then
					'			intRowNr_LoopWS_RowNr_StartOn 	= 1110  '	1st 2 WIGS cases, investigating    Ab) varAr contsins scalar(PROCESSED) Bb) gui refs not working
					'			intRowNr_LoopWS_RowNr_FinishOn 	= 1110 '  112 	'
					'		End If
					'		If 1 = 100001 Then
					'			intRowNr_LoopWS_RowNr_StartOn 	= 1002	'	Cats 4.2 cases
					'			intRowNr_LoopWS_RowNr_FinishOn 	= 1003 ' 1272 ' 003 ' 116 ' 002 ' 1006  1116      ' 
					'		else
					'			intRowNr_LoopWS_RowNr_StartOn 	= 1116	'	 WIGS cases	
					'			intRowNr_LoopWS_RowNr_FinishOn 	= 1116 ' 8     single-stage
					'		End If
						End If
						objWS_DataParameter.calculate	'	to ensure the filename is updated
									
									
									
					Dim  str_xlBitNess : str_xlBitNess = objWS_DataParameter.range("rgWS_XL_BitNess_x32x64").value
									
									' qq also write a tab-deliminted line for pasting into 
									'              \\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\DataSheets\runs\AutomationRun_Register.xlsm
					sb_File_WriteContents  _
						gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
						fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
						Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
					
					
						' sblEnCl_Set_colNrInScope_fromRun_User_and_PcName  intColNr_InScope, dictWSDataParameter_KeyName_ColNr
								'		'	Moved to fnLib_ExecutionControl
					
								'			' qq  the per-PC, per-User scenario-to-run config (i.e. below) needs to become table-based
								'			  select case gObjNet.ComputerName
								'			    case "PCA18123", "PCA16100", "CORPVMUFT08"    ' Umesh
								'			      intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("InScope")
								'			    case "PCA15187xxx" '  Brian
								'			      intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("debug_InScope")
								'			    Case else
								'			      intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("InScope")
								'			  End select
								'			  
								'			  select case gObjNet.UserName
								'				case "brmoloneyQQ"
								'			      intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("debug_InScope")
								'			    Case else
								'			      intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("InScope")
								'			  End select
						  
						  intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
						  intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")
					
						Dim blnColumnSet : blnColumnSet = cbool(true)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
						If IsEmpty(intColNr_InScope) then	
							blnColumnSet =false
						ElseIf intColNr_InScope = -1 then
							blnColumnSet =false
						End if
						If not blnColumnSet then 
					'		qq log this	
					'		objXLapp.Calculation = xlManual
							objXLapp.screenUpdating=True
							exittest
						End If
					
						objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
						objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope
						objWS_DataParameter.range("rgWS_Stage_Config_LocalOrGlobal") = "Local"			'		<<<===   should this always be the way the run-scope is managed ?
						'objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").formula = "'" & intRowNr_LoopWS_RowNr_StartOn
						objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
						'objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).formula = "'" & intRowNr_LoopWS_RowNr_FinishOn
						objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
	'		objWS_DataParameter.cells ( intRowNr_LoopWS_RowNr_StartOn  , intColNr_InScope ).select   '   qq reInstate ?? 
						objWS_DataParameter.calculate
						objxlapp.screenupdating = true
					'	objWS_DataParameter.Calculate
					
						objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
						
					tsAr_EventEndedAtTS(iTS_Stage_0) = now()
					
					sbScenarioStageRole_OutcomeList_Verify_vn01 _
						"acSetup"    , _
						objWB_Master.worksheets("wsSpec_ScenarioOutcomeVerify").range("rgWsDyn_Table_ScenarioOutcome_Verify"),  _
						mt, _
						mt, mt, mt, mt, mt, _
						gdict_FwkRun_DataContext_AllScenarios , mt, gDict_FieldName_ScenarioOutComeArrayRowNr , _
						gvntAr_fwScenarioOutcome_Verify_tableAr, mt, mt,  mt 
						
					
					' =========
					' =========  FrameworkPhase 0c   - Setup the MultiStage Array                                           =============
					' =========
					
					Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
					Dim  intMasterZoomLevel 
					
					Dim rowStart, rowEnd, colStart, colEnd
					Dim RangeAr 
					
					
					'    ===>>>   Enable HotKeys
					objWB_Master.Application.Run "HotKeys_Set"
					
					'    ===>>>   Make the RunBook Un-Shared, so that all later processes can run, esp the next, Expand, process
					objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
					
					
	End if
	If 1 = 0  Then		

'	believed to be not-required for B2B_SO					
'	believed to be not-required for B2B_SO					
					
			'								' ===>>>   Expand the MasterSheet into the singleProcess Sheets
			'								  objWB_Master.Application.Run "unitTest_WS_MultiProcess_Expand" ' this populates 
			'								'																		  	objWS_DataParameter.Names("rgWsListDyn_MultiStageProcess_WorksheetNames").RefersToRange
			'								'																	  and from that
			'								'																			strAr_listProcessStage_WorkSheetNames
			'								
			'								' qq remove the below ??
			'								dim strList_Scratch, m, mMax
			'								Dim strAr_listProcessStage_WorkSheetNames, strAr_Scratch1, strAr_Scratch2, strAr_Scratch3, strAr_Scratch
			'								Dim strAr_listProcessStages_Relevant, strAr_listProcessStages_All
			'								' wsSpecAndData_Expand_CreateMultiStageWorkSheets ActiveWorkbook.Sheets("wsMsDT_mrCntestbty_Objectn")
			'								strAr_Scratch = objWS_DataParameter.Names("rgWsListDyn_MultiStageProcess_WorksheetNames").RefersToRange
			'								
			'								mMax = UBound(strAr_Scratch, 1)
			'								strAr_listProcessStage_WorkSheetNames = split ( string(mMax, ","), ",")
			'								For m = 1 to mMax
			'								  strAr_listProcessStage_WorkSheetNames(m) =  strAr_Scratch(m,1)
			'								Next
			'								
			'								
			'								strAr_Scratch = objWB_Master.Names("rgWbListDyn_SelectedProcess_AllStages").RefersToRange
			'								mMax = UBound(strAr_Scratch, 2)
			'								strAr_listProcessStages_All = split ( string(mMax, ","), ",")
			'								For m = 1 to mMax
			'								  strAr_listProcessStages_All(m) = strAr_Scratch(1,m)
			'								Next
			'								' qq remove the above ??
			'								
'
						'	qq below is the XML config setup for the MultiStage processes, this needs to change for the B2B_SO
						If 1 = 0  Then
					
								' create the row (StageNames) and column (XML Template and Tag names) dictionaries
								' and populate them with RowNr and ColNr for the XML_ColumnNames, and Process_RowNames, respectively
									Dim dictWsDP_XML_TemplateAndTag_Name_ColNr, dictWsDP_SingleProcess_Name_RowNr
									set dictWsDP_XML_TemplateAndTag_Name_ColNr 			= CreateObject("Scripting.Dictionary")
									set dictWsDP_SingleProcess_Name_RowNr 		 		= CreateObject("Scripting.Dictionary")
									dictWsDP_XML_TemplateAndTag_Name_ColNr.CompareMode 	= vbTextCompare
									dictWsDP_SingleProcess_Name_RowNr.CompareMode       = vbTextCompare
									
									Dim iAr : iAr = Array(0,0,0,0,0) ' array of integers, 1&2 are start and end rowNrs, 3 & 4 are start and end colNrs
									
									iAr(1) = objWS_DataParameter.names("rgWS_XML_TemplateTag_RowNr_and_ColNrStart").row
									iAr(3) = objWS_DataParameter.names("rgWS_XML_TemplateTag_RowNr_and_ColNrStart").column
									iAr(4) = objWS_DataParameter.names("rgWS_XML_TemplateTag_RowNr_and_ColNrEnd"  ).column
								'	fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, iAr(1), iAr(3), iAr(4)-iAr(3)+1, "", dictWsDP_XML_TemplateAndTag_Name_ColNr 
									
									iAr(3) = objWS_DataParameter.names("rgWS_ProcessList_ColNr_and_RowNrStart").column
									iAr(1) = objWS_DataParameter.names("rgWS_ProcessList_ColNr_and_RowNrStart").row
									iAr(2) = objWS_DataParameter.names("rgWS_ProcessList_ColNr_and_RowNrEnd"  ).row
								'	fnWS_LoadKey_RowOffsets_toDictionary objWS_DataParameter, iAr(3), iAr(1), iAr(2)-iAr(1)+1, "", dictWsDP_SingleProcess_Name_RowNr 
								' XML_ColumnName & Process_RowName dictionaries now initialized & ready to use
								
								
								
								intRowNr_CurrentScenario = 1115 ' qq - a) move to make this per-row for multi-stage  b) then review the loop structures inside multi-stage for efficiency
								objWB_Master.Names("rgWB_ProcessScenario_Selector").formula = _
								  "'" & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Scenario_ProcessPath") ).value
					

						End If

'	believed to be not-required for B2B_SO					
'	believed to be not-required for B2B_SO					

' qq
					Dim strStageIsOmitted              : strStageIsOmitted = objWB_Master.names("rgWBconst_isAnOmitted_ProcessStage").refersToRange.value
					Dim intNrOf_InScope_singleProcesses : intNrOf_InScope_singleProcesses = 0
					
								'		strAr_Scratch = objWB_Master.Names("rgWB_ProcessStages_InScope").RefersToRange
								'		mMax = UBound(strAr_Scratch, 2)
								'		strAr_listProcessStages_Relevant = split ( string(mMax, ","), ",")
								'		For m = 1 to mMax
								'		  strAr_listProcessStages_Relevant(m) = strAr_Scratch(1,m)
								'		  If strAr_listProcessStages_Relevant(m) <> strStageIsOmitted Then
								'		    intNrOf_InScope_singleProcesses = intNrOf_InScope_singleProcesses + 1
								'		  End If
								'		Next
					objWS_DataParameter.calculate
					intNrOf_InScope_singleProcesses = objWS_DataParameter.range("rgWS_NrOf_InScope_singleProcesses").value
					
					If intNrOf_InScope_singleProcesses < 1 Then
						Reporter.ReportEvent micFail , "the process is configured with `0` stages to run, fix and rerun", ""
					'	objXLapp.Calculation = xlManual
						objXLapp.screenUpdating=True
						exitTest
					End IF
					
					'If intNrOf_InScope_singleProcesses = 1 Then
		'			    set objWS_useInEach_ProcessStage = objWS_DataParameter
					    
					
					    '	the first one is always used for Phase_I  Phase I
					'else
					'	set objWS_useInEach_ProcessStage = objWB_Master.sheets(strAr_listProcessStage_WorkSheetNames(iSingleProcess))
					'End if
	End If



					objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope
					
					
					fnLog_Iteration  cFW_Exactly, 100 '  intRowNr_CurrentScenario ' cFW_Plus cFW_Minus 
					
					' strAr_listProcessStages_Relevant = fnSlice_Array(objXLapp, strAr_Scratch, true, 1)
					
					    ' strAr_Scratch (1) ' objWB_Master.Names("rgWB_ProcessStages_InScope").RefersToRange ' qq
					
					
					' qqqq Phase 0 needs to check the inputs for whether they are invalid, and if-invalid, whether they are a) validly-configured negative test cases, or b) errors in data-setup made by the SME
					' - qqqq  a) can be determined by adopting a format-standard (needs to be clearly visible, e.g. Font.Color=Orange) that denotes a negative test case
					
					' ==========================================================************************************************================
					
					
	
	
					
Const gcInt_NrOf_OneBased_FlowFunctions = 10									'	"OneBased" means that q(0,n) will never be used operationally
Const gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction = 10		'	"OneBased" means that q(n,0) will never be used operationally
dim	   gcInt_NrOf_Flows
Const cstrDelimiter = "," : const cAllElements = -1
Const cStr_flowFnName_NoneProvidedYet = "flowFnName_NoneProvidedYet"
Const cStr_Template_dictionaryKey_ScenarioRow_FlowNrName_FlowStepNrName_ParameterNrName_BeforeAfter = "row`^flow`^step`^parm`^befAft`"

Dim q ()	'	this is the 2-D FlowFunction_ParameterArray; its name is "q" to keep the name as short as possible
Const c_StaticValue = "§"	'	this value : 	a) indicates that a flowFunction parameter is static, and 
'											b) is ASSUMED to be  value that will never be passed as a parameter to a FlowFunction
Dim qNm()				'	this is the parameterName array
Dim qBL()				'	this is the parameterBaseLine array
Dim fParmsCt()			'	this is the FunctionParmsCount array
Dim strAr_Flow_FunctionNames()		'	this is the FlowNr / FunctionList array
Dim intAr_Flow_nrOf_Functions_inThisFlow()		

Dim strParameterNameList_for_aSingle_FlowFunction

'	make the parameterArray large enough to handle all the FlowFunctions and all the parms for whichever FlowFunction has the most parms
ReDim 	q 				( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )
ReDim 	qNm			( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )
ReDim	qBL				( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )			
ReDim	fParmsCt		( gcInt_NrOf_OneBased_FlowFunctions                                                                                                                       )	
                                                        				 	  gcInt_NrOf_Flows = 16	      
ReDim	strAr_Flow_fNames						( gcInt_NrOf_Flows )	 	
ReDim intAr_Flow_nrOf_Functions_inThisFlow	( gcInt_NrOf_Flows )
'                                                                          *****    don't forget the leading comma, aligned at the  left   of the asterisks, which makes sure that the array is one-based
'                                                                        ******    don't forget the leading comma, aligned at the LEFT  of the asterisks, which makes sure that the array is one-based
'                                                                      *******    don't forget the leading comma, aligned at the  left   of the asterisks, which makes sure that the array is one-based
		strAr_Flow_fNames ( 1 ) = split(",DataMine,XmlPrep,DropXmlOnGateway,MTS_Check_SOstatus_isRaised,CIS_Check_SOstatus_isRaised", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 1 ) = uBound( strAr_Flow_fNames(1) )
		strAr_Flow_fNames ( 2 ) = split(",Flow2Function1,Flow2Function2,Flow2Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 2 ) = uBound( strAr_Flow_fNames(2) )
		strAr_Flow_fNames ( 3 ) = split(",Flow3Function1,Flow3Function2,Flow3Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 3 ) = uBound( strAr_Flow_fNames(3) )
		strAr_Flow_fNames ( 4 ) = split(",Flow4Function1,Flow4Function2,Flow4Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 4 ) = uBound( strAr_Flow_fNames(4) )
		strAr_Flow_fNames ( 5 ) = split("Flow5Function1,Flow5Function2,Flow5Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 5 ) = uBound( strAr_Flow_fNames(5) )
		strAr_Flow_fNames ( 6 ) = split(",Flow6Function1,Flow6Function2,Flow6Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 6 ) = uBound( strAr_Flow_fNames(6) )
		strAr_Flow_fNames ( 7 ) = split(",Flow7Function1,Flow7Function2,Flow7Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 7 ) = uBound( strAr_Flow_fNames(7) )
		strAr_Flow_fNames ( 8 ) = split(",Flow8Function1,Flow8Function2,Flow8Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 8 ) = uBound( strAr_Flow_fNames(8) )
		strAr_Flow_fNames ( 9 ) = split(",Flow9Function1,Flow9Function2,Flow9Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 9 ) = uBound( strAr_Flow_fNames(9) )
		strAr_Flow_fNames ( 10 ) = split(",Flow10Function1,Flow10Function2,Flow10Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 10 ) = uBound( strAr_Flow_fNames(10) )
		strAr_Flow_fNames ( 11 ) = split(",Flow11Function1,Flow11Function2,Flow11Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 11 ) = uBound( strAr_Flow_fNames(11) )
		strAr_Flow_fNames ( 12 ) = split(",Flow12Function1,Flow12Function2,Flow12Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 12 ) = uBound( strAr_Flow_fNames(12) )
		strAr_Flow_fNames ( 13 ) = split(",Flow13Function1,Flow13Function2,Flow13Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 13 ) = uBound( strAr_Flow_fNames(13) )
		strAr_Flow_fNames ( 14 ) = split(",Flow14Function1,Flow14Function2,Flow14Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 14 ) = uBound( strAr_Flow_fNames(14) )
		strAr_Flow_fNames ( 15 ) = split(",Flow15Function1,Flow15Function2,Flow15Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 15 ) = uBound( strAr_Flow_fNames(15) )
		strAr_Flow_fNames ( 16 ) = split(",Flow16Function1,Flow16Function2,Flow16Function3", cComma, cAllElements, vbBinaryCompare )
			intAr_Flow_nrOf_Functions_inThisFlow ( 16 ) = uBound( strAr_Flow_fNames(16) )


		


'	f is a row-Function-index, p is a column-Parameter-index, both into the ParameterArray q() and ParameterName array qNm()

Dim f, p: f= cint(0) : p = cint(0)

For f = 0 To gcInt_NrOf_OneBased_FlowFunctions 									'	iterate over the functionRows
	For p = 1 To gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction	'		iterate over the ParameterColumns
		If f = 0 then 
			q(f , 0) = "!! It is a design constraint, that none of the cells, or columns, of the array, in THIS row-zero, are used, for any purpose, specifically, none of row zero is used, and " & _
					  "the first column/element of rows one-and-later, contain the FlowFunction-Name for the row  !! "
		else
			q(f , p) = c_StaticValue		'	parameters that are not static will have their c_StaticValue replaced with a variant that contains the variable value to be used
										'	this means that those cells must never be static in themselves, as they will be passed to FlowFunctions as ByRef parameters,
										' 		so that if the function changes the value, we can detect and log the fact, then later desk-debug what happened, without having to do a rerun
		End If
	next
'	set the FlowFunctionName equal to none for all function-parameter rows (as these will align with the case statement that processes the functions, below
'		i.e. q(f,0) will contain the FlowFunctionName
	q(f , 0) = cStr_flowFnName_NoneProvidedYet
next


		'			gFWstr_RunFolder = _
		'				"\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\DataSheets\runs\GUITest21_POC_FnRet_v0.02_DEVMC_CITI_@CsSktLanLib_2017`03Mar`wk10`11Sat_20`43`32\"


dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = cint(0)	: intFirst= cint(0)	: intSecond = cint(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = cint(1)	' qq



sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , true, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf
'	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn)  )  )
	

'	determine the number of in-scope rows
Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun

tsStartOfRun = now()


'	qq     intNrOfRows_Inscope_and_Unfinished

sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , false, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf

'sb_File_WriteContents  _
'	gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
'	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
'	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  





' -------------------------------------------------------------------------------
'	Setup the global_DataContextDictionary here

'	the lines below are in      fnLib_TestData_Mgt.qfl
'	Dim 	gdict_FwkRun_DataContext_AllScenarios
'	 Set		gdict_FwkRun_DataContext_AllScenarios = CreateObject("Scripting.Dictionary")  
'	        	gdict_FwkRun_DataContext_AllScenarios.CompareMode = vbTextCompare
        	
Dim strRunScenario_PreScenarioRow_keyTemplate , strRunScenario_PreScenarioRow_Key
	 strRunScenario_PreScenarioRow_keyTemplate = _
		"^RunNr`{0}^TestName`{1}^Envt`{2}^Coy`{3}^ScenarioList`^ScenarioChunk`^DriverSheetName`{4}^" ' 
'	Examples   1 2 3                              DEVMC      CITI                                                                                                              

strRunScenario_PreScenarioRow_Key  = fn_StringFormatReplacement ( _
		strRunScenario_PreScenarioRow_keyTemplate , _
			Array ( fnTimeStamp( now(), 	cFWstrRequestID_StandardFormat) , gQtTest.Name  , Parameter.Item("Environment") , Parameter.Item("Company")  , objWS_DataParameter.name ) )

' the keys below must be set from inside the DataSheet-ScenarioRow processing loop
	Dim strRunScenario_ScenarioRow_keyTemplate, strRunScenario_ScenarioRow_Key
	strRunScenario_ScenarioRow_keyTemplate = "ScenarioRowNr`{0}^InfoType`FieldValue_Actual^FlowName`{1}^"
	' examples                                                                                  1001 1100                                                                   Flow02
	Dim str_FlowFunction_Seq_Name_templateKey , str_FlowFunction_Seq_Name_Key 
	str_FlowFunction_Seq_Name_templateKey  = "FlowFunction_Seq_Name`{0}_{1}^"
	'                                                                                                                                 1   DataMine
	Dim str_DataContext_FieldName_templateKey,  str_DataContext_FieldName_Key
	str_DataContext_FieldName_templateKey = "DataContext_System_AccessMethod_AccessPoint`{0}_{1}_{2}^FieldName`{3}^Field_ActualValue`inItem^"
	'                                                                                                                      dcCIS_SQL_ap1  dcMTS_GUI_ServiceOrder                       NMI


'	Setup the global_DataContextDictionary here
' -------------------------------------------------------------------------------

' qq - WHY IS THIS NEEDED ? At how many places does one need to define a flow ????????????????

Dim dict_FlowName_FlowNr 
Set	dict_FlowName_FlowNr 					= CreateObject("Scripting.Dictionary")
	dict_FlowName_FlowNr.CompareMode 	= vbTextCompare
	dict_FlowName_FlowNr.Add "Flow1", 1	: 	dict_FlowName_FlowNr.Add "Flow2", 2	:	dict_FlowName_FlowNr.Add "Flow3", 3	:	dict_FlowName_FlowNr.Add "Flow4", 4
	dict_FlowName_FlowNr.Add "Flow5", 5	:	dict_FlowName_FlowNr.Add "Flow6", 6	:	dict_FlowName_FlowNr.Add "Flow7", 7	:	dict_FlowName_FlowNr.Add "Flow8", 8
	dict_FlowName_FlowNr.Add "Flow9", 9	:	dict_FlowName_FlowNr.Add "Flow10", 10	:	dict_FlowName_FlowNr.Add "Flow11", 11	:	dict_FlowName_FlowNr.Add "Flow12", 12
	dict_FlowName_FlowNr.Add "Flow13", 13	:	dict_FlowName_FlowNr.Add "Flow14", 14	:	dict_FlowName_FlowNr.Add "Flow15", 15	:	dict_FlowName_FlowNr.Add "Flow16", 16

' ---------------

dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow


			' sbDictLoad_GlobalRunContext_Item_Key_n_Value        exists in          fnLibFW_ManagementOf_Logging_Data_Context_Exception.qfl,   calls to it exist in     fnLib_AUT_MTS.qfl
			' sbDictLoad_GlobalRunContext_Item_Key_n_Value gdict_FwkRun_DataContext_AllScenarios, strRunScenario_ActualResults_keyTemplate, "TableCol_01_ID", local_strMTSID, strAr_Single_VerifyKey_Fields


			

'	now setup to do process the DriverSheet


Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_Scenario : intRowNr_CurrentScenario = cInt(0) ' formerly cLng
Dim cInt_MinutesRequired_toComplete_eachScenario, cDbl_ExtraFactor_ToWait_for_RunToComplete, tsStopRunWhenRunningTooLong
Dim cInt_MinutesPerDay : cInt_MinutesPerDay = 24 * 60
Dim cInt_SecondsPerDay : cInt_SecondsPerDay = 60 * cInt_MinutesPerDay 
Dim int_rgWS_NumberOfIncompleteScenarios
Dim dt_rgWS_RunWillCeaseAt_TimeStamp : dt_rgWS_RunWillCeaseAt_TimeStamp = now()-7
cInt_MinutesRequired_toComplete_eachScenario = cInt(5)
cDbl_ExtraFactor_ToWait_for_RunToComplete = cdbl(1.2)




int_rgWS_NumberOfIncompleteScenarios = objWS_DataParameter.range("rgWS_NumberOfIncompleteScenarios").value
dt_rgWS_RunWillCeaseAt_TimeStamp = _
	now() + ( cDbl_ExtraFactor_ToWait_for_RunToComplete * _
	                ( ( int_rgWS_NumberOfIncompleteScenarios *  cInt_MinutesRequired_toComplete_eachScenario ) / cInt_MinutesPerDay ) )
	                
	                objWS_DataParameter.range("rgWS_RunWillCeaseAt_TimeStamp") = dt_rgWS_RunWillCeaseAt_TimeStamp
	                
	                
Dim str_ScenarioRow_FlowName 
Dim int_FlowHasNotStartedYet : int_FlowHasNotStartedYet =  objWS_DataParameter.range("rgWS_TestHasNotStarted").value
Dim cStr_FlowNumbers 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= cInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= cInt(-1)

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = false

Dim intColNr_Request_ID 
' intColNr_Request_ID 				= dictWSDataParameter_KeyName_ColNr("Request_ID")
intColNr_Request_ID 				= dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
 intColNr_ScenarioStatus 			= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")
 DIm intColNr_Flow_CurrentStepNr : intColNr_Flow_CurrentStepNr = dictWSDataParameter_KeyName_ColNr("Flow_CurrentStepNr") 
 Dim strRole, strInitiator_Role

Dim strInScope, strRowCompletionStatus

objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=true
objWB_Master.save





	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
		' Get headers of table in a dictionary
	Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	

	
		' Load the first/title row in a dictionary
	int_MaxColumns = UBound(										gvntAr_RuleTable,2)
	
'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue
	

'	qq	the code below was in the CatsWigs_MultiStage test case, but can be found nowhere in this one , is this significant ? 		<<===  BrianM 2017`03Mar`w11`15Wed
		'		fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table
		'	int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)



' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
' Launch CIS
fnCIS_OpenApplication
' Login to application
Select case Parameter.Item("Environment")
	Case "SITMC"
		fnCIS_Login "n", "UANAND", "qwerty123", "CCISMT" ' qq - the parameters should come from excel, NEEd TO CHANGE
	Case "DEVMC"
		fnCIS_Login "n", "UANAND", "qwerty123", "CCISMD" ' qq - the parameters should come from excel, NEEd TO CHANGE
End Select


' Launch MTS
fnMTS_WinClose
OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")

' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	
' Reset error flags
fn_reset_error_flags




For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	' Umesh - This need to be reviewed during execution

								'	intRowNr_LoopWS_RowNr_StartOn   qq

	'	if the scenarioRow is InScope-and-Incomplete                                                                                                                 
	strInScope 				= 	ucase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
	strRowCompletionStatus = 		 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_RowCompletionStatus	).value
	str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
	
	If  (	( strInScope = cY)  and ( strRowCompletionStatus	= cIncomplete	) and str_ScenarioRow_FlowName <> "" ) Then
	
	
	End If
	
Next 


'	while     not    bln_All_InScope_ScenarioRows_HaveFinished_Executing
Do	

'	for all rows between rowStart and rowEnd
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	' Umesh - This need to be reviewed during execution
									'	intRowNr_LoopWS_RowNr_StartOn   qq

	'	if the scenarioRow is InScope-and-Incomplete                                                                                                                 
	
	strInScope 				= 	ucase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
	strRowCompletionStatus = 		 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_RowCompletionStatus	).value
	str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
	
	If  (	( strInScope = cY)  and ( strRowCompletionStatus	= cIncomplete	) and str_ScenarioRow_FlowName <> "" ) Then
		
		'	shade the InScope colNr cell to show that it has started processing	
			objWS_DataParameter.Cells( intRowNr_CurrentScenario, intColNr_InScope ).Interior.Color = objWS_DataParameter.range("rgWS_status_CaseStarted").Interior.Color
		
			
			intFlowNr_Current						= dict_FlowName_FlowNr ( str_ScenarioRow_FlowName )
			strRunScenario_ScenarioRow_Key	= _
				fn_StringFormatReplacement ( _
					strRunScenario_ScenarioRow_keyTemplate , _
						Array ( intRowNr_CurrentScenario , str_ScenarioRow_FlowName   ) )
										
			'				'	put these keys in the right place
			'
			'											Dim str_FlowFunction_Seq_Name_templateKey , str_FlowFunction_Seq_Name_Key 
			'											str_FlowFunction_Seq_Name_templateKey  = "FlowFunction_Seq_Name`{0}_{1}^"
			'											'                                                                                                                                 1   DataMine
			'											str_FlowFunction_Seq_Name_Key	= _
			'												fn_StringFormatReplacement ( _
			'													str_FlowFunction_Seq_Name_templateKey , _
			'						qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq )
			'											
			'											Dim str_DataContext_FieldName_templateKey,  str_DataContext_FieldName_Key
			'										'	put these keys in the right place

			

			int_ScenarioRow_Flow_CurrentStepNr = intRowNr_CurrentScenario
			int_ScenarioRow_Flow_CurrentStepNr = objWS_DataParameter.cells( intRowNr_CurrentScenario , intColNr_Flow_CurrentStepNr).value
			if int_ScenarioRow_Flow_CurrentStepNr = "" or int_ScenarioRow_Flow_CurrentStepNr = int_FlowHasNotStartedYet then _
				fn_update_current_flow_number objWS_DataParameter, intRowNr_CurrentScenario,  intColNr_Flow_CurrentStepNr, "0"
				int_ScenarioRow_Flow_CurrentStepNr = 1	
			do	' synthetic goto

			do	'	do all the functions for this flow   (first, irrespective of what the flow is   <<== this eliminates the need to put this loop inside each    "case <FlowName>"  select-boundary
			       '     ...   yes, it does look "back to front", but it flattens the number of required loops from the `NrOfFlows` to `One`
		'	Loop while int_ScenarioRow_Flow_CurrentStepNr < intAr_Flow_nrOf_Functions_inThisFlow ( intFlowNr_Current ) 	
				
				strFlowFunctionName = strAr_Flow_fNames (  dict_FlowName_FlowNr ( str_ScenarioRow_FlowName )) (int_ScenarioRow_Flow_CurrentStepNr)		'	strFlowFunctionName = "DataMine"
					
				Select Case str_ScenarioRow_FlowName
					Case "Flow1"
						
					'	do
						f = int_ScenarioRow_Flow_CurrentStepNr
						Select Case strFlowFunctionName 
								
							case "DataMine"
								If  q(f,0) = cStr_flowFnName_NoneProvidedYet  Then
								End if
								
								
							'	get NMIS from CIS as a list in a string variaible that the MTS query uses as "where nmi in (<strList>)"
								strSQL_TemplateName_Current 	= "CIS_ActiveNMIFinder"
								strMasterWS_ColumnName 		= strSQL_TemplateName_Current
					                	intSQL_ColNr           				= dictWSTestRunContext_KeyName_ColNr( strMasterWS_ColumnName )  
					                  	strQueryTemplate   				= objWS_TestRunContext.cells(2,intSQL_ColNr).value '
				                		
				                		strSQL_CIS = strQueryTemplate 
					                 	strSQL_CIS = replace ( strSQL_CIS  , "<Meter_Type_CIS>", _
					                 		objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Meter_Type_CIS") ).value, 1, -1, vbTextCompare)

						              ' ... run the SQL
						             fn_DbQuery_v3 DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, int_NrOf_InScope_Rows
								If objDBRcdSet_TestData_A.eof or (int_NrOf_InScope_Rows = 0 ) Then
								'	qq qq qq  handle zero-rows-returned here
									int_NrOf_InScope_Rows = int_NrOf_InScope_Rows
								else
									int_NrOf_InScope_Rows = int_NrOf_InScope_Rows
								End If


							'	Use the retrievd CIS.NMI_List to find a good row in MTS, and put that into the MTS Query
								strSQL_TemplateName_Current 	= "MTS_ActiveNMIFinder"
								strMasterWS_ColumnName 		= strSQL_TemplateName_Current
							       intSQL_ColNr           				= dictWSTestRunContext_KeyName_ColNr( strMasterWS_ColumnName )  
							       strQueryTemplate       				= objWS_TestRunContext.cells(2,intSQL_ColNr).value '
							       
							       
							       strSQL_MTS = strQueryTemplate
						            	strSQL_MTS = replace ( strSQL_MTS , "<NMI_Size>" , _
						             		objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI_Size") ).value, 1, -1, vbTextCompare)
						            	strSQL_MTS = replace ( strSQL_MTS , "<Meter_Type_MTS>", _
						             		objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Meter_Type_MTS") ).value, 1, -1, vbTextCompare)
						            	strSQL_MTS = replace ( strSQL_MTS , "<AMI_RWD0or1>", _
						             		objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("AMI_RWD0or1") ).value, 1, -1, vbTextCompare)
						            	strSQL_MTS = replace ( strSQL_MTS , "<NMI_LIST>", _
									objDBRcdSet_TestData_A.Fields ("CIS_NMI_LIST") )


					              	' ... run the SQL
					              	fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, int_NrOf_InScope_Rows_B
								If objDBRcdSet_TestData_B.eof or (int_NrOf_InScope_Rows_B = 0 ) Then
								'	qq qq qq  handle zero-rows-returned here
									int_NrOf_InScope_Rows_B = int_NrOf_InScope_Rows_B
								else
									int_NrOf_InScope_Rows_B = int_NrOf_InScope_Rows_B
								End If				


			dim int_EveryRS_FieldFirst_Nr 	: int_EveryRS_FieldFirst_Nr	= cint(0)
			Dim int_idxRS_ColNr 				: int_idxRS_ColNr				 = cint(0)
			Dim int_idxMax					: int_idxMax					 = cint(0)
			dim intRS_B_ColsCount
'			Dim strSqlCols_ValuesList, strSqlCols_NamesList

			intRS_B_ColsCount = objDBRcdSet_TestData_B.Fields.Count
		       strSqlCols_NamesList = "" 
		       strSqlCols_ValuesList = ""  
		       strSql_Suffix = ""  ' the list is 0-based
		       strSql_Suffix = ","  ' the list is 1-based
		
                    ' retrieve and accumulate into a comma-separated list, the SQL_Query's Column_Names-and-Values
                    															int_idxMax	 = objDBRcdSet_TestData_B.Fields.Count -1
                    For int_idxRS_ColNr 	= int_EveryRS_FieldFirst_Nr To 	int_idxMax
		        	strSqlCols_NamesList 	= strSqlCols_NamesList  	& strSql_Suffix & objDBRcdSet_TestData_B.Fields(int_idxRS_ColNr).Name
		             strSqlCols_ValuesList		= strSqlCols_ValuesList 	& strSql_Suffix & objDBRcdSet_TestData_B.Fields(int_idxRS_ColNr).Value
		             strSql_Suffix = ","
		       Next
		       
			Dim tsNext_ScenarioFunction_canStart_atOrAfter, tsNow, intWaitLengthInSeconds
			dim intColNr_SQL_columnNames	:	intColNr_SQL_columnNames	= dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")
			dim intColNr_SQL_columnValues	:	intColNr_SQL_columnValues	= dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")
													intColNr_NMI 					= dictWSDataParameter_KeyName_ColNr("NMI")
		
		       objWS_DataParameter.Cells( intRowNr_CurrentScenario, intColNr_SQL_columnNames).formula = "'" & strSqlCols_NamesList
		       objWS_DataParameter.Cells( intRowNr_CurrentScenario, intColNr_SQL_columnValues ).formula = "'" & strSqlCols_ValuesList
		       
		       objWS_DataParameter.Cells( intRowNr_CurrentScenario, intColNr_NMI ).formula = "'" & objDBRcdSet_TestData_B.Fields("NMI").value
		
				objWS_DataParameter.calculate
				objXLapp.screenUpdating=true
				' objXLapp.screenUpdating=False
				objWB_Master.save
				' q1                
				                
				                
' ===>>>   push from SQL to WS
' NMI_List


' q2
				
' ===>>>   push from SQL to WS
' MTS  =? populate cols from SQL cols of same name
' NMI
' ''''  Initiator	


					
'	XML MERGE
' 1) xmlTemplate (done)
' 2)  xmlTags (done)


' drop xml on queue

'  wait   120

'  CIS / MTS Fns
								' Check Required_SP_Status_MTS = D and create service order in CIS
								str_Required_SP_Status_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_MTS") ).value 
								
								If lcase(str_Required_SP_Status_MTS) = "d" Then
									temp_DataPrep_CIS_SOType = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataPrep_CIS_SOType") ).value 
									temp_DataPrep_CIS_SOReason = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataPrep_CIS_SOReason") ).value 
									temp_DataPrep_CIS_TaskType = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataPrep_CIS_TaskType") ).value 
									Create_and_Complete_Service_Order objDBRcdSet_TestData_B.Fields("NMI").value, temp_DataPrep_CIS_SOType, temp_DataPrep_CIS_SOReason, temp_DataPrep_CIS_TaskType
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_Post_CIS_Data_Prep, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									' Function to update the flow number
									fn_update_current_flow_number objWS_DataParameter, intRowNr_CurrentScenario,  intColNr_Flow_CurrentStepNr, int_ScenarioRow_Flow_CurrentStepNr
								Else
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									' Function to update the flow number
									fn_update_current_flow_number objWS_DataParameter, intRowNr_CurrentScenario,  intColNr_Flow_CurrentStepNr, int_ScenarioRow_Flow_CurrentStepNr
								End If
								
								
							case "XmlPrep"
							
									tsNext_ScenarioFunction_canStart_atOrAfter = _
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									tsNow = now() 
									IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									'	qq write code for the next line
									'	StoreTheCurrentScenarioFlowStepNr_Into_TheWorksheet qqAnd EnsureItIsUsedNextTime_BrianIsNotSureBestApproachHere
										Exit do
									End if

								If  q(f,0) = cStr_flowFnName_NoneProvidedYet  Then
								End if
								
								
												'   this is not a loop, it is the mechanism to do a goto
											Do
											
												gFWbln_ExitIteration = False
												tsAr_EventPermutationsCount(iTS_Stage_II) = tsAr_EventPermutationsCount(iTS_Stage_II) + 1 ' increment the permutations count
												
												BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from framework
												BASE_XML_Template_DIR = cBASE_XML_Template_DIR
												
												' The RULE HERE is that when an SQL is specified, so will be the corresponding list of SQL-sourced-DataTags
												' strScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
												strScratch = ""
												strScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
												If strScratch = "" then
													gFWbln_ExitIteration = true
													objWS_DataParameter.Cells( intRowNr_CurrentScenario, intColNr_ExitTestIteration_NY 					).value = cY
													objWS_DataParameter.Cells( intRowNr_CurrentScenario, intColNr_ExitTestIteration_ReasonContextEtc	).value = _
														fn_StringFormatReplacement (	_
															"Column of name `xmlTemplateName` needed a value, but was `blank`, in wsRow{0}, for procesing in Flow`{1}`Step `{2}`.", _
															array( intRowNr_CurrentScenario, str_ScenarioRow_FlowName, strFlowFunctionName ) )
												End if
			'												If strScratch <> "" Then ' ignore blank cells
			'													strSQL_TemplateName_Previous = strXML_TemplateName_Current
			'													strXML_TemplateName_Current = strScratch
			'												End If
												
												StrXMLTemplateLocation = BASE_XML_Template_DIR & strXML_TemplateName_Current
												'               StrXMLTemplateLocation = BASE_XML_Template_DIR & "CN`1xxx_200BadMeter.xmlTP_AvB.xml" ' qq hardcoded but need to come from framework
												'       int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
												
												' qq - check how this unique reference is generated for single process and multi process
												' int_UniqueReferenceNumberforXML = gFwAr_strRequestID( iSingleProcess, tempInt_RoleIdentificationCounter )
												
												' The unique reference need to come from the cell in corresponding worksheet
												int_UniqueReferenceNumberforXML = "" ' clear previous request ID, if any     intRowNr_CurrentScenario intRowNr_CurrentScenario
												int_UniqueReferenceNumberforXML = fnTimeStamp( now(), cFWstrRequestID_StandardFormat) 
												Str_TransactionID 						=  int_UniqueReferenceNumberforXML ' "txnID_" &
												' objWS_DataParameter.Cells( intRowNr_CurrentScenario , intColNr_Request_ID).value = int_UniqueReferenceNumberforXML
												objWS_DataParameter.Cells( intRowNr_CurrentScenario , intColNr_Request_ID).value = int_UniqueReferenceNumberforXML

												
												' Date_TransactionDate = now ' "<dataRunDt_SrcFWK_fmtAllBut_TimeZone>+10:00" ' sample #11/24/2016 7:12:53 PM#
												Date_TransactionDate = Now ' year(now) & "-" & month(now) & "-" & day(now)
												
												strCaseGroup = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
												strCaseNr    = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_ID")).value
												
												'C:\_data\XMLs\MC
												Dim strFolderNameWithSlashSuffix_Scratch
												Err.clear
												strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"
												'strScratch = strAr_acrossA_Role(tempInt_RoleIdentificationCounter) & objWS_DataParameter.range("rgWS_objRS_ColumnValues_roleSuffix").value
' qq believed not needed for B2B_SO			strScratch = strAr_acrossA_Role(tempInt_RoleIdentificationCounter) & objWS_DataParameter.range("rgWS_objRS_ColumnValues_roleSuffix").value
												
												' strFolderNameWithSlashSuffix_Scratch = "F:\qqq\"
												gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file = 0
												
							'					str_ChangeStatusCode = strAr_listProcessStages_All(iSingleProcess)
												'	"REQ" ' qq - This is hardcoded for this script but in a multistage process, this should be the process name. A tag in XML would be updated with this value
												
												
												' fnCreateCATSXML Str_CaseGroup & "_" & Str_CATS_CR, StrXMLTemplate_Folder, StrXMLTemplate_FileNameWoFolder, objWS_DataParameter, "1", gvntAr_RuleTable, gdictWStemplateXML_TagDataSrc_Tables_ColValue
												'                      fnCreateCATSXML strCaseGroup & "_" & strCaseNr, BASE_XML_Template_DIR, strXML_TemplateName_Current, objWS_DataParameter, intRowNr_CurrentScenario, gvntAr_RuleTable, gdictWStemplateXML_TagDataSrc_Tables_ColValue, Date_TransactionDate, int_UniqueReferenceNumberforXML, Str_Role, str_NMI
												
												' set cellScratch to the current-row XML_TemplateName and retrieve the value referred to by the cell from the configuration table
												' Set cellScratch = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name"))
												Set cellScratch = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name"))
												' qq qq qqqq
												
												
												If fnElement(cellScratch,"_",0,-1,"L") <> "tplt_" Then					
													strXML_TemplateName_Current = cellScratch.value
												Else
													strXML_TemplateName_Current = _
														objWS_DataParameter.cells(dictWsDP_SingleProcess_Name_RowNr(strAr_listProcessStages_All(iSingleProcess)) , dictWsDP_XML_TemplateAndTag_Name_ColNr(cellScratch.value)).value
													'objWS_DataParameter.cells(dictWsDP_SingleProcess_Name_RowNr(strAr_listProcessStages_All(iSingleProcess)) , dictWsDP_XML_TemplateAndTag_Name_ColNr(cellScratch.value)).value
													
												End If
												
				'								str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML(Environment.Value("COMPANY"), strCaseGroup & "_" & strCaseNr, str_ChangeStatusCode, BASE_XML_Template_DIR, strXML_TemplateName_Current, _
				'								strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, intRowNr_CurrentScenario, gvntAr_RuleTable, _
				'								gdictWStemplateXML_TagDataSrc_Tables_ColValue, Date_TransactionDate, int_UniqueReferenceNumberforXML, _
				'								Str_Role, str_NMI, "csListSqlColumn_Names", strScratch)
				'
				'	===>>> Create the XML file
												dim str_DataParameter_PopulatedXML_FileName_withoutFolder
											'	strCaseGroup = "scg_None" : strCaseNr = "ccr_None" : 
												str_ChangeStatusCode = "csc_None"  
											'	qq shoudl strRole be the Initiator column ?
												strInitiator_Role 	= objWS_DataParameter.Cells( intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Initiator")).value
												strRole				= strInitiator_Role
												strRole 			= replace ( strRole, "ALT"	, mt, 1, -1, vbTextCompare )
												strRole 			= replace ( strRole, "ROLE", mt, 1, -1, vbTextCompare )
												strRole 			= replace ( strRole, "_"		, mt, 1, -1, vbTextCompare )
												strNMI = objWS_DataParameter.Cells( intRowNr_CurrentScenario, intColNr_NMI ).value
												str_DataParameter_PopulatedXML_FileName_withoutFolder = _
													fnMergeXML_vn01 (  _
														Environment.Value("COMPANY"), strCaseGroup & "_" & strCaseNr, str_ChangeStatusCode, BASE_XML_Template_DIR,  _
													       strXML_TemplateName_Current, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter,objWS_DataParameter, _
													       dictWSDataParameter_KeyName_ColNr, 	intRowNr_CurrentScenario, _
													       	gvntAr_RuleTable, _
													       g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Date_TransactionDate,  _
													       int_UniqueReferenceNumberforXML,  strRole, strNMI, "csListSqlColumn_Names", "csListSqlColumn_Values" )
													       
											       If gFWbln_ExitIteration = True Then
													objWS_DataParameter.Cells( intRowNr_CurrentScenario, intColNr_ExitTestIteration_NY 					).value = cY
'													objWS_DataParameter.Cells( intRowNr_CurrentScenario, intColNr_ExitTestIteration_ReasonContextEtc	).value = _
'														fn_StringFormatReplacement (	_
'															"Column of name `xmlTemplateName` needed a value, but was `blank`, in wsRow{0}, for procesing in Flow`{1}`Step `{2}`.", _
'															array( intRowNr_CurrentScenario, str_ScenarioRow_FlowName, strFlowFunctionName ) )
'
											       	Exit do
											       End If
				
												' Set cellScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario, intColNr_ScenarioStatus )
												Set cellScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario,  intColNr_ScenarioStatus )
												str_DataParameter_PopulatedXML_FqFileName = strFolderNameWithSlashSuffix_Scratch & str_DataParameter_PopulatedXML_FileName_withoutFolder
												strScratch = str_DataParameter_PopulatedXML_FqFileName
												
												' qq - need to fix as hyperlinks are not working
												
												'objWS_DataParameter.Hyperlinks.Add cellScratch   , strScratch
												'objWS_DataParameter.Hyperlinks.Add cellExpectedValue, strScratch
												' cellScratch.value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_InProgress").value
												cellScratch.value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_InProgress").value
												
												Dim intErrCount
												
												intErrCount = 0
												
												
												intScratch = Len ( str_DataParameter_PopulatedXML_FileName_withoutFolder )
												If  intScratch > gFWint_AEMO_HUB_MaxFileNameLength_Limit Then
													strScratch = ""
													intErrCount = intErrCount + 1
													With cellScratch
														.value = "TF"
														On Error Resume Next
														strScratch = .Comment.Text
														On Error Goto 0
														.AddComment
														.Comment.Visible = False
														' StringBuilder function replaced with fn_StringFormatReplacement function
														' strScratch = strScratch & hFWobj_SB.AppendFormat ("HUB.Parameter`FileName.MaximumLength` was exceeded, value is `{0}`, `{1}` was submitted.", gFWint_AEMO_HUB_MaxFileNameLength_Limit, intScratch  ).ToString
														strScratch = strScratch & fn_StringFormatReplacement("HUB.Parameter`FileName.MaximumLength` was exceeded, value is `{0}`, `{1}` was submitted.", array(gFWint_AEMO_HUB_MaxFileNameLength_Limit, intScratch))
														.Comment.Text strScratch
														Reporter.ReportEvent micFail, strScratch, str_DataParameter_PopulatedXML_FqFileName
														' set the scenario status to TechFail
														.value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_Framework_TechFail").value
														gFWbln_ExitIteration = True
														objWS_DataParameter.Cells(intRowNr_CurrentScenario, intColNr_ExitTestIteration_NY) = cY
										                      	objWS_DataParameter.Cells(intRowNr_CurrentScenario, intColNr_ExitTestIteration_ReasonContextEtc) = _ 
										                      		fn_StringFormatReplacement (_
										                      			"TestName={0},Scenario{1},RunSheet(2),RowNr{3},Reason={4},LimitName={5},Limit={6},Name={7},ActualLen={9}", _
																Array(gQtTest.Name, str_rowScenarioID, objWS_DataParameter.Name, intRowNr_CurrentScenario, "c_FolderNameTooLong" , _
																			"gFWint_AEMO_HUB_MaxFileNameLength_Limit", gFWint_AEMO_HUB_MaxFileNameLength_Limit, _
																			str_DataParameter_PopulatedXML_FileName_withoutFolder, intScratch ) )
														
													End With
													' Set cellScratch = nothing
												End If
												
												If gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file > 0 Then
													strScratch = ""
													intErrCount = intErrCount + 1
													With cellScratch
														.value = "TF"
														On Error Resume Next
														strScratch = .Comment.Text
														'On error goto 0
														.AddComment
														.Comment.Visible = False
														' StringBuilder function replaced with fn_StringFormatReplacement function
														' strScratch = strScratch & hFWobj_SB.AppendFormat ("The PopulatedXML still contains `{0}` Un-Replaced tags; iteration abandoned.  Fix and rerun.", gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file).ToString
														strScratch = strScratch & fn_StringFormatReplacement("The PopulatedXML still contains `{0}` Un-Replaced tags; iteration abandoned.  Fix and rerun.", array(gFWint_NrOf_UnReplaced_Tags_inThe_PopulatedXML_file))
														'On error resume next ' a) in case the cell already has a hyperlink b) qq move this to a function that handles that concern
														On Error Goto 0
														.Comment.Text strScratch
														Reporter.ReportEvent micFail, strScratch, str_DataParameter_PopulatedXML_FqFileName
														' set the scenario status to TechFail
														.value = objWS_DataParameter.range("rgWS_SingleScenario_RunStatus_Framework_TechFail").value ' qq should this move to the fnLib_Automation, if so, push the value to this range as part of FW.TestCase_Setup
														gFWbln_ExitIteration = True
														objWS_DataParameter.Cells(intRowNr_CurrentScenario, intColNr_ExitTestIteration_NY) = cY
										                      	objWS_DataParameter.Cells(intRowNr_CurrentScenario, intColNr_ExitTestIteration_ReasonContextEtc) = _ 
										                      		fn_StringFormatReplacement (_
										                      			"TestName={0},Scenario{1},RunSheet(2),RowNr{3},Reason={4},FileName={5},Limit={6},Name={7},ActualLen={9}", _
																Array(gQtTest.Name, str_rowScenarioID, objWS_DataParameter.Name, intRowNr_CurrentScenario, "d_UnReplacedTags_Exist_in_PopulatedXML_File" , _
																			strFolderNameWithSlashSuffix_Scratch & str_DataParameter_PopulatedXML_FileName_withoutFolder ) )
														
													End With
													' Set cellScratch = nothing
												End If
												
											'	if intErrCount > 0 then
											'		cellScratch.Hyperlinks.Add  cellScratch, str_DataParameter_PopulatedXML_FqFileName 
											'	End if
												copyfile str_DataParameter_PopulatedXML_FileName_withoutFolder , strFolderNameWithSlashSuffix_Scratch , strRunLog_Folder
												cellScratch.Hyperlinks.Add  cellScratch  ,  strRunLog_Folder & str_DataParameter_PopulatedXML_FileName_withoutFolder 
				
											' StringBuilder function replaced with fn_StringFormatReplacement function
				'								goStrBldr.Clear
				'								strBarText = goStrBldr.AppendFormat ( cStr_BarText_Format,  _
				'									gQtTest.Name ,	Parameter.Item("Environment") ,	Parameter.Item("Company") ,	intRowNr_CurrentScenario ,	_
				'									objWS_DataParameter.Cells(intRowNr_CurrentScenario, intCatsCR_ColNr).value , strPhase , Str_Role , strStage, int_UniqueReferenceNumberforXML , str_NMI  ).ToString
												strBarText = fn_StringFormatReplacement(cStr_BarText_Format, array(gQtTest.Name ,	Parameter.Item("Environment") ,	Parameter.Item("Company") ,	intRowNr_CurrentScenario ,	_
													 int_UniqueReferenceNumberforXML , strNMI  ))
				
												objXLapp.statusbar = strBarText
				
												Set cellScratch = Nothing
												strScratch_YN =  fnSetIfTrueFalse ( gFWbln_ExitIteration  , "EQ", True, cY, cN )
				
												strAr_Single_VerifyKey_Fields_logXML ( ciField_HyperLink 			 		) = strRunLog_Folder & str_DataParameter_PopulatedXML_FileName_withoutFolder 
												strAr_Single_VerifyKey_Fields_logXML ( ciField_or_Object_Type		 		) = "ft_txnXML" 		'	qq should this become an enumerated list ?
												strAr_Single_VerifyKey_Fields_logXML ( ciField_ValueWasUsed_YN	 		) = strScratch_YN
												strAr_Single_VerifyKey_Fields_logXML ( ciField_Value_orObject_orBlob	) = gstr_generated_txnXML
												strAr_Single_VerifyKey_Fields_logXML ( ciAsOfTimestamp 					) = now
						
												strScratch_dictionary_TemplateKey = _
													fnCreate_Run_DataKey_Name ( _	
														cFormatStr_I_guiMTS, _
														mt, mt, mt, mt, mt, "InBound", mt,  _
															mt, strInitiator_Role   , int_UniqueReferenceNumberforXML , mt, mt, mt, "fw_PhaseII", gcStr_toReplace_FieldName )
															
															
													'	mt, mt, mt, mt, mt, "InBound", objWS_DataParameter.Cells(intRowNr_CurrentScenario, intCatsCR_ColNr).value,  _
													'		strStage, str_Role   , int_UniqueReferenceNumberforXML , mt, mt, mt, "fw_PhaseII", gcStr_toReplace_FieldName )

													'	IO - taken care of by the template, i.e. I
													
													
													' 	       RunNr    ScenarioNr               CatsCR                                                                                     
													'      	       ScenarioList   InOutBound                                                                                                        
													'      	stage        role                                             RequestID                                                     txStat   VerificationPtSeqNr    
													'                                                                                                                                                             raisedByRetailerNm
													'                                                                                                                                                                           FieldSource    FieldName
													'            qqqq RunNr needs a value, from the external generator in the common Oracle DB
													'              so do ScenarioList and Scenario
														
												strScratch_dictionary_FieldName = "txnClob_XML" 
																																																								  strScratch_dictionary_FieldName = strScratch_dictionary_FieldName 
												gstrFW_dictionaryKey_fwPhase_IIExecute = replace ( 													strScratch_dictionary_TemplateKey ,	 gcStr_toReplace_FieldName	, strScratch_dictionary_FieldName , 1	, cInt_Split_or_Replace_ProcessOrReturn_AllElements , vbTextCompare )
												strAr_Single_VerifyKey_Fields_logXML ( ciField_Key_storedWith			) = gstrFW_dictionaryKey_fwPhase_IIExecute
								'				sbDictLoad_GlobalRunContext_Item_Key_n_Value  	gdict_FwkRun_DataContext_AllScenarios, 	strScratch_dictionary_TemplateKey ,	 gcStr_toReplace_FieldName	, strAr_Single_VerifyKey_Fields_logXML, strAr_Single_VerifyKey_Fields_logXML 
												
											'	sbDictLoad_GlobalRunContext_Item_Key_n_Value ( 	pio_Dictionary								, piStr_Dictionary_KeyName_Template	, piStr_Item_Name , piVnt_Item_Value_or_Instance		, 	piVnt_Item								)
											'	sbDictLoad_GlobalRunContext_Item_Key_n_Value 	( 	pio_Dictionary 								, piStr_Dictionary_KeyName_Template 	, piStr_Item_Name , piVnt_Item_Value_or_Instance , piVnt_Item 						)
												
								'				sbPushValuesToLog_wsTable _
								'					cstrLogEntryType_DataFile ,	"",  _
								'						objWS_DataParameter ,		intRowNr_CurrentScenario							, _
								'						objWB_Master.worksheets("CatsCrRoleStage_VerifyDump")						, _
								'						str_dictDataContext_Key															, _
								'						gdict_FwkRun_DataContext_AllScenarios.item (  str_dictDataContext_Key )		, _
								'						piRgWsDyn_Table_ScenarioOutcome_Verify	,	intRowNr_of_FieldVerify_rangeTableAr	,	 strPass_YN
				
									If 			gFWbln_ExitIteration = True or lcase(gFWbln_ExitIteration) = "y" Then Exit Do ' gFWbln_ExitIteration is reset to true at the beginning of the iteration
												
												strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
												' copy the PopulatedXML file from the temp folder to the input folder
												copyfile str_DataParameter_PopulatedXML_FileName_withoutFolder, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")
												
												' ZIP XML File
												Dim vntDuration : vntDuration = now
												ZipFile str_DataParameter_PopulatedXML_FileName_withoutFolder , BASE_XML_Template_DIR & "runs\"
												vntDuration = now - vntDuration
												strBarText_Save = objXLapp.StatusBar
												strMsg = "Zip duration was " & fnTimeStamp ( vntDuration, "mm'ss")
												objXLapp.StatusBar = strMsg & " - " & strBarText_Save
												  ' wait 0, 750
				'								objXLapp.StatusBar = strBarText_Save
				'								objXLapp.StatusBar = ""
												
												temp_role = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value 
												GetCATSDestinationFolder_V2 Environment.Value("COMPANY"), Environment.Value("ENV"), "B2B", temp_role , gvntAr_CATS_Destination_Folder_Table,  gdictWSCATSDestinationFolder_Table_ColValue
												
												If Environment.Value("CATSDestinationFolder")  <> ""  Then
													copyfile Left(str_DataParameter_PopulatedXML_FileName_withoutFolder, Len(str_DataParameter_PopulatedXML_FileName_withoutFolder)-4) &".zip",BASE_XML_Template_DIR & "runs\" ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
													Reporter.ReportEvent micPass, "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
												Else
													Reporter.ReportEvent micFail,"Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													gFWbln_ExitIteration = "y"
												    gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing" & "- Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												End If
																							
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												' Function to update the flow number
												fn_update_current_flow_number objWS_DataParameter, intRowNr_CurrentScenario,  intColNr_Flow_CurrentStepNr, int_ScenarioRow_Flow_CurrentStepNr
												
'												If gFWbln_ExitIteration = "y" or gFWbln_ExitIteration = True Then
'													' Write to file the current flow number
'													objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Flow_CurrentStepNr") ).value
'													
'												End If
											
											Exit Do
											Loop	'	just-inside   "if this is not a no-Run_Permutation", 	for 			IterationExit management
								
							case "DropXmlOnGateway"
								If  q(f,0) = cStr_flowFnName_NoneProvidedYet  Then
								End if
								
								
								' ===============================================================================   CIS & MTS AUT GUI Functions after this line ========
								
																' ===============================================================================   CIS & MTS AUT GUI Functions after this line - START ========
								
							case "MTS_Check_SOstatus_isRaised"
							
								' fn_compare_Current_and_expected_execution_time
								
								tsNext_ScenarioFunction_canStart_atOrAfter = _
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								tsNow = now() 
								IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								'	qq write code for the next line
								'	StoreTheCurrentScenarioFlowStepNr_Into_TheWorksheet qqAnd EnsureItIsUsedNextTime_BrianIsNotSureBestApproachHere
									Exit do
								End if
							
								If  q(f,0) = cStr_flowFnName_NoneProvidedYet  Then
								End if
								
								'Navigate to "Transactions;Service Orders;Service Order Request Search"
								fn_MTS_MenuNavigation "transactions;service orders;service order request search"
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))	
								
								' fn_MTS_Serach_Service_Order_Request "001911747", "", "VEPL" ,"6102268911" ,"06/09/2016" ,"19/09/2016" , "", "", "Adds And Alts", "Install Meter", str_Request_Status
								fn_MTS_Serach_Service_Order_Request "", "", "" , int_NMI, fnTimeStamp(now,"DD/MM/YYYY"), fnTimeStamp(now,"DD/MM/YYYY") , "", "", "", "", ""
								
								fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord "first", "","","","","","","","",objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")) ,"y"
								
								Select case lcase(objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")))
									Case "aq": 
									
										fn_MTS_Click_button_any_screen "Search_Service_Order_Request_Screen", "btn_Details"
										
										' fn_MTS_Service_Order_Tracking_VerifyAQ "SO_MTRADDALTS", "A Meter Add/Alt SO has been received and will need to be raised manually", "OPEN","y"
										fn_MTS_Service_Order_Tracking_VerifyAQ objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("AQ_Name")), objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("AQ_Explaination")), objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("AQ_Status")),"y"
									
									Case "rejected":
										fn_MTS_Click_button_any_screen "Search_Service_Order_Request_Screen", "btn_Details"
										' fn_MTS_Service_Order_Tracking_VerifyRejection "New Request with previously used RetServiceOrder.", "[Not Provided]", "y"
										
										temp_Rej_Exp = objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("Rejection_Explanation"))
										If temp_Rej_Exp  = "" Then
											temp_Rej_Exp = "[Not Provided]"
										End If
											fn_MTS_Service_Order_Tracking_VerifyRejection objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("Rejection_Reason")), temp_Rej_Exp, "y"
								End Select	
								
								
								fn_MTS_Click_button_any_screen "Search_Service_Order_Request_Screen", "btn_Close"
								
								If gFWbln_ExitIteration = "Y" Then ' write an error in the results
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            							End If

								' Function to write timestamp for next process
								fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								' Function to update the flow number
								fn_update_current_flow_number objWS_DataParameter, intRowNr_CurrentScenario,  intColNr_Flow_CurrentStepNr, int_ScenarioRow_Flow_CurrentStepNr
								
							case "CIS_Check_SOstatus_isRaised"
								If  q(f,0) = cStr_flowFnName_NoneProvidedYet  Then
								End if
								
								' Flow to check service order in CIS  - START 
								
								' Click on Menu File -> Open Explorer
								fn_CIS_MenuNavigation "File;Open Explorer	Ctrl+O"
								
								' Search for a record
								' fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria "61020000040", "NMI Number.", "All Roles","y","n",""
								fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("NMI" )), "NMI Number.", "All Roles","y","n",""
								
								' Select the first open from displayed record
								fnCIS_Select_Record_Search_Results_Post_Legal_Entry_Property_Search "", "first", "", "n",""
								
								' Click on Menu Property -> Service Order
								fn_CIS_MenuNavigation "Property;Service Orders"
							
								' fnCIS_Service_Order_Selection "OF","","","13/03/2017","","","","","","","","y","n","",""
								fnCIS_Service_Order_Selection objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )),"","",fnTimeStamp(objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("ScheduledDate")),"DD/MM/YYYY") ,"","","","","","","","y","n","",""
								
								
								' Click on Order Tab and capture runtime values
								' fn_CIS_Customer_Service_Order_Select_Record_Order_Tab "y", "n", "","y", ""
								' fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Order_Tab  "y", "n", "", "n", "",  ""
								
								
								' Click on Task Tab and capture runtime values
								' fn_CIS_Customer_Service_Order_Select_Record_Task_Tab "y", "first", "n", "n", "y",""
								fn_CIS_Customer_Service_Order_Select_and_Verify_Record_Task_Tab "y", "first", "n", "n", "y", "",   objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("NewSO_Status_InCIS" )), objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )), objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("ScheduledDate")), objWS_DataParameter(dictWSDataParameter_KeyName_ColNr("ScheduledDate"))
							
								fn_CIS_Close_Window "Customer_Service_Order_Screen"
								fn_CIS_Close_Window "search_results"

								If gFWbln_ExitIteration = "Y" Then ' write an error in the results
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            						' BRIAN M - PLEASE UPDATE THE ROW COMPLETION STATUS IN THE DATA SHEET AS THE ERROR IS CAPTURED IN gFWstr_ExitIteration_Error_Reason 
            					End If
            							
								' Function to write timestamp for next process
								fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								' Function to update the flow number
								fn_update_current_flow_number objWS_DataParameter, intRowNr_CurrentScenario,  intColNr_Flow_CurrentStepNr, int_ScenarioRow_Flow_CurrentStepNr
				
								' ===============================================================================   CIS & MTS AUT GUI Functions after this line - END ========

								
'							case "MTS_Check_SOstatus_isRaised"
'								If  q(f,0) = cStr_flowFnName_NoneProvidedYet  Then
'								End if
'							case "CIS_Check_SOstatus_isRaised"
'								If  q(f,0) = cStr_flowFnName_NoneProvidedYet  Then
'								End if
'				
		
			
						'	if rowNr_Current <> rowNr_Prev then intFlowStepNr = 1
						' qq need cols for 
						' - inscope
						' - ExitTestIteration
						' - last step nr executed
						'            strFlowFunctionName = ????    and may need an _01 or _02 suffix if the step is repeated
						'            strFlowFunctionName = ????    and may need an _01 or _02 suffix if the step is repeated
						'            strFlowFunctionName = ????    and may need an _01 or _02 suffix if the step is repeated
								'	qq amend how-F-is-set     if there is a seq-suffix to the strFlowFunctionName
								'	qq handle timeouts
								f = 1 : 	If  q(f,0) = cStr_flowFnName_NoneProvidedYet  Then
										'   do this line here or above, preferably not both	
									sbLoad_FlowFunction_SplitValues_To_Ar ",pNm1,pNm2,,pNm4,pNm5,pNm6,pNm7,pNm8,pNm9,pNm10"	, cstrDelimiter, qNm	, f , intFirst			'	use ~ as the first char of the parm name if you want to track it in the run log
						'			sbLoad_FlowFunction_SplitValues_To_Ar ",pVl1,pVl2,,pVl4,pVl5,pVl6,pVl7,pVl8,pVl9,pVl10,pVl11"			, cstrDelimiter, q		, f , intSecond 		'	triggers err
									sbLoad_FlowFunction_SplitValues_To_Ar ",pVl1,pVl2,,pVl4,pVl5,pVl6,pVl7,pVl8,pVl9,pVl10"			, cstrDelimiter, q		, f , intSecond 	
									if intFirst = intSecond then 
										fParmsCt ( f ) = intFirst 	'					int_thisFlowFunction_nrOfParms = intFirst 
									else        
										intFirst = intFirst
										' qq exit test iteration
									end if
									q(f,0) 		= strFlowFunctionName 
									qNm(f,0) 	= strFlowFunctionName
								End if
							'	load the non-StaticString values to the							 parameterValue array	:	q
								q ( f , 2 ) = 1 
								q ( f , 3 ) = fnTimeStamp(now, "DD/MM/YYYY") 
								sbLogParmsOfInterest  qNm, q , fParmsCt ( f )  
							'	sbLoad_Row_Flow_Function_ParmValue_toDict ( _
							'		dict , q, qf, cStr_Template_dictionaryKey_ScenarioRow_FlowNrName_FlowStepNrName_ParameterNrName_BeforeAfter , intWS_Scenario_RowNr, intFlowStepNr, strFlowFunctionName )
								' qq load the baseline array   (i.e. initial-value array)	qBL  from the  parameterValue array	:	q
								sbLoadParmArray_Row_to_BaseLineArray_Row  q, qBL, f, fParmsCt ( f ) 
								' call the function
								fnFlowStep q(f,2)
								' qq report changes in the paramaterValue array   q   against the 
								
						'            fn_MTS_Create_New_Provide_Meter_Data_Request  _
						'            		Array("y", fnTimeStamp((now - 30), "DD/MM/YYYY"), fnTimeStamp(now, "DD/MM/YYYY"), temp_NMI, strScratch , "Missing", "LNSP", "y", strRunLog_ScreenCapture_fqFileName)
						'
						'	fn_MTS_Create_New_Provide_Meter_Data_Request  _
						'            	"y", fnTimeStamp((now - 30), "DD/MM/YYYY"), fnTimeStamp(now, "DD/MM/YYYY"), temp_NMI, strScratch , "Missing", "LNSP", "y", strRunLog_ScreenCapture_fqFileName
						'		qNm ( 1 ) = split ( strFlowFunctionName & ",parm1Name, parm2Name", cstrDelimiter, cAllElements, vbBinaryCompare)
							'	q(
								
								
						'						fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
						'					Array(gQtTest.Name, gobjNet.Co
							
						
						End Select   '	process the Flow_FunctionSteps for this function
						'
					
					'	Exit do	'	synthetic goto to allow 
					'	loop
						
					Case else
					    gFWbln_ExitIteration = "Y"
					    gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
						Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
						
				End Select  ' end of Select Case str_ScenarioRow_FlowName '	process the flow that this row requests
				
				int_ScenarioRow_Flow_CurrentStepNr = int_ScenarioRow_Flow_CurrentStepNr + 1	'	go to the next stepNr in the flow
				
		'	do all the functions in the current flow				
			Loop while int_ScenarioRow_Flow_CurrentStepNr < intAr_Flow_nrOf_Functions_inThisFlow ( intFlowNr_Current ) 	

			Exit do 	' synthetic goto to drop out of the flow
			loop
			
				
			intFlowNr_Current = intFlowNr_Current + 1
				

		End If 	'	process the row, only if its status is         incomplete
	
	' Before resetting the flags, write the last error in comments column
	If gFWstr_ExitIteration_Error_Reason <> "" Then
		strScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value 
		strScratch = strScratch & " : " & gFWstr_ExitIteration_Error_Reason
		objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value = strScratch 
	End If
	
	' Reset error flags
	fn_reset_error_flags
			
		
	Next	'	For intRowNr_CurrentScenario = int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
	
'	assumption :: the accuracy of this next line depends on the worksheet being updated to reflect changes of status as a result of flow-processing
'	assumption :: the accuracy of this next line depends on the worksheet being updated to reflect changes of status as a result of flow-processing
'	assumption :: the accuracy of this next line depends on the worksheet being updated to reflect changes of status as a result of flow-processing
	int_rgWS_NumberOfIncompleteScenarios = objWS_DataParameter.range("rgWS_NumberOfIncompleteScenarios").value


	bln_All_InScope_ScenarioRows_HaveFinished_Executing = objWS_DataParameter.range("rgWS_NumberOfIncompleteScenarios").value = 0
	If not bln_All_InScope_ScenarioRows_HaveFinished_Executing Then 
	
		'	qq cInt_MinutesRequired_toComplete_eachScenario can be tuned here
		dt_rgWS_RunWillCeaseAt_TimeStamp = _
			now() + ( cDbl_ExtraFactor_ToWait_for_RunToComplete * _
		       	         (( int_rgWS_NumberOfIncompleteScenarios *  cInt_MinutesRequired_toComplete_eachScenario ) / cInt_MinutesPerDay ))
		       	         
		objWS_DataParameter.range("rgWS_RunWillCeaseAt_TimeStamp") = dt_rgWS_RunWillCeaseAt_TimeStamp
		
	'	this code has been reached because one or more InScope ScenarioRows are still Incomplete
	'	however, if the entire-run time-limit has been exceeded, then stop the run, regardless on Incomplete Inscope ScenarioRows
		bln_All_InScope_ScenarioRows_HaveFinished_Executing = (now() > dt_rgWS_RunWillCeaseAt_TimeStamp )

	End if
	
Loop  while not bln_All_InScope_ScenarioRows_HaveFinished_Executing



' Phase III : 

'        							sbScenarioStageRole_OutcomeList_Verify_vn01  _
'								"bcRun" , _
'								objWB_Master.Names("rgWBdyn_Table_ScenarioOutcome_Verify").RefersToRange, _
'									objWS_DataParameter.Cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(strHeader_Role_VerificationList) ).Value, _
'										objWS_DataParameter.Cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CATS_CR") ).Value, _
'										objWB_Master.Names("rgWB_fwStage_MasterTxnList_CATS").RefersToRange.cells(1,1).value, _
'										gstr_CurrentStageName , _
'										gstr_CurrentRole   ,  _
'										objWS_DataParameter.Cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr ( strHeader_Role_RequestID ) ).Value, _	
'											gdict_FwkRun_DataContext_AllScenarios, _
'											cY, _
'												gDict_FieldName_ScenarioOutComeArrayRowNr , _
'												gvntAr_fwScenarioOutcome_Verify_tableAr , _
'												gStrList_Verification_FieldNames ,  _
'												gStrList_VerificationValues_Expected , _
'												gStrList_VerificationValues_Actual 
        



' =================   Cleanup

sbScenarioStageRole_OutcomeList_Verify_vn01  _
	"ccFinalCleanup"    , mt, mt, mt, mt, mt, mt, mt, _
	gdict_FwkRun_DataContext_AllScenarios , mt, gDict_FieldName_ScenarioOutComeArrayRowNr , gvntAr_fwScenarioOutcome_Verify_tableAr, mt, mt,  mt 


' Set objWS_useInEach_ProcessStage = nothing
	gfwobjWS_LogSheet.activate	'	activating the logSheet helps show that the test case is finished
	objWB_Master.save
	objWB_Master.saved = true ' prevent save-dialog appearing on close
set	gfwobjWS_LogSheet = nothing ' qqqqq and the others   e.g. DataParameter, any others ?

' now push the execution iterations and elapsedTimes into the metrics table
' qq this should be inside multi-process, and again at the end for overall stats

iTS_Max = 3 ' Ubound(tsAr_EventEndedAtTS)
For iTS = iTS_Stage_0 To iTS_Stage_III
  cellTopLeft.cells(rowTL    , colTL + iTS).value = tsAr_EventStartedAtTS(iTS)
  cellTopLeft.cells(rowTL + 1, colTL + iTS).value = tsAr_EventEndedAtTS(iTS)
  '        row             +2&3 is the duration, endTS-startTS
  cellTopLeft.cells(rowTL + 4, colTL + iTS).value = tsAr_EventPermutationsCount(iTS)
Next


Set cellTopLeft = nothing

Set gQtResultsOpt = Nothing ' Release the Run Results Options object
set gQtApp   = nothing
Set gQtTest  = nothing

' qq Save the UFT.RunLog to the RunFolder (or just configure the runlog.folder @ startOfScript, to the RunFolder ? - is that possible )

 ExitAction

' The below code is commented by Umesh 

'
'sub sbLoad_Row_Flow_Function_ParmValue_toDict ( piDictionary_DataStore , pioQ, pioQF, piStrTemplate_KeyName, piInt_Scenario_RowNr, piInt_FlowStepNr, piStr_FlowFunctionName, poStr_dictionaryKey )
'
'
''	poStr_dictionaryKey = fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
''	Array(gQtTest.Name, gob
'	
'	
'End sub
'Sub sbLoad_FlowFunction_SplitValues_To_Ar ( piStr_ParmNameList, piStr_Delimiter,  piAr, piInt_FunctionRowNr, poInt_Ubound_of_p1ParmNameList_Array_AfterSplitIsDone )
'	Dim str_splitAr, i, iLimit : i = cint(0)
'	str_splitAr = split ( piStr_ParmNameList, piStr_Delimiter, cAllElements, vbBinaryCompare )
'	iLimit = ubound(str_splitAr) : poInt_Ubound_of_p1ParmNameList_Array_AfterSplitIsDone = iLimit
'	For i = 1 to iLImit 
'		If str_splitAr(i) <> "" Then
'			On error resume next
'			piAr (piInt_FunctionRowNr, i) = str_splitAr(i)
'			If err.number <> 0 Then
'				i = i ' qq handle error
'			End If
'			On error goto 0
'		End If
'	Next
'End Sub
'
'Sub sbLoadParmArray_Row_to_BaseLineArray_Row ( piParmArray, piBaseLineArray, piInt_FunctionRowNr, piInt_NrOfParms )
'	Dim i
'	For i  = 1 To piInt_NrOfParms
'		 piBaseLineArray ( piInt_FunctionRowNr , i) = piParmArray ( piInt_FunctionRowNr , i)
'	Next
'End Sub
'
'ExitTest
'
'Dim dict_Test2
'Set dict_Test2 = nothing
'Set dict_Test2 = CreateObject("Scripting.Dictionary")
'dict_Test2.CompareMode = vbTextCompare
'
'
'Dim intCanThisChange_QQ : intCanThisChange_QQ = cint(0)
'Dim strItemKey : strItemKey = "abc"
'Dim intFinal : intFinal = cInt(-1)
'Dim y
'
'err.clear
'dict_Test2.RemoveAll()
'
'y = 0
'
'fnFlowStep  dict_Test2( y  )
'
'dict_Test2.Add  strItemKey , intCanThisChange_QQ 
'
'fnFlowStep  dict_Test2( strItemKey  )
'
'y = 0
'
'
'
'set x = fnReturnTheItemOfThisKey ( strItemKey ) 
'x = 1
'intFinal = dict_Test2( strItemKey)
'intFinal = intFinal 
'
'dict_Test2.RemoveAll
'Set dict_Test2 = nothing
'
'
'Function fnFlowStep ( byref  pioInt_Var1 )
'	pioInt_Var1 = pioInt_Var1 + 1
'End Function
'
'Function fnReturnTheItemOfThisKey( piStr_Dict_Key )
'	set fnReturnTheItemOfThisKey	=	dict_Test2 ( piStr_Dict_Key )
'End Function
'
'
'
'
'
'
'' =========
'' =========  FrameworkPhase I   - Gather NMI's & other test-data that will serve as inputs to the test =============
'' =========            - Begins :
'' =========
'
'
'strPhase = "I_Prepare"
'
'tsAr_EventStartedAtTS(iTS_Stage_I) = tsAr_EventEndedAtTS(iTS_Stage_0)
'
'cellReportStatus_FwkProcessStage.formula = "'" & "=========  Phase I   - Gather NMI's & other test-data that will serve as inputs to the test"
'cellReportStatus_ROLE.formula = "'"
'cellReportStatus_SingleScenario.formula = "'"
'
'
'Dim singleProcess_InScope_strAr
'Dim rangeProcessControlTable,rangeMultiProcess_ControlTable_Ar 
'' rangeProcessControlTable = objWS_DataParameter.names("rgWS_MultiProcess_ControlTable").refersToRange ' same as line below
'rangeMultiProcess_ControlTable_Ar =objWS_DataParameter.range("rgWS_MultiProcess_ControlTable")
'const cRowNr_RowsThatRun_thisSingleProcess_Count = 2
'const cRowNr_SingleProcess_Name = 3
'
'
'strAr_ProcessStage = strAr_listProcessStages_Relevant
'mMax = UBound(rangeMultiProcess_ControlTable_Ar , 2)
'intArSz_ProcessStage = mMax
'
'
''   qq load the dictionary from the rgWS_ColumName_Row
'    intColNr_RowType = dictWSDataParameter_KeyName_ColNr.Item("RowType")
'' qq - FrameWork.Rule - retain this single evaluation here so that work can be done concurrently by different test-engineers
'
'
'
'
'
'
'
'' set the state to run the SingleScenarios over, based on the company being tested
'  Select Case ucase(Parameter.Item("Company"))
'    Case "CITI", "PCOR"
'      strScratch = "VIC"
'    Case "SAPN"
'      strScratch = "SA"
'    Case else
'    
'	' StringBuilder function replaced with fn_StringFormatReplacement function
'      ' strScratch =  hFWobj_SB.AppendFormat ("Case `COMPANY` parm was passed, but value was invalid (`{0}`), i.e. not one of {CITI`POWERCOR`SAP}.", Parameter.Item("Company") ).ToString
'      
'      strScratch =  fn_StringFormatReplacement("Case `COMPANY` parm was passed, but value was invalid (`{0}`), i.e. not one of {CITI`POWERCOR`SAP}.", array(Parameter.Item("Company")))
'      
'      
'      reporter.ReportEvent micFail, strScratch, ""
'      ExitAction ' exit the test case
'  End Select
'  Dim tgtCol_State : tgtCol_State = dictWSDataParameter_KeyName_ColNr("list_tState")
'  
'  
'objXLapp.Calculation = xlManual
'objXLapp.screenUpdating=False
'
'  For intScratch = intRowNr_LoopWS_RowNr_StartOn to intRowNr_LoopWS_RowNr_FinishOn
'    ' If objWS_DataParameter.cells( intScratch, intColNr_RowType).value = "rtSingleScenario" Then
'    If objWS_DataParameter.cells( intScratch, intColNr_RowType).value = "rtSingleScenario" Then
'      objWS_DataParameter.cells( intScratch,tgtCol_State ).formula = strScratch
'    End If
'  Next
'
'
'' qq select case strMachineName    set     intColNr_InScope
'' qq select case strMachineName    set     intColNr_InScope
'' qq select case strMachineName    set     intColNr_InScope
'
'
'    Dim vntAr_RowTypes, vntAR_ColumnTypes
'
''   Create new SingleScenario rows from the DataTemplate rows
'
'
'
''   <<==  Remove all rtDataParameter Rows
''         Remove all rtDataParameter Rows
''         Remove all rtDataParameter Rows
''         Remove all rtDataParameter Rows
' '            sb_rtTemplate_RemoveDataParm_Rows_WS objWS_DataParameter, dictWSDataParameter_KeyName_ColNr
'
''   Create new SingleScenario rows from the DataTemplate rows
'
'    Dim intArSz_Role, intArSz_State, intArSz_NorC, intArSz_sizeNMI
'    Dim iRole, iState, iNorC, isizeNMI
'    Dim colNr_Role, colNr_State, colNr_NorC, colNr_sizeNMI
'    Dim strAr_acrossA_Role, strAr_downA_State, strAr_downB_NorC, strAr_downC_sizeNMI
'    Dim strRow_SingleScenario_Template, strRow_SingleScenario: strRow_SingleScenario_Template = "tr<TemplateRow>rl<RoleNr>st<State>nc<NorC>nmi<NMI>"
'    Dim objCell_NewRowCell
'    Dim strRole_NorC_Negative, cStr_OmitThisCombination: cStr_OmitThisCombination = "-"
'    Dim intNrOfNewRows: intNrOfNewRows = 0
'    Dim intDisplayChanges_Interval: intDisplayChanges_Interval = 0
'    Dim intColNr_SQL_DataParameter, strMasterWS_ColumnName
'    Dim intColNr_XML_DataTemplate_FileNameQQ
'    Dim strXML_TemplateName_Current, strXML_TemplateName_Previous
'    Dim intColNr_NMI, intColNr_TestResult
'    Dim strSuffix_Expected, strSuffix_Actual, strSuffix_NMI, strPrefix_RoleIsRelevant, str_Role_Execution_result
'    Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true
'    Dim qintColNr, qintColsCount, strSqlCols_NamesList, strSqlCols_ValuesList, strSql_Suffix, qintFieldFirst_Nr : qintFieldFirst_Nr = 0
'    Dim strAr_SqlCol_NamesAr, strAr_SqlCol_ValuesAr, dictSqlData_forThisRow_ColValues
'    Dim colNr_SqlCol_Names, colNr_SqlCol_Values, strPrefixRequestID
'    Dim strInScope_SmallLarge_Ar, intCt_AdditionalCriteria_All, cellNMI, cellNMI_data, strCellFormula, cellExpectedValue, cellActualValue
'    Dim intNMIdata_ColNr_Start, intNMIdata_ColNr_End, intColNr_NMIdata
'    Dim cellTarget
'
'    strSuffix_Expected 			= objWS_DataParameter.range("rgWS_Suffix_Actual").value
'    strSuffix_Actual   			= objWS_DataParameter.range("rgWS_Suffix_Expected").value
'    ' strSuffix_NMI      			= objWS_DataParameter.range("rgWS_nameNMI").Value ' As the NMI column has chaned hence we need oo refer to new column
'    strSuffix_NMI      			= objWS_DataParameter.range("rgWs_prefixNMI").Value
'    strPrefixRequestID      		= objWS_DataParameter.range("rgWs_prefixRequestID").Value
'    strPrefix_RoleIsRelevant 	= objWS_DataParameter.range("rgWS_RoleIsRelevant_Prefix").Value
'    str_Role_Execution_result	= objWS_DataParameter.range("rgWS_Role_ExpectedResult").Value 
'    
'' this column contains the name of the SQL to be used for this group of Cats_CR's
'  intColNr_SQL_DataParameter = dictWSDataParameter_KeyName_ColNr("SQL_Template_Name")
'
''   the columns that the DataParms appear in, are constant, for    State, NorC and sizeNMI
'    colNr_State   = dictWSDataParameter_KeyName_ColNr("list_tState")
'  colNr_NorC    = dictWSDataParameter_KeyName_ColNr("this_tNorC")
'    colNr_sizeNMI = dictWSDataParameter_KeyName_ColNr("list_tNMI")
'
'    colNr_SqlCol_Names  = dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")
'    colNr_SqlCol_Values = dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")
'    
'	intNMIdata_ColNr_Start = dictWSDataParameter_KeyName_ColNr( "FRMP_NMIdata") ' qq - refine this so that it is dynamic rather than hard-coded
'	intNMIdata_ColNr_End   = dictWSDataParameter_KeyName_ColNr( "LNSP_NMIdata")
'
''   the DataParms for Role are constant for the run
'	' Incase 
'	If objWS_DataParameter.Range("rgWsConfig_RoleMode_is_Global_or_ScenarioSpecific").value = "Global" Then
'    	strAr_acrossA_Role = Split(fnAttachStr(objWS_DataParameter.Range("rgWS_Role_List").value, ",", "L", True), ","): intArSz_Role = UBound(strAr_acrossA_Role)
'    ElseIf objWS_DataParameter.Range("rgWsConfig_RoleMode_is_Global_or_ScenarioSpecific").value = "ScenarioSpecific" Then
'      '                                                     **    RP becomes MC after MeterContestability
'      ' strAr_acrossA_Role  = Split((",FRMP,LR,MDP,MPB,RoLR,RP,LNSP"),","): intArSz_Role = UBound(strAr_acrossA_Role)
'	strAr_acrossA_Role  = Split((",FRMP,LR,MDP,MPB,RoLR,MC,LNSP"),",") ' qq - this hardcoding has to be removed
'	intArSz_Role = UBound(strAr_acrossA_Role)
'    End if
'
'' Retrieve the ColNr for the SQL_Template names
'    intColNr_SQL_DataParameter = dictWSDataParameter_KeyName_ColNr("SQL_Template_Name")
'    strSQL_TemplateName_Previous = ""
'
'' Retrieve the ColNr for the XML_Template names
'  intColNr_XML_DataTemplate_FileNameQQ = dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")
'
'
'  strAr_downC_sizeNMI = Split(fnAttachStr(objWS_DataParameter.Range("rgWS_listOf_sizeOf_NMI").value, ",", "L", True), ","): intArSz_sizeNMI = UBound(strAr_downC_sizeNMI)
'  For isizeNMI = 1 To intArSz_sizeNMI
'  
'
'    strSQL_TemplateName_Previous = "<none>"
'    strSQL_TemplateName_Current = ""
'
'    ' iterate over all the SingleScenario Rows
'      intRowNr_CurrentScenario          = intRowNr_LoopWS_RowNr_StartOn
'      Do
'      
'		str_rowScenarioID = objWS_DataParameter.Cells(intRowNr_CurrentScenario, intColNr_ScenarioID ).value
'
'        cellReportStatus_SingleScenario.formula = "'" & intRowNr_CurrentScenario
'        fnLog_Iteration  cFW_Exactly, intRowNr_CurrentScenario ' cFW_Plus cFW_Minus 
'
'      ' InScope Row
'          If ( (UCase(objWS_DataParameter.Cells(intRowNr_CurrentScenario, intColNr_InScope).Value) = "Y") and _
'               (objWS_DataParameter.Cells(intRowNr_CurrentScenario, intColNr_InScope).EntireRow.Height <> 0 ) ) _
'          Then ' InScope       and RowIsNot_Hidden_ieHeightIsZero       ' qq remove the row.Hidden part ?  or retain for flexibility
'
'             ' objWS_DataParameter.Rows(intRowNr_CurrentScenario).Select
'
'
'              If objWS_DataParameter.Cells(intRowNr_CurrentScenario, intColNr_RowType).Value = cStintRowNr_CurrentScenario Then
'
'              	objWS_DataParameter.Cells(intRowNr_CurrentScenario, intColNr_InScope).Interior.Color = objWS_DataParameter.range("rgWS_status_CaseStarted").Interior.Color
'
'        ' if the SizeOfNMIs the current query is returning is the same as this row ...
'          If strAr_downC_sizeNMI(iSizeNMI) = objWS_DataParameter.Cells( intRowNr_CurrentScenario, colNr_sizeNMI ).value Then
'          
'          
'          
'          
'          ' =======================  code below is to get the Phase_II code above to match
'          End if
'          
'          End if
'          End if
'        Loop until 1 = 1
'        
'        
'        
'        next

' Once the test is complete, move the results to network drive
copyfolder  gFWstr_RunFolder , Cstr_B2BSO_Final_Result_Location




'
'
'
'
'
'
'
'
'
'
'
'
'
'
'
'