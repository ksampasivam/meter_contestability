﻿						
											
						Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong
						
						Dim intScratch
						gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function
						gstr_Field_Seperator = "^"
						' Dim bln_Scratch :  bln_Scratch = cbool(true)
						
						dim intColNr_InScope, intColNr_RowCompletionStatus
						intColNr_InScope = cint(-1) : intColNr_RowCompletionStatus = cint(-1)
						
						Dim str_rowScenarioID, intColNR_ScenarioID
						
						Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true
						Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
						Dim gFWcell_xferNMI 
						
						BASE_XML_Template_DIR = cBASE_XML_Template_DIR
						
						Dim strFolderNameWithSlashSuffix_Scratch
						' strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"
						
						Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq
						
						gDt_Run_TS = tsOfRun
						
						Dim BASE_GIT_DIR ' : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"
						BASE_GIT_DIR =   cBASE_GIT_DIR
						
						Dim strCaseBaseAutomationDir 
						strCaseBaseAutomationDir  = BASE_GIT_DIR
						
						
						' ####################################################################################  UFT configurations at the start of test  - START ####################################################################################  
						
						Dim Temp_Execution_Results_Location  : Temp_Execution_Results_Location = cTemp_Execution_Results_Location
						Dim strRunLog_Folder
						Dim str_CorrelationId
						dim qtTest, qtResultsOpt, qtApp
						
						
						Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
						If qtApp.launched <> True then
						    qtApp.Launch
						End If
						
						qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
						' qtApp.Options.Run.RunMode = "Fast"
						qtApp.Options.Run.ViewResults = False
						
						
						
						Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
						Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539
						
						
						Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
						str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
						If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
							str_AnFw_FocusArea = ""
						else
							str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
						End If
						
						strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", array(fnAttachStr(Temp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
						    gQtTest.Name                                 , _
						    Parameter.Item("Environment")                       , _
						    Parameter.Item("Company")                           , _
						    fnTimeStamp(gDt_Run_TS, "YYYY`MM`DD_HH`mm`ss")))
						    ' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))
						
						
						
						
						Path_MultiLevel_CreateComplete strRunLog_Folder
						gFWstr_RunFolder = strRunLog_Folder
						
						qtResultsOpt.ResultsLocation = gFWstr_RunFolder
						gQtResultsOpt.ResultsLocation = strRunLog_Folder
						
						
						qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
						qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
						qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
						qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
						qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
						qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
						qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
						qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
						qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
						qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
						qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
						qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
						qtApp.Options.Run.ScreenRecorder.RecordSound = False
						qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True
						
						'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
						gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
						'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
						
						gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
						' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
						gQtApp.Options.Run.ViewResults                = False
						
						strFolderNameWithSlashSuffix_Scratch = strRunLog_Folder
						
						
						' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  
						
						' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  
						
						initialize_scratch_Dictionary
						
						'Set gdict_scratch = CreateObject("Scripting.Dictionary")
						'gdict_scratch.CompareMode = vbTextCompare
						'
						clear_scratch_Dictionary gdict_scratch
						
						' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  
						
						
						
						'  Description:-  Function to replace arguments in a string format
						'   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is 
						
						'						The template should have arguments within {} and the numbering should s
						
						If 1 = 1  Then		
										
										
										Environment.Value("COMPANY")=Parameter.Item("Company")
										Environment.Value("ENV")=Parameter.Item("Environment")
										    
										' Load the MasterWorkBook objWB_Master
										Dim strWB_noFolderContext_onlyFileName  
										strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
										
										Dim wbScratch
										Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
										dim  dictWSDataParameter_KeyName_ColNr
										
										fnExcel_CreateAppInstance  objXLapp, true
										
										' Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO"
										' LoadData_RunContext_V2  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
										' LoadData_RunContext_V3 objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
										LoadData_RunContext objXLapp, wbScratch, 0, Parameter.Item("Worksheet_Name"), strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, 0, tsOfRun, strRunLog_Folder
						
										Dim	strEnvCoy
										strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")
						
										' ####################################################################################################################################################################################
										' #####################################################                 LOGGING STEP 1 - SETUP      #################################################################################################
										' ####################################################################################################################################################################################
										fn_setup_Test_Execution_Log Parameter.Item("Company"), Parameter.Item("Environment"), "N", "CDN", GstrRunFileName, "CDN", strRunLog_Folder, gObjNet.ComputerName
										Printmessage "i", "Start of Execution", "Starting execution of CDN scripts"
										
										Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
										Set wbScratch = nothing
													
										objXLapp.Calculation = xlManual
										'objXLapp.screenUpdating=False
										
										Dim objWS_DataParameter 						 
										set objWS_DataParameter           					= objWB_Master.worksheets(Parameter.Item("Worksheet_Name"))
										objWS_DataParameter.activate
										
										Dim objWS_templateXML_TagDataSrc_Tables 	
										set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
										gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
						
										Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
										
									'	Get headers of table in a dictionary
										Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
										gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
										
										objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
										objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
										objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
										objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
										objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
										objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = now 
						
										gDt_Run_TS = now
										gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 
						
										Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE
										
										'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
										Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
										dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
										Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
										intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
										intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
										intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
										fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
										
										' =========
										' =========  FrameworkPhase 0b   - Technical Setup for Running the SingleScenario Rows   =============
										' =========
										
										Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
										
										dim r
										Dim objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, objDBConn_TestData_A, objDBRcdSet_TestData_A
										Dim objDBConn_TestData_CIS, objDBRcdSet_TestData_CIS
										
						'					On error resume next
							'	add more of these as-required, for querying databases
							'	add more of these as-required, for querying databases
										Set objDBConn_TestData_IEE    = CreateObject("ADODB.Connection")
										r = 0
										set objDBRcdSet_TestData_IEE  = CreateObject("ADODB.Recordset")
							'	add more of these as-required, for querying databases
						
										Set objDBConn_TestData_CIS    = CreateObject("ADODB.Connection")
										r = 0
										set objDBRcdSet_TestData_CIS  = CreateObject("ADODB.Recordset")
						
						
										Dim objDBConn_TestData_MTS, objDBRcdSet_TestData_MTS
										Set objDBConn_TestData_MTS    = CreateObject("ADODB.Connection") : set objDBRcdSet_TestData_MTS  = CreateObject("ADODB.Recordset")
						
								Dim	DB_CONNECT_STR_MTS
										DB_CONNECT_STR_MTS = _
												"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
											Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
											Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
											Environment.Value("USERNAME") 	& 	";Password=" & _
											Environment.Value("PASSWORD") 	& ";"
						
								Dim	DB_CONNECT_STR_IEE
										DB_CONNECT_STR_IEE = _
												"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
											Environment.Value(strEnvCoy & 	"_IEE_HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
											Environment.Value(strEnvCoy & 	"_IEE_SERVICENAME") 	&	"))); User ID=" & _
											Environment.Value(strEnvCoy & 	"_IEE_USERNAME") 	& 	";Password=" & _
											Environment.Value(strEnvCoy & 	"_IEE_PASSWORD") 	& ";"
						
								Dim	DB_CONNECT_STR_CIS
										DB_CONNECT_STR_CIS = _
												"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
											Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
											Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
											Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
											Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"
						'
						'					Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL") & "SORD\"
						'					
										Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
										Dim strQueryTemplate, dtD, Hyph, strNMI
										
										Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
										Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
										' intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
										
										Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
										
										Dim intSQL_Pkg_ColNr, strSQL_RunPackage
										
										
										Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
										
										
										If Parameter.Item("RowNr_StartOn") = 0 and Parameter.Item("RowNr_FinishOn") = 0 Then
											intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
											intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
										Else
											intRowNr_LoopWS_RowNr_StartOn 	= Parameter.Item("RowNr_StartOn")
											intRowNr_LoopWS_RowNr_FinishOn	= Parameter.Item("RowNr_FinishOn")
										End If
										
											'intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
											'intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
											
											intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
										
													
												'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
											intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
											sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
										
											Dim strColName
											objWS_DataParameter.calculate	'	to ensure the filename is updated
														
														
														
										sb_File_WriteContents  _
											gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
											fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
											Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, "", objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
										
									    Dim intColNr_ScenarioStatus, intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
											  
											  intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
											  intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")
										
											Dim blnColumnSet : blnColumnSet = cbool(true)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
											If IsEmpty(intColNr_InScope) then	
												blnColumnSet =false
											ElseIf intColNr_InScope = -1 then
												blnColumnSet =false
											End if
											If not blnColumnSet then 
										'		qq log this	
										'		objXLapp.Calculation = xlManual
												objXLapp.screenUpdating=True
												exittest
											End If
										
											objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
											objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
											objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
											
											Printmessage "i", "Start row number", "Start row for the test is " & intRowNr_LoopWS_RowNr_StartOn
											Printmessage "i", "End row number", "End row for the test is " & intRowNr_LoopWS_RowNr_FinishOn
											
											objWS_DataParameter.calculate
											objxlapp.screenupdating = true
										
											' objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
										
										' =========
										' =========  FrameworkPhase 0c   - Setup the MultiStage Array                                           =============
										' =========
										
										Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
										Dim  intMasterZoomLevel 
										
										Dim rowStart, rowEnd, colStart, colEnd
										Dim RangeAr 
										
										
										'    ===>>>   Enable HotKeys
										' NOT NEEDED objWB_Master.Application.Run "HotKeys_Set"
										
										'    ===>>>   Make the RunBook Un-Shared, so that all later processes can run, esp the next, Expand, process
										' NOT NEEDED objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
										
										
						End if
						
									objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = Parameter("InScope_Column_Name")
										
						dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
						Dim temp_all_Error_Messages : temp_all_Error_Messages = ""
						Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = cint(0)	: intFirst= cint(0)	: intSecond = cint(0)
						Dim z : z = "z"
						Dim intWS_RowNr : intWS_Scenario_RowNr = cint(1)	' qq
						
						
						sb_File_WriteContents   	_
						str_Fq_LogFileName, gfsoForAppending , true, _
						fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
						Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf
						
						
						
						'	determine the number of in-scope rows
						Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun
						
						tsStartOfRun = now()
						
						
						'	qq     intNrOfRows_Inscope_and_Unfinished
						
						sb_File_WriteContents   	_
						str_Fq_LogFileName, gfsoForAppending , false, _
						fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
						Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf
						
						
						
						dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow
						
						'	now setup to do process the DriverSheet
						
						
						Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
						Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
						Const cIncomplete = "Incomplete"
						Dim intRowNr_Scenario : intRowNr_CurrentScenario = cInt(0) ' formerly cLng
						
						Dim str_ScenarioRow_FlowName 
						Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
						Dim  intFlowNr_Current 						:	intFlowNr_Current 						= cInt(0)
						Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= cInt(-1)
						
						Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = false
						
						Dim intColNr_Request_ID 
						intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
						intColNr_ScenarioStatus 			= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")
						Dim strRole, strInitiator_Role, var_ScheduledDate
						
						Dim strInScope, strRowCompletionStatus
						
						objXLapp.Calculation = xlAutomatic
						objXLapp.screenUpdating=true
						objWB_Master.save
						
						
						
						gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
						
							' Get headers of table in a dictionary
						Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
						g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
						
						
						
							' Load the first/title row in a dictionary
						int_MaxColumns = UBound(gvntAr_RuleTable,2)
						
						'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
						fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue
						
						
						
						' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch applications at startup - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
						'Close MTS
						'fnMTS_WinClose
						'Launch MTS
						'OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")
						'
						initialize_performance_Dictionary
						
						' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
						''	
						' Reset error flags
						fn_reset_error_flags
						Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
						Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
						Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID
						' Dim str_Company_Name, ctr_Stage, tempInt_RoleIdentificationCounter, Str_Role
						'																	
						Dim temp_XML_Tag_MeterDetails, temp_XML_Tag_MeterDetails_runtimeUpdated, temp_XML_Tag_MeterDetails_FinalTag, temp_XML_Tag_CSVIntervalData
						Dim temp_XML_Tag_RegisterDetails, temp_XML_Tag_RegisterDetails_runtimeUpdated, temp_XML_Tag_RegisterDetails_FinalTag																	
						Dim str_Company_Name, ctr_Stage, tempInt_RoleIdentificationCounter, Str_Role, temp_str_aqs, temp_bln_scratch
						Dim temp_no_of_times, int_Ctr_No_Of_Times

						
						int_ctr_total_Eligible_rows = 0
						int_ctr_no_of_passed_rows = 0
						temp_bln_scratch = false
						int_Ctr_No_Of_Times = 0 
						
						i = 1
						
						Dim strAr_acrossA_Role, strAr_Stages
						Dim intArSz_Role, intArSz_Stages, strPrefix_RoleIsRelevant
						Dim strCaseGroup, strCaseNr, strXML_TemplateName_Current, Date_TransactionDate
						Dim str_DataParameter_PopulatedXML_FqFileName, str_MTS_Txn_Status
						Dim strVerification_String, temp_range_rg_VerificationString_Suffix, varAr_Verification_String
						Dim strSQL_MTS,strSQL_CIS, strSQL_IEE, temp_XML_Tag_CSVIntervalData_FinalTag, temp_XML_Tag_ConcatenatedRegisters
						Dim temp_strSqlCols_NamesList, temp_strSqlCols_ValuesList, temp_strSql_Suffix, temp_IEE_MeterNumber, temp_IEE_Meter_Counter, temp_IEE_Register_Counter, temp_No_of_Required_Meters
						'
						Dim IntRowNr_DataPermutation, Int_CurrentRecordNumber
						
						
						' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						
						Do
						
						For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
						
														'	intRowNr_LoopWS_RowNr_StartOn   qq
						
							'	if the scenarioRow is InScope-and-Incomplete                                                                                                                 
							strInScope 							= 	ucase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
							str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
							str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
							temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value
							temp_no_of_times			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NO_OF_TIMES") ).value
							
							If temp_no_of_times = "" Then
								temp_no_of_times = 1
							End If
							For int_Ctr_No_Of_Times = 1 To temp_no_of_times
						
							' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
							
								If   ucase(strInScope) = ucase(cY)  and str_ScenarioRow_FlowName <> "" and lcase(str_Next_Function) <> "fail" and lcase(str_Next_Function) <> "end" Then
						
									If i = 1 Then
										If ucase(Parameter.Item("RunMode")) = "N" Then ' Value can be N- New or R - restart
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start-SQL"
											str_Next_Function = "start-SQL"
										End If
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
										int_ctr_total_Eligible_rows = int_ctr_total_Eligible_rows + 1
										objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
									End If
								
									' Generate a filename for screenshot file
									gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, left(temp_Scenario_ID,10)))
						
									' ####################################################################################################################################################################################
									' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
									' ####################################################################################################################################################################################
									fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", "", "", "", "", "", ""
									
									' We need to clear NMI and SQL columns for performance runs
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = ""
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )) = ""
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )) = ""
									
									
								Select Case lcase(str_ScenarioRow_FlowName)
								
									' ################################################################# START of first flow ####################################################################################################
									' ################################################################# START of first flow ####################################################################################################
						Case "flow1", "flow2", "flow3", "flow4":
								Do
									str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
										Select Case str_Next_Function
											Case "start-SQL" ' this is datamine
												' First function
												tsNow = now() 
												tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
												IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
													Exit do
												End if
												
											               
										' Now run the MTS Query to fetch the NMI and write in NMI column
										strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_Template_Name")).value 
		
										
										If trim(strSQL_MTS) <> ""  Then
											
											PrintMessage "p", "MTS SQL template","Scenario row("& intRowNr_CurrentScenario &") - MTS SQL Template ("& strSQL_MTS &") "
										    strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
										        
									        If lcase(strQueryTemplate) <> "fail" Then
												strSQL_MTS = strQueryTemplate
													
									            	' Append the query in SQL Populated Coulmn
									            	fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "MTS_SQL_Template_Name", strSQL_MTS 
									            	
													' Execute MTS SQL
													strScratch = Execute_SQL_Populate_DataSheet_Hatseperator (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_MTS, objDBRcdSet_TestData_MTS, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
											
                                                  	If strScratch = "PASS" Then										
											            ' Write the data in all respective columns for all roles                			                                               
														' Reserve NMI
														str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
															
														' ####################################################################################################################################################################################
														' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
														' ####################################################################################################################################################################################
														fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", str_NMI, "", "", "", "", ""
														PrintMessage "p", "Reserve NMI - CANDIDATE","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& str_NMI &") in KDR as candidate"															
														fnKDR_Insert_Data_V1 "n", "n",str_NMI, temp_Scenario_ID, "candidate"
													End If 'strScratch = "PASS" Then
													
											End If 'end of If lcase(strQueryTemplate) <> "fail" Then
										End If 'end of If trim(strSQL_MTS) <> ""  Then
										
													' At the end, check if any error was there and write in corresponding column
													If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
														PrintMessage "f", "Problem with SQL, moving to the next row", "Query did not return any data"
														' create a function to dump error in a file
														strScratch = gFWstr_RunFolder & "Error_Dump_" & fnTimeStamp(now, "YYYY`MM`DD_HH`mm`ss") & ".txt"
														sb_File_WriteContents  strScratch, gfsoForAppending, true, gFWstr_ExitIteration_Error_Reason
														PrintMessage "f", "MILESTONE - Problem with SQL, moving to the next row", "Error with SQL logged in file ("& strScratch &")"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
														int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
														Exit do
													Else
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML_Creation"
														PrintMessage "p", "MILESTONE - Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
														fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
					            					End If
					            					'End if 
					            					
													strSQL_MTS = ""
													
													
											Case "XML_Creation"
											
															tsNow = now() 
															tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
															IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
																Exit do
															End if
															
															
															temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
												
															If temp_XML_Template  <> "" Then
												
																PrintMessage "p", "Starting XML creation","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation with template ("& temp_XML_Template &")"
													
																int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
																objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")).value = "CDN_" & int_UniqueReferenceNumberforXML
																fn_append_To_SQLAttributes_And_SQLValues objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "XML_RUNTIME_REQUEST_ID", "CDN_" & int_UniqueReferenceNumberforXML, "^"
																
																temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
																
																temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("To_Role")).value ' To Role for gateway
																
																str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
																
																str_ChangeStatusCode = "CDN"		
																
																
																' ####################################################################################################################################################################################
																' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
																' ####################################################################################################################################################################################
																fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", str_NMI,  int_UniqueReferenceNumberforXML,  temp_Role, str_ChangeStatusCode, "", ""
																PrintMessage "p", "Starting XML creation","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for ("& str_ChangeStatusCode &") - NMI ("& int_NMI &") - Role ("& temp_Role &")"
																
																str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
																temp_XML_Template, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
																intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now,  int_UniqueReferenceNumberforXML, _
																temp_Role, str_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
																
																' If there were no errors during XML creation, drop in gateway else exit the current row
													            If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
													            	PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
													            Else
																
																	strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")	
																	PrintMessage "p", "XML File","Scenario row("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
																
														            ' ZIP XML File
														            ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
														            PrintMessage "p", "Zippng XML File","Scenario row("& intRowNr_CurrentScenario &") - Zipping XML file for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &")"
														                    
														            GetDestinationFolder Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B",  temp_Role
								
																	If Environment.Value("CATSDestinationFolder") = "<notfound>" or Environment.Value("CATSDestinationFolder") = "" Then
																		PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																		gFWbln_ExitIteration = "y"
																	      gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																	Else
																		' copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder")
																		store_value_performance_Dictionary gdict_XMLFilename_DestinationFolder, Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", Environment.Value("CATSDestinationFolder")
																		PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& Str_Role &") - Application (CDN - same location as CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																		objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder" )) = Environment.Value("CATSDestinationFolder") 
																	End If
													          	  End If
																				            
																Else
																	PrintMessage "i", "Skipping XML creation", "Skipping XML creation as no template specified in xmlTemplate_Name column, This is a mandatory step hence please correct the config."
																End If ' end of If temp_XML_Template  <> "" Then
						
																' At the end, check if any error was there and write in corresponding column
																If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
																	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
																	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
																	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
																	PrintMessage "f", "MILESTONE - XML creation failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
																	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
																	int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
																	Exit do
																Else
																
																	If int_Ctr_No_Of_Times = temp_no_of_times  Then
																		objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
																		objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
																		int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
																		int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
																	Else
																		objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "start-SQL"
																		objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
																		int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
																		int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
																	End If
																	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation complete"
																	PrintMessage "p", "MILESTONE - XML creation complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
																	' Function to write timestamp for next process
																	fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
																	Exit Do
																	
																	
'																	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
'																	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
'																	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CDN_BUSINESS_ACK_CHECK"
'																	 PrintMessage "p", "MILESTONE - XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
'																	' Function to write timestamp for next process
'																	fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_TwoMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								            					End If
						
															case "FAIL"
																Exit Do
															case else ' incase there is no function (which would NEVER happen) name
																Exit Do
													End Select
										Loop While  (lcase(gFWbln_ExitIteration) = "n" or gFWbln_ExitIteration = false)
						
									' ################################################################# END of first flow ####################################################################################################
									' ################################################################# END of first flow ####################################################################################################
						
						
									Case else ' Incase there is no elaboration for a flow, we need to report in datasheet and move on to next row
									    gFWbln_ExitIteration = "Y"
									    gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
										PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = gFWstr_ExitIteration_Error_Reason
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
								End Select ' end of 	Select Case str_ScenarioRow_FlowName
							
							End If ' end of If  (	( strInScope = cY)  and str_ScenarioRow_FlowName <> "" and (trim(str_Next_Function) = "" or lcase(str_Next_Function) <> "fail" or lcase(str_Next_Function) <> "end") ) Then
						
							' ####################################################################################################################################################################################
							' #####################################################                 LOGGING STEP 3 - Clear data before moving to next row #########################################################################
							' ####################################################################################################################################################################################
							fnTestExeLog_ClearRuntimeValues
							
							' Reset error flags
							fn_reset_error_flags
							
							' clear scratch dictionary
							clear_scratch_Dictionary gdict_scratch
							
							Next 'end of For int_Ctr_No_Of_Times = 1 To temp_no_of_times
						
						Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
						
						' objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
						objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
						
						' Reset error flags
						fn_reset_error_flags
						
						i = 2
						objWB_Master.save ' Save the workbook 
						Loop While int_ctr_no_of_Eligible_rows > 0
						
						' Once all the XML's are created, now we need to copy them to gateway in blocks..
						Performance_Copy_XML_Files_In_Batches gFWstr_RunFolder, 10, 20


						PrintMessage "i", "MILESTONE - Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & ") - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
						objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
						objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
						objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= now
						
						objWB_Master.save ' Save the workbook 
						Printmessage "i", "MILESTONE - End of Execution", "Finished execution of CDN"
						
						' Close MTS
						'fnMTS_WinClose
						
						' Once the test is complete, move the results to network drive
						Printmessage "i", "MILESTONE - Copy to network drive", "Copying results to network drive ("& Cstr_CDN_Final_Result_Location &")"
						copyfolder  gFWstr_RunFolder , Cstr_CDN_Final_Result_Location
 @@ hightlight id_;_264390_;_script infofile_;_ZIP::ssf95.xml_;_