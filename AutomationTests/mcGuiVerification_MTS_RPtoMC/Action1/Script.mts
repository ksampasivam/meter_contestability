﻿' Script used to verify on MTS GUI all changes for RP to MC change

Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong


Set hFWobj_SB = DotNetFactory.CreateInstance( "System.Text.StringBuilder" ) 
Set gFWobj_SystemDate = DotNetFactory.CreateInstance( "System.DateTime" )
' hFWobj_SB.AppendFormat "My name is {0} {1}, I own a {2} and {3} name is {4}", fName, lName,  "dog", "his", "Star"
' Print gFWobj_SB.ToString


' push the RunStats to the DriverWS report-area
Dim cellTopLeft, iTS, iTS_Max, rowTL, colTL

Dim strRunLog_Folder

' -----

Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq



dim qtTest, qtResultsOpt, qtApp
Dim i_timetowait : i_timetowait = 300

Set qtApp = CreateObject("QuickTest.Application")	' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
If qtApp.launched <> True then
	qtApp.Launch
End If

qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
qtApp.Options.Run.RunMode = "Fast"
qtApp.Options.Run.ViewResults = False


Set qtTest = qtApp.Test											' http://www.geekinterview.com/question_details/64501
Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions") 	' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539

Dim strRunLog_ScreenCapture_fqFileName




gDt_Run_TS = tsOfRun
Dim scratch
scratch = fnAttachStr(BASE_AUTOMATION_DIR,"\","R", true)

'strRunLog_Folder = hFWobj_SB.AppendFormat ("{0}DataSheets\runs\{1}_{2}_{3}_{4}\"					, _
'		fnAttachStr(BASE_AUTOMATION_DIR,"\","R", gcLibBln_OnlyIfNotAlreadyThere)	, _
'		qtTest.Name																	, _
'		Parameter.Item("Environment")												, _
	'	Parameter.Item("Company") 													, _
	'	fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`ddDdD_HH`mm")	).toString

strRunLog_Folder = hFWobj_SB.AppendFormat ("{0}{1}_{2}_{3}_{4}\"					, _
		fnAttachStr(cTemp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)	, _
		qtTest.Name																	, _
		Parameter.Item("Environment")												, _
		Parameter.Item("Company") 													, _
        fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`ddDdD_HH`mm")	).toString

Path_MultiLevel_CreateComplete strRunLog_Folder 
gFWstr_RunFolder = strRunLog_Folder 

' ----------------------------------

qtResultsOpt.ResultsLocation = strRunLog_Folder

'										
'										qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
'										qtApp.Options.Run.MovieCaptureForTestResults = "Never"
'										qtApp.Options.Run.MovieSegmentSize = 2048
'										qtApp.Options.Run.RunMode = "Fast"
'										qtApp.Options.Run.SaveMovieOfEntireRun = False
'										qtApp.Options.Run.StepExecutionDelay = 0
'										qtApp.Options.Run.ViewResults = False
qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
qtApp.Options.Run.AutoExportReportConfig.ExportLocation = strRunLog_Folder
qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
qtApp.Options.Run.ScreenRecorder.RecordSound = False
qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True

		gFwInt_DataParameterRow_ErrorsTotal = 0 : gFwInt_AbandonIteration_ReasonCount = 0
		gFWbln_ExitIteration = false
    	
		Set gFWoDnf_SysDiags = Dotnetfactory.CreateInstance("System.Diagnostics.StackFrame")
		
    	dim MethodName 
    	On error resume next
    '	MethodName = new oDnf_SysDiags.StackTrace().GetFrame(0).GetMet hod().Name
    	MethodName = oDnf_SysDiags.GetMethod().Name
    	On error goto 0
    	
    	
    	


' =========  
' =========  Stage 00   - Setup the Stage-reporting elements =============
' =========  

Dim micFinalStatus, intStageSuccessCount, intWantedSuccessCount : intStageSuccessCount = 0
Dim ctSucc, intStageSuccessAr : intStageSuccessAr = split(",0,0,0,0", ",") : intWantedSuccessCount = uBound(intStageSuccessAr)
Dim BASE_XML_Template_DIR

' =========  
' =========  Stage 0a   - Expand the Template Rows into DataPermutation Rows  =============
' =========  

					'''''Dim tsAr_EventStartedAtTS : tsAr_EventStartedAtTS = split(",,,,,,,,,,", ",")
					'''''Dim tsAr_EventEndedAtTS : tsAr_EventEndedAtTS = split(",,,,,,,,,,", ",")
Dim tsAr_EventPermutationsCount : tsAr_EventPermutationsCount = split("0,0,0,0,0,0,0,0,0,0,0", ",")
const iTS_Stage_0 = 0
const iTS_Stage_I = 1
const iTS_Stage_II = 2
const iTS_Stage_III = 3

Dim str_CatsInboxFolder, str_ackfile_all

					'''''tsAr_EventStartedAtTS(iTS_Stage_0) = now()

Environment.Value("COMPANY")=Parameter.Item("Company")
Environment.Value("ENV")=Parameter.Item("Environment")
Environment.Value("Verify_RPorMC")=Parameter.Item("Verify_RPorMC")


Dim objNet : Set objNet = CreateObject("WScript.NetWork") '  objNet.UserName objNet.ComputerName 

' Environment.Value("RUNCOUNT")=Parameter.Item("RunCount") ' not required

Dim intColNames_RowNr, intColNames_ColNr_Start, intColNames_ColNr_End
Dim r_rtTemplate, r_rtDataPermutation, intColNr_RowType, intColNr_DataCol_First, intColNr_DataCol_Last, intColNr_InScope, intColNr_ScenarioStatus


Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"

' Dim strWB_noFolderContext_onlyFileName : strWB_noFolderContext_onlyFileName = "MeterContestablity_Objections.v12.xls"
' Dim strWB_noFolderContext_onlyFileName : strWB_noFolderContext_onlyFileName = "MC_Objections.v32.xlsm"
Dim strWB_noFolderContext_onlyFileName : strWB_noFolderContext_onlyFileName =  "Automation_Plan_Book.xlsm"

Dim BASE_GIT_DIR : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"


fnExcel_CreateAppInstance  objXLapp, true
LoadData_RunContext objXLapp, objWB_Master, objWS_TestRunContext, 1,  BASE_GIT_DIR & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder

' load the DataParameter worksheet objWS_DataParameter - this contains the DataScenarios to test
'
' set objWS_DataParameter    = objWB_Master.worksheets("wsDT_MeterCntestblty_Objections")
'Umesh -  set objWS_DataParameter    = objWB_Master.worksheets("wsSsDT_mrCntestbty_Objectns") 
set objWS_templateXML_TagDataSrc_Tables = objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' store XML Template WS name
set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference

set objWS_MTS_GUIVerification    = objWB_Master.worksheets("wsVefiry_MTS_GUI_Changes")

objWS_MTS_GUIVerification.activate


'MethodName = gFWoDnf_SysDiags.GetMethod().Name

objWS_MTS_GUIVerification.range("rgWS_RunConfig_ENV"             ).formula = "'" & Parameter.Item("Environment")
objWS_MTS_GUIVerification.range("rgWS_runConfig_COY"             ).formula = "'" & Parameter.Item("Company")
objWS_MTS_GUIVerification.range("rgWS_Verify_RPorMC"             ).formula = "'" & Parameter.Item("Verify_RPorMC")


'Umesh -  int_NrOf_InScope_Rows      = objWS_DataParameter.range("rgWS_Count_NrOf_needed_NMIs").value
'Umesh -  int_NrOf_InScope_Rows      = cInt(int_NrOf_InScope_Rows * 1.5) + 10 ' get some extra rows in case some of those identified are reserved for other cases
'Umesh -  int_NrOf_NMI_rows_required = int_NrOf_InScope_Rows * (intSize_listOldNew + 1)

Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE, cellReportStatus_SingleScenario

'Umesh -  set cellReportStatus_FwkProcessStage = objWS_DataParameter.range("rgWS_cellReportStatus_FwkProcessStage")
'Umesh -  set cellReportStatus_ROLE            = objWS_DataParameter.range("rgWS_cellReportStatus_ROLE")


					'''''Set cellReportStatus_SingleScenario = objWS_DataParameter.range("rgWS_cellReportStatus_SingleScenario")


'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr

'Umesh -  Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
'Umesh -  dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare

Set gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr = CreateObject("Scripting.Dictionary")
gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr.CompareMode = vbTextCompare


Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
intWsRowNr_ColumNames = objWS_MTS_GUIVerification.range("rgWS_ColumName_Row").row
intWsColNr_ParmsStart = objWS_MTS_GUIVerification.range("rgWS_DataCol_First").column
intWsColNr_ParmsEnd   = objWS_MTS_GUIVerification.range("rgWS_DataCol_Last").column

'Umesh -  fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
fnWS_LoadKey_ColOffsets_toDictionary objWS_MTS_GUIVerification, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"


Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
Dim tempInt_RoleIdentificationCounter

gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
' Load the first/title row in a dictionary
int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue



'Umesh -  set cellTopLeft = objWS_DataParameter.range("rgWS_RunReport_TopLeftCell_Stage0_StartTS")
rowTL = 1 : colTL = 1


' =========  
' =========  Stage 0b   - Technical Setup for Running the DataPermutation Rows   =============
' =========  


Dim objDBConn_TestData_A, objDBRcdSet_TestData_A 
dim r

On error resume next
Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
'Umesh -  r = 0
set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")


Dim strAr_listOldNew, intSize_listOldNew, str_listOldNew


Dim objXLapp, objWB_Master, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
dim objWS_DataParameter, int_NrOf_InScope_Rows, int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr

Dim DB_CONNECT_STR_MTS 
DB_CONNECT_STR_MTS = "Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & Environment.Value("HOSTNAME") & ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & Environment.Value("SERVICENAME") & "))); User ID=" & Environment.Value("USERNAME") & ";Password=" & Environment.Value("PASSWORD") & ";"



Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL")&"SORD\"

Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize 
Dim strQueryTemplate, dtD, Hyph, strNMI

Dim strList_NMIsizes, strAr_NMIsizes, nmiSizes
strList_NMIsizes = ",SMALL,LARGE"
strAr_NMIsizes = split(strList_NMIsizes, ",")

Dim oldnew, oldnewMax, n
oldnewMax = intSize_listOldNew

Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
					'''''intTestData_RowIsInScope_ColNr 	= dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
					'''''intCatsCR_ColNr                	= dictWSDataParameter_KeyName_ColNr("CATS_CR")
					'''''intColNR_BaseDate				= dictWSDataParameter_KeyName_ColNr("BaseDate") 
					'''''intColNR_DateOffset				= dictWSDataParameter_KeyName_ColNr("DayOffset")
					'''''intColNr_sizeOfNMI				= dictWSDataParameter_KeyName_ColNr("list_tNMI")

Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0

Dim intSQL_Pkg_ColNr, strSQL_RunPackage


'Umesh -  Dim intRowNr_DPs_SmallLarge : intRowNr_DPs_SmallLarge = objWS_DataParameter.range("rgWS_RowNr_DPs_SmallLarge").row

Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn


intRowNr_LoopWS_RowNr_StartOn = objWS_MTS_GUIVerification.Range("rgWS_DataRow_First_MinusOne").Row + 1
'intRowNr_LoopWS_RowNr_StartOn = 26
'intWsRowNr_ColumNames = intRowNr_LoopWS_RowNr_StartOn - 1 
'intRowNr_LoopWS_RowNr_StartOn = fnSetIfTrueFalse(objNet.UserName, "EQ", "uanand", 1225, intRowNr_LoopWS_RowNr_StartOn)

intRowNr_LoopWS_RowNr_FinishOn = objWS_MTS_GUIVerification.range("rgWS_DataRow_Last_PlusOne").row - 1

'intRowNr_LoopWS_RowNr_FinishOn = fnSetIfTrueFalse(objNet.UserName, "EQ", "uanand", 1231, intRowNr_LoopWS_RowNr_FinishOn)

'intRowNr_LoopWS_RowNr_FinishOn = 26

					'''''tsAr_EventEndedAtTS(iTS_Stage_0) = now()

' =========  
' =========  Stage I   - Gather NMI's & other test-data that will serve as inputs to the test =============
' =========  


					'''''tsAr_EventStartedAtTS(iTS_Stage_I) = tsAr_EventEndedAtTS(iTS_Stage_0)
					'''''
					'''''cellReportStatus_FwkProcessStage.formula = "'" & "=========  Stage I   - Gather NMI's & other test-data that will serve as inputs to the test"
					'''''cellReportStatus_ROLE.formula = "'" 
					'''''cellReportStatus_SingleScenario.formula = "'" 
'''''
    
'   qq load the dictionary from the rgWS_ColumName_Row
    intColNr_RowType = dictWSDataParameter_KeyName_ColNr.Item("RowType")
' qq - FrameWork.Rule - retain this single evaluation here so that work can be done concurrently by different test-engineers
 
 
 	select case objNet.ComputerName 
 		case "PCA18123", "PCA15187", "PCA16100", "CORPVMUFT08"    ' Umesh
			intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("InScope")
 		case "PCA15187" '  Brian
			intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("debug_InScope") 
		Case else
			intColNr_InScope = dictWSDataParameter_KeyName_ColNr.Item("InScope")
 	End select
 	
 	
intColNr_InScope =gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr( Parameter("InScope_Column_Name"))

' The below function is not working because of sblMsWin_Initialize_gobjNetwork	
'sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr, objWS_MTS_GUIVerification  ' set the Run Scope  i.e. Scenarios to run, etc

'	set the state to run the dataPermutations over, based on the company being tested
	Select Case ucase(Parameter.Item("Company"))
		Case "CITI", "PCOR"
			strScratch = "VIC"
		Case "SAPN"
			strScratch = "SA"
		Case else
			strScratch =  hFWobj_SB.AppendFormat ("Case `COMPANY` parm was passed, but value was invalid (`{0}`), i.e. not one in {CITI`POWERCOR`SAP}.", Parameter.Item("Company") ).ToString
			reporter.ReportEvent micFail, strScratch, ""
			ExitAction ' exit the test case
	End Select
	
	Dim tgtCol_State : tgtCol_State = dictWSDataParameter_KeyName_ColNr("list_tState")
	For intScratch = intRowNr_LoopWS_RowNr_StartOn to intRowNr_LoopWS_RowNr_FinishOn 
		' If objWS_DataParameter.cells( intScratch, intColNr_RowType).value = "rtDataPermutation" Then
		If objWS_DataParameter.cells( intScratch, intColNr_RowType).value = "rtSingleScenario" Then
			objWS_DataParameter.cells( intScratch,tgtCol_State ).formula = strScratch
		End If
	Next
 	
 
    Dim vntAr_RowTypes, vntAR_ColumnTypes
    
'   Create new DataPermutation rows from the DataTemplate rows

    
    
'   <<==  Remove all rtDataParameter Rows
'         Remove all rtDataParameter Rows
'         Remove all rtDataParameter Rows
'         Remove all rtDataParameter Rows
 '            sb_rtTemplate_RemoveDataParm_Rows_WS objWS_DataParameter, dictWSDataParameter_KeyName_ColNr
    
'   Create new DataPermutation rows from the DataTemplate rows
    
    Dim intArSz_Role, intArSz_State, intArSz_NorC, intArSz_sizeNMI
    Dim iRole, iState, iNorC, isizeNMI
    Dim colNr_Role, colNr_State, colNr_NorC, colNr_sizeNMI
    Dim strAr_acrossA_Role, strAr_downA_State, strAr_downB_NorC, strAr_downC_sizeNMI
    Dim strRow_DataPermutation_Template, strRow_DataPermutation: strRow_DataPermutation_Template = "tr<TemplateRow>rl<RoleNr>st<State>nc<NorC>nmi<NMI>"
    Dim objCell_NewRowCell
    Dim strRole_NorC_Negative, cStr_OmitThisCombination: cStr_OmitThisCombination = "-"
    Dim intNrOfNewRows: intNrOfNewRows = 0
    Dim intDisplayChanges_Interval: intDisplayChanges_Interval = 0
    Dim intColNr_SQL_DataParameter, strMasterWS_ColumnName
    Dim intColNr_XML_DataTemplate_FileNameQQ
    Dim strSQL_TemplateName_Current, strSQL_TemplateName_Previous, strScratch, intScratch, cellScratch
    Dim strXML_TemplateName_Current, strXML_TemplateName_Previous
    Dim intColNr_NMI, intColNr_TestResult
    Dim strSuffix_Expected, strSuffix_Actual, strSuffix_NMI
    Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true
    Dim qintColNr, qintColsCount, strSqlCols_NamesList, strSqlCols_ValuesList, strSql_Suffix, qintFieldFirst_Nr : qintFieldFirst_Nr = 0
    Dim strAr_SqlCol_NamesAr, strAr_SqlCol_ValuesAr, dictSqlData_forThisRow_ColValues
    Dim colNr_SqlCol_Names, colNr_SqlCol_Values
    Dim strInScope_SmallLarge_Ar, intCt_AdditionalCriteria_All, cellNMI, cellExpectedValue, cellActualValue
    Dim strArr_SQL_RowData_columnNames, strArr_SQL_RowData_columnValues
    
    strSuffix_Expected = objWS_DataParameter.range("rgWS_Suffix_Actual").value
    strSuffix_Actual   = objWS_DataParameter.range("rgWS_Suffix_Expected").value
    strSuffix_NMI      = objWS_DataParameter.range("rgWS_nameNMI").Value


Dim dictWSDataParameter_KeyColName_ItemColValue
Set dictWSDataParameter_KeyColName_ItemColValue = CreateObject("Scripting.Dictionary")
dictWSDataParameter_KeyColName_ItemColValue.CompareMode = vbTextCompare

Dim StrXMLTemplateLocation, Str_TransactionID, Date_TransactionDate, Str_Role, Str_RoleStatus, Str_Parcipant, Str_ChangeReasonCode
Dim date_ChgDt_Type_A_P, Int_NMI, Str_list_tState, Str_list_tNMI, Int_DayOffset, strJurisdiction
Dim str_DataParameter_PopulatedXML_FqFileName, int_UniqueReferenceNumberforXML

' load the SQL{ColNames and ColValues) into splitAr's
Dim strAr_ColumnNames, int_ArSize_ColNames, strAr_ColumnValue, int_ArSize_ColValues, intArSQL_elNr
Dim vntSqlCell, strSqlCellContents

' Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage

gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
' Get headers of table in a dictionary
Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare


int_MaxColumns = ubound(gvntAr_RuleTable,2)

' Load the first/title row in a dictionary
fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_RuleTable, 1, int_MaxColumns, gdictWStemplateXML_TagDataSrc_Tables_ColValue
					

Dim strScratch_MatchStr

'BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from framework
BASE_XML_Template_DIR = cBASE_XML_Template_DIR
Dim strCaseGroup, strCaseNr, strCsList1_TagsNeedingFlexibleManagement_Current, strCsList1_TagsNeedingFlexibleManagement_Previous
Dim intSeconds 

Dim str_MTS_Objection_Reason_Code
Dim intColNr_CatsCrCode_Actual 


' Close any open instance of the application
fnMTS_WinClose ' qq cleanup RunAction "A_MTS_CloseMTS [A_MTS_CloseMTS]", oneIteration

' Open MTS aplication
fnMTS_Open_wEnvVars ' qq remove following :   RunAction "A_MTS_OpenMTS [A_MTS_OpenMTS]", oneIteration

	' We need Environment.Value("COMPANY")
	' We need Environment.Value("ENV")
	' strSource = "C:\_data\GUILoader\MTS\" ' this source folder needs to be there
	' "C:\_data\GUILoader\MTS\" needs to have ini files
	
' To call the function please use the next line:
' str_MTS_Objection_Reason_Code =  fnCheckCats_MTS(strRequestId, strNMI, "", "", "")


					
Dim temp_InScopeYN, temp_Navigation_Path, temp_fnVerify_ScreenName
Dim temp_Object_Type, temp_NMI, temp_Object_Name, temp_Scenario_Name
Dim temp_Object_Key_Value, temp_PassORFail, temp_SQL_Query, temp_XML_Template
Dim temp_csListSqlColumn_Names, temp_csListSqlColumn_Values, temp_Investigation_Code
Dim int_ctr_currentRow, temp_TRANSACTION_GRP
Dim dt_from, dt_to, int_datagridcount
Dim int_temp_exit_row
Dim str_WhetherFailure , str_Scratch_Error_Comment

Dim strFolderNameWithSlashSuffix_Scratch
Dim ack_objFSO, objFile_ack

Dim intTotalWait


strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"


	For int_ctr_currentRow = intWsRowNr_ColumNames + 1 To intRowNr_LoopWS_RowNr_FinishOn
	
		' Fetch data for row
		'temp_InScopeYN = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("InScopeYN")).value
		temp_InScopeYN = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, intColNr_InScope ).value
		int_temp_exit_row = "n"
		str_WhetherFailure = "n"
		str_Scratch_Error_Comment = ""
		intTotalWait = 0
		
		 ' strSqlCols_NamesList = "" : strSql_Suffix = "," ' we'll make the list 1-based
		 ' strSqlCols_ValuesList = "" : strSqlCols_ValuesList = "," ' we'll make the list 1-based
				
		If trim(lcase(temp_InScopeYN)) = "y" Then
		
			temp_Scenario_Name = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Scenario_Name")).value
			temp_Navigation_Path = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Navigation_Path")).value
			temp_fnVerify_ScreenName = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("fnVerify_ScreenName")).value
			temp_SQL_Query = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("SQL_Query")).value
			temp_XML_Template = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("XML_Template")).value
			temp_csListSqlColumn_Names = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Names")).value
			temp_csListSqlColumn_Values = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Values")).value
			temp_Object_Type = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Object_Type")).value
			temp_Investigation_Code = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Investigation_Code")).value
			temp_TRANSACTION_GRP = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("TRANSACTION_GRP")).value
			temp_gw_document_Direction = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("gw_document_Direction")).value

			temp_NMI = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("NMI")).value
			temp_Object_Name = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Object_Name")).value
			dt_from = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Date_From")).value
			dt_to = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Date_To")).value
			
			If lcase(trim(Environment.Value("Verify_RPorMC"))) = "rp" Then
				temp_Object_Key_Value = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Object_Key_Value_RP")).value
			ElseIf lcase(trim(Environment.Value("Verify_RPorMC"))) = "mc" Then
				temp_Object_Key_Value = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Object_Key_Value_MC")).value
			End If
			' temp_PassORFail = objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("PassORFail")).value		
			
			
			Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
			
			strRunLog_ScreenCapture_fqFileName = ""
			
			goStrBldr.clear
			
			strRunLog_ScreenCapture_fqFileName =  _
					goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
					gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString

			
			
			Select Case lcase(temp_Navigation_Path) ' click on the corresponding menu option - this should be the exact menu option

				Case "transactions;cats;search cats notifications received" 
				    Window("MTS_Window").WinMenu("Menu").Select "Transactions;CATS;Search CATS Notifications Received"
				    
		           	If temp_NMI <> "" Then
		           		PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_search").SetCellData "#1","NMI",temp_NMI            
		           	End if
		    	
		           	If dt_from <> "" Then
				    	PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_search").SetCellData "#1","created_dt_from", dt_from
				    End If
				    
		           	If dt_to <> "" Then
				    	PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_search").SetCellData "#1","created_dt_to", dt_to
				    End If
				    
				    PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbButton("Display").Click
				    
				    do while( PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbButton("Display").exist(0) = false )
				    	intTotalWait = intTotalWait + 1
				        wait 1
				        ' If intTotalWait >= 180 Then 
				        If intTotalWait >= i_timetowait Then 
				        	'ExitAction
				        	Exit do 
				        End if
				    loop
				    
					If PbWindow("MTS").Dialog("DialogMTS").Static("NoRecordsWereFoundForTheSpecifiedSearchCriteria").exist(3) then
						Reporter.ReportEvent micFail, "No records found for search criteria on MTS", "No records found for search criteria on MTS"
				    	PbWindow("MTS").Dialog("DialogMTS").WinButton("OK").Click ' return the app to it's post-search usual-state
					    PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbButton("Close").Click
					    int_temp_exit_row = "y"
				    End if				    
				    	
				    If int_temp_exit_row = "n" Then
				    	If lcase(trim(temp_Object_Type)) = "label" Then
					    	temp_total_rows = PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_slct").RowCount
						    If temp_total_rows > 0  Then
						    	PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_slct").SelectCell "#1","date_received"
						    	PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbDataWindow("dw_slct").SelectCell "#1","date_received"
						    	PbWindow("MTS").PbWindow("w_search_cats_notifications_recvd").PbButton("Details").Click
						    	
						    	PbWindow("MTS").PbWindow("w_cats_notifi_recevd_details").PbDataWindow("dw_middle").highlight
						    	
						    	For i = 1 To PbWindow("MTS").PbWindow("w_cats_notifi_recevd_details").PbDataWindow("dw_middle").RowCount
						    		For j = 1 To PbWindow("MTS").PbWindow("w_cats_notifi_recevd_details").PbDataWindow("dw_middle").ColumnCount
						    			' msgbox "row (" & i & "), col(" & j & ") - " & PbWindow("MTS").PbWindow("w_cats_notifi_recevd_details").PbDataWindow("dw_middle").GetCellData("#"&i,"#"&j)
						    		Next
						    	Next
								' PbWindow("MTS").PbWindow("w_cats_notifi_recevd_details").PbDataWindow("dw_middle").GetCellData "#1","#1" ' "objection_reason_code"
						    End If
   				    	ElseIf lcase(trim(temp_Object_Type)) = "list" Then
   				    		

					    End If
				    	
				    End If
				    	
				    
				Case "transactions;meter data received;search verify meter data request received" 
				    Window("MTS_Window").WinMenu("Menu").Select "Transactions;Meter Data Received;Search Verify Meter Data Request Received"

					'If ucase(trim(temp_Scenario_Name)) = "B2B_INV_CODE_DEAC_003_01" Then ' This scenario needs executing a query and checking returned value as Y
						
						' Read query - Select Active_YN from B2B_MD_INVESTIGATION_CODE where INVESTIGATION_CD in  ('Require Latest Version' ,'Recipient Not Responsible For The NMI')
						If temp_SQL_Query <> ""  Then
						' Capture MTS SQL
						intSQL_ColNr           = dictWSTestRunContext_KeyName_ColNr(temp_SQL_Query)  '   "MTS_FIND_NMI_withNo_METER"
						'strQueryTemplate       = objWS_TestRunContext.cells(2,intSQL_ColNr).value ' 	

                         strQueryTemplate = fnRetrieve_SQL(temp_SQL_Query)
                         
                         If LCASE(strQueryTemplate) <> "fail" Then
 
                         temp_SQL_Query = strQueryTemplate

						
						' Update query to replace with runtime parameters
						strQueryTemplate = replace ( strQueryTemplate , "<Investigation_Code>", temp_Investigation_Code, 1, -1, vbTextCompare)
						strQueryTemplate = replace ( strQueryTemplate , "<TRANSACTION_GRP>"  , temp_TRANSACTION_GRP   , 1, -1, vbTextCompare) ' qq
						strQueryTemplate = replace ( strQueryTemplate , "<document_Direction>"  , temp_gw_document_Direction   , 1, -1, vbTextCompare) ' qq
						
						objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("SQL_Populated")).value = "'" & strQueryTemplate
						
						' Execute query
						fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strQueryTemplate, "10"
						
						' traverse through recordset and check for Y
						
						End If
												
			            End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
						
						If objDBRcdSet_TestData_A.RecordCount > 0 Then
							objDBRcdSet_TestData_A.MoveFirst
							
							'while not objDBRcdSet_TestData_A.EOF 
								objDBRcdSet_TestData_A.MoveFirst
								strSqlCols_NamesList = ""
								strSqlCols_ValuesList  = ""
								
								For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count -1 
									strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
									strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
									strSql_Suffix = ","
								Next	
	
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Names")).formula = "'" & strSqlCols_NamesList
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Values")).formula = "'" & strSqlCols_ValuesList


								' temp_str_replacementvalue = strArr_SQL_RowData_columnValues ( dictSQL_keyColNames_itemArIndexNr( "NMI" ) )
								PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_srch").SetCellData "#1","nmi",objDBRcdSet_TestData_A.Fields("NMI").Value @@ hightlight id_;_461538_;_script infofile_;_ZIP::ssf53.xml_;_
								
								' temp_str_replacementvalue = strArr_SQL_RowData_columnValues ( dictSQL_keyColNames_itemArIndexNr( "FILE_TIME_STAMP" ) )
								' PbWindow("w_frame").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_srch").SetCellData "#1","start_trans_dt", objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value
								PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_srch").SetCellData "#1","start_trans_dt", fnTimeStamp(objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value,"DD/MM/YYYY")
								
								PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbButton("Display").Click @@ hightlight id_;_1182456_;_script infofile_;_ZIP::ssf56.xml_;_
								
								PbWindow("w_frame_3").Dialog("Dialog_continue").WinButton("Yes").Click
								
								' check number of records displayes
								if PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").RowCount > 0 Then
								
									For int_datagridcount = 1 To PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").RowCount 
										if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").GetCellData("#"& int_datagridcount,"investigation_code"))) = trim(lcase(temp_Investigation_Code)) Then @@ hightlight id_;_1051432_;_script infofile_;_ZIP::ssf58.xml_;_
											int_temp_exit_row = "y"	
											
											PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").SelectCell "#"& int_datagridcount,"investigation_code"
											
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											' take screenshot
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
											
											PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbButton("Details").Click
																	
											do while( PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").Exist(0) = false )
										    	intTotalWait = intTotalWait + 1
										        wait 1
										        ' If intTotalWait >= 180 Then 
										        If intTotalWait >= i_timetowait Then 
													str_WhetherFailure = "y"
													' str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for 180 seconds however the detailed screen was not displayed"
													str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for "& i_timetowait &" seconds however the detailed screen was not displayed"
													strRunLog_ScreenCapture_fqFileName = ""
													Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
													goStrBldr.clear
													strRunLog_ScreenCapture_fqFileName =  _
															goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
															gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
													call PbWindow("w_frame_3").CaptureBitmap( strRunLog_ScreenCapture_fqFileName, True )
													Exit Do
										        End if
										    loop	
										    
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											' take screenshot
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )


											if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").GetCellData("#1","INVESTIGATION_CODE"))) <> trim(lcase(temp_Investigation_Code)) Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Different investigation code on detail screen for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ". Expected value was ("& temp_Investigation_Code&") whereas actual is ("& trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").GetCellData("#1","INVESTIGATION_CODE"))) &"))"
											End if
											
											PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbButton("Close").Click @@ hightlight id_;_1510032_;_script infofile_;_ZIP::ssf68.xml_;_
											int_datagridcount = PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").RowCount + 1
											
										Else
											int_temp_exit_row = "n"
										End if
										
									Next
									
									If int_temp_exit_row = "n" Then
										str_WhetherFailure = "y"
										str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Not able to find a single record with investigation code (" & temp_Investigation_Code &") for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									End If
								Else
									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No records displayed on grid for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
									strRunLog_ScreenCapture_fqFileName = ""
																		goStrBldr.clear
									strRunLog_ScreenCapture_fqFileName =  _
											goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
											gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
									' take screenshot
									call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
									
								End if
								
								objDBRcdSet_TestData_A.MoveNext
							'wend
							'objDBRcdSet_TestData_A.Close
							'objDBConn_TestData_A.Close
						Else
							str_WhetherFailure = "y"
							str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No data returned by query (" & strQueryTemplate & ")"
						End If
						
					'End If
						PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").Close

				Case "transactions;meter data sent;search verify meter data request sent" 
					Window("MTS_Window").WinMenu("Menu").Select "Transactions;Meter Data Sent;Search Verify Meter Data Request Sent"
				    

					'If ucase(trim(temp_Scenario_Name)) = "B2B_INV_CODE_DEAC_003_02" Then ' This scenario needs executing a query and checking returned value as Y
						
						' Read query - Select Active_YN from B2B_MD_INVESTIGATION_CODE where INVESTIGATION_CD in  ('Require Latest Version' ,'Recipient Not Responsible For The NMI')
						If temp_SQL_Query <> ""  Then
						'capute sql
						intSQL_ColNr           = dictWSTestRunContext_KeyName_ColNr(temp_SQL_Query)  '   "MTS_FIND_NMI_withNo_METER"
						'strQueryTemplate       = objWS_TestRunContext.cells(2,intSQL_ColNr).value ' 

                         strQueryTemplate = fnRetrieve_SQL(temp_SQL_Query)
                         
                         If LCASE(strQueryTemplate) <> "fail" Then
 
                         temp_SQL_Query = strQueryTemplate
						
						' Update query to replace with runtime parameters
						strQueryTemplate = replace ( strQueryTemplate , "<Investigation_Code>", temp_Investigation_Code, 1, -1, vbTextCompare)
						strQueryTemplate = replace ( strQueryTemplate , "<TRANSACTION_GRP>"  , temp_TRANSACTION_GRP   , 1, -1, vbTextCompare) ' qq
						strQueryTemplate = replace ( strQueryTemplate , "<document_Direction>"  , temp_gw_document_Direction   , 1, -1, vbTextCompare) ' qq
						
						objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("SQL_Populated")).value = "'" & strQueryTemplate
						
						' Execute query
						fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strQueryTemplate, "10"
						
						' traverse through recordset and check for Y
						
						End If	
						
			            End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
						
						If objDBRcdSet_TestData_A.RecordCount > 0 Then
							objDBRcdSet_TestData_A.MoveFirst
							
							'while not objDBRcdSet_TestData_A.EOF 
								objDBRcdSet_TestData_A.MoveFirst
								strSqlCols_NamesList = ""
								strSqlCols_ValuesList  = ""
								
								For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count -1
									strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
									strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
									strSql_Suffix = ","
								Next	
	
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Names")).formula = "'" & strSqlCols_NamesList
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Values")).formula = "'" & strSqlCols_ValuesList
								
								PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").PbDataWindow("dw_srch").SetCellData "#1","nmi",objDBRcdSet_TestData_A.Fields("NMI").Value @@ hightlight id_;_461538_;_script infofile_;_ZIP::ssf53.xml_;_
								
								' PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").PbDataWindow("dw_srch").SetCellData "#1","start_trans_dt",objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value @@ hightlight id_;_2099414_;_script infofile_;_ZIP::ssf80.xml_;_
								PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").PbDataWindow("dw_srch").SetCellData "#1","start_trans_dt", fnTimeStamp(objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value,"DD/MM/YYYY")
								
								PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").PbButton("Display").Click @@ hightlight id_;_1182456_;_script infofile_;_ZIP::ssf56.xml_;_
								' PbWindow("w_frame").Dialog("CitiPower (CMTSMD) MTS").WinButton("btnYes").Click
								PbWindow("w_frame").Dialog("Dialog_continue").WinButton("Yes").Click
								
								' check number of records displayes
								if PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").PbDataWindow("dw_slct").RowCount > 0 Then
								
									For int_datagridcount = 1 To PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").PbDataWindow("dw_slct").RowCount 
									
										if trim(lcase(PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").PbDataWindow("dw_slct").GetCellData("#"& int_datagridcount,"investigation_cd"))) = trim(lcase(temp_Investigation_Code)) Then @@ hightlight id_;_1051432_;_script infofile_;_ZIP::ssf58.xml_;_
											int_temp_exit_row = "y"	
											
											PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").PbDataWindow("dw_slct").SelectCell "#"& int_datagridcount,"investigation_cd"
											
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											' take screenshot
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
											
											PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").PbButton("Details").Click
											
											do while( PbWindow("w_frame").PbWindow("w_verify_meter_data_req_sent_detail").PbDataWindow("dw_md_detail").Exist(0) = false )
										    	intTotalWait = intTotalWait + 1
										        wait 1
										        ' If intTotalWait >= 180 Then 
										        If intTotalWait >= i_timetowait Then 
													str_WhetherFailure = "y"
													' str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for 180 seconds however the detailed screen was not displayed"
													str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for "& i_timetowait &" seconds however the detailed screen was not displayed"
													strRunLog_ScreenCapture_fqFileName = ""
													Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
													goStrBldr.clear
													strRunLog_ScreenCapture_fqFileName =  _
															goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
															gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
													call PbWindow("w_frame").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
													Exit do 
										        End if
										    loop	
										    
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											' take screenshot
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
											
											if trim(lcase(PbWindow("w_frame").PbWindow("w_verify_meter_data_req_sent_detail").PbDataWindow("dw_md_detail").GetCellData("#1","investigation_cd"))) <> trim(lcase(temp_Investigation_Code)) Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Different investigation code on detail screen for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ". Expected value was ("& temp_Investigation_Code&") whereas actual is ("& trim(lcase(PbWindow("w_frame").PbWindow("w_verify_meter_data_req_sent_detail").PbDataWindow("dw_md_detail").GetCellData("#1","investigation_cd"))) &"))"
											End if
											
											
											PbWindow("w_frame").PbWindow("w_verify_meter_data_req_sent_detail").PbButton("Close").Click
											int_datagridcount = PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").PbDataWindow("dw_slct").RowCount  + 1
											
										Else
											int_temp_exit_row = "n"
										End if
										
									Next
									
									If int_temp_exit_row = "n" Then
										str_WhetherFailure = "y"
										str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Not able to find a single record with investigation code (" & temp_Investigation_Code &") for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									End If
								Else
									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No records displayed on grid for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
								End if
								
								objDBRcdSet_TestData_A.MoveNext
							'wend
							'objDBRcdSet_TestData_A.Close
							'objDBConn_TestData_A.Close
						Else
							str_WhetherFailure = "y"
							str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No data returned by query (" & strQueryTemplate & ")"
						End If
						
					'End If
					
						PbWindow("w_frame").PbWindow("w_verify_meter_data_sent_srch").Close

				Case "transactions;all aqs"
					Window("MTS_Window").WinMenu("Menu").Select "Transactions;All AQs"
					
					


					'If ucase(trim(temp_Scenario_Name)) = "B2B_INV_CODE_DEAC_003_02" Then ' This scenario needs executing a query and checking returned value as Y
						
						' Read query - Select Active_YN from B2B_MD_INVESTIGATION_CODE where INVESTIGATION_CD in  ('Require Latest Version' ,'Recipient Not Responsible For The NMI')
						If temp_SQL_Query <> ""  Then
						'capture sql
						intSQL_ColNr           = dictWSTestRunContext_KeyName_ColNr(temp_SQL_Query)  '   "MTS_FIND_NMI_withNo_METER"
						'strQueryTemplate       = objWS_TestRunContext.cells(2,intSQL_ColNr).value ' 						
						
						strQueryTemplate = fnRetrieve_SQL(temp_SQL_Query)
                         
                         If LCASE(strQueryTemplate) <> "fail" Then
 
                         temp_SQL_Query = strQueryTemplate
						
						
						' Update query to replace with runtime parameters
						strQueryTemplate = replace ( strQueryTemplate , "<Investigation_Code>", temp_Investigation_Code, 1, -1, vbTextCompare)
						strQueryTemplate = replace ( strQueryTemplate , "<TRANSACTION_GRP>"  , temp_TRANSACTION_GRP   , 1, -1, vbTextCompare) ' qq
						strQueryTemplate = replace ( strQueryTemplate , "<document_Direction>"  , temp_gw_document_Direction   , 1, -1, vbTextCompare) ' qq
						
						objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("SQL_Populated")).value = "'" & strQueryTemplate
						
						' Execute query
						fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strQueryTemplate, "10"
						
						' traverse through recordset and check for Y
						End If
												
			            End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
												
						
						If objDBRcdSet_TestData_A.RecordCount > 0 Then
							objDBRcdSet_TestData_A.MoveFirst
							
							'while not objDBRcdSet_TestData_A.EOF 
								objDBRcdSet_TestData_A.MoveFirst
								strSqlCols_NamesList = ""
								strSqlCols_ValuesList  = ""
								
								For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count  -1 
									strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
									strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
									strSql_Suffix = ","
								Next	
	
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Names")).formula = "'" & strSqlCols_NamesList
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Values")).formula = "'" & strSqlCols_ValuesList


								PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_search").SetCellData "#1","nmi",objDBRcdSet_TestData_A.Fields("NMI").Value @@ hightlight id_;_461538_;_script infofile_;_ZIP::ssf53.xml_;_
								'PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_search").SetCellData "#1","created_dt_from", objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value @@ hightlight id_;_2099414_;_script infofile_;_ZIP::ssf80.xml_;_
								'PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_search").SetCellData "#1","created_dt_to", objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value
								PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_search").SetCellData "#1","created_dt_from", fnTimeStamp(objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value,"DD/MM/YYYY")
								PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_search").SetCellData "#1","created_dt_to", fnTimeStamp(objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value,"DD/MM/YYYY")
								PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_search").SetCellData "#1","#10","(ALL)" @@ hightlight id_;_394706_;_script infofile_;_ZIP::ssf138.xml_;_


								PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbButton("Display").Click @@ hightlight id_;_1182456_;_script infofile_;_ZIP::ssf56.xml_;_
								If PbWindow("w_frame_3").Dialog("DialogMTS").Static("NoRecordsWereFoundForTheSpecifiedSearchCriteria").exist(2) then

									On Error Resume Next
									strRunLog_ScreenCapture_fqFileName = ""
									goStrBldr.clear
									Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
									strRunLog_ScreenCapture_fqFileName =  _
											goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
											gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
									
									call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName ,True) 'p2 is overwrite=true/false
 									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No record found for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									On Error Goto 0
							    	
							    	PbWindow("w_frame_3").Dialog("DialogMTS").WinButton("OK").Click ' return the app to it's post-search usual-state
							    End if
								
								
								' PbWindow("w_frame").Dialog("CitiPower (CMTSMD) MTS").WinButton("btnYes").Click
								
								' check number of records displayes
								if PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_slct").RowCount > 0 Then
								
									For int_datagridcount = 1 To PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_slct").RowCount 
									
										if trim(lcase(PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_slct").GetCellData("#"& int_datagridcount,"trans_type"))) = "meterdataverifyrequest" Then @@ hightlight id_;_1051432_;_script infofile_;_ZIP::ssf58.xml_;_
											int_temp_exit_row = "y"	
											
											
											' PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_slct").SelectCell "#1","trans_type"
											PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_slct").SelectCell "#"& int_datagridcount, "trans_type"

											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											' take screenshot
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
											
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
			
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											
											PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").PbButton("Details").Click
											
											do while( PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").Exist(0) = false )
										    	intTotalWait = intTotalWait + 1
										        wait 1
										        ' If intTotalWait >= 180 Then 
										        If intTotalWait >= i_timetowait Then 
													str_WhetherFailure = "y"
													' str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for 180 seconds however the detailed screen was not displayed"
													str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for "& i_timetowait &" seconds however the detailed screen was not displayed"
													strRunLog_ScreenCapture_fqFileName = ""
													goStrBldr.clear
													Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
													strRunLog_ScreenCapture_fqFileName =  _
															goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
															gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
													
													call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
													Exit Do 
										        End if
										    loop	


											strRunLog_ScreenCapture_fqFileName = ""
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString

											' take screenshot
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
											
											
											if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").GetCellData("#1","INVESTIGATION_CODE"))) <> trim(lcase(temp_Investigation_Code)) Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Different investigation code on detail screen for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ". Expected value was ("& temp_Investigation_Code&") whereas actual is ("& trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").GetCellData("#1","INVESTIGATION_CODE")))  &"))"
											End if
											
											


											PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbButton("Close").Click
											
											' int_datagridcount = PbWindow("w_frame").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_slct").RowCount  + 1
											
										Else
											int_temp_exit_row = "n"
										End if
										
									Next
									
									If int_temp_exit_row = "n" Then
										str_WhetherFailure = "y"
										str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Not able to find a single record with tranasction type (MeterDataVerifyRequest) for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									End If
								Else
									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No records displayed on grid for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
								End if
								
								objDBRcdSet_TestData_A.MoveNext
							'wend
							'objDBRcdSet_TestData_A.Close
							'objDBConn_TestData_A.Close
						Else
							str_WhetherFailure = "y"
							str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No data returned by query (" & strQueryTemplate & ")"
						End If
						
					'End If

						
						PbWindow("w_frame_3").PbWindow("w_all_activity_queue_srch").Close
						

				Case "transactions;meter data received;search received meter data request aqs" ' working here
					Window("MTS_Window").WinMenu("Menu").Select "Transactions;Meter Data Received;Search Received Meter Data Request AQs"
					

					'If ucase(trim(temp_Scenario_Name)) = "B2B_INV_CODE_DEAC_003_02" Then ' This scenario needs executing a query and checking returned value as Y
						
						' Read query - Select Active_YN from B2B_MD_INVESTIGATION_CODE where INVESTIGATION_CD in  ('Require Latest Version' ,'Recipient Not Responsible For The NMI')
						If temp_SQL_Query <> ""  Then
						'capture sql
						intSQL_ColNr           = dictWSTestRunContext_KeyName_ColNr(temp_SQL_Query)  '   "MTS_FIND_NMI_withNo_METER"
						'strQueryTemplate       = objWS_TestRunContext.cells(2,intSQL_ColNr).value ' 

                         strQueryTemplate = fnRetrieve_SQL(temp_SQL_Query)
                         
                         If LCASE(strQueryTemplate) <> "fail" Then
 
                         temp_SQL_Query = strQueryTemplate
						 
						
						' Update query to replace with runtime parameters
						strQueryTemplate = replace ( strQueryTemplate , "<Investigation_Code>", temp_Investigation_Code, 1, -1, vbTextCompare)
						strQueryTemplate = replace ( strQueryTemplate , "<TRANSACTION_GRP>"  , temp_TRANSACTION_GRP   , 1, -1, vbTextCompare) ' qq
						strQueryTemplate = replace ( strQueryTemplate , "<document_Direction>"  , temp_gw_document_Direction   , 1, -1, vbTextCompare) ' qq
						
						objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("SQL_Populated")).value = "'" & strQueryTemplate
						
						' Execute query
						fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strQueryTemplate, "10"
						
						' traverse through recordset and check for Y
						End If
												
			            End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
			            
			            				
						
						If objDBRcdSet_TestData_A.RecordCount > 0 Then
							objDBRcdSet_TestData_A.MoveFirst
							
							'while not objDBRcdSet_TestData_A.EOF 
								objDBRcdSet_TestData_A.MoveFirst
								strSqlCols_NamesList = ""
								strSqlCols_ValuesList  = ""
								
								For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count -1 
									strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
									strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
									strSql_Suffix = ","
								Next	
	
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Names")).formula = "'" & strSqlCols_NamesList
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Values")).formula = "'" & strSqlCols_ValuesList

								
								
								PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbDataWindow("dw_search").SetCellData "#1","nmi",objDBRcdSet_TestData_A.Fields("NMI").Value
								
'								PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbDataWindow("dw_search").SetCellData "#1","sent_from_dt", objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value
'								PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbDataWindow("dw_search").SetCellData "#1","sent_to_dt", objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value
								PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbDataWindow("dw_search").SetCellData "#1","sent_from_dt", fnTimeStamp(objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value,"DD/MM/YYYY")
								PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbDataWindow("dw_search").SetCellData "#1","sent_to_dt", fnTimeStamp(objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value,"DD/MM/YYYY")

								PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbButton("Display").Click

								If PbWindow("w_frame_3").Dialog("DialogMTS").Static("NoRecordsWereFoundForTheSpecifiedSearchCriteria").exist(2) then

									On Error Resume Next
									Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
									strRunLog_ScreenCapture_fqFileName = ""
									goStrBldr.clear
									strRunLog_ScreenCapture_fqFileName =  _
											goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
											gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
									' take screenshot
									call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
									
 									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No record found for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									On Error Goto 0
							    	
							    	PbWindow("w_frame_3").Dialog("DialogMTS").WinButton("OK").Click ' return the app to it's post-search usual-state
							    End if

								
								' PbWindow("w_frame").Dialog("CitiPower (CMTSMD) MTS").WinButton("btnYes").Click
								
								' check number of records displayes
								if PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbDataWindow("dw_slct").RowCount > 0 Then
								
									For int_datagridcount = 1 To PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbDataWindow("dw_slct").RowCount 
									
										if trim(lcase(PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbDataWindow("dw_slct").GetCellData("#"& int_datagridcount,"investigation_cd"))) = trim(lcase(temp_Investigation_Code)) Then @@ hightlight id_;_1051432_;_script infofile_;_ZIP::ssf58.xml_;_
											int_temp_exit_row = "y"	
											
											PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbDataWindow("dw_slct").SelectCell "#"& int_datagridcount,"investigation_cd"

											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											' take screenshot
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )

											PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbButton("Details").Click
											
											do while( PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").Exist(0) = false )
										    	intTotalWait = intTotalWait + 1
										        wait 1
										        ' If intTotalWait >= 180 Then 
										        If intTotalWait >= i_timetowait Then 
													str_WhetherFailure = "y"
													' str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for 180 seconds however the detailed screen was not displayed"
													str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for "& i_timetowait &" seconds however the detailed screen was not displayed"
													Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
													strRunLog_ScreenCapture_fqFileName = ""
													goStrBldr.clear
													strRunLog_ScreenCapture_fqFileName =  _
															goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
															gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
													
													call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
													Exit Do
										        End if
										    loop	
										    

											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											' take screenshot
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
											
											if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").GetCellData("#1","INVESTIGATION_CODE"))) <> trim(lcase(temp_Investigation_Code)) Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Different investigation code on detail screen for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ". Expected value was ("& temp_Investigation_Code&") whereas actual is ("& trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").GetCellData("#1","INVESTIGATION_CODE"))) &"))"
											End if
											
											
											
											PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbButton("Close").Click
											
											int_datagridcount = PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").PbDataWindow("dw_slct").RowCount   + 1
											
										Else
											int_temp_exit_row = "n"
										End if
										
									Next
									
									If int_temp_exit_row = "n" Then
										str_WhetherFailure = "y"
										str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Not able to find a single record with investigation code (" & temp_Investigation_Code &") for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									End If
								Else
									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No records displayed on grid for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
									strRunLog_ScreenCapture_fqFileName = ""
									goStrBldr.clear
									strRunLog_ScreenCapture_fqFileName =  _
											goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
											gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
									' take screenshot
									call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
								End if
								
								objDBRcdSet_TestData_A.MoveNext
							'wend
							'objDBRcdSet_TestData_A.Close
							'objDBConn_TestData_A.Close
						Else
							str_WhetherFailure = "y"
							str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No data returned by query (" & strQueryTemplate & ")"
						End If
						
					'End If
					
						PbWindow("w_frame_3").PbWindow("w_mdr_rcvd_aq_srch").Close





				Case "transactions;meter data sent;search sent meter data request aqs" ' working here
					Window("MTS_Window").WinMenu("Menu").Select "Transactions;Meter Data Sent;Search Sent Meter Data Request AQs"


					'If ucase(trim(temp_Scenario_Name)) = "B2B_INV_CODE_DEAC_003_02" Then ' This scenario needs executing a query and checking returned value as Y
						
						' Read query - Select Active_YN from B2B_MD_INVESTIGATION_CODE where INVESTIGATION_CD in  ('Require Latest Version' ,'Recipient Not Responsible For The NMI')
						If temp_SQL_Query <> ""  Then
						'capture sql
						intSQL_ColNr           = dictWSTestRunContext_KeyName_ColNr(temp_SQL_Query)  '   "MTS_FIND_NMI_withNo_METER"
						'strQueryTemplate       = objWS_TestRunContext.cells(2,intSQL_ColNr).value ' 	

                         trQueryTemplate = fnRetrieve_SQL(temp_SQL_Query)
                         
                         If LCASE(strQueryTemplate) <> "fail" Then
 
                         temp_SQL_Query = strQueryTemplate

						
						' Update query to replace with runtime parameters
						strQueryTemplate = replace ( strQueryTemplate , "<Investigation_Code>", temp_Investigation_Code, 1, -1, vbTextCompare)
						strQueryTemplate = replace ( strQueryTemplate , "<TRANSACTION_GRP>"  , temp_TRANSACTION_GRP   , 1, -1, vbTextCompare) ' qq
						strQueryTemplate = replace ( strQueryTemplate , "<document_Direction>"  , temp_gw_document_Direction   , 1, -1, vbTextCompare) ' qq
						
						objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("SQL_Populated")).value = "'" & strQueryTemplate
						
						' Execute query
						fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strQueryTemplate, "10"
						
						' traverse through recordset and check for Y
						
						  End If
												
			            End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
						
						If objDBRcdSet_TestData_A.RecordCount > 0 Then
							objDBRcdSet_TestData_A.MoveFirst
							
							'while not objDBRcdSet_TestData_A.EOF 
								objDBRcdSet_TestData_A.MoveFirst
								strSqlCols_NamesList = ""
								strSqlCols_ValuesList  = ""
								
								For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count -1
									strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
									strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
									strSql_Suffix = ","
								Next	
	
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Names")).formula = "'" & strSqlCols_NamesList
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Values")).formula = "'" & strSqlCols_ValuesList
							
								PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_search").SetCellData "#1","nmi",objDBRcdSet_TestData_A.Fields("NMI").Value
								
'								PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_search").SetCellData "#1","sent_from_dt", objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value
'								PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_search").SetCellData "#1","sent_to_dt",objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value

								PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_search").SetCellData "#1","sent_from_dt", fnTimeStamp(objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value,"DD/MM/YYYY")
								PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_search").SetCellData "#1","sent_to_dt", fnTimeStamp(objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value,"DD/MM/YYYY")
								PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_search").SetCellData "#1","#7","(ALL)"

								PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbButton("Display").Click
								
								If PbWindow("w_frame_3").Dialog("DialogMTS").Static("NoRecordsWereFoundForTheSpecifiedSearchCriteria").exist(2) then

									On Error Resume Next
									Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
									strRunLog_ScreenCapture_fqFileName = ""
									goStrBldr.clear
									strRunLog_ScreenCapture_fqFileName =  _
											goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
											gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
									' take screenshot
									call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
									
 									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No record found for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									On Error Goto 0
							    	
							    	PbWindow("w_frame_3").Dialog("DialogMTS").WinButton("OK").Click ' return the app to it's post-search usual-state
							    End if
								

								' check number of records displayes
								if PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_slct").RowCount > 0 Then
								
									For int_datagridcount = 1 To PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_slct").RowCount 
									
										if trim(lcase(PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_slct").GetCellData("#"& int_datagridcount,"investigation_cd"))) = trim(lcase(temp_Investigation_Code)) Then @@ hightlight id_;_1051432_;_script infofile_;_ZIP::ssf58.xml_;_
											int_temp_exit_row = "y"	
											
											PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_slct").SelectCell "#"& int_datagridcount,"investigation_cd"

											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											' take screenshot
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
											
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
			
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											
											PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbButton("Details").Click
											
											' code related to wait here
											
										    do while( PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_sent_detail").PbDataWindow("dw_md_detail").Exist(0) = false )
										    	intTotalWait = intTotalWait + 1
										        wait 1
										        ' If intTotalWait >= 180 Then 
										        If intTotalWait >= i_timetowait Then 
													str_WhetherFailure = "y"
													' str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for 180 seconds however the detailed screen was not displayed"
													str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for "& i_timetowait &" seconds however the detailed screen was not displayed"
													Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
					
													strRunLog_ScreenCapture_fqFileName = ""
													goStrBldr.clear
													strRunLog_ScreenCapture_fqFileName =  _
															goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
															gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
													call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
													Exit Do
										        End if
										    loop	
										    

											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											' take screenshot
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
											
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
			
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
 @@ hightlight id_;_2819520_;_script infofile_;_ZIP::ssf168.xml_;_
 
											if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_sent_detail").PbDataWindow("dw_md_detail").GetCellData("#1","investigation_cd"))) <> trim(lcase(temp_Investigation_Code)) Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Different investigation code on detail screen for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ". Expected value was ("& temp_Investigation_Code&") whereas actual is ("& trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_sent_detail").PbDataWindow("dw_md_detail").GetCellData("#1","investigation_cd"))) &"))"
											End if
											
											PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_sent_detail").PbButton("Close").Click
											
											int_datagridcount = PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").PbDataWindow("dw_slct").RowCount + 1
											
										Else
											int_temp_exit_row = "n"
										End if
										
									Next
									
									If int_temp_exit_row = "n" Then
										str_WhetherFailure = "y"
										str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Not able to find a single record with investigation code (" & temp_Investigation_Code &") for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									End If
								Else
									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No records displayed on grid for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
								End if
								
								objDBRcdSet_TestData_A.MoveNext
							'wend
							'objDBRcdSet_TestData_A.Close
							'objDBConn_TestData_A.Close
						Else
							str_WhetherFailure = "y"
							str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No data returned by query (" & strQueryTemplate & ")"
						End If
						
					'End If
						
						PbWindow("w_frame_3").PbWindow("w_mdr_sent_aq_srch").Close



'				Case "transactions;cats;search cats aqs" 
'					Window("MTS_Window").WinMenu("Menu").Select "Transactions;CATS;Search CATS AQs"
'				Case "transactions;cats;search cats change requests sent" 
'					Window("MTS_Window").WinMenu("Menu").Select "Transactions;CATS;Search CATS Change Requests Sent"
'				Case "transactions;service orders;service order request search" 
'					Window("MTS_Window").WinMenu("Menu").Select "Transactions;Service Orders;Service Order Request Search"
'				Case "transactions;service orders;Service order aq search" 
'					Window("MTS_Window").WinMenu("Menu").Select "Transactions;Service Orders;Service Order AQ Search"
'				Case "transactions;service orders;service order response search" 
'					Window("MTS_Window").WinMenu("Menu").Select "Transactions;Service Orders;Service Order Response Search"
'				Case "transactions;service orders;service order response aq search" 
'					Window("MTS_Window").WinMenu("Menu").Select "Transactions;Service Orders;Service Order Response AQ Search"
'				Case "transactions;service orders;service order aq status" 
'					Window("MTS_Window").WinMenu("Menu").Select "Transactions;Service Orders;Service Order AQ Status"
'				Case "reference data;nmi reference details..." ' "reference data;nmi reference details" 
'					Window("MTS_Window").WinMenu("Menu").Select "Reference Data;NMI Reference Details..."
'					
				Case "sql" 	' cases where we need to run a query only
				
					'If ucase(trim(temp_Scenario_Name)) = "B2B_INV_CODE_DEAC_001" Then ' This scenario needs executing a query and checking returned value as Y
						
						' Read query - Select Active_YN from B2B_MD_INVESTIGATION_CODE where INVESTIGATION_CD in  ('Require Latest Version' ,'Recipient Not Responsible For The NMI')
						If temp_SQL_Query <> ""  Then
						'capture sql
						intSQL_ColNr           = dictWSTestRunContext_KeyName_ColNr(temp_SQL_Query)  '   "MTS_FIND_NMI_withNo_METER"
						'strQueryTemplate       = objWS_TestRunContext.cells(2,intSQL_ColNr).value ' 
                           strQueryTemplate = fnRetrieve_SQL(temp_SQL_Query)
                         
                         If LCASE(strQueryTemplate) <> "fail" Then
 
                         temp_SQL_Query = strQueryTemplate
						 
						' Execute query
						objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("SQL_Populated")).value = "'" & strQueryTemplate
						
						fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strQueryTemplate, "100"
						
						' traverse through recordset and check for Y
						
						End If
												
			            End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
						
						
						If objDBRcdSet_TestData_A.RecordCount > 0 Then
							objDBRcdSet_TestData_A.MoveFirst
							
							strSqlCols_NamesList = ""
							strSqlCols_ValuesList  = ""
								
							while not objDBRcdSet_TestData_A.EOF 
							
								' msgbox objDBRcdSet_TestData_A.Fields("Active_YN").Value
								For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count -1
									strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
									strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
									strSql_Suffix = ","
								Next	
								
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Names")).formula = "'" & strSqlCols_NamesList
								objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Values")).formula = "'" & strSqlCols_ValuesList
								
								If lcase(objDBRcdSet_TestData_A.Fields("Active_YN").Value) <> "n" then
									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "The query (" & strQueryTemplate & ") has resulted in a value other than 'N'. "
									objDBRcdSet_TestData_A.MoveLast
								End if
								objDBRcdSet_TestData_A.MoveNext
							wend
							'objDBRcdSet_TestData_A.Close
							'objDBConn_TestData_A.Close
						Else
							str_WhetherFailure = "y"
							str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No data returned by query (" & strQueryTemplate & ")"
						End If
						

					'End If
					
					Case "sql_xml" 	' cases where we need to run a query only
				
					'If ucase(trim(temp_Scenario_Name)) = "B2B_INV_CODE_DEAC_004" Then ' This scenario needs executing a query and checking returned value as Y
						
						' Read query - Select Active_YN from B2B_MD_INVESTIGATION_CODE where INVESTIGATION_CD in  ('Require Latest Version' ,'Recipient Not Responsible For The NMI')
						If temp_SQL_Query <> ""  Then
						'capture sql
						intSQL_ColNr           = dictWSTestRunContext_KeyName_ColNr(temp_SQL_Query)  '   "MTS_FIND_NMI_withNo_METER"
						'strQueryTemplate       = objWS_TestRunContext.cells(2,intSQL_ColNr).value ' 

                         strQueryTemplate = fnRetrieve_SQL(temp_SQL_Query)
                         
                         If LCASE(strQueryTemplate) <> "fail" Then
 
                         temp_SQL_Query = strQueryTemplate


						objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("SQL_Populated")).value = "'" & strQueryTemplate
						objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("SQL_Populated")).wraptext = false
						
						' Execute query
						fn_DbQuery_v3 DB_CONNECT_STR_MTS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strQueryTemplate, "100"
						
						' traverse through recordset and create an XML
						
						End If
												
			            End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
						
						If objDBRcdSet_TestData_A.RecordCount > 0 Then
							objDBRcdSet_TestData_A.MoveFirst
							strSqlCols_NamesList = ""
								strSqlCols_ValuesList  = ""
								
							For qintColNr = qintFieldFirst_Nr To objDBRcdSet_TestData_A.Fields.Count -1
								strSqlCols_NamesList  = strSqlCols_NamesList  & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Name
								strSqlCols_ValuesList = strSqlCols_ValuesList & strSql_Suffix & objDBRcdSet_TestData_A.Fields(qintColNr).Value
								strSql_Suffix = ","
							Next	

							objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Names")).formula = "'" & strSqlCols_NamesList
							objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("csListSqlColumn_Values")).formula = "'" & strSqlCols_ValuesList


							'while not objDBRcdSet_TestData_A.EOF 
								
								
								
								' Create an XML
								int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
								' "CCYYMMDDHHmmss99hhmmss"
								' Str_TransactionID =  int_UniqueReferenceNumberforXML ' "txnID_" &
								' Int_NMI = objDBRcdSet_TestData_A.Fields("SERVICEPOINT").Value
								Int_NMI = objDBRcdSet_TestData_A.Fields("NMI").Value
							
								
								str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("XML_Template_Uniq_Ref")).value, str_ChangeStatusCode, BASE_XML_Template_DIR, _
								                                            temp_XML_Template, strFolderNameWithSlashSuffix_Scratch, objWS_MTS_GUIVerification,objWS_MTS_GUIVerification, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr, _ 
								                                            int_ctr_currentRow, gvntAr_RuleTable, gdictWStemplateXML_TagDataSrc_Tables_ColValue, now, int_UniqueReferenceNumberforXML, _
																			"MDP", Int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")


'								str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_Scenario_Name, str_ChangeStatusCode, BASE_XML_Template_DIR, _
'								                                            temp_XML_Template, strFolderNameWithSlashSuffix_Scratch, objWS_MTS_GUIVerification,objWS_MTS_GUIVerification, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr, _ 
'								                                            int_ctr_currentRow, gvntAr_RuleTable, gdictWStemplateXML_TagDataSrc_Tables_ColValue, now, int_UniqueReferenceNumberforXML, _
'																			"MDP", Int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
'
								
								
								strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
								
'								' Drop the XML in 
'								GetCATSDestinationFolder_V1 Environment.Value("COMPANY"),Environment.Value("ENV"),"MDP", gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
'								
								'---------------------------- START OF - Code to copy XML's to run specific folder -------------------------------

									' copy the PopulatedXML file from the temp folder to the input folder
									'copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")

									' copy the PopulatedXML file from the temp folder to the results folder
									copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, gFWstr_RunFolder  ' Environment.Value("CATSDestinationFolder")
									
									' ZIP XML File
									ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
							
									
									GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", "MDP", gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
									
									If Environment.Value("CATSDestinationFolder") = "<notfound>" Then
										str_WhetherFailure = "y"
										str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Cannot find outbox folder for Company("& Environment.Value("COMPANY") & ")-Environment("& Environment.Value("ENV") & ")-Application(B2B)-Role(MDP). Exiting row..."
									Else
										copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
					
										
								
											objDBRcdSet_TestData_A.MoveNext
										'wend
										
										'objDBRcdSet_TestData_A.Close
										'objDBConn_TestData_A.Close
										
										
										' Perform verifications in inbox
										str_CatsInboxFolder = replace(Environment.Value("CATSDestinationFolder"),"\outbox","\inbox")
										' Read from .ackfile Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip"
										
										wait 20 ' Intentional wait for ack to come
										
										    Set ack_objFSO = CreateObject("Scripting.FileSystemObject")
										    strScratch = Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".ack"
										    Set objFile_ack = ack_objFSO.OpenTextFile( str_CatsInboxFolder & strScratch, "1")
										    str_ackfile_all = objFile_ack.ReadAll
										    objFile_ack.Close
										    
											' check status="Reject"
											
											If instr(str_ackfile_all, "status=""Accept""") <= 0 Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "status=""Accept"" not found in ack file ("& str_CatsInboxFolder & strScratch &") "
											End if
											
											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
											' MTS screen verification

								wait 45 ' Intentional wait for process to finish
								
				    			Window("MTS_Window").WinMenu("Menu").Select "Transactions;Meter Data Received;Search Verify Meter Data Request Received"
								
								' WORKING HERE
						
								' temp_str_replacementvalue = strArr_SQL_RowData_columnValues ( dictSQL_keyColNames_itemArIndexNr( "NMI" ) )
								PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_srch").SetCellData "#1","req_id", "txnID_" & int_UniqueReferenceNumberforXML
								
								PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbButton("Display").Click
								
								' Check if its rejected
								
								' PbWindow("w_frame_3").Dialog("Dialog_continue").WinButton("Yes").Click
								
								' check number of records displayed
								
								If PbWindow("w_frame_3").Dialog("DialogMTS").Static("NoRecordsWereFoundForTheSpecifiedSearchCriteria").exist(2) then

									On Error Resume Next
									Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
	
									strRunLog_ScreenCapture_fqFileName = ""
									goStrBldr.clear
									strRunLog_ScreenCapture_fqFileName =  _
											goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
											gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
									
									call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName ,True) 'p2 is overwrite=true/false
 									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No record found for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ") - transaction received date from ("& objDBRcdSet_TestData_A.Fields("FILE_TIME_STAMP").Value & ")"
									On Error Goto 0
							    	
							    	PbWindow("w_frame_3").Dialog("DialogMTS").WinButton("OK").Click ' return the app to it's post-search usual-state
							    End if
							    
								if PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").RowCount > 0 Then
								
									For int_datagridcount = 1 To PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").RowCount 
										if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").GetCellData("#"& int_datagridcount,"investigation_code"))) = trim(lcase(temp_Investigation_Code)) _
										and trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").GetCellData("#"& int_datagridcount,"req_status_cd"))) = "rejected" Then
											int_temp_exit_row = "y"	
											
											PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").SelectCell "#"& int_datagridcount,"investigation_code"

											' take screenshot
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
			
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
											
											
											PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbButton("Details").Click
																	
											do while( PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").Exist(0) = false )
										    	intTotalWait = intTotalWait + 1
										        wait 1
										        ' If intTotalWait >= 180 Then 
										        If intTotalWait >= i_timetowait Then 
													str_WhetherFailure = "y"
													' str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for 180 seconds however the detailed screen was not displayed"
													str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Waited for "& i_timetowait &" seconds however the detailed screen was not displayed"
													Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
					
													strRunLog_ScreenCapture_fqFileName = ""
													goStrBldr.clear
													strRunLog_ScreenCapture_fqFileName =  _
															goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
															gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
													
													call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
													Exit Do
										        End if
										    loop	

											' take screenshot
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
			
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString
											
											call PbWindow("w_frame_3").CaptureBitmap ( strRunLog_ScreenCapture_fqFileName, True )
											
											Scratch = int_ctr_currentRow & "_" & temp_Scenario_Name & "_" & fnTimeStamp ( now , "yyyy`MMMmM`ddDdD_HH`mm`ss.t" )
			
											strRunLog_ScreenCapture_fqFileName = ""
											goStrBldr.clear
											strRunLog_ScreenCapture_fqFileName =  _
													goStrBldr.AppendFormat ("{0}{1}`{2}_{3}.png"					, _
													gFWstr_RunFolder, 	Parameter.Item("Environment"), Parameter.Item("Company"), Scratch).toString

											' Check correct investigation code
											if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").GetCellData("#1","INVESTIGATION_CODE"))) <> trim(lcase(temp_Investigation_Code)) Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Different investigation code on detail screen for Request ID (txnID_" & int_UniqueReferenceNumberforXML & "). Expected value was ("& temp_Investigation_Code&") whereas actual is ("& trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").GetCellData("#1","INVESTIGATION_CODE"))) &"))"
											End if
											
											' Check correct request status
											if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").GetCellData("#1","req_status_cd"))) <> "rejected" Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Different request status code on detail screen for Request ID (txnID_" & int_UniqueReferenceNumberforXML & "). Expected value was (REJECTED) whereas actual is ("& trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_md_detail").GetCellData("#1","req_status_cd"))) &"))"
											End if
											

											' Check correct status
											if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_bus_acc_rej").GetCellData("#1","bus_acc_rej_status"))) <> "reject" Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Different status on detail screen for Request ID (txnID_" & int_UniqueReferenceNumberforXML & "). Expected value was (Reject) whereas actual is ("& trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_bus_acc_rej").GetCellData("#1","bus_acc_rej_status"))) &"))"
											End if

											' Check correct event code
											if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_bus_acc_rej").GetCellData("#1","bus_rej_event_cd"))) <> "invalid request" Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Different event code on detail screen for Request ID (txnID_" & int_UniqueReferenceNumberforXML & "). Expected value was (invalid request) whereas actual is ("& trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_bus_acc_rej").GetCellData("#1","bus_rej_event_cd"))) &"))"
											End if


											' Check correct explanation
											if trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_bus_acc_rej").GetCellData("#1","bus_rej_explanation"))) <> "invalid investigation code supplied" Then
												str_WhetherFailure = "y"
												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Different explanation on detail screen for Request ID (txnID_" & int_UniqueReferenceNumberforXML & "). Expected value was (invalid investigation code supplied) whereas actual is ("& trim(lcase(PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbDataWindow("dw_bus_acc_rej").GetCellData("#1","bus_rej_explanation"))) &"))"
											End if

																						
											PbWindow("w_frame_3").PbWindow("w_verify_meter_data_req_recvd_detai").PbButton("Close").Click
											int_datagridcount = PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").PbDataWindow("dw_slct").RowCount + 1
											
										Else
											int_temp_exit_row = "n"
										End if
										
									Next
									
									If int_temp_exit_row = "n" Then
										str_WhetherFailure = "y"
										str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Not able to find a single record with investigation code (" & temp_Investigation_Code &") and Request Status (REJECTED) for Request ID (txnID_"& int_UniqueReferenceNumberforXML & ")"
									End If
								Else
									int_temp_exit_row = "y"
									str_WhetherFailure = "y"
									str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No records displayed on grid for NMI ("& objDBRcdSet_TestData_A.Fields("NMI").Value & ")"
								End if
								
						PbWindow("w_frame_3").PbWindow("w_verify_meter_data_rcvd_srch").Close


' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
' -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------											
											
'											' check Invalid Request
'											If instr(str_ackfile_all, "Invalid Request") <= 0 Then
'												str_WhetherFailure = "y"
'												str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "Invalid Request not found in ack file ("& str_CatsInboxFolder & strScratch &") "
'											End if
										
										
									End If
									
									
								'---------------------------- START OF - Code to copy XML's to run specific folder -------------------------------
										
						Else
							str_WhetherFailure = "y"
							str_Scratch_Error_Comment = str_Scratch_Error_Comment  & "No data returned by query (" & strQueryTemplate & ")"
						End If
						

					'End If

				End Select
			
			If str_WhetherFailure = "y" Then
				objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("PassORFail")).value = "FAIL"
				objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Comments")).value = str_Scratch_Error_Comment
			Else
				objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("PassORFail")).value = "PASS"
				objWS_MTS_GUIVerification.Cells(int_ctr_currentRow, gdictwsVefiry_MTS_GUI_Changes_KeyName_ColNr("Comments")).value = ""
			End If

		End If
		
		str_WhetherFailure = "n"
		str_Scratch_Error_Comment = ""
		objWB_Master.save
		
	Next
	
	fnMTS_WinClose

	objWB_Master.save
'	objWB_Master.close

Set cellTopLeft = nothing

Set qtResultsOpt = Nothing ' Release the Run Results Options object
set qtApp   = nothing
Set qtTest = Nothing ' Release the Test object 


' qq Save the UFT.RunLog to the RunFolder (or just configure the runlog.folder @ startOfScript, to the RunFolder ? - is that possible )

' ExitAction


' Once the test is complete, move the results to network drive
copyfolder  gFWstr_RunFolder , Cstr_B2BMeterData_15_Final_Result_Location