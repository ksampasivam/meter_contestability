﻿
Dim strSQL_TemplateName_Current, strSQL_TemplateName_Previous, intScratch, cellScratch

Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong

gfwint_DiagnosticLevel = 0 ' CHanging diagnostic level for debugging a particular row

gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

Dim bln_Scratch :  bln_Scratch = CBool(True)

Dim intColNr_InScope, intColNr_RowCompletionStatus
intColNr_InScope = CInt(-1) : intColNr_RowCompletionStatus = CInt(-1)

Dim str_rowScenarioID, intColNR_ScenarioID, temp_Next_ScenarioFunction_canStart_atOrAfter_TS

Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = True

Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
Dim thisCell
Dim strPhase, strStage
Dim gFWcell_xferNMI, tmp_Retailer_code, tmp_Retailer_Name
Dim strBarText, strBarText_Save, strMsg
Dim cStr_BarText_Format
Dim temp_DataPrep_CIS_SOType, temp_DataPrep_CIS_SOReason , temp_DataPrep_CIS_TaskType, str_Required_SP_Status_MTS, temp_NMI_List

'BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from GIT
BASE_XML_Template_DIR = cBASE_XML_Template_DIR


cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^RequestID`{4}^NMI`{5} ."

Dim strFolderNameWithSlashSuffix_Scratch
strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"


'			' ReDim gFwAr_strRequestID( cFWint_MaxNrOfProcesses_Required , cFWint_MaxNrOfRoles_Required ) ' no redim preserve required as the Ar has no values in it yet
'

strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciMetaData_Type_and_VersionNr	) = "is_FieldSetVerification_vn01" ' is_ is InformationSet	( 0)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldName						) = "<fn_UnInitialized>"								'	( 1)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldValue						) = "<fv_UnInitialized>"								'	( 2)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciAsOfTimestamp					) = "<aoTS_UnInitialized>"							'	( 3)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldType							) = "<ft_UnInitialized>"								'	( 4)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldVerificationPoint			) =  1													'	( 5)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciFieldSource						) = "gMTS`<tbd_qq>"									'	( 6)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare01					) = "<mt>"												'	( 7)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare02					) = "<mt>"												'	( 8)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare03					) = "<mt>"												'	( 9)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_spare04					) = "<mt>"												'	(10)
strAr_Single_VerifyKey_Fields_testCase_Defaults ( ciField_NameIndexList				) = _
",ciMetaData_Type_and_VersionNr`0,ciFieldName`1,ciFieldValue`2,ciAsOfTimestamp`3,ciFieldType`4,ciFieldVerificationPoint`5,ciFieldSource`6" & _
",ciField_spare01`7,ciField_spare02`8,ciField_spare03`9,ciField_spare04`10,ciField_NameIndexList`11,"						' 	(11)


'		On error resume next
'			strAr_Single_VerifyKey_Fields_local_Defaults	= scratchVariant
'		On error goto 0
strAr_Single_VerifyKey_Fields_local_Defaults	= strAr_Single_VerifyKey_Fields_testCase_Defaults	 
strAr_Single_VerifyKey_Fields 					= strAr_Single_VerifyKey_Fields_local_Defaults ' set the work-area strAr from the local defaults-strAr
strAr_Single_VerifyKey_Fields_logXML			= strAr_Single_VerifyKey_Fields_local_Defaults 


Set hFWobj_SB = DotNetFactory.CreateInstance( "System.Text.StringBuilder" )
Set gFWobj_SystemDate = DotNetFactory.CreateInstance( "System.DateTime" )

' push the RunStats to the DriverWS report-area
Dim cellTopLeft, iTS, iTS_Max, rowTL, colTL

Dim tsOfRun : tsOfRun = DateAdd("d", -0, Now()) ' qq

gDt_Run_TS = tsOfRun

Dim BASE_GIT_DIR : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"
Dim strCaseBaseAutomationDir 
' strCaseBaseAutomationDir = BASE_AUTOMATION_DIR
strCaseBaseAutomationDir  = BASE_GIT_DIR
'strCaseBaseAutomationDir = "r:\"

' ####################################################################################  UFT configurations at the start of test  - START ####################################################################################  

Dim Temp_Execution_Results_Location  : Temp_Execution_Results_Location = "C:\_data\Test_Execution_Results\"        'Share drive for AMI Energisation End to End project
Dim strRunLog_Folder

Dim qtTest, qtResultsOpt, qtApp


Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
If qtApp.launched <> True Then
	qtApp.Launch
End If 'end of If qtApp.launched <> True Then

qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
qtApp.Options.Run.RunMode = "Fast"
qtApp.Options.Run.ViewResults = False


Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539

Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
	str_AnFw_FocusArea = ""
Else
	str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
End If 'end of If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 

strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", Array(fnAttachStr(Temp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
gQtTest.Name                                 , _
Parameter.Item("Environment")                       , _
Parameter.Item("Company")                           , _
fnTimeStamp(gDt_Run_TS, "YYYY`MM`DD_HH`mm`ss")))
' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))

Path_MultiLevel_CreateComplete strRunLog_Folder
gFWstr_RunFolder = strRunLog_Folder

qtResultsOpt.ResultsLocation = gFWstr_RunFolder
gQtResultsOpt.ResultsLocation = strRunLog_Folder


'                                        
'                                        qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
'                                        qtApp.Options.Run.MovieCaptureForTestResults = "Never"
'                                        qtApp.Options.Run.MovieSegmentSize = 2048
qtApp.Options.Run.RunMode = "Fast"
'                                        qtApp.Options.Run.SaveMovieOfEntireRun = False
'                                        qtApp.Options.Run.StepExecutionDelay = 0
'                                        qtApp.Options.Run.ViewResults = False
qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
qtApp.Options.Run.ScreenRecorder.RecordSound = False
qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True

'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html

gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
gQtApp.Options.Run.ViewResults                = False



' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  

' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  

initialize_scratch_Dictionary
clear_scratch_Dictionary gdict_scratch

' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  


If 1 = 1  Then		
	
	
	gFwInt_DataParameterRow_ErrorsTotal = 0 : gFwInt_AbandonIteration_ReasonCount = 0
	gFWbln_ExitIteration = False
	
	Set gFWoDnf_SysDiags = Dotnetfactory.CreateInstance("System.Diagnostics.StackFrame")
	
	Dim MethodName
	On Error Resume Next
	' MethodName = new oDnf_SysDiags.StackTrace().GetFrame(0).GetMet hod().Name
	MethodName = oDnf_SysDiags.GetMethod().Name
	On Error Goto 0
	
	Dim micFinalStatus, intStageSuccessCount, intWantedSuccessCount : intStageSuccessCount = 0
	Dim ctSucc, intStageSuccessAr : intStageSuccessAr = Split(",0,0,0,0", ",") : intWantedSuccessCount = UBound(intStageSuccessAr)
	Dim BASE_XML_Template_DIR
	
	Dim intNrOfEventsToTrack    : intNrOfEventsToTrack      = 10
	Dim tsAr_EventStartedAtTS     : tsAr_EventStartedAtTS     = Split(String(intNrOfEventsToTrack-1, ","), ",")
	Dim tsAr_EventEndedAtTS       : tsAr_EventEndedAtTS       = Split(String(intNrOfEventsToTrack-1, ","), ",")
	Dim tsAr_EventPermutationsCount : tsAr_EventPermutationsCount = Split(Replace(String(intNrOfEventsToTrack-1, ","), ",", "0,")&"0", ",")
	
	Const iTS_Stage_0 = 0
	Const iTS_Stage_I = 1
	Const iTS_Stage_II = 2
	Const iTS_Stage_III = 3
	
	
	tsAr_EventStartedAtTS(iTS_Stage_0) = Now()
	
	Environment.Value("COMPANY")=Parameter.Item("Company")
	Environment.Value("ENV")=Parameter.Item("Environment")
	
	
	Dim intColNames_RowNr, intColNames_ColNr_Start, intColNames_ColNr_End
	Dim r_rtTemplate, intColNr_RowType, intColNr_DataCol_First, intColNr_DataCol_Last,  intColNr_ScenarioStatus
	Dim intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
	
	' Load the MasterWorkBook objWB_Master
	Dim strWB_noFolderContext_onlyFileName  
	strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
	
	Dim wbScratch
	
	Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
	Dim  int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr
	
	
	
	fnExcel_CreateAppInstance  objXLapp, True
	
	'Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"
	LoadData_RunContext_V3  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
	
	If LCase(Environment.Value("COMPANY")) = "sapn" Then
		Environment.Value("COMPANY_CODE")="ETSA"
	Else
		Environment.Value("COMPANY_CODE")=Environment.Value("COMPANY")
	End If 'end of If LCase(Environment.Value("COMPANY")) = "sapn" Then
	
	' ####################################################################################################################################################################################
	' #####################################################                 LOGGING STEP 1 - SETUP      #################################################################################################
	' ####################################################################################################################################################################################
	fn_setup_Test_Execution_Log Parameter.Item("Company"), Parameter.Item("Environment"), "N", "eConnect_B2B_SO", GstrRunFileName, "B2B_SO", strRunLog_Folder, gObjNet.ComputerName
	Printmessage "i", "Start of Execution", "Starting execution of eConnect_B2B_SO scripts"
	
	
	' qq 2017`01Jan`24Tue_16`33`22 BrianM Workaround
	Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
	Set wbScratch = Nothing
	
	
	objXLapp.Calculation = xlManual
	objXLapp.screenUpdating=False
	
	Dim objWS_DataParameter 						 
	Set objWS_DataParameter           					= objWB_Master.worksheets(Parameter("Worksheet")) ' wsMsDT_mrCntestbty_Objectn") ' qq this must become a parm
	objWS_DataParameter.activate
	
	Dim objWS_templateXML_TagDataSrc_Tables 	
	Set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
	Set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
	' Dim objWS_ScenarioOutComesValidations		: set objWS_ScenarioOutComeValidations 		= objWB_Master.worksheets("wsSpec_ScenarioOutcomeVerify") ' reference the Scenario ValidationRules WS
	
	Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
	Dim tempInt_RoleIdentificationCounter
	' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
	'	Get headers of table in a dictionary
	Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	
	
	gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
	Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
	gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
	' Load the first/title row in a dictionary
	int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
	
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	
	Dim intLogCol_Start, intLogCol_End,  intLogRow_Start, intLogRow_End 
	
	'	if logging is being done to an array, get the size of the range to be loaded to the array, and load it
	If gcFw_intLogMethod    =     gcFw_intLogMethod_RangeToWS Then	
		
		intLogRow_Start        =    gfwobjWS_LogSheet.range("rgWS_RowNr_Data_FirstMinusOne").row        +    1
		intLogRow_End         =    gfwobjWS_LogSheet.range("rgWS_RowNr_Data_LastPlusOne").row        -    1
		intLogCol_Start        =     gfwobjWS_LogSheet.range("rgWS_ColNr_Data_First_MinusOne").column    +    1
		intLogCol_End        =     gfwobjWS_LogSheet.range("rgWS_ColNr_Data_Last_PlusOne").column    -    1
		
		gfwAr_DriverSheet_Log_rangeFor_PullPush = _
		gfwobjWS_LogSheet.range ( gfwobjWS_LogSheet.cells ( intLogRow_Start, intLogCol_Start), gfwobjWS_LogSheet.cells(intLogRow_End, intLogCol_End) )
		intLogRow_End = intLogRow_End         ' qq for debugging    '        else            '    =    gcFw_intLogMethod_WSdirect
		
	End If 'end of If gcFw_intLogMethod    =     gcFw_intLogMethod_RangeToWS Then	
	
	Dim vntNull, mt
	
	MethodName = gFWoDnf_SysDiags.GetMethod().Name		
	objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
	objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
	'objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
	'objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
	objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= "0"
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
	objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = Now 
	
	
	' setAll_RequestIDs_Unique objWS_DataParameter
	
	'objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula = "'" & fnWindows_UserName(false)'
	'objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula = "'" & fnFW_GetComputerName()
	
	
	gDt_Run_TS = Now
	gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 
	
	Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE, cellReportStatus_SingleScenario
	'		set cellReportStatus_FwkProcessStage = objWS_DataParameter.range("rgWS_cellReportStatus_FwkProcessStage")
	'		set cellReportStatus_ROLE            = objWS_DataParameter.range("rgWS_cellReportStatus_ROLE")
	On Error Resume Next
	' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	'Set cellReportStatus_SingleScenario = objWS_DataParameter.range("rgWS_cellReportStatus_SingleScenario")
	On Error Goto 0
	
	
	'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
	Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
	dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
	Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
	intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
	intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
	intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
	fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
	
	'set cellTopLeft = objWS_DataParameter.range("rgWS_RunReport_TopLeftCell_Stage0_StartTS")
	' rowTL = cellTopLeft.row : colTL = cellTopLeft.column
	rowTL = 1 : colTL = 1
	
	Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
	
	Dim r
	Dim objDBConn_TestData_A, objDBRcdSet_TestData_A
	
	Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
	r = 0
	Set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")
	
	
	Dim objDBConn_TestData_B, objDBRcdSet_TestData_B
	Set objDBConn_TestData_B    = CreateObject("ADODB.Connection") : Set objDBRcdSet_TestData_B  = CreateObject("ADODB.Recordset") ': set objDBRcdSet_TestData_C  = CreateObject("ADODB.Recordset")
	Dim	DB_CONNECT_STR_MTS
	DB_CONNECT_STR_MTS = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
	Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
	Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
	Environment.Value("USERNAME") 	& 	";Password=" & _
	Environment.Value("PASSWORD") 	& ";"
	
	Dim objDBConn_TestData_C, objDBRcdSet_TestData_C
	Set objDBConn_TestData_C    = CreateObject("ADODB.Connection")
	Set objDBRcdSet_TestData_C  = CreateObject("ADODB.Recordset")
	
	
	Dim	strEnvCoy
	strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")
	
	Dim	DB_CONNECT_STR_CIS
	DB_CONNECT_STR_CIS = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
	Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
	Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
	Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
	Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"
	
	Dim objDBConn_TestData_TEL, objDBRcdSet_TestData_TEL
	Set objDBConn_TestData_TEL    = CreateObject("ADODB.Connection")
	Set objDBRcdSet_TestData_TEL  = CreateObject("ADODB.Recordset")
			
	DB_CONNECT_STR_TEL = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
	Environment.Value(strEnvCoy & "_TEL_HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
	Environment.Value(strEnvCoy & "_TEL_SERVICENAME") 	&	"))); User ID=" & _
	Environment.Value(strEnvCoy & "_TEL_USERNAME") 	& 	";Password=" & _
	Environment.Value(strEnvCoy & "_TEL_PASSWORD") 	& ";"
	
	'Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL") & "SORD\"
	
	Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
	Dim strQueryTemplate, dtD, Hyph, strNMI
	Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
	Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
	intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
	
	Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
	
	Dim intSQL_Pkg_ColNr, strSQL_RunPackage
	
	
	'					Dim intRowNr_DPs_SmallLarge : intRowNr_DPs_SmallLarge = objWS_DataParameter.range("rgWS_RowNr_DPs_SmallLarge").row
	
	Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
	intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
	intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
	
	intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
	
	
	'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
	intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
	sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
	
	Dim strColName
	
	intScratch = CInt (fnTimeStamp ( Now,  "yW" ) )
	If ((intScratch 	= 710) Or (intScratch 	= 711)) Then
		strColName 	= "InScope_YN"		  			
		'	\Select Case gobjNet.UserName 
		Select Case gobjNet.ComputerName
			Case "CORPVMUFT01z"		:	strColName 	= "qq"
			Case "CORPVMUFT02z" 	:	strColName 	= "qq"
			Case "PCA19323b"		 	:	strColName 	= "InScope_Preeti"
			Case "PCA19323c"		 	:	strColName 	= "InScope_Brian"
			Case Else	'	CORPVMUFT06	CORPVMUFT02 PCA19323
		End Select
		
	End If 'end of If ((intScratch 	= 710) Or (intScratch 	= 711)) Then
	objWS_DataParameter.calculate	'	to ensure the filename is updated
	
	
	
	Dim  str_xlBitNess : str_xlBitNess = objWS_DataParameter.range("rgWS_XL_BitNess_x32x64").value
	
	sb_File_WriteContents  _
	gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , True, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
	
	
	intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
	intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")
	
	Dim blnColumnSet : blnColumnSet = CBool(True)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
	If IsEmpty(intColNr_InScope) Then	
		blnColumnSet =False
	ElseIf intColNr_InScope = -1 Then
		blnColumnSet =False
	End If 'end of If IsEmpty(intColNr_InScope) Then	
	If Not blnColumnSet Then 
		'		qq log this	
		'		objXLapp.Calculation = xlManual
		objXLapp.screenUpdating=True
		exittest
	End If 'end of If Not blnColumnSet Then 
	
	objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
	'objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope
	'objWS_DataParameter.range("rgWS_Stage_Config_LocalOrGlobal") = "Local"			'		<<<===   should this always be the way the run-scope is managed ?
	objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").formula = "'" & intRowNr_LoopWS_RowNr_StartOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).formula = "'" & intRowNr_LoopWS_RowNr_FinishOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
	'		objWS_DataParameter.cells ( intRowNr_LoopWS_RowNr_StartOn  , intColNr_InScope ).select   '   qq reInstate ?? 
	objWS_DataParameter.calculate
	objxlapp.screenupdating = True
	'	objWS_DataParameter.Calculate
	
	objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
	
	tsAr_EventEndedAtTS(iTS_Stage_0) = Now()
	
	Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
	Dim  intMasterZoomLevel 
	
	Dim rowStart, rowEnd, colStart, colEnd
	Dim RangeAr 
	
	'objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
	
	
End If 'end of If 1 = 1  Then	

objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope


fnLog_Iteration  cFW_Exactly, 100 '  intRowNr_CurrentScenario ' cFW_Plus cFW_Minus 




Const gcInt_NrOf_OneBased_FlowFunctions = 10									'	"OneBased" means that q(0,n) will never be used operationally
Const gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction = 10		'	"OneBased" means that q(n,0) will never be used operationally
Dim	   gcInt_NrOf_Flows
Const cstrDelimiter = "," : Const cAllElements = -1
Const cStr_flowFnName_NoneProvidedYet = "flowFnName_NoneProvidedYet"
Const cStr_Template_dictionaryKey_ScenarioRow_FlowNrName_FlowStepNrName_ParameterNrName_BeforeAfter = "row`^flow`^step`^parm`^befAft`"

Dim q ()	'	this is the 2-D FlowFunction_ParameterArray; its name is "q" to keep the name as short as possible
Const c_StaticValue = "§"	'	this value : 	a) indicates that a flowFunction parameter is static, and 
'											b) is ASSUMED to be  value that will never be passed as a parameter to a FlowFunction
Dim qNm()				'	this is the parameterName array
Dim qBL()				'	this is the parameterBaseLine array
Dim fParmsCt()			'	this is the FunctionParmsCount array
Dim strAr_Flow_FunctionNames()		'	this is the FlowNr / FunctionList array
Dim intAr_Flow_nrOf_Functions_inThisFlow()		

Dim strParameterNameList_for_aSingle_FlowFunction

'	make the parameterArray large enough to handle all the FlowFunctions and all the parms for whichever FlowFunction has the most parms
ReDim 	q 				( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )
ReDim 	qNm			( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )
ReDim	qBL				( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )			
ReDim	fParmsCt		( gcInt_NrOf_OneBased_FlowFunctions                                                                                                                       )	
gcInt_NrOf_Flows = 16	      
ReDim	strAr_Flow_fNames						( gcInt_NrOf_Flows )	 	
ReDim intAr_Flow_nrOf_Functions_inThisFlow	( gcInt_NrOf_Flows )


Dim f, p: f= CInt(0) : p = CInt(0)

For f = 0 To gcInt_NrOf_OneBased_FlowFunctions 									'	iterate over the functionRows
	For p = 1 To gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction	'		iterate over the ParameterColumns
		If f = 0 Then 
			q(f , 0) = "!! It is a design constraint, that none of the cells, or columns, of the array, in THIS row-zero, are used, for any purpose, specifically, none of row zero is used, and " & _
			"the first column/element of rows one-and-later, contain the FlowFunction-Name for the row  !! "
		Else
			q(f , p) = c_StaticValue		'	parameters that are not static will have their c_StaticValue replaced with a variant that contains the variable value to be used
			'	this means that those cells must never be static in themselves, as they will be passed to FlowFunctions as ByRef parameters,
			' 		so that if the function changes the value, we can detect and log the fact, then later desk-debug what happened, without having to do a rerun
		End If 'end of If f = 0 Then 
	Next ' For p = 1 To gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction	'		iterate over the ParameterColumns
	'	set the FlowFunctionName equal to none for all function-parameter rows (as these will align with the case statement that processes the functions, below
	'		i.e. q(f,0) will contain the FlowFunctionName
	q(f , 0) = cStr_flowFnName_NoneProvidedYet
Next ' For f = 0 To gcInt_NrOf_OneBased_FlowFunctions 									'	iterate over the functionRows

Dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = CInt(0)	: intFirst= CInt(0)	: intSecond = CInt(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = CInt(1)	' qq



sb_File_WriteContents   	_
str_Fq_LogFileName, gfsoForAppending , True, _
fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf

Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun

tsStartOfRun = Now()

sb_File_WriteContents   	_
str_Fq_LogFileName, gfsoForAppending , False, _
fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf

Dim strRunScenario_PreScenarioRow_keyTemplate , strRunScenario_PreScenarioRow_Key
strRunScenario_PreScenarioRow_keyTemplate = _
"^RunNr`{0}^TestName`{1}^Envt`{2}^Coy`{3}^ScenarioList`^ScenarioChunk`^DriverSheetName`{4}^" ' 
'	Examples   1 2 3                              DEVMC      CITI                                                                                                              

strRunScenario_PreScenarioRow_Key  = fn_StringFormatReplacement ( _
strRunScenario_PreScenarioRow_keyTemplate , _
Array ( fnTimeStamp( Now(), 	cFWstrRequestID_StandardFormat) , gQtTest.Name  , Parameter.Item("Environment") , Parameter.Item("Company")  , objWS_DataParameter.name ) )

' the keys below must be set from inside the DataSheet-ScenarioRow processing loop
Dim strRunScenario_ScenarioRow_keyTemplate, strRunScenario_ScenarioRow_Key
strRunScenario_ScenarioRow_keyTemplate = "ScenarioRowNr`{0}^InfoType`FieldValue_Actual^FlowName`{1}^"
' examples                                                                                  1001 1100                                                                   Flow02
Dim str_FlowFunction_Seq_Name_templateKey , str_FlowFunction_Seq_Name_Key 
str_FlowFunction_Seq_Name_templateKey  = "FlowFunction_Seq_Name`{0}_{1}^"
'                                                                                                                                 1   DataMine
Dim str_DataContext_FieldName_templateKey,  str_DataContext_FieldName_Key
str_DataContext_FieldName_templateKey = "DataContext_System_AccessMethod_AccessPoint`{0}_{1}_{2}^FieldName`{3}^Field_ActualValue`inItem^"
'                                                                                                                      dcCIS_SQL_ap1  dcMTS_GUI_ServiceOrder                       NMI


Dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow


Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_Scenario : intRowNr_CurrentScenario = CInt(0) ' formerly cLng
Dim cInt_MinutesRequired_toComplete_eachScenario, cDbl_ExtraFactor_ToWait_for_RunToComplete, tsStopRunWhenRunningTooLong
Dim cInt_MinutesPerDay : cInt_MinutesPerDay = 24 * 60
Dim cInt_SecondsPerDay : cInt_SecondsPerDay = 60 * cInt_MinutesPerDay 
Dim int_rgWS_NumberOfIncompleteScenarios
Dim dt_rgWS_RunWillCeaseAt_TimeStamp : dt_rgWS_RunWillCeaseAt_TimeStamp = Now()-7
cInt_MinutesRequired_toComplete_eachScenario = CInt(5)
cDbl_ExtraFactor_ToWait_for_RunToComplete = CDbl(1.2)


Dim str_ScenarioRow_FlowName 
'Dim int_FlowHasNotStartedYet : int_FlowHasNotStartedYet =  objWS_DataParameter.range("rgWS_TestHasNotStarted").value
Dim cStr_FlowNumbers 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= CInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= CInt(-1)

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = False

Dim intColNr_Request_ID 
' intColNr_Request_ID 				= dictWSDataParameter_KeyName_ColNr("Request_ID")
intColNr_Request_ID 				= dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
intColNr_ScenarioStatus 			= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")
Dim intColNr_Flow_CurrentStepNr : intColNr_Flow_CurrentStepNr = dictWSDataParameter_KeyName_ColNr("Flow_CurrentStepNr") 
Dim strRole, strInitiator_Role, var_ScheduledDate, temp_INITIATOR, temp_INITIATOR_NAME

Dim strInScope, strRowCompletionStatus

objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=True
objWB_Master.save



gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

' Get headers of table in a dictionary
Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare



' Load the first/title row in a dictionary
int_MaxColumns = UBound(										gvntAr_RuleTable,2)

'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue

'fnMTS_WinClose
'OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")

' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
''	
' Reset error flags
fn_reset_error_flags
Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID

int_ctr_no_of_Eligible_rows = 0
int_ctr_total_Eligible_rows = 0
int_ctr_no_of_passed_rows = 0


' For i  = 1 to 1000 ' this loop would be decided on whether there are any remaining rows to be executed
i = 1
Do
	
	
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart To int_rgWS_cellReportStatus_rowEnd	
		
		
		strInScope 							= 	UCase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
		str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
		str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
		temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value
		
		
		' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
		
		If   strInScope = cY  And str_ScenarioRow_FlowName <> "" And LCase(str_Next_Function) <> "fail" And LCase(str_Next_Function) <> "end" Then
			
			If i = 1 Then
				If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 or InStr(1,LCase(temp_Scenario_ID),"step3") > 0 Then
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "WaitingForPrevStep"
				Else
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start"
					str_Next_Function = "start"
					fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
				End If 'end of If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 Then
				
				int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
				int_ctr_total_Eligible_rows = int_ctr_total_Eligible_rows + 1			
				objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows					
			End If ' end of If i = 1 Then
			
			' Generate a filename for screenshot file
			gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", Array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, Left(temp_Scenario_ID,10)))
			
			' ####################################################################################################################################################################################
			' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
			' ####################################################################################################################################################################################
			fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", "", "", "", "", "", ""
			
			Select Case LCase(str_ScenarioRow_FlowName)
				
				Case "flow1", "flow2","flow3":
				Do
					str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
					Select Case str_Next_Function
						Case "start" ' this is datamine
						' First function
						tsNow = Now() 
						tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
						If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							Exit Do
						End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
						
						
						If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
							
							PrintMessage "p", "Data mining", "Scenario row ("& intRowNr_CurrentScenario &") - Starting data mine"
							
							' capture CIS query
							strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SQL_Template_Name")).value 
							
							If strSQL_CIS <> ""  Then
								
								PrintMessage "p", "CIS Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_CIS  &")"
								strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
								
								If InStr(1, strSQL_CIS,"<Meter_Type_CIS>") > 0 Then
									strSQL_CIS = Replace ( strSQL_CIS , "<Meter_Type_CIS>" , objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Meter_Type_CIS") ).value, 1, -1, vbTextCompare)
								End If 
								
								
								strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
								strScratch = strScratch & vbCrLf & "CIS_SQL_Template_Name Query:" & vbCrLf & strSQL_CIS
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
								
								Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", dictWSDataParameter_KeyName_ColNr("NMI_LIST"),""
								
							
								
							End If ' end of If strSQL_CIS <> ""  Then
							
							
							' Capture MTS SQL
							strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_Template_Name")).value 
							If strSQL_MTS <> ""  Then	
								PrintMessage "p", "MTS Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_MTS  &")"
								strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
								
								' Perform replacements
								If InStr(1, strSQL_MTS,"<NMI_Size>") > 0 Then
									strSQL_MTS = Replace ( strSQL_MTS , "<NMI_Size>" , _
									objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI_Size") ).value, 1, -1, vbTextCompare)
								End If ' end of If InStr(1, strSQL_MTS,"<NMI_Size>") > 0 Then
								
								If InStr(1, strSQL_MTS,"<Deen_Method_MTS>") > 0 Then
									strSQL_MTS = Replace ( strSQL_MTS , "<Deen_Method_MTS>" , _
									objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_Deen_Method_MTS") ).value, 1, -1, vbTextCompare)
								End If ' end of If InStr(1, strSQL_MTS,"<Deen_Method_MTS>") > 0 Then
								
								If InStr(1, strSQL_MTS,"<Meter_Type_MTS>") > 0 Then
									strSQL_MTS = Replace ( strSQL_MTS , "<Meter_Type_MTS>", _
									objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Meter_Type_MTS") ).value, 1, -1, vbTextCompare)
								End If ' end of If InStr(1, strSQL_MTS,"<Meter_Type_MTS>") > 0 Then
								
								If InStr(1, LCase(strSQL_MTS),"<ami_rwd0or1>") > 0 Then
									strSQL_MTS = Replace ( strSQL_MTS , "<AMI_RWD0or1>", _
									objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("AMI_RWD0or1") ).value, 1, -1, vbTextCompare)
								End If	' end of If InStr(1, LCase(strSQL_MTS),"<ami_rwd0or1>") > 0 Then
								
								If InStr(1, (strSQL_MTS),"<NMI>") > 0 Then
									strSQL_MTS = Replace ( strSQL_MTS , "<NMI>", _
									objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI") ).value, 1, -1, vbTextCompare)								             				
								End If 'end of If InStr(1, (strSQL_MTS),"<NMI>") > 0 Then
								
								If InStr(1, strSQL_MTS,"<NMI_LIST>") > 0 Then
									If  objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI_LIST") ).value <>"" Then
										'strSQL_MTS = Replace ( strSQL_MTS , "<NMI_LIST>", objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI_LIST") ).value, 1, -1, vbTextCompare)		
										temp_NMI_List = objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI_LIST") ).value
										temp_NMI_List = Mid(temp_NMI_List,2,Len(temp_NMI_List)-1 )
										strSQL_MTS = Replace ( strSQL_MTS , "<NMI_LIST>", temp_NMI_List, 1, -1, vbTextCompare)
									Else
										PrintMessage "f", "Empty CIS Query", "NMI list is empty in ("& strSQL_MTS  &") "											
										' Generate error if the CIS query is not returning any results and if the quey template is empty												
										gFWbln_ExitIteration = True
										gFWstr_ExitIteration_Error_Reason = "Unable to run (" & strSQL_CIS & ") didn't return any row"
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"					           
									End If 'end of If objDBRcdSet_TestData_A.Fields ("CIS_NMI_LIST") <>"" Then
								End If ' end of If InStr(1, strSQL_MTS,"<NMI_LIST>") > 0 Then
								
								
								' Update query in "SQL_Populated" cell
								strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
								strScratch = strScratch & vbCrLf & "MTS_SQL_Template_Name Query:" & vbCrLf & strSQL_MTS
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
								
								' Execute MTS SQL
								Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI")
'							Else							
'								PrintMessage "i", "Scenario row ("& intRowNr_CurrentScenario &") - No MTS Query defined in cell: MTS_SQL_Template_Name"		
							End If ' end of If strSQL_MTS <> ""  Then	
							
							' Capture MTS - ECONNECT_DATAMINE SQL
							strSQL_MTS = ""
							strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_ECONNECT_DATAMINE_QUERY")).value 
							If strSQL_MTS <> ""  Then	
								PrintMessage "p", "MTS ECONNECT_DATAMINE Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_MTS  &")"
								strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
								
								If InStr(1, UCASE(strSQL_MTS),"<SCENARIO_NAME>") > 0 Then
									strSQL_MTS = Replace ( strSQL_MTS , "<SCENARIO_NAME>" ,MID(objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value,1,5) , 1, -1, vbTextCompare)
								End If 
								
								' Update query in "SQL_Populated" cell
								strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
								strScratch = strScratch & vbCrLf & "MTS ECONNECT_DATAMINE Query:" & vbCrLf & strSQL_MTS
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
								
								' Execute MTS SQL
								Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_TEL, objDBConn_TestData_TEL, objDBRcdSet_TestData_TEL, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI")
							
							End If ' end of If strSQL_MTS <> ""  Then	 for ECONNECT_DATAMINE SQL
							
							If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 Then
								
									objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = objWS_DataParameter.Cells( intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value 
									objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = objWS_DataParameter.Cells( intRowNr_CurrentScenario - 1 , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
									objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value = objWS_DataParameter.Cells( intRowNr_CurrentScenario - 1 , dictWSDataParameter_KeyName_ColNr("NMI")).value
									
							End If
							
							If InStr(1,LCase(temp_Scenario_ID),"step3") > 0  Then
								
								objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = objWS_DataParameter.Cells( intRowNr_CurrentScenario -2 , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value 
								objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = objWS_DataParameter.Cells( intRowNr_CurrentScenario - 2 , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
								objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value = objWS_DataParameter.Cells( intRowNr_CurrentScenario - 2 , dictWSDataParameter_KeyName_ColNr("NMI")).value
																	
							End If
								
							' Need to reserve NMI
							int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
							
							If int_NMI <> "" And gFWbln_ExitIteration = False Then 
								
								str_Required_SP_Status_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_MTS") ).value 
								str_Required_Deen_Method_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_Deen_Method_MTS") ).value 
								
								'Create_Find_CompleteSO str_ScenarioRow_FlowName, objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, str_Required_SP_Status_MTS, str_Required_Deen_Method_MTS, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A	

								' Reserve NMI in CIS
								If blnDataReservationIsActive_DefaultTrue Then
									fnKDR_Insert_Data_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
									fnKDR_Insert_Data_CIS_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									
									' ####################################################################################################################################################################################
									' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
									' ####################################################################################################################################################################################
									fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", int_NMI, "", "", "", "", ""						                  		
									PrintMessage "p", "Reserve NMI - CANDIDATE","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR as candidate"	
								End If 'end of If blnDataReservationIsActive_DefaultTrue Then
								
								' capture CIS Details query
								strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_Details_Query")).value 
								If strSQL_CIS <> ""  Then
									PrintMessage "p", "CIS Details Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_CIS  &")"
									strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
									' perform all replacements
									strSQL_CIS = Replace ( strSQL_CIS  , "<NMI>", objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI") ).value, 1, -1, vbTextCompare)
									
									' Update query in SQL Query - SQL_Populated cell
									strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
									strScratch = strScratch & vbCrLf & "CIS_Details_Query Query:" & vbCrLf & strSQL_CIS
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
									
									'Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("CIS_Details_Columns_Names"), dictWSDataParameter_KeyName_ColNr("CIS_Details_Columns_Values"),""
									Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"),  dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),""
									
								End If ' end of If strSQL_CIS <> ""  Then
							
							Else
								gFWbln_ExitIteration = True
								gFWstr_ExitIteration_Error_Reason = "NMI not written in row, may be some problem with QUERY"
							End If ' end of If int_NMI <> "" And gFWbln_ExitIteration = False Then 
							'End If 'end check related to MTS Query
							
							' At the end, check if any error was there and write in corresponding column
							If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
								Printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
								int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
								Exit Do
							Else
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML"
								Printmessage "p", "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
							End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
							
							strSQL_CIS = ""
							strSQL_MTS = ""
							
						End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
						'										
						Case "XML"
						
						tsNow = Now() 
						tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
						If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							Exit Do
						End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
						
						If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
							
							temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
							
							If temp_XML_Template  <> "" Then
								
								PrintMessage "p", "Starting B2BSO XML creation","Scenario row("& intRowNr_CurrentScenario &") - starting B2B XML creation with template ("& temp_XML_Template &")"
								
								int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
								objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")).value = int_UniqueReferenceNumberforXML
								wait(1)
								
								'objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss")  ' This would NOT be same as int_UniqueReferenceNumberforXML
								'objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss")  ' This would NOT be same as int_UniqueReferenceNumberforXML
								strScratch = objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("WhetherGenerate_Retailer_Service_OrderNo_YN")).value  
								If Trim(strScratch ) = "" Or Trim(LCase(strScratch)) = "y" Then
									objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss")  ' This would NOT be same as int_UniqueReferenceNumberforXML
								Else
									objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "RET_SERVICEORDERNUMBER")
								End If 'end of If Trim(strScratch ) = "" Or Trim(LCase(strScratch)) = "y" Then
								
								temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
								temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
								str_ChangeStatusCode = "B2BSO"
							
								
								' ####################################################################################################################################################################################
								' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
								' ####################################################################################################################################################################################
								fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", int_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""
								PrintMessage "p", "Populate XML","Scenario row("& intRowNr_CurrentScenario &") - starting XML creation for ("& str_ChangeStatusCode &") - NMI ("& int_NMI &") - Role ("& temp_Role &")"
								
								str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
								temp_XML_Template, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
								intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now,  int_UniqueReferenceNumberforXML, _
								temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
								
								strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
								PrintMessage "p", "XML File","Scenario row("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
								
								' copy the PopulatedXML file from the temp folder to the input folder
								' copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")
								
								' copy the PopulatedXML file from the temp folder to the results folder
								copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, gFWstr_RunFolder  ' Environment.Value("CATSDestinationFolder")
								
								' ZIP XML File
								ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
								PrintMessage "p", "Zippng XML File","Scenario row("& intRowNr_CurrentScenario &") - Zipping XML file for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &")"									            
								
								GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
								
								If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
									PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
									gFWbln_ExitIteration = "y"
									gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
								Else
									copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
									PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
									objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
								End If 'end of If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
								
'								'################# flow1 START ##################################################												
								If LCase(str_ScenarioRow_FlowName) = "flow1" Then
									
									wait(10)
									
									strSQL_MTS = ""
									strSQL_MTS = "MTS_EVENT_INSERT"
									PrintMessage "p", "MTS_EVENT_INSERT", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_MTS  &")"
									strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
									
									temp_INITIATOR = objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator")).value
									temp_INITIATOR_NAME = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, temp_INITIATOR)
									
									strSQL_MTS = Replace ( strSQL_MTS  , "<INITIATOR>", temp_INITIATOR_NAME ,  1, -1, vbTextCompare)
									strSQL_MTS = Replace ( strSQL_MTS  , "<INIT_TRANSACTION_ID>", objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")) ,  1, -1, vbTextCompare)
									strSQL_MTS = Replace ( strSQL_MTS  , "<PARAM_EVENT_GROUP>", objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataPrep_CIS_Action")) ,  1, -1, vbTextCompare)

									' Update query in SQL Query cell
									strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
									strScratch = strScratch & vbCrLf & "MTS_EVENT_INSERT Query:" & vbCrLf & strSQL_MTS
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False

									Execute_Insert_SQL   DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS
									
								End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow1" Then
'								'################# flow1b END ##################################################	
								
								 tmp_Retailer_code = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator") ).value)							
							
								strSQL_CIS = "Retailer_Names"
								strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
									
									' Perform replacements
								If InStr(1, strSQL_CIS,"<PARAM_RETAILER>") > 0 Then
									strSQL_CIS = Replace ( strSQL_CIS , "<PARAM_RETAILER>" , tmp_Retailer_code, 1, -1, vbTextCompare)
								End If 
								
								' Update query in "SQL_Populated" cell
								strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
								strScratch = strScratch & vbCrLf & "Retailer_Names Query:" & vbCrLf & strSQL_CIS
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
								
								' Execute CIS SQL
								Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2","" ,"" ,"" , ""
									
								If objDBRcdSet_TestData_A.State > 0 Then	
									If objDBRcdSet_TestData_A.RecordCount > 0  Then
										If Trim(LCase(objDBRcdSet_TestData_A.Fields("RETAILER_NAME"))) <> "" Then 
											tmp_Retailer_Name =  Trim(objDBRcdSet_TestData_A.Fields("RETAILER_NAME"))
										Else 
											tmp_Retailer_Name = "Not Found"
										End If
									Else
										temp_error_message =  "RETAILER NAME not found in CIS."
										gFWbln_ExitIteration = True
										gFWstr_ExitIteration_Error_Reason = temp_error_message
									End If
								Else 
										temp_error_message =  "RETAILER NAME not found in CIS."
										gFWbln_ExitIteration = True
										gFWstr_ExitIteration_Error_Reason = temp_error_message
								End If
								temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value
								
								If InStr(1,LCase(temp_Scenario_ID),"econnect") <=0 Then
									
									fn_eConnect_Datamine_Insert_Data Parameter.Item("Company"),  Parameter.Item("Environment"), intRowNr_CurrentScenario, _
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value, _
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CONNECTION_TYPE") ).value, _
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("workSubType") ).value, _
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI") ).value, _
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber") ).value, _
										tmp_Retailer_Name , tmp_Retailer_code, "available"
								
									PrintMessage "p", "eConnect_Datamine_Insert_Data complete", "Input for eConnect_Datamine inserted in table for row ("  & intRowNr_CurrentScenario &  ")" 
									
									If LCASE(TRIM(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Update_ECONNECT_DATAMINE_ROW") ).value)) ="y" THEN
										fn_eConnect_Datamine_update_Data "ALLOCATE NMI", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI") ).value, "READY" , "used"
									End if
								
								End If
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else											
									If LCase(str_ScenarioRow_FlowName) = "flow1" Then
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML Done"
										PrintMessage "p", "XML", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ") Continue manual check now" 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Exit Do
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SO_Status"										
										PrintMessage "p", "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")													
									End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow1" Then
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
							Else
								PrintMessage "p", "XML Template cell blank", "No XML template located in cell (xmlTemplate_Name) for row (" & intRowNr_CurrentScenario & ")" 
							End If ' end of If temp_XML_Template  <> "" Then
							
						End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
									
						
						Case "MTS_Check_SO_Status"
						
						' fn_compare_Current_and_expected_execution_time
						
						tsNow = Now() 
						tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
						If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							Exit Do
						End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
						
						int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
						
'						MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"first",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value, _
'						objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
'						objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value,"y", "n", "",""

						' Capture MTS SQL
						strSQL_MTS = "B2B_SO_Status_Check_MTS"
						strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
							
							' Perform replacements
						If InStr(1, strSQL_MTS,"<NMI>") > 0 Then
							strSQL_MTS = Replace ( strSQL_MTS , "<NMI>" , int_NMI, 1, -1, vbTextCompare)
						End If 
						
						If InStr(1, strSQL_MTS,"<RET_SERVICEORDERNUMBER>") > 0 Then
							strSQL_MTS = Replace ( strSQL_MTS , "<RET_SERVICEORDERNUMBER>" , objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value, 1, -1, vbTextCompare)
						End If 
						
						' Update query in "SQL_Populated" cell
						strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
						strScratch = strScratch & vbCrLf & "B2B_SO_Status_Check_MTS Query:" & vbCrLf & strSQL_MTS
						objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
						objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
						
						' Execute MTS SQL
						Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), ""

						If objDBRcdSet_TestData_B.State > 0 Then	
							If objDBRcdSet_TestData_B.RecordCount > 0  Then
								If Trim(LCase(objDBRcdSet_TestData_B.Fields ("SO_STATUS"))) = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value)) Then
									gFWbln_ExitIteration = False
									If  Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value)) <> "" Then 
										If Trim(LCase(objDBRcdSet_TestData_B.Fields ("AQ_NAME"))) = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value)) Then
											gFWbln_ExitIteration = False
										Else
											temp_error_message =  "Expected AQ Name: " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value & " Actual AQ NAME : " & objDBRcdSet_TestData_B.Fields ("AQ_NAME")
											gFWbln_ExitIteration = True
											gFWstr_ExitIteration_Error_Reason = "B2B SO Status Check Failed - " & temp_error_message
										End If
									End If
								Else
									temp_error_message =  "Expected AQ Status: " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value & " Actual B2B SO Status was: " & objDBRcdSet_TestData_B.Fields ("SO_STATUS")
									gFWbln_ExitIteration = True
									gFWstr_ExitIteration_Error_Reason = "B2B SO Status Check:- " & temp_error_message	
								End If 
							Else
								temp_error_message =  "B2B SO not found in MTS"
								gFWbln_ExitIteration = True
								gFWstr_ExitIteration_Error_Reason = "B2B SO Status Check:- " & temp_error_message	
							End If 'end of If objDBRcdSet_TestData_B.RecordCount > 0  Then
						Else
							temp_error_message =  "B2B SO not found in MTS."
							gFWbln_ExitIteration = True
							gFWstr_ExitIteration_Error_Reason = "B2B SO Status Check:- " & temp_error_message
						End If
						
						' At the end, check if any error was there and write in corresponding column
						If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_Check_SO_Status because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
							Printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
							int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
							Exit Do
						Else
							If LCase(str_ScenarioRow_FlowName) = "flow2" Then
								
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SO_Status complete"
								PrintMessage "p", "MTS_Check_SO_Status complete", "MTS_Check_SO_Status complete for row ("  & intRowNr_CurrentScenario &  ")" 
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
								' Function to write timestamp for next process
								fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
								int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
								Exit Do		
							Else 
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SO_Status complete"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML_Second"										
								PrintMessage "p", "MTS_Check_SO_Status complete. Starting with Second XMl prep for Cancel SO", "MTS_Check_SO_Status complete for row ("  & intRowNr_CurrentScenario &  ")" 
								' Function to write timestamp for next process
								fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")													
							End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow2" Then							
							
						End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
						
						Case "XML_Second"
						
						tsNow = Now() 
						tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
						If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							Exit Do
						End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
						
						If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
							
							temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
							
							If temp_XML_Template  <> "" Then
								
								int_UniqueReferenceNumberforXML = ""
								
								objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("actionType")).value = "Cancel"
								int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
								temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
								temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
								str_ChangeStatusCode = "B2BSO"
								
								
								' ####################################################################################################################################################################################
								' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
								' ####################################################################################################################################################################################
								fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", int_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""
								
								PrintMessage "p", "Starting Cancel XML creation","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &")"
								
								str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
								temp_XML_Template, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
								intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now, int_UniqueReferenceNumberforXML, _
								temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
								
								If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
						            		
						            		PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
						           
						            Else
						            
									strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
									PrintMessage "p", "XML File","Scenario row ("& intRowNr_CurrentScenario &") - XML file for ("& str_ChangeStatusCode &") - Role ("& temp_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
									
									' copy the PopulatedXML file from the temp folder to the input folder
									'copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")
									
									' copy the PopulatedXML file from the temp folder to the results folder
									copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, gFWstr_RunFolder  ' Environment.Value("CATSDestinationFolder")
									
									' ZIP XML File
									ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
									PrintMessage "p", "Zippng XML File","Scenario row ("& intRowNr_CurrentScenario &") - Zipping XML file ("& str_DataParameter_PopulatedXML_FqFileName &")"
									
									GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
									
									If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
										PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
										gFWbln_ExitIteration = "y"
										gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
									Else
										copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
										PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
										objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
									End If 'end of If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
								
								End If ' end of If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during Cancel XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " XML creation error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Cancel XML creation and placing on gateway complete"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SOstatus_isRaised_Second"
									PrintMessage "p", "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
							Else
								PrintMessage "p", "XML Template cell blank", "No XML template located in cell (xmlTemplate_Name) for row (" & intRowNr_CurrentScenario & ")" 
							End If ' end of If temp_XML_Template  <> "" Then
							
						End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
						
						
						Case "MTS_Check_SOstatus_isRaised_Second" 
						
						' fn_compare_Current_and_expected_execution_time
						
						tsNow = Now() 
						tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
						If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							Exit Do
						End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
						
						int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
						
						PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Scenario row ("& intRowNr_CurrentScenario &") - Last SO."
						'MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"last",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CancelSO_Status")).value, _
						'objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
						'objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value,"n", "n", "",""
						
						'PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Scenario row ("& intRowNr_CurrentScenario &") - First SO."
						'MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"first",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("PreviousSO_Status_InMTS_Updated")).value, _
						'objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
						'objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value,"n", "n", "",""
						
						' Capture MTS SQL
						strSQL_MTS = "B2B_SO_Status_Check_MTS"
						strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
							
							' Perform replacements
						If InStr(1, strSQL_MTS,"<NMI>") > 0 Then
							strSQL_MTS = Replace ( strSQL_MTS , "<NMI>" , int_NMI, 1, -1, vbTextCompare)
						End If 
						
						If InStr(1, strSQL_MTS,"<RET_SERVICEORDERNUMBER>") > 0 Then
							strSQL_MTS = Replace ( strSQL_MTS , "<RET_SERVICEORDERNUMBER>" , objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value, 1, -1, vbTextCompare)
						End If 
						
						' Update query in "SQL_Populated" cell
						strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
						strScratch = strScratch & vbCrLf & "B2B_SO_Status_Check_MTS Query:" & vbCrLf & strSQL_MTS
						objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
						objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
						
						' Execute MTS SQL
						Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), ""

						If objDBRcdSet_TestData_B.State > 0 Then	
							If objDBRcdSet_TestData_B.RecordCount > 0  Then
								If Trim(LCase(objDBRcdSet_TestData_B.Fields ("SO_STATUS"))) = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value)) Then
									gFWbln_ExitIteration = False
									If  Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value)) <> "" Then 
										If Trim(LCase(objDBRcdSet_TestData_B.Fields ("AQ_NAME"))) = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value)) Then
											gFWbln_ExitIteration = False
										Else
											temp_error_message =  "Expected AQ Name: " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value & " Actual AQ NAME : " & objDBRcdSet_TestData_B.Fields ("AQ_NAME")
											gFWbln_ExitIteration = True
											gFWstr_ExitIteration_Error_Reason = "B2B SO Status Check Failed - " & temp_error_message
										End If
									End If
								Else
									temp_error_message =  "Expected AQ Status: " & objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value & " Actual B2B SO Status was: " & objDBRcdSet_TestData_B.Fields ("SO_STATUS")
									gFWbln_ExitIteration = True
									gFWstr_ExitIteration_Error_Reason = "B2B SO Status Check:- " & temp_error_message	
								End If 
							Else
								temp_error_message =  "B2B SO not found in MTS"
								gFWbln_ExitIteration = True
								gFWstr_ExitIteration_Error_Reason = "B2B SO Status Check:- " & temp_error_message	
							End If 'end of If objDBRcdSet_TestData_B.RecordCount > 0  Then
						Else
							temp_error_message =  "B2B SO not found in MTS."
							gFWbln_ExitIteration = True
							gFWstr_ExitIteration_Error_Reason = "B2B SO Status Check:- " & temp_error_message
						End If
						
						' At the end, check if any error was there and write in corresponding column
						If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Check_SOstatus_isRaised_Second because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
							PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isRaised_Second error), moving to the next row", gFWstr_ExitIteration_Error_Reason
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
							int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
							Exit Do
						Else
							
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised_Second complete"
							PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"								
							int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
							int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1	                                           
							Exit Do

						End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the result
													
					
						Case "WaitingForPrevStep"
						
						temp_last_row_status = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value
						
						If LCase(temp_last_row_status ) = "pass" Then
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start"
							
						ElseIf LCase(temp_last_row_status ) = "fail" Then
							gFWbln_ExitIteration = True
							gFWstr_ExitIteration_Error_Reason =  "Row failed as Previous Step row failed"
							
							If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed as Previous Step row failed"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
								PrintMessage "f", "Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
								int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
							End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
							
						Else
							Exit Do
						End If 'end of If LCase(temp_last_row_status ) = "pass" Then
						
						
						Case "FAIL"
						Exit Do
					End Select 'end of Select Case str_Next_Function
					
					
					' Loop While  (lcase(gFWbln_ExitIteration) <> "n" or gFWbln_ExitIteration <> false)
				Loop While  (LCase(gFWbln_ExitIteration) = "n" Or gFWbln_ExitIteration = False)
				

				
				
				Case Else
					gFWbln_ExitIteration = "Y"
					gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
					PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
			End Select ' end of Select Case LCase(str_ScenarioRow_FlowName)
		End If ' If   strInScope = cY  And str_ScenarioRow_FlowName <> "" And LCase(str_Next_Function) <> "fail" And LCase(str_Next_Function) <> "end" Then
		
		' ####################################################################################################################################################################################
		' #####################################################                 LOGGING STEP 3 - Clear data before moving to next row #########################################################################
		' ####################################################################################################################################################################################
		fnTestExeLog_ClearRuntimeValues
		
		
		'		' Before resetting the flags, write the last error in comments column
		'		If gFWstr_ExitIteration_Error_Reason <> "" Then
		'			strScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value 
		'			strScratch = strScratch & " : " & gFWstr_ExitIteration_Error_Reason
		'			objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value = strScratch 
		'		End If
		'		
		' Reset error flags
		fn_reset_error_flags
		
		' clear scratch dictionary
		clear_scratch_Dictionary gdict_scratch
		
	Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
	
	objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
	
	fn_reset_error_flags
	
	i = 2
	objWB_Master.save ' Save the workbook 
	
Loop While int_ctr_no_of_Eligible_rows > 0

' Next ' end of For i  = 1 to 1000 

PrintMessage "i", "MILESTONE - Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & " ) - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= Now

objWB_Master.save ' Save the workbook
Printmessage "i", "MILESTONE - End of Execution", "Finished execution of B2BSO scripts"

' Once the test is complete, move the results to network drive
Printmessage "i", "MILESTONE - Copy to network drive", "Copying results to network drive ("& Cstr_B2BSO_Final_Result_Location &")"
copyfolder  gFWstr_RunFolder , Cstr_B2BSO_Final_Result_Location
'Window("z_Retired").Click 173,82 @@ hightlight id_;_65806_;_script infofile_;_ZIP::ssf80.xml_;_
