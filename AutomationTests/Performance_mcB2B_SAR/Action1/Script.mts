﻿
Dim intScratch
gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

' Dim bln_Scratch :  bln_Scratch = cbool(true)

dim intColNr_InScope, intColNr_RowCompletionStatus
intColNr_InScope = cint(-1) : intColNr_RowCompletionStatus = cint(-1)

Dim str_rowScenarioID, intColNR_ScenarioID

Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true

Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
Dim thisCell
Dim strPhase, strStage
Dim gFWcell_xferNMI 
Dim strBarText, strBarText_Save, strMsg
Dim cStr_BarText_Format
Dim temp_DataPrep_CIS_SOType, temp_DataPrep_CIS_SOReason , temp_DataPrep_CIS_TaskType, str_Required_SP_Status_MTS, temp_NMI_List

' BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from GIT
BASE_XML_Template_DIR = cBASE_XML_Template_DIR


cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^RequestID`{4}^NMI`{5} ."

Dim strFolderNameWithSlashSuffix_Scratch
strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"

' push the RunStats to the DriverWS report-area
Dim iTS, iTS_Max, rowTL, colTL
Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq

gDt_Run_TS = tsOfRun

Dim BASE_GIT_DIR : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"
Dim strCaseBaseAutomationDir 
strCaseBaseAutomationDir  = BASE_GIT_DIR

' ####################################################################################  UFT configurations at the start of test  - START ####################################################################################  

	Dim Temp_Execution_Results_Location  : Temp_Execution_Results_Location = "C:\_data\Test_Execution_Results\"        'Share drive for AMI Energisation End to End project
	Dim strRunLog_Folder
	
	dim qtTest, qtResultsOpt, qtApp
	
	
	Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
	If qtApp.launched <> True then
	    qtApp.Launch
	End If
	
	qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	qtApp.Options.Run.RunMode = "Fast"
	qtApp.Options.Run.ViewResults = False
	
	
	Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
	Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539


	Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
	str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
	If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
		str_AnFw_FocusArea = ""
	else
		str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
	End If
	
	strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", array(fnAttachStr(Temp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
	    gQtTest.Name                                 , _
	    Parameter.Item("Environment")                       , _
	    Parameter.Item("Company")                           , _
	    fnTimeStamp(gDt_Run_TS, "YYYY`MM`DD_HH`mm`ss")))
	    ' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))


	Path_MultiLevel_CreateComplete strRunLog_Folder
	gFWstr_RunFolder = strRunLog_Folder
	
	qtResultsOpt.ResultsLocation = gFWstr_RunFolder
	gQtResultsOpt.ResultsLocation = strRunLog_Folder
	

	qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
	qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
	qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
	qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
	qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
	qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
	qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
	qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
	qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
	qtApp.Options.Run.ScreenRecorder.RecordSound = False
	qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True
	
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	
	gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
	gQtApp.Options.Run.ViewResults                = False

	strFolderNameWithSlashSuffix_Scratch = strRunLog_Folder


' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  

' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  

initialize_scratch_Dictionary

'Set gdict_scratch = CreateObject("Scripting.Dictionary")
'gdict_scratch.CompareMode = vbTextCompare
'
clear_scratch_Dictionary gdict_scratch

' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  


 
	'  Description:-  Function to replace arguments in a string format
	'   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is 
	'						cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^CRCode`{4}^Phase`{5}^Role`{6}^Stage`{7}^RequestID`{8}^NMI`{9} ."
	'						The template should have arguments within {} and the numbering should s
	
	

					
	If 1 = 1  Then		
					
					
					    gFwInt_DataParameterRow_ErrorsTotal = 0 : gFwInt_AbandonIteration_ReasonCount = 0
					    gFWbln_ExitIteration = false
					
					    Set gFWoDnf_SysDiags = Dotnetfactory.CreateInstance("System.Diagnostics.StackFrame")
					
					      dim MethodName
					      On error resume next
					    ' MethodName = new oDnf_SysDiags.StackTrace().GetFrame(0).GetMet hod().Name
					      MethodName = oDnf_SysDiags.GetMethod().Name
					      On error goto 0
					
					
					
					
					
					' =========
					' =========  FrameworkPhase 00   - Setup the Stage-reporting elements =============
					' =========
					
					Dim micFinalStatus, intStageSuccessCount, intWantedSuccessCount : intStageSuccessCount = 0
					Dim ctSucc, intStageSuccessAr : intStageSuccessAr = split(",0,0,0,0", ",") : intWantedSuccessCount = uBound(intStageSuccessAr)
					' Dim BASE_XML_Template_DIR
					
					Dim intNrOfEventsToTrack    : intNrOfEventsToTrack      = 10
					Dim tsAr_EventStartedAtTS     : tsAr_EventStartedAtTS     = split(string(intNrOfEventsToTrack-1, ","), ",")
					Dim tsAr_EventEndedAtTS       : tsAr_EventEndedAtTS       = split(string(intNrOfEventsToTrack-1, ","), ",")
					Dim tsAr_EventPermutationsCount : tsAr_EventPermutationsCount = split(replace(string(intNrOfEventsToTrack-1, ","), ",", "0,")&"0", ",")
					
					const iTS_Stage_0 = 0
					const iTS_Stage_I = 1
					const iTS_Stage_II = 2
					const iTS_Stage_III = 3
					
					' =========
					' =========  FrameworkPhase 0a   - Expand the Template Rows into SingleScenario Rows  =============
					' =========
					
					
					
					tsAr_EventStartedAtTS(iTS_Stage_0) = now()
					
					Environment.Value("COMPANY")=Parameter.Item("Company")
					Environment.Value("ENV")=Parameter.Item("Environment")
					
					'   qq  moved to fnLib_MsWindows Dim gObjNet : Set gObjNet = CreateObject("WScript.NetWork") '  gObjNet.UserName gObjNet.ComputerName
					
					' Environment.Value("RUNCOUNT")=Parameter.Item("RunCount") ' not required
					
					
					    Dim intColNames_RowNr, intColNames_ColNr_Start, intColNames_ColNr_End
					    Dim r_rtTemplate, intColNr_RowType, intColNr_DataCol_First, intColNr_DataCol_Last,  intColNr_ScenarioStatus
					    Dim intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
					    
					    
					' Load the MasterWorkBook objWB_Master
					Dim strWB_noFolderContext_onlyFileName  
											' strWB_noFolderContext_onlyFileName = "Automation_Plan_Book_B2B_SO.xlsm"
											' strWB_noFolderContext_onlyFileName = "Automation_Plan_Book.xlsm"
											strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
					
					Dim wbScratch
					
					Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
					dim  int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr
					
					
					
					fnExcel_CreateAppInstance  objXLapp, true
					
					Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO" ' "MTS_FIND_MRIM_SO"
					LoadData_RunContext_V3  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
					

					' ####################################################################################################################################################################################
					' #####################################################                 LOGGING STEP 1 - SETUP      #################################################################################################
					' ####################################################################################################################################################################################
					fn_setup_Test_Execution_Log  Parameter.Item("Company"), Parameter.Item("Environment"),  "N", "SAR", GstrRunFileName, "ws_SAR", strRunLog_Folder, gObjNet.ComputerName
					Printmessage "i", "Start of Execution", "Starting execution of SAR"
					
								' qq 2017`01Jan`24Tue_16`33`22 BrianM Workaround
								Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
								Set wbScratch = nothing
								
								
								objXLapp.Calculation = xlManual
								'objXLapp.screenUpdating=False
					
					
					' load the DataParameter worksheet objWS_DataParameter - this contains the DataScenarios to test
					'
					' set objWS_DataParameter         = objWB_Master.worksheets("wsDT_MeterCntestblty_Objections")
					'set objWS_DataParameter            = objWB_Master.worksheets("wsSsDT_mrCntestbty_Objectns")
					
					' Dim objWB_Master , ' objWS_useInEach_ProcessStage <<== moved to automation framework
					
					Dim objWS_DataParameter 						 
					set objWS_DataParameter           					= objWB_Master.worksheets("ws_SAR") ' wsMsDT_mrCntestbty_Objectn") ' qq this must become a parm
					objWS_DataParameter.activate
					
					
					Dim objWS_templateXML_TagDataSrc_Tables 	
					set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
					set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
					' Dim objWS_ScenarioOutComesValidations		: set objWS_ScenarioOutComeValidations 		= objWB_Master.worksheets("wsSpec_ScenarioOutcomeVerify") ' reference the Scenario ValidationRules WS
					
					Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
					Dim tempInt_RoleIdentificationCounter
					' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
					gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

				'	Get headers of table in a dictionary
					Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
					gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
					
					
					gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
					Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
					gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
					' Load the first/title row in a dictionary
					int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
					fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
					
					
					int_MaxColumns = UBound(gvntAr_RuleTable,2)

					
					' Prepare the runLogAr for processing by loading it from the relevant worksheet
					    ' Set gfwobjWS_LogSheet = objWB_Master.worksheets("ws_RunLog")
					    Dim intLogCol_Start, intLogCol_End,  intLogRow_Start, intLogRow_End 
					    
					'	if logging is being done to an array, get the size of the range to be loaded to the array, and load it
					
					
					
					Dim vntNull, mt
							
							
					
					MethodName = gFWoDnf_SysDiags.GetMethod().Name
'	believed to be not-required for B2B_SO					
'									gFWrange_NMI_Spec   		= objWS_DataParameter.range("rgWS_dataNMI_Specification")
'									gFWrange_NMI_SpecResult 	= objWS_DataParameter.range("rgWS_dataNMI_SpecificationResult")
'	believed to be not-required for B2B_SO					
					objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
					objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
					objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
					objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
					objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = now 

					gDt_Run_TS = now
					gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 
					'setAll_RequestIDs_to     objWS_DataParameter, gFwTsRequestID_Common 

					Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE
					
					'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
					Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
					dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
					Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
					intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
					intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
					intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
					fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
					
					rowTL = 1 : colTL = 1
					
					'  <<<=========   expand the rows (already done)
					'                 expand the rows (already done) - in the objWS_DataParameter in the MasterWB
					'                 expand the rows (already done)
					'                 expand the rows (already done)
					'                 expand the rows (already done)
					' sb_rtTemplate_Expand objWS_DataParameter, dictWSDataParameter_KeyName_ColNr
					
					
					' objExcel.Application.Run "test.xls!sheet1.csi"
					' objXLapp.Run gStrWB_noFolderContext_only_RunVersion_FileName & "!sb_rtTemplate_Expand"     ' qqq <<<====   needs fixing, wouldn't run the macro
					
					' objXLapp.Calculation = xlAutomatic qq later, and ensure that    xlAutomatic     is defined
					
					
					
					' =========
					' =========  FrameworkPhase 0b   - Technical Setup for Running the SingleScenario Rows   =============
					' =========
					
					Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
					
					dim r
					Dim objDBConn_TestData_A, objDBRcdSet_TestData_A
					
'					On error resume next
		'	add more of these as-required, for querying databases
		'	add more of these as-required, for querying databases
					Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
					r = 0
					set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")
		'	add more of these as-required, for querying databases
		
		
					Dim objDBConn_TestData_B, objDBRcdSet_TestData_B
					Set objDBConn_TestData_B    = CreateObject("ADODB.Connection") : set objDBRcdSet_TestData_B  = CreateObject("ADODB.Recordset")


'	believed to be not-required for B2B_SO
	'					Dim strAr_listOldNew, intSize_listOldNew, str_listOldNew
'	believed to be not-required for B2B_SO
					'str_listOldNew = ucase(Parameter.Item("listOldNew")) ' for debugging
					'if left(str_listOldNew,1) <> "," then str_listOldNew = "," & str_listOldNew
					'Environment.Value("listOldNew") = str_listOldNew
					
					'strAr_listOldNew = split(Environment.Value("listOldNew"), ",")  ' this list will be one of :  old  new old,new  so we split the list with a comma to find out what permutations we're going to test
					'intSize_listOldNew = uBound(strAr_listOldNew)
					
					
					'fnWS_LoadKey_ColOffsets_toDictionary objWS_TestRunContext, 1, 1, 50, "", dictWSTestRunContext_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
			Dim	DB_CONNECT_STR_MTS
					DB_CONNECT_STR_MTS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
						Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
						Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
						Environment.Value("USERNAME") 	& 	";Password=" & _
						Environment.Value("PASSWORD") 	& ";"

			Dim	strEnvCoy
					strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")
					
					'	Available <Environment>_<Company>_<DataBaseConnectionParameters> are :
					'		Environment.Value( strEnvCoy & 	"_HOSTNAME"			)     
					'		Environment.Value( strEnvCoy & 	"_SERVICENAME"		)   	
					'		Environment.Value( strEnvCoy & 	"_USERNAME"			)     
					'		Environment.Value( strEnvCoy & 	"_PASSWORD"			)     
					'		Environment.Value( strEnvCoy &	 "_MTS_SO_INBOX"	)  	
					'		Environment.Value( strEnvCoy & 	"_MTS_SO_OUTBOX"	) 	
			Dim	DB_CONNECT_STR_CIS
					DB_CONNECT_STR_CIS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
						Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
						Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
						Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
						Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"

					' dim strTestName, uftapp : Set uftapp = CreateObject("QuickTest.Application")
					' strTestName = uftapp.Test.Name
					' Set uftapp = nothing
					
					' Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL") & "SORD\"
					
					Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
					Dim strQueryTemplate, dtD, Hyph, strNMI
					

					Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
					Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
					intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
					
					Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
					
					Dim intSQL_Pkg_ColNr, strSQL_RunPackage
					
					
					Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
					
					
					' qq this needs to change so that it is driven from a table which is a sibling to the run log table so that this per run/env/user/machine i.e. context-run config becomes table driven
						
						' Setup the SingleScenario row range to run over - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
						'                                                 - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
						'                                                 - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
						' Setup the SingleScenario row range to run over - qq this was done when TemplateRows had already been expanded to SingleScenarioRows by Stage 0; how will this be managed when Stage 0 is reinstated ?
						intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
						intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
						intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
					
								
							'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
						intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
						sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
					
						Dim strColName
						objWS_DataParameter.calculate	'	to ensure the filename is updated
									
									
					Dim temp_all_Error_Messages  : temp_all_Error_Messages = ""
					Dim  str_xlBitNess : str_xlBitNess = objWS_DataParameter.range("rgWS_XL_BitNess_x32x64").value
									
					sb_File_WriteContents  _
						gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
						fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
						Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
					
						  intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
						  intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")
					
						Dim blnColumnSet : blnColumnSet = cbool(true)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
						If IsEmpty(intColNr_InScope) then	
							blnColumnSet =false
						ElseIf intColNr_InScope = -1 then
							blnColumnSet =false
						End if
						If not blnColumnSet then 
					'		qq log this	
					'		objXLapp.Calculation = xlManual
							objXLapp.screenUpdating=True
							exittest
						End If
					
						objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
						objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope
						objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
						objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
						objWS_DataParameter.calculate
						objxlapp.screenupdating = true
					
						' objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
						
					tsAr_EventEndedAtTS(iTS_Stage_0) = now()
					
'					sbScenarioStageRole_OutcomeList_Verify_vn01 _
'						"acSetup"    , _
'						objWB_Master.worksheets("wsSpec_ScenarioOutcomeVerify").range("rgWsDyn_Table_ScenarioOutcome_Verify"),  _
'						mt, _
'						mt, mt, mt, mt, mt, _
'						gdict_FwkRun_DataContext_AllScenarios , mt, gDict_FieldName_ScenarioOutComeArrayRowNr , _
'						gvntAr_fwScenarioOutcome_Verify_tableAr, mt, mt,  mt 
						
					
					' =========
					' =========  FrameworkPhase 0c   - Setup the MultiStage Array                                           =============
					' =========
					
					Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
					Dim  intMasterZoomLevel 
					
					Dim rowStart, rowEnd, colStart, colEnd
					Dim RangeAr 
					
					
					'    ===>>>   Enable HotKeys
					' NOT NEEDED objWB_Master.Application.Run "HotKeys_Set"
					
					'    ===>>>   Make the RunBook Un-Shared, so that all later processes can run, esp the next, Expand, process
					' NOT NEEDED objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
					
					
	End if
	If 1 = 0  Then		

'	believed to be not-required for B2B_SO					
'	believed to be not-required for B2B_SO					
					
			'								' ===>>>   Expand the MasterSheet into the singleProcess Sheets
			'								  objWB_Master.Application.Run "unitTest_WS_MultiProcess_Expand" ' this populates 
			'								'																		  	objWS_DataParameter.Names("rgWsListDyn_MultiStageProcess_WorksheetNames").RefersToRange
			'								'																	  and from that
			'								'																			strAr_listProcessStage_WorkSheetNames
			'								
			'								' qq remove the below ??
			'								dim strList_Scratch, m, mMax
			'								Dim strAr_listProcessStage_WorkSheetNames, strAr_Scratch1, strAr_Scratch2, strAr_Scratch3, strAr_Scratch
			'								Dim strAr_listProcessStages_Relevant, strAr_listProcessStages_All
			'								' wsSpecAndData_Expand_CreateMultiStageWorkSheets ActiveWorkbook.Sheets("wsMsDT_mrCntestbty_Objectn")
			'								strAr_Scratch = objWS_DataParameter.Names("rgWsListDyn_MultiStageProcess_WorksheetNames").RefersToRange
			'								
			'								mMax = UBound(strAr_Scratch, 1)
			'								strAr_listProcessStage_WorkSheetNames = split ( string(mMax, ","), ",")
			'								For m = 1 to mMax
			'								  strAr_listProcessStage_WorkSheetNames(m) =  strAr_Scratch(m,1)
			'								Next
			'								
			'								
			'								strAr_Scratch = objWB_Master.Names("rgWbListDyn_SelectedProcess_AllStages").RefersToRange
			'								mMax = UBound(strAr_Scratch, 2)
			'								strAr_listProcessStages_All = split ( string(mMax, ","), ",")
			'								For m = 1 to mMax
			'								  strAr_listProcessStages_All(m) = strAr_Scratch(1,m)
			'								Next
			'								' qq remove the above ??
			'								
'
						'	qq below is the XML config setup for the MultiStage processes, this needs to change for the B2B_SO
						If 1 = 0  Then
					
								' create the row (StageNames) and column (XML Template and Tag names) dictionaries
								' and populate them with RowNr and ColNr for the XML_ColumnNames, and Process_RowNames, respectively
									Dim dictWsDP_XML_TemplateAndTag_Name_ColNr, dictWsDP_SingleProcess_Name_RowNr
									set dictWsDP_XML_TemplateAndTag_Name_ColNr 			= CreateObject("Scripting.Dictionary")
									set dictWsDP_SingleProcess_Name_RowNr 		 		= CreateObject("Scripting.Dictionary")
									dictWsDP_XML_TemplateAndTag_Name_ColNr.CompareMode 	= vbTextCompare
									dictWsDP_SingleProcess_Name_RowNr.CompareMode       = vbTextCompare
									
								intRowNr_CurrentScenario = 1115 ' qq - a) move to make this per-row for multi-stage  b) then review the loop structures inside multi-stage for efficiency
								objWB_Master.Names("rgWB_ProcessScenario_Selector").formula = _
								  "'" & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Scenario_ProcessPath") ).value
					

						End If

					Dim strStageIsOmitted              : strStageIsOmitted = objWB_Master.names("rgWBconst_isAnOmitted_ProcessStage").refersToRange.value
					objWS_DataParameter.calculate
					
					
	End If



					objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope
					
					
					fnLog_Iteration  cFW_Exactly, 100 '  intRowNr_CurrentScenario ' cFW_Plus cFW_Minus 
					
	
					
Const gcInt_NrOf_OneBased_FlowFunctions = 10									'	"OneBased" means that q(0,n) will never be used operationally
Const gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction = 10		'	"OneBased" means that q(n,0) will never be used operationally
dim	   gcInt_NrOf_Flows
Const cstrDelimiter = "," : const cAllElements = -1
Const cStr_flowFnName_NoneProvidedYet = "flowFnName_NoneProvidedYet"
Const cStr_Template_dictionaryKey_ScenarioRow_FlowNrName_FlowStepNrName_ParameterNrName_BeforeAfter = "row`^flow`^step`^parm`^befAft`"

Dim q ()	'	this is the 2-D FlowFunction_ParameterArray; its name is "q" to keep the name as short as possible
Const c_StaticValue = "§"	'	this value : 	a) indicates that a flowFunction parameter is static, and 
'											b) is ASSUMED to be  value that will never be passed as a parameter to a FlowFunction
Dim qNm()				'	this is the parameterName array
Dim qBL()				'	this is the parameterBaseLine array
Dim fParmsCt()			'	this is the FunctionParmsCount array
Dim strAr_Flow_FunctionNames()		'	this is the FlowNr / FunctionList array
Dim intAr_Flow_nrOf_Functions_inThisFlow()		

Dim strParameterNameList_for_aSingle_FlowFunction

'	make the parameterArray large enough to handle all the FlowFunctions and all the parms for whichever FlowFunction has the most parms
ReDim 	q 				( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )
ReDim 	qNm			( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )
ReDim	qBL				( gcInt_NrOf_OneBased_FlowFunctions , gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction )			
ReDim	fParmsCt		( gcInt_NrOf_OneBased_FlowFunctions                                                                                                                       )	
                                                        				 	  gcInt_NrOf_Flows = 16	      
ReDim	strAr_Flow_fNames						( gcInt_NrOf_Flows )	 	
ReDim intAr_Flow_nrOf_Functions_inThisFlow	( gcInt_NrOf_Flows )


'	f is a row-Function-index, p is a column-Parameter-index, both into the ParameterArray q() and ParameterName array qNm()

Dim f, p: f= cint(0) : p = cint(0)

For f = 0 To gcInt_NrOf_OneBased_FlowFunctions 									'	iterate over the functionRows
	For p = 1 To gcInt_MaxNrOf_OneBased_Parameters_inAnyOne_FlowFunction	'		iterate over the ParameterColumns
		If f = 0 then 
			q(f , 0) = "!! It is a design constraint, that none of the cells, or columns, of the array, in THIS row-zero, are used, for any purpose, specifically, none of row zero is used, and " & _
					  "the first column/element of rows one-and-later, contain the FlowFunction-Name for the row  !! "
		else
			q(f , p) = c_StaticValue		'	parameters that are not static will have their c_StaticValue replaced with a variant that contains the variable value to be used
										'	this means that those cells must never be static in themselves, as they will be passed to FlowFunctions as ByRef parameters,
										' 		so that if the function changes the value, we can detect and log the fact, then later desk-debug what happened, without having to do a rerun
		End If
	next
'	set the FlowFunctionName equal to none for all function-parameter rows (as these will align with the case statement that processes the functions, below
'		i.e. q(f,0) will contain the FlowFunctionName
	q(f , 0) = cStr_flowFnName_NoneProvidedYet
next


		'			gFWstr_RunFolder = _
		'				"\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\DataSheets\runs\GUITest21_POC_FnRet_v0.02_DEVMC_CITI_@CsSktLanLib_2017`03Mar`wk10`11Sat_20`43`32\"


dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = cint(0)	: intFirst= cint(0)	: intSecond = cint(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = cint(1)	' qq



sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , true, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf

	

'	determine the number of in-scope rows
Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun

tsStartOfRun = now()


'	qq     intNrOfRows_Inscope_and_Unfinished

sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , false, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf






' -------------------------------------------------------------------------------
'	Setup the global_DataContextDictionary here

'	the lines below are in      fnLib_TestData_Mgt.qfl
'	Dim 	gdict_FwkRun_DataContext_AllScenarios
'	 Set		gdict_FwkRun_DataContext_AllScenarios = CreateObject("Scripting.Dictionary")  
'	        	gdict_FwkRun_DataContext_AllScenarios.CompareMode = vbTextCompare
        	
Dim strRunScenario_PreScenarioRow_keyTemplate , strRunScenario_PreScenarioRow_Key
	 strRunScenario_PreScenarioRow_keyTemplate = _
		"^RunNr`{0}^TestName`{1}^Envt`{2}^Coy`{3}^ScenarioList`^ScenarioChunk`^DriverSheetName`{4}^" ' 
'	Examples   1 2 3                              DEVMC      CITI                                                                                                              

strRunScenario_PreScenarioRow_Key  = fn_StringFormatReplacement ( _
		strRunScenario_PreScenarioRow_keyTemplate , _
			Array ( fnTimeStamp( now(), 	cFWstrRequestID_StandardFormat) , gQtTest.Name  , Parameter.Item("Environment") , Parameter.Item("Company")  , objWS_DataParameter.name ) )

' the keys below must be set from inside the DataSheet-ScenarioRow processing loop
	Dim strRunScenario_ScenarioRow_keyTemplate, strRunScenario_ScenarioRow_Key
	strRunScenario_ScenarioRow_keyTemplate = "ScenarioRowNr`{0}^InfoType`FieldValue_Actual^FlowName`{1}^"
	' examples                                                                                  1001 1100                                                                   Flow02
	Dim str_FlowFunction_Seq_Name_templateKey , str_FlowFunction_Seq_Name_Key 
	str_FlowFunction_Seq_Name_templateKey  = "FlowFunction_Seq_Name`{0}_{1}^"
	'                                                                                                                                 1   DataMine
	Dim str_DataContext_FieldName_templateKey,  str_DataContext_FieldName_Key
	str_DataContext_FieldName_templateKey = "DataContext_System_AccessMethod_AccessPoint`{0}_{1}_{2}^FieldName`{3}^Field_ActualValue`inItem^"
	'                                                                                                                      dcCIS_SQL_ap1  dcMTS_GUI_ServiceOrder                       NMI


dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow

'	now setup to do process the DriverSheet


Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_Scenario : intRowNr_CurrentScenario = cInt(0) ' formerly cLng
Dim cInt_MinutesRequired_toComplete_eachScenario, cDbl_ExtraFactor_ToWait_for_RunToComplete, tsStopRunWhenRunningTooLong
Dim cInt_MinutesPerDay : cInt_MinutesPerDay = 24 * 60
Dim cInt_SecondsPerDay : cInt_SecondsPerDay = 60 * cInt_MinutesPerDay 
Dim dt_rgWS_RunWillCeaseAt_TimeStamp : dt_rgWS_RunWillCeaseAt_TimeStamp = now()-7
cInt_MinutesRequired_toComplete_eachScenario = cInt(5)
cDbl_ExtraFactor_ToWait_for_RunToComplete = cdbl(1.2)

Dim temp_Notification_Status, temp_Notification_Sent_YN
	                
	                
Dim str_ScenarioRow_FlowName 
Dim cStr_FlowNumbers 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= cInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= cInt(-1)

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = false

Dim intColNr_Request_ID 
' intColNr_Request_ID 				= dictWSDataParameter_KeyName_ColNr("Request_ID")
intColNr_Request_ID 				= dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
 intColNr_ScenarioStatus 			= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")
 DIm intColNr_Flow_CurrentStepNr : intColNr_Flow_CurrentStepNr = dictWSDataParameter_KeyName_ColNr("Flow_CurrentStepNr") 
 Dim strRole, strInitiator_Role, var_ScheduledDate

Dim strInScope, strRowCompletionStatus

objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=true
objWB_Master.save





	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
		' Get headers of table in a dictionary
	Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	

	
		' Load the first/title row in a dictionary
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	
'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue
	

'	qq	the code below was in the CatsWigs_MultiStage test case, but can be found nowhere in this one , is this significant ? 		<<===  BrianM 2017`03Mar`w11`15Wed
		'		fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table
		'	int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)



' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch applications at startup - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
'' Launch MTS
'fnMTS_WinClose
'OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")

initialize_performance_Dictionary

' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
''	
' Reset error flags
fn_reset_error_flags
Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID

Dim temp_no_of_times, int_Ctr_No_Of_Times


int_ctr_no_of_Eligible_rows = 0
int_ctr_total_Eligible_rows = 0
int_ctr_no_of_passed_rows = 0
int_Ctr_No_Of_Times = 0 


' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Update the Configuration so that SANs are generated in 5 minutes - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
strQueryTemplate = fnRetrieve_SQL("SAR_IN_Configuration_Variable")
												
If LCASE(strQueryTemplate) <> "fail" Then
	Execute_Insert_SQL   DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strQueryTemplate 
	fn_reset_error_flags ' clear errors incase there was any error during updating configuration
End If
' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Update the Configuration so that SANs are generated in 5 minutes - END xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx



i = 1
Do
	
	
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	' Umesh - This need to be reviewed during execution
	
									'	intRowNr_LoopWS_RowNr_StartOn   qq
	
		'	if the scenarioRow is InScope-and-Incomplete                                                                                                                 
		strInScope 							= 	ucase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
		str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
		str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
		temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value
		temp_no_of_times			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NO_OF_TIMES") ).value
		
		If temp_no_of_times = "" Then
			temp_no_of_times = 1
		End If
		
		For int_Ctr_No_Of_Times = 1 To temp_no_of_times
	
		
		' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
		
			If   strInScope = cY  and str_ScenarioRow_FlowName <> "" and lcase(str_Next_Function) <> "fail" and lcase(str_Next_Function) <> "end" Then
			
				if i = 1 Then
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
					int_ctr_total_Eligible_rows = int_ctr_no_of_Eligible_rows
					objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
					
					If ucase(Parameter.Item("RunMode")) = "N" Then ' Value can be N- New or R - restart
						objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start-SQL"
						str_Next_Function = "start-SQL"
					End If
					fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
				End If
			
				' Generate a filename for screenshot file
				gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, left(temp_Scenario_ID,10)))

				' ####################################################################################################################################################################################
				' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
				' ####################################################################################################################################################################################
				fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", "", "", "", "", "", ""

				' We need to clear NMI and SQL columns for performance runs
				objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = ""
				objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )) = ""
				objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )) = ""
				
				
			Select Case lcase(str_ScenarioRow_FlowName)
				
				' ################################################################# START of first flow ####################################################################################################
				' ################################################################# START of first flow ####################################################################################################
				Case "flow1":
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
								Select Case str_Next_Function
									Case "start-SQL" ' this is datamine
										' First function
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										IF tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
											PrintMessage "P", "Data Mine", "Starting with Datamining"
										
											' capture MTS query
											strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_Template_Name")).value 

											If strSQL_MTS <> ""  Then
												PrintMessage "p", "Data Mine", "MTS query - " & strSQL_MTS
												' Capture MTS SQL
										        ' intSQL_ColNr           				= dictWSTestRunContext_KeyName_ColNr( strSQL_MTS )  
										        ' strQueryTemplate       				= objWS_TestRunContext.cells(2,intSQL_ColNr).value
										        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
												
												If LCASE(strQueryTemplate) <> "fail" Then
													
													
													strSQL_MTS = strQueryTemplate
													
														' Perform replacements
										            	strSQL_MTS = replace ( strSQL_MTS , "<Meter_Type>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Meter_Type")).value , 1, -1, vbTextCompare)
										            	strSQL_MTS = replace ( strSQL_MTS , "<NMI_Size>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI_SIZE")).value, 1, -1, vbTextCompare)
										            	strSQL_MTS = replace ( strSQL_MTS , "<From_Role>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("From_Role")).value, 1, -1, vbTextCompare)
										            	strSQL_MTS = replace ( strSQL_MTS , "<To_Role>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("To_Role")).value, 1, -1, vbTextCompare)
										            	
										            	
										            	If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Parent_Scenario_ID"))) <> "" Then
										            		strSQL_MTS = replace ( strSQL_MTS , "<Parent_Scenario_ID>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Parent_Scenario_ID")).value, 1, -1, vbTextCompare)
										            	End If
										            	
										            	
													' Execute MTS SQL
													strScratch = Execute_SQL_Populate_DataSheet_handlecommainSQLvalue(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
													
													If strScratch = "PASS" Then
														' Reserve NMI
														str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
														
															' First check if we need to generate a checksum for NMI
															
															If instr(1,str_NMI, "GenerateCS",1) > 0 or instr(1,str_NMI, "GenerateValidCS",1) > 0 or instr(1,str_NMI, "GenerateInvalidCS",1) > 0  Then
																strScratch = mid(str_NMI,1, instr(1,str_NMI,"Generate",1)-1)
																str_NMI = calculateChecksum(strScratch)
																objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = str_NMI
																																
																strScratch = fn_Update_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value , objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value , ",", "NMI", str_NMI)
																'Set it back to the driversheet
																objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = strScratch
	
															End If 'end of If instr(1,str_NMI, "GenerateCS") > 0 or instr(1,str_NMI, "GenerateValidCS") > 0 or instr(1,str_NMI, "GenerateInvalidCS") > 0  Then


														PrintMessage "p", "NMI Reserve", "Reserving NMI ("& str_NMI &")"
														' ####################################################################################################################################################################################
														' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
														' ####################################################################################################################################################################################
														fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", str_NMI, "", "", "", "", ""
														
														fnKDR_Insert_Data_V1 "n", "n",str_NMI, temp_Scenario_ID, "available"
													End If
												
												End If ' end of If LCASE(strQueryTemplate) <> "fail" Then

											End If ' end of check related to query

											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "micFail", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML_Creation"
												PrintMessage "p", "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
			            					End If
											strSQL_CIS = ""
											strSQL_MTS = ""
											
										End If ' end of IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										
									Case "XML_Creation"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
			
											temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
											
									        If temp_XML_Template  <> "" Then
									            
									            int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
									            objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID") ).value = int_UniqueReferenceNumberforXML
									            temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
									            temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
									            str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
									            str_ChangeStatusCode = "SAR"
									            
									            ' Populate the runtime requestid in csListSqlColumn_Names and csListSqlColumn_Values cells

											' ####################################################################################################################################################################################
											' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
											' ####################################################################################################################################################################################
											fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID,  "",  "", str_NMI,   int_UniqueReferenceNumberforXML,  temp_Role, "", "", ""


		            							' concatenate names
											strScratch = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names") ).value
											strScratch = strScratch & "," & "XML_RUNTIME_REQUEST_ID"
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names") ).value = strScratch
											
											' concatenate values
											strScratch = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values") ).value
											strScratch = strScratch & ",txnID_" & trim(int_UniqueReferenceNumberforXML)
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values") ).value = strScratch
									            
									            str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
									                                                        temp_XML_Template, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
									                                                        intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, now,  int_UniqueReferenceNumberforXML, _
									                                                        temp_Role, str_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
									
									            strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
									            
									            If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
									            	PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
									            Else
									            
										            ' copy the PopulatedXML file from the temp folder to the input folder
										            ' copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")
										
										            ' copy the PopulatedXML file from the temp folder to the results folder
										            copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, gFWstr_RunFolder  ' Environment.Value("CATSDestinationFolder")
										            
										            ' ZIP XML File
										            ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
										                    
										            GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
				
													If Environment.Value("CATSDestinationFolder") = "<notfound>" or Environment.Value("CATSDestinationFolder") = "" Then
														printmessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
														gFWbln_ExitIteration = "y"
													    gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													Else
														' copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
														store_value_performance_Dictionary gdict_XMLFilename_DestinationFolder, Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", Environment.Value("CATSDestinationFolder")
														printmessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
														objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
													End If
									            
									            	
									            End If
												
												' At the end, check if any error was there and write in corresponding column
												If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
													printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
													int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
													Exit do
												Else
													If int_Ctr_No_Of_Times = temp_no_of_times  Then
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
														int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
														int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
													Else
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "start-SQL"
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
														int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
														int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
													End If
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation complete"
													PrintMessage "p", "MILESTONE - XML creation complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
													' Function to write timestamp for next process
													fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
													Exit Do
													
				            					End If
															
									        Else
												printmessage "p", "XML Template cell blank", "No XML template located in cell (xmlTemplate_Name) for row (" & intRowNr_CurrentScenario & ")" 
												' What needs to happen in this case? Marknig as fail
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
												
									        End If ' end of If temp_XML_Template  <> ""  Then
			
											
										End If ' end of IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									

									case "SAR_TRANS_ACK_CHECK"
									
											If trim(lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Ack_File_Status")).value)) <> "delayed" and _
											trim(lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Request_Status")).value)) <> "delayed" Then
											
													tsNow = now() 
													tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
													IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
														Exit do
													End if
													
													' capture MTS query to check trans ack
													strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_SAR_Trans_ack_Template_Name")).value 
		
													If strSQL_MTS <> ""  Then
													
												        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
														
														If LCASE(strQueryTemplate) <> "fail" Then
															
															
															strSQL_MTS = strQueryTemplate
															
																' Perform replacements
												            	strSQL_MTS = replace ( strSQL_MTS , "<INIT_TRANSACTION_ID>", fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","XML_RUNTIME_REQUEST_ID") , 1, -1, vbTextCompare)
												            	
												            	' Update query in SQL Query cell
												            	strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
												            	strScratch = strScratch & vbCrLf & "SAR_TRANS_ACK_CHECK Query:" & vbCrLf & strSQL_MTS
												            	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
				        										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
				        										
															' Execute MTS SQL
															strScratch = Execute_SQL_Populate_DataSheet_handlecommainSQLvalue(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "")
															
															objWB_Master.save
															
															' Verify output of query
															' Code
															If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","CODE")) _
															= trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Code")).value) Then
																PrintMessage "p", "XML Ack Verification",  "Actual value of Code (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","CODE")) & ") is same as expected value ("& trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Code")).value) &")"
															Else
																fn_set_error_flags true, "Actual value of Code (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","CODE")) & ") is DIFFERENT to expected value ("& trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Code")).value) &")"
																PrintMessage "f", "XML Ack Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
															' Description
															If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","DESCRIPTION")) _
															= trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Description")).value) Then
																PrintMessage "p", "XML Ack Verification",  "Actual value of Description (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","DESCRIPTION")) & ") is same as expected value ("& trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Description")).value) &")"
															Else
																fn_set_error_flags true, "Actual value of Description (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","DESCRIPTION")) & ") is DIFFERENT to expected value ("& trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Description")).value) &")"
																PrintMessage "f", "XML Ack Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
															' Explanation
															If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","EXPLANATION")) _
															= trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Explanation")).value) Then
																PrintMessage "p", "XML Ack Verification",  "Actual value of Explanation (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","EXPLANATION")) & ") is same as expected value ("& trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Explanation")).value) &")"
															Else
																fn_set_error_flags true, "Actual value of Explanation (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","EXPLANATION")) & ") is DIFFERENT to expected value ("& trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Explanation")).value) &")"
																PrintMessage "f", "XML Ack Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
															' Status
															If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","STATUS")) _
															= trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Ack_File_Status")).value) Then
																PrintMessage "p", "XML Ack Verification",  "Actual value of STATUS (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","STATUS")) & ") is same as expected value ("& trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Ack_File_Status")).value) &")"
															Else
																fn_set_error_flags true, "Actual value of STATUS (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","STATUS")) & ") is DIFFERENT to expected value ("& trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Ack_File_Status")).value) &")"
																PrintMessage "f", "XML Ack Verification",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
															End If
														
															If gFWbln_ExitIteration Then
																gFWstr_ExitIteration_Error_Reason = temp_all_Error_Messages
																temp_all_Error_Messages = ""
															End If ' end of If gFWbln_ExitIteration Then
														
														End If ' end of If LCASE(strQueryTemplate) <> "fail" Then
		
													End If ' end of check related to query
													
													
													' At the end, check if any error was there and write in corresponding column
													If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML - ACK verification  because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
														printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
														int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
														Exit do
													Else
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML - ACK verification complete"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SAN_OUT_XML_CHECK"
														printmessage "p", "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
														' Function to write timestamp for next process
														fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_SevenMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
						            					End If ' end of If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											Else
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML - ACK verification not in scope for request status delayed"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SAN_OUT_XML_CHECK"
														printmessage "p", "XML - ACK verification not in scope for request status delayed", "XML - ACK verification not in scope for request status delayed"
														' Function to write timestamp for next process
														fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_SevenMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")											
											End If 'end of If trim(lcase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Ack_File_Status")).value)) = ""  Then


									case "SAN_OUT_XML_CHECK"
											tsNow = now() 
											tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
											IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
												Exit do
											End if
											
											' capture MTS query to check trans ack
											strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_SAN_OUT_XML_Template_Name")).value 

											If strSQL_MTS <> ""  Then
											
										        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
												
												If LCASE(strQueryTemplate) <> "fail" Then
													
													strSQL_MTS = strQueryTemplate
														' Perform replacements
										            	strSQL_MTS = replace ( strSQL_MTS , "<INIT_TRANSACTION_ID>", fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","XML_RUNTIME_REQUEST_ID") , 1, -1, vbTextCompare)
										            	
										            	' Update query in SQL Query cell
										            	strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										            	strScratch = strScratch & vbCrLf & "SAN_OUT_XML_CHECK Query:" & vbCrLf & strSQL_MTS
										            	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
		        										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
										            	
										            		If lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Verification_Notification_Sent_YN") ).value))  = "y" Then
																' Execute MTS SQL
																strScratch = Execute_SQL_Populate_DataSheet_handlecommainSQLvalue(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "")
															
																If strScratch = "PASS" Then
																	
																	objWB_Master.save
			
																		If lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Verification_Notification_Sent_YN") ).value))  = "n" Then
																			' No file should be sent
																			If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FILENAME")) <> "" Then
																				fn_set_error_flags true, " An output file with the name ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FILENAME"))  &") is generated for SAR OUT scenario. This is not the expected value as no file should be generated"
																				PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
																				temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
																			End If
																		Else
																			' file should be sent
																			If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FILENAME")) = "" Then
																				fn_set_error_flags true, " An output file (SAN XML) is not generated. This is not the expected value as no file should be generated"
																				PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
																				temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
																			End If
																			' Verify TO_MARKET_PART
																			If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_MARKET_PART")) = _
																			trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_XML"))  Then
																				PrintMessage "p", "SAN_OUT_XML_CHECK step", " TO_MARKET_PART ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_MARKET_PART"))  &") is same as FROM_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_XML"))   &")"
																			Else
																				fn_set_error_flags true, " TO_MARKET_PART ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_MARKET_PART"))  &") is DIFFERENT FROM FROM_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_XML"))   &")"
																				PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
																				temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
																			End If
																			' Verify FROM_MARKET_PART
																			If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_MARKET_PART")) = _
																			trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_XML"))  Then														
																				PrintMessage "p", "SAN_OUT_XML_CHECK step", " FROM_MARKET_PART ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_MARKET_PART"))  &") is same as TO_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_XML"))   &")"
																			Else
																				fn_set_error_flags true, " FROM_MARKET_PART ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_MARKET_PART"))  &") is DIFFERENT FROM TO_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_XML"))   &")"
																				PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
																				temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
																			End If

																			' Verify FROM_SAN_XML
																			If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_SAN_XML")) = _
																			trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_XML"))  Then														
																				PrintMessage "p", "SAN_OUT_XML_CHECK step", " FROM_SAN_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_SAN_XML"))  &") is same as TO_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_XML"))   &")"
																			Else
																				fn_set_error_flags true, " FROM_SAN_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_SAN_XML"))  &") is DIFFERENT FROM TO_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_XML"))   &")"
																				PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
																				temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
																			End If

																			' Verify TO_SAN_XML
																			If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_SAN_XML")) = _
																			trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_XML"))  Then														
																				PrintMessage "p", "SAN_OUT_XML_CHECK step", " TO_SAN_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_SAN_XML"))  &") is same as FROM_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_XML"))   &")"
																			Else
																				fn_set_error_flags true, " TO_SAN_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","TO_SAN_XML"))  &") is DIFFERENT FROM FROM_XML ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_XML"))   &")"
																				PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
																				temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
																			End If

																			' Verify SAN_ACCESSDETAIL
																			If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","SAN_ACCESSDETAIL")) = _
																			trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","ACCESS_DETAILS"))  Then														
																				PrintMessage "p", "SAN_OUT_XML_CHECK step", " SAN_ACCESSDETAIL ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","SAN_ACCESSDETAIL"))  &") is same as ACCESS_DETAILS ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","ACCESS_DETAILS"))   &")"
																			Else
																				fn_set_error_flags true, " SAN_ACCESSDETAIL ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","SAN_ACCESSDETAIL"))  &") is DIFFERENT FROM ACCESS_DETAILS ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","ACCESS_DETAILS"))   &")"
																				PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
																				temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
																			End If
' Lower case check for the time being, till the defect is fixed
' Lower case check for the time being, till the defect is fixed
' Lower case check for the time being, till the defect is fixed
' Lower case check for the time being, till the defect is fixed
																			' Verify SAN_HAZARD - 
																			If lcase(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","SAN_HAZARD"))) = _
																			lcase(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","HAZARD")))  Then														
																				PrintMessage "p", "SAN_OUT_XML_CHECK step", " SAN_HAZARD ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","SAN_HAZARD"))  &") is same as HAZARD ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","HAZARD"))   &")"
																			Else
																				fn_set_error_flags true, " SAN_HAZARD ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","SAN_HAZARD"))  &") is DIFFERENT FROM HAZARD ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","HAZARD"))   &")"
																				PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
																				temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "
																			End If
																			
																			If gFWbln_ExitIteration Then
																				gFWstr_ExitIteration_Error_Reason = temp_all_Error_Messages
																				temp_all_Error_Messages = ""
																			End If ' end of If gFWbln_ExitIteration Then

' SAN_LASTMODIFIEDDATE commented because of defect
' SAN_LASTMODIFIEDDATE commented because of defect
' SAN_LASTMODIFIEDDATE commented because of defect
'																			' Verify SAN_LASTMODIFIEDDATE
'																			If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","SAN_LASTMODIFIEDDATE")) = _
'																			trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","LASTMODIFIEDDATE"))  Then														
'																				PrintMessage "p", "SAN_OUT_XML_CHECK step", " SAN_LASTMODIFIEDDATE ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","SAN_LASTMODIFIEDDATE"))  &") is same as LASTMODIFIEDDATE ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","LASTMODIFIEDDATE"))   &")"
'																			Else
'																				fn_set_error_flags true, " SAN_LASTMODIFIEDDATE ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","SAN_LASTMODIFIEDDATE"))  &") is DIFFERENT FROM LASTMODIFIEDDATE ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","LASTMODIFIEDDATE"))   &")"
'																				PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
'																			End If
'

				'															' Verify Process_Step - Commented after discussion with Surya
				'															If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","process_step")) = _
				'															trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Notification_Status" )).value)  Then														
				'																PrintMessage "p", "SAN_OUT_XML_CHECK step", " Process Step ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","process_step"))  &") is same as Verification_Notification_Status ("& trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Notification_Status" )).value)  &")"
				'															Else
				'																fn_set_error_flags true, " Process Step ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","process_step"))  &") is DIFFERENT as Verification_Notification_Status ("& trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Notification_Status" )).value)  &")"
				'																PrintMessage "f", "SAN_OUT_XML_CHECK step", gFWstr_ExitIteration_Error_Reason
				'															End If
					
																		End If ' end of If lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Verification_Notification_Sent_YN") ).value))  = "n" Then
			
																End If ' end of If strScratch = "PASS" Then
														
													End If ' end of If lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Verification_Notification_Sent_YN") ).value))  = "y" Then

												End If ' end of If LCASE(strQueryTemplate) <> "fail" Then

											End If ' end of If strSQL_MTS <> ""  Then
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at SAN_OUT_XML_CHECK because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SAN_OUT_XML_CHECK complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "DROP_SAN_ACK_XML"
												printmessage "p", "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
				            					End If
			            					

									case "DROP_SAN_ACK_XML"
											tsNow = now() 
											tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
											IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
												Exit do
											End if


										If tsNow >= tsNext_ScenarioFunction_canStart_atOrAfter Then
										
											If lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Verification_Notification_Sent_YN") ).value))  = "y" Then
	
												temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_SAN_ACK")).value
													
											        If temp_XML_Template  <> "" Then
											            
											            ' int_UniqueReferenceNumberforXML = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","CODE"))
											            int_UniqueReferenceNumberforXML = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Runtime_RequestID") ).value
											            temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
											            temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
											            str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
											            str_ChangeStatusCode = "SAR"
											            
											            ' Populate the runtime requestid in csListSqlColumn_Names and csListSqlColumn_Values cells
											            
											            str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
											                                                        temp_XML_Template, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
											                                                        intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, now,  int_UniqueReferenceNumberforXML, _
											                                                        temp_Role, str_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
											
											            strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
											            
											            If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											            	PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
											            Else
											            
												            ' copy the PopulatedXML file from the temp folder to the input folder
												            ' copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, BASE_XML_Template_DIR & "runs\"  ' Environment.Value("CATSDestinationFolder")
												
												            ' copy the PopulatedXML file from the temp folder to the results folder
												            copyfile strScratch, strFolderNameWithSlashSuffix_Scratch, gFWstr_RunFolder  ' Environment.Value("CATSDestinationFolder")
												            
												            ' ZIP XML File
												            ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
												                    
												            GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
						
														If Environment.Value("CATSDestinationFolder") = "<notfound>" or Environment.Value("CATSDestinationFolder") = "" Then
															printmessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
															gFWbln_ExitIteration = "y"
														    gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
														Else
															copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
															printmessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
															objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
														End If
											            	
											            End If ' end of  If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											            
														' At the end, check if any error was there and write in corresponding column
														If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
															objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at SAN_OUT_XML_CHECK because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
															printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
															int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
															Exit do
														Else
															objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "DROP_SAN_ACK_XML complete"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_SAR_Screen_Verification"
															printmessage "p", "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
															' Function to write timestamp for next process
															fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
							            					End If
											        Else
														printmessage "p", "XML Template cell blank", "No XML template located in cell (xmlTemplate_Name) for row (" & intRowNr_CurrentScenario & ")" 
														' What needs to happen in this case? Marknig as fail
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
														printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
														int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
														Exit do
														
											        End If ' end of If temp_XML_Template  <> ""  Then
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "DROP_SAN_ACK_XML not applicable"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_SAR_Screen_Verification"
												printmessage "p", "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												
											End if ' end of If lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Verification_Notification_Sent_YN") ).value))  = "y" Then
											        
										End If ' end of IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									

									case "MTS_SAR_Screen_Verification"
										
											' fn_compare_Current_and_expected_execution_time
											
											tsNow = now() 
											tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
											IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
												Exit do
											End if
											
											str_NMI = left(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value,10)
											
											fn_MTS_MenuNavigation "Transactions;Customer / Site Details;Search Site Access Request Received"
											
											fn_MTS_Search_Site_Access_Request_Received str_NMI, fnTimeStamp(now, "DD/MM/YYYY"), fnTimeStamp(now, "DD/MM/YYYY"), "", "", "", "",  "y", "y", gFW_strRunLog_ScreenCapture_fqFileName
											
											Dim temp_Reason_Code, temp_Request_Status, temp_Request_ID
											
											temp_Reason_Code = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Reason_Code" )).value
											temp_Request_ID = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Runtime_RequestID" )).value
											temp_Request_Status = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Request_Status" )).value
											temp_Notification_Status = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Notification_Status" )).value
											temp_Notification_Sent_YN = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Notification_Sent_YN" )).value
											temp_SpecialNotes = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Special_Notes" )).value
											temp_Verification_Description = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Description" )).value
											temp_Verification_Explanation = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Explanation" )).value
											temp_Verification_Request_Status = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_Request_Status" )).value
											temp_Access_Details = fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","ACCESS_DETAILS") 
											temp_from_participant = fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FROM_XML") 
											
											If lcase(trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Verification_Notification_Sent_YN") ).value))  = "y" Then
												temp_NotificationFilename = fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","FILENAME") 
												temp_HAZARD = fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","HAZARD") 
												temp_LASTMODIFIEDDATE = fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","LASTMODDATEGUI") 
												
												' Verify on grid
												fn_MTS_Search_Site_Access_Request_Received_SelectandVerify_Record_Results  "", "txnID_" & temp_Request_ID,    fnTimeStamp(now, "DD/MM/YYYY"), str_NMI,  temp_from_participant, temp_Reason_Code, temp_Request_Status, temp_Notification_Sent_YN,  temp_Notification_Status,   fnTimeStamp(now, "DD/MM/YYYY"),   temp_NotificationFilename, "Y", gFW_strRunLog_ScreenCapture_fqFileName

												' Click Details Button - Activate would happen in previous function
												'fn_MTS_Click_button_any_screen "Search_Site_Access_Request_Received", "btn_Details"
												
												' Verify on Details screen
												fn_MTS_Search_Site_Access_Request_Received_Details_Screen_Verification  "txnID_" & temp_Request_ID,  temp_Reason_Code,   fnTimeStamp(now, "DD/MM/YYYY"), temp_SpecialNotes, temp_from_participant, str_NMI, temp_Verification_Description, temp_Verification_Request_Status,  temp_Verification_Explanation, temp_Notification_Sent_YN, fnTimeStamp(now, "DD/MM/YYYY"), temp_Notification_Status, temp_NotificationFilename, temp_Access_Details, temp_HAZARD, temp_LASTMODIFIEDDATE, "Y", gFW_strRunLog_ScreenCapture_fqFileName

											Else
												temp_NotificationFilename = ""
												temp_HAZARD = ""
												temp_LASTMODIFIEDDATE = ""
												
												' Verify on grid
												fn_MTS_Search_Site_Access_Request_Received_SelectandVerify_Record_Results  "", "txnID_" & temp_Request_ID,    fnTimeStamp(now, "DD/MM/YYYY"), str_NMI,  temp_from_participant, temp_Reason_Code, temp_Request_Status, temp_Notification_Sent_YN,  temp_Notification_Status,   "",   temp_NotificationFilename, "Y", gFW_strRunLog_ScreenCapture_fqFileName

												' Click Details Button - Activate would happen in previous function
												'fn_MTS_Click_button_any_screen "Search_Site_Access_Request_Received", "btn_Details"
												
												' Verify on Details screen
												fn_MTS_Search_Site_Access_Request_Received_Details_Screen_Verification  "txnID_" & temp_Request_ID,  temp_Reason_Code,   fnTimeStamp(now, "DD/MM/YYYY"), temp_SpecialNotes, temp_from_participant, str_NMI, temp_Verification_Description, temp_Verification_Request_Status,  temp_Verification_Explanation, temp_Notification_Sent_YN, "<blank>", temp_Notification_Status, temp_NotificationFilename, temp_Access_Details, temp_HAZARD, temp_LASTMODIFIEDDATE, "Y", gFW_strRunLog_ScreenCapture_fqFileName

											End If	
											
											
											
											' close details MTS - SAR
											fn_MTS_Close_any_screen "Search_Site_Access_Request_Received_Details"
											
											' close screen MTS - SAR
											fn_MTS_Close_any_screen "Search_Site_Access_Request_Received"
											
											temp_Reason_Code = ""
											temp_Request_Status = ""
											temp_Request_ID = ""
											temp_Notification_Status = ""
											temp_Notification_Sent_YN = "" 
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_Check_SOstatus_isRaised because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_SAR_Screen_Verification complete"
												printmessage "p", "CIS_Check_SOstatus_isRaised complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												Exit Do
			            					End If
											

										case "FAIL"
											Exit Do
								End Select
					Loop While  (lcase(gFWbln_ExitIteration) = "n" or gFWbln_ExitIteration = false)

				' ################################################################# END of first flow ####################################################################################################
				' ################################################################# END of first flow ####################################################################################################


				Case else ' Incase there is no elaboration for a flow, we need to report in datasheet and move on to next row
				    gFWbln_ExitIteration = "Y"
				    gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
					printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
			End Select ' end of 	Select Case str_ScenarioRow_FlowName
		
		End If ' end of If  (	( strInScope = cY)  and str_ScenarioRow_FlowName <> "" and (trim(str_Next_Function) = "" or lcase(str_Next_Function) <> "fail" or lcase(str_Next_Function) <> "end") ) Then

'		' Before resetting the flags, write the last error in comments column
'		If gFWstr_ExitIteration_Error_Reason <> "" Then
'			strScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value 
'			strScratch = strScratch & " : " & gFWstr_ExitIteration_Error_Reason
'			objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value = strScratch 
'		End If
'
		' ####################################################################################################################################################################################
		' #####################################################                 LOGGING STEP 3 - Clear data before moving to next row #########################################################################
		' ####################################################################################################################################################################################
		fnTestExeLog_ClearRuntimeValues


		' Reset error flags
		fn_reset_error_flags
		
		' first write the data of scratch dictionary in excel
		
		' clear scratch dictionary
		clear_scratch_Dictionary gdict_scratch
		Next 'end of For int_Ctr_No_Of_Times = 1 To temp_no_of_times
	Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	

	objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows

	' Reset error flags
	fn_reset_error_flags

	i = 2
	objWB_Master.save ' Save the workbook 
Loop While int_ctr_no_of_Eligible_rows > 0


' Once all the XML's are created, now we need to copy them to gateway in blocks..
Performance_Copy_XML_Files_In_Batches gFWstr_RunFolder, 10, 20


PrintMessage "i", "Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & " ) - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= now

objWB_Master.save ' Save the workbook 

' fnMTS_WinClose

' Once the test is complete, move the results to network drive
copyfolder  gFWstr_RunFolder , Cstr_SAR_Final_Result_Location
