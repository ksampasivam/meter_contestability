﻿'
'
'msgbox fn_compare_String_LengthandContent("XML", "Field1","exact","umesh  anand", "umesh anand", "y")
'

Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong

Dim intScratch
gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

' Dim bln_Scratch :  bln_Scratch = cbool(true)

dim intColNr_InScope, intColNr_RowCompletionStatus
intColNr_InScope = cint(-1) : intColNr_RowCompletionStatus = cint(-1)

Dim str_rowScenarioID, intColNR_ScenarioID

Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true
Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
Dim gFWcell_xferNMI 

BASE_XML_Template_DIR = cBASE_XML_Template_DIR

Dim strFolderNameWithSlashSuffix_Scratch
' strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"

Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq

gDt_Run_TS = tsOfRun

Dim BASE_GIT_DIR ' : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"
BASE_GIT_DIR =   cBASE_GIT_DIR

Dim strCaseBaseAutomationDir 
strCaseBaseAutomationDir  = BASE_GIT_DIR


' ####################################################################################  UFT configurations at the start of test  - START ####################################################################################  

	Dim Temp_Execution_Results_Location  : Temp_Execution_Results_Location = cTemp_Execution_Results_Location
	Dim strRunLog_Folder
	Dim str_CorrelationId
	dim qtTest, qtResultsOpt, qtApp
	
	
	Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
	If qtApp.launched <> True then
	    qtApp.Launch
	End If
	
	qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	' qtApp.Options.Run.RunMode = "Fast"
	qtApp.Options.Run.ViewResults = False
	
	
	Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
	Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539


	Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
	str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
	If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
		str_AnFw_FocusArea = ""
	else
		str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
	End If
	
	strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", array(fnAttachStr(Temp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
	    gQtTest.Name                                 , _
	    Parameter.Item("Environment")                       , _
	    Parameter.Item("Company")                           , _
	    fnTimeStamp(gDt_Run_TS, "YYYY`MM`DD_HH`mm`ss")))
	    ' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))



	Path_MultiLevel_CreateComplete strRunLog_Folder
	gFWstr_RunFolder = strRunLog_Folder
	
	qtResultsOpt.ResultsLocation = gFWstr_RunFolder
	gQtResultsOpt.ResultsLocation = strRunLog_Folder
	

	qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
	qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
	qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
	qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
	qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
	qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
	qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
	qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
	qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
	qtApp.Options.Run.ScreenRecorder.RecordSound = False
	qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True
	
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	
	gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
	gQtApp.Options.Run.ViewResults                = False

	strFolderNameWithSlashSuffix_Scratch = strRunLog_Folder


' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  

' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  

initialize_scratch_Dictionary

'Set gdict_scratch = CreateObject("Scripting.Dictionary")
'gdict_scratch.CompareMode = vbTextCompare
'
clear_scratch_Dictionary gdict_scratch

' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  


 
	'  Description:-  Function to replace arguments in a string format
	'   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is 

	'						The template should have arguments within {} and the numbering should s
	
	If 1 = 1  Then		
					
					
					Environment.Value("COMPANY")=Parameter.Item("Company")
					Environment.Value("ENV")=Parameter.Item("Environment")
					    
					' Load the MasterWorkBook objWB_Master
					Dim strWB_noFolderContext_onlyFileName  
					strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
					
					Dim wbScratch
					Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
					dim  dictWSDataParameter_KeyName_ColNr
					
					fnExcel_CreateAppInstance  objXLapp, true
					
					If LCase(Environment.Value("COMPANY")) = "sapn" Then
						Environment.Value("COMPANY_CODE")="ETSA"
					Else
						Environment.Value("COMPANY_CODE")=Environment.Value("COMPANY")
					End If 'end of If LCase(Environment.Value("COMPANY")) = "sapn" Then
					
					' Environment.Value("DbQuery_Scenario") = "MTS_OUT_Q_SO"
					' LoadData_RunContext_V2  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
					LoadData_RunContext_V3 objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder

					Dim	strEnvCoy
					strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")

					' ####################################################################################################################################################################################
					' #####################################################                 LOGGING STEP 1 - SETUP      #################################################################################################
					' ####################################################################################################################################################################################
					fn_setup_Test_Execution_Log Parameter.Item("Company"), Parameter.Item("Environment"), "N", "CATS_WIGS - CATS_4pt2_with_Verification", GstrRunFileName, "CATS_WIGS", strRunLog_Folder, gObjNet.ComputerName
					Printmessage "i", "Start of Execution", "Starting execution of CATS_WIGS - CATS_4pt2_with_Verification scripts"
					
					Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
					Set wbScratch = nothing
								
					objXLapp.Calculation = xlManual
					'objXLapp.screenUpdating=False
					
					Dim objWS_DataParameter 						 
					set objWS_DataParameter           					= objWB_Master.worksheets("CATS_WIGS") ' wsMsDT_mrCntestbty_Objectn") ' qq this must become a parm
					objWS_DataParameter.activate
					
					Dim objWS_templateXML_TagDataSrc_Tables 	
					set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
					'set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
					
					Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
					' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
					gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

				'	Get headers of table in a dictionary
					Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
					gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
					
					
					' gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
					Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
					gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
					' Load the first/title row in a dictionary
					' int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
					' fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
					int_MaxColumns = UBound(gvntAr_RuleTable,2)

					objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
					objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
					objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
					objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
					' objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = now 

					gDt_Run_TS = now
					gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 

					Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE
					
					'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
					Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
					dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
					Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
					intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
					intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
					intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
					fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
					
					' =========
					' =========  FrameworkPhase 0b   - Technical Setup for Running the SingleScenario Rows   =============
					' =========
					
					Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
					
					dim r
					Dim objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, objDBConn_TestData_A, objDBRcdSet_TestData_A
					Dim objDBConn_TestData_CIS, objDBRcdSet_TestData_CIS
					
'					On error resume next
		'	add more of these as-required, for querying databases
		'	add more of these as-required, for querying databases
					Set objDBConn_TestData_IEE    = CreateObject("ADODB.Connection")
					r = 0
					set objDBRcdSet_TestData_IEE  = CreateObject("ADODB.Recordset")
		'	add more of these as-required, for querying databases

					Set objDBConn_TestData_CIS    = CreateObject("ADODB.Connection")
					r = 0
					set objDBRcdSet_TestData_CIS  = CreateObject("ADODB.Recordset")


					Dim objDBConn_TestData_B, objDBRcdSet_TestData_B
					Set objDBConn_TestData_B    = CreateObject("ADODB.Connection") : set objDBRcdSet_TestData_B  = CreateObject("ADODB.Recordset")

			Dim	DB_CONNECT_STR_MTS
					DB_CONNECT_STR_MTS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
						Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
						Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
						Environment.Value("USERNAME") 	& 	";Password=" & _
						Environment.Value("PASSWORD") 	& ";"

			Dim	DB_CONNECT_STR_IEE
					DB_CONNECT_STR_IEE = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
						Environment.Value(strEnvCoy & 	"_IEE_HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
						Environment.Value(strEnvCoy & 	"_IEE_SERVICENAME") 	&	"))); User ID=" & _
						Environment.Value(strEnvCoy & 	"_IEE_USERNAME") 	& 	";Password=" & _
						Environment.Value(strEnvCoy & 	"_IEE_PASSWORD") 	& ";"

			Dim	DB_CONNECT_STR_CIS
					DB_CONNECT_STR_CIS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
						Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
						Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
						Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
						Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"
'
'					Environment.Value("TESTFOLDER") = Environment.Value("AUTO_FOLDER_LOCAL") & "SORD\"
'					
					Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
					Dim strQueryTemplate, dtD, Hyph, strNMI
					
					Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
					Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
					intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
					
					Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
					
					Dim intSQL_Pkg_ColNr, strSQL_RunPackage
					
					
					Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
					
					
					If Parameter.Item("RowNr_StartOn") = 0 and Parameter.Item("RowNr_FinishOn") = 0 Then
						intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
						intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
					Else
						intRowNr_LoopWS_RowNr_StartOn 	= Parameter.Item("RowNr_StartOn")
						intRowNr_LoopWS_RowNr_FinishOn	= Parameter.Item("RowNr_FinishOn")
					End If
					
						
						intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
					
								
							'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
						intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
						sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
					
						Dim strColName
						objWS_DataParameter.calculate	'	to ensure the filename is updated
									
									
									
					Dim  str_xlBitNess : str_xlBitNess = ""
									
					sb_File_WriteContents  _
						gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
						fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
						Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
					
				    Dim intColNr_ScenarioStatus, intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
						  
						  intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
						  intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")
					
						Dim blnColumnSet : blnColumnSet = cbool(true)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
						If IsEmpty(intColNr_InScope) then	
							blnColumnSet =false
						ElseIf intColNr_InScope = -1 then
							blnColumnSet =false
						End if
						If not blnColumnSet then 
					'		qq log this	
					'		objXLapp.Calculation = xlManual
							objXLapp.screenUpdating=True
							exittest
						End If
					
						objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
						objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
						objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
						
						Printmessage "i", "Start row number", "Start row for the test is " & intRowNr_LoopWS_RowNr_StartOn
						Printmessage "i", "End row number", "End row for the test is " & intRowNr_LoopWS_RowNr_FinishOn
						
						objWS_DataParameter.calculate
						objxlapp.screenupdating = true
					
						' objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
					
					' =========
					' =========  FrameworkPhase 0c   - Setup the MultiStage Array                                           =============
					' =========
					
					Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
					Dim  intMasterZoomLevel 
					
					Dim rowStart, rowEnd, colStart, colEnd
					Dim RangeAr 
					
					
					'    ===>>>   Enable HotKeys
					' NOT NEEDED objWB_Master.Application.Run "HotKeys_Set"
					
					'    ===>>>   Make the RunBook Un-Shared, so that all later processes can run, esp the next, Expand, process
					' NOT NEEDED objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
					
					
	End if

				objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = Parameter("InScope_Column_Name")
					
dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = cint(0)	: intFirst= cint(0)	: intSecond = cint(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = cint(1)	' qq


sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , true, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf

	

'	determine the number of in-scope rows
Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun

tsStartOfRun = now()


'	qq     intNrOfRows_Inscope_and_Unfinished

sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , false, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf



dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow

'	now setup to do process the DriverSheet


Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_Scenario : intRowNr_CurrentScenario = cInt(0) ' formerly cLng
Dim cInt_MinutesRequired_toComplete_eachScenario, cDbl_ExtraFactor_ToWait_for_RunToComplete, tsStopRunWhenRunningTooLong
Dim cInt_MinutesPerDay : cInt_MinutesPerDay = 24 * 60
Dim cInt_SecondsPerDay : cInt_SecondsPerDay = 60 * cInt_MinutesPerDay 
Dim dt_rgWS_RunWillCeaseAt_TimeStamp : dt_rgWS_RunWillCeaseAt_TimeStamp = now()-7
cInt_MinutesRequired_toComplete_eachScenario = cInt(5)
cDbl_ExtraFactor_ToWait_for_RunToComplete = cdbl(1.2)

	                
Dim str_ScenarioRow_FlowName 
Dim cStr_FlowNumbers 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= cInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= cInt(-1)

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = false

Dim intColNr_Request_ID 
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
 intColNr_ScenarioStatus 			= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")
 Dim strRole, strInitiator_Role, var_ScheduledDate

Dim strInScope, strRowCompletionStatus

objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=true
objWB_Master.save


	
	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
		' Get headers of table in a dictionary
	Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	

	
		' Load the first/title row in a dictionary
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	
'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue
	

'	qq	the code below was in the CatsWigs_MultiStage test case, but can be found nowhere in this one , is this significant ? 		<<===  BrianM 2017`03Mar`w11`15Wed
		'		fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table
		'	int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)



' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch applications at startup - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
' Close MTS
fnMTS_WinClose
' Launch MTS
OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")
'
' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
''	
' Reset error flags
fn_reset_error_flags
Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID
' Dim str_Company_Name, ctr_Stage, tempInt_RoleIdentificationCounter, Str_Role
'																	
Dim temp_XML_Tag_MeterDetails, temp_XML_Tag_MeterDetails_runtimeUpdated, temp_XML_Tag_MeterDetails_FinalTag, temp_XML_Tag_CSVIntervalData
Dim temp_XML_Tag_RegisterDetails, temp_XML_Tag_RegisterDetails_runtimeUpdated, temp_XML_Tag_RegisterDetails_FinalTag																	
Dim str_Company_Name, ctr_Stage, tempInt_RoleIdentificationCounter, Str_Role, temp_str_aqs, temp_bln_scratch


int_ctr_total_Eligible_rows = 0
int_ctr_no_of_passed_rows = 0
temp_bln_scratch = false

i = 1

' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PREP - Update company related information in driver sheet %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

' set the state to run the SingleScenarios over, based on the company being tested
  Select Case ucase(Parameter.Item("Company"))
    Case "CITI", "PCOR"
      str_Company_Name = "VIC"
      PrintMessage "p","Company Name for Test", "Company name passed as parameter ("& Parameter.Item("Company") &"). Company name used for the test is ("& str_Company_Name  &")"
    Case "SAPN"
      str_Company_Name = "SA"
      PrintMessage "p","Company Name for Test", "Company name passed as parameter ("& Parameter.Item("Company") &"). Company name used for the test is ("& str_Company_Name  &")"
    Case else
    	PrintMessage "f","Invalid company name passed as parameter", "Company name passed as parameter ("& Parameter.Item("Company") &") is not handled in script...., exiting test"
      ExitAction ' exit the test case
  End Select


Dim strAr_acrossA_Role, strAr_Stages
Dim intArSz_Role, intArSz_Stages, strPrefix_RoleIsRelevant
Dim strCaseGroup, strCaseNr, strXML_TemplateName_Current, Date_TransactionDate
Dim str_DataParameter_PopulatedXML_FqFileName, str_MTS_Txn_Status
Dim strVerification_String, temp_range_rg_VerificationString_Suffix, varAr_Verification_String
Dim strSQL_MTS, strSQL_IEE, temp_XML_Tag_CSVIntervalData_FinalTag, temp_XML_Tag_ConcatenatedRegisters
Dim temp_strSqlCols_NamesList, temp_strSqlCols_ValuesList, temp_strSql_Suffix, temp_IEE_MeterNumber, temp_IEE_Meter_Counter, temp_IEE_Register_Counter, temp_No_of_Required_Meters

temp_range_rg_VerificationString_Suffix = objWS_DataParameter.Range("rgWS_VerificationString_Suffix").value

strAr_acrossA_Role = Split(objWS_DataParameter.Range("rgws_Roles_In_Scope").value,",")
Printmessage "i", "Roles for test", "Roles selected for test is/are : " &  objWS_DataParameter.Range("rgws_Roles_In_Scope").value
intArSz_Role = UBound(strAr_acrossA_Role)

strAr_Stages = Split(objWS_DataParameter.Range("rgws_Process_Stages").value,",")
intArSz_Stages = UBound(strAr_Stages)

strPrefix_RoleIsRelevant 	= objWS_DataParameter.range("rgWS_RoleIsRelevant_Prefix").Value


' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Do
	
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
	
									'	intRowNr_LoopWS_RowNr_StartOn   qq
	
		'	if the scenarioRow is InScope-and-Incomplete                                                                                                                 
		strInScope 							= 	ucase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
		str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
		str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
		temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value

		' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
		
			If   ucase(strInScope) = ucase(cY)  and str_ScenarioRow_FlowName <> "" and lcase(str_Next_Function) <> "fail" and lcase(str_Next_Function) <> "end" Then

				If i = 1 Then
					If ucase(Parameter.Item("RunMode")) = "N" Then ' Value can be N- New or R - restart
					
						If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 Then
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "WaitingForPrevStep"
						Else
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start-SQL"
							str_Next_Function = "start-SQL"
						End if' End of InStr(1,LCase(temp_Scenario_ID),"step2") > 0
					End If
					fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
					int_ctr_total_Eligible_rows = int_ctr_total_Eligible_rows + 1
					objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
					' Update the company / state name in worksheet for an eligible row
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("list_tState") ).value = str_Company_Name
				End If
			
				' Generate a filename for screenshot file
				gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, left(temp_Scenario_ID,10)))

				' ####################################################################################################################################################################################
				' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
				' ####################################################################################################################################################################################
				fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", "", "", "", "", "", ""
				
			Select Case lcase(str_ScenarioRow_FlowName)
				
				' ################################################################# START of first flow ####################################################################################################
				' ################################################################# START of first flow ####################################################################################################
				Case "flow1", "flow2", "flow3","flow4","flow5","flow6","flow7", "flow7a","flow7b", "flow8",  "flow9":
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
								Select Case str_Next_Function
									Case "start-SQL" ' this is datamine
										' First function
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											
												' Query would be executed once for every row. It would not be executed for ALL ROLES as there are no roles related replacements
												
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%
											' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   NOTE  %%%%%%%%%%%%%%%%%%%%%%%%%%%

											' capture MTS query
											strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_Template_Name")).value 

											If trim(strSQL_MTS) <> ""  Then
											
												  PrintMessage "p", "MTS SQL template","Scenario row("& intRowNr_CurrentScenario &") - MTS SQL Template ("& strSQL_MTS &") "
											        strQueryTemplate = fnRetrieve_SQL(strSQL_MTS)
											        
											        
											        If lcase(strQueryTemplate) <> "fail" Then
											        	
														strSQL_MTS = strQueryTemplate
														
														temp_NMI_Check_if_Provided = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI_Same_or_Different_or_Provided")).value
														
														If trim(lcase(temp_NMI_Check_if_Provided)) = "p" Then
															str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
															strSQL_MTS = replace ( strSQL_MTS , "<NMI>", str_NMI, 1, -1, vbTextCompare)
														Else
															' Perform replacements
												            	strSQL_MTS = replace ( strSQL_MTS , "<NMI_Size>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("list_tNMI")).value, 1, -1, vbTextCompare)
												            	strSQL_MTS = replace ( strSQL_MTS , "<Meter_Type>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Meter_Type")).value, 1, -1, vbTextCompare)
												            	strSQL_MTS = replace ( strSQL_MTS , "<rownum>", "1", 1, -1, vbTextCompare)
														End If
											            	
											            	PrintMessage "p", "SQL Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing MTS SQL ("& strSQL_MTS &") "
											            	
														' Execute MTS SQL
														strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI"))
														
														If strScratch = "PASS" Then
															' Write the data in all respective columns for all roles
															' Reserve NMI
															str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
															
															' First check if we need to generate a checksum for NMI
															
															If instr(1,str_NMI, "GenerateCS",1) > 0 or instr(1,str_NMI, "GenerateValidCS",1) > 0 or instr(1,str_NMI, "GenerateInvalidCS",1) > 0  Then
																strScratch = mid(str_NMI,1, instr(1,str_NMI,"Generate",1)-1)
																str_NMI = calculateChecksum(strScratch)
																objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = str_NMI
																																
																strScratch = fn_Update_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value , objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value , ",", "NMI", str_NMI)
																'Set it back to the driversheet
																objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = strScratch
	
															End If 'end of If instr(1,str_NMI, "GenerateCS") > 0 or instr(1,str_NMI, "GenerateValidCS") > 0 or instr(1,str_NMI, "GenerateInvalidCS") > 0  Then

															
															' ####################################################################################################################################################################################
															' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
															' ####################################################################################################################################################################################
															fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", str_NMI, "", "", "", "", ""
															PrintMessage "p", "Reserve NMI - CANDIDATE","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& str_NMI &") in KDR as candidate"															
															fnKDR_Insert_Data_V1 "n", "n",str_NMI, temp_Scenario_ID, "candidate"
														End If
													End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
																											'Else ' Commenting as the user may wish to use the same NMI in multiple rows
																												'fn_set_error_flags true, "Query for row (" & intRowNr_CurrentScenario & ") is missing"
											End If ' end of If strSQL_MTS <> ""  Then
											
											
											'CIS API Modify Service Provision Run - SIT Testing only
											If lcase(str_ScenarioRow_FlowName) = "flow7a"  Then
																
																' We need to execute additional API for ModifyServiceProvisionRequest If Required_SP_Status_CIS <> “”
												strScratch = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS")).value 
												
												If Trim(strScratch) <> "" Then
													strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_API_ModifyServiceProvisionRequest")).value 		
													PrintMessage "p", "Starting CIS_API_ModifyServiceProvisionRequest","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - API Modify SP Query ("& strSQL_CIS &")"
													strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
													
													' perform all replacements
													strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
													strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
													Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
													strSQL_CIS = Replace ( strSQL_CIS  , "<Param_NMI>", int_NMI, 1, -1, vbTextCompare)
													strSQL_CIS = Replace ( strSQL_CIS  , "<param_SP_Status>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS" )), 1, -1, vbTextCompare)
													
													fn_wait(5)
				
													' Update query in SQL Query cell
													strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
													strScratch = strScratch & vbCrLf & "CIS_API_ModifyServiceProvisionRequest Query:" & vbCrLf & strSQL_CIS
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
				
													'Execute Query
													Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
													strSQL_CIS = ""
												End If 'end of If Trim(strScratch) <> "" Then
												
												
											End If ' End of lcase(str_ScenarioRow_FlowName) = "flow7" 
											
											If (lcase(str_ScenarioRow_FlowName) = "flow2" or lcase(str_ScenarioRow_FlowName) = "flow3" or lcase(str_ScenarioRow_FlowName) = "flow4" or lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow5" or lcase(str_ScenarioRow_FlowName) = "flow7" or lcase(str_ScenarioRow_FlowName) = "flow7a" or lcase(str_ScenarioRow_FlowName) = "flow7b" or lcase(str_ScenarioRow_FlowName) = "flow8" or lcase(str_ScenarioRow_FlowName) = "flow9") and lcase(gFWbln_ExitIteration) <> "y" and gFWbln_ExitIteration <> true Then ' IEE Meter related information is minned only for flow 2
												
													' capture IEE query to fetch Meter Data
													strSQL_IEE = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_Meter_Details_SQL_Template_Name")).value 
		
													If trim(strSQL_IEE) <> ""  Then
														
														If str_NMI <> "" Then
														  PrintMessage "p", "IEE SQL template","Scenario row("& intRowNr_CurrentScenario &") - IEE SQL Template ("& strSQL_IEE &") "
													        strQueryTemplate = fnRetrieve_SQL(strSQL_IEE)
													        
													        If lcase(strQueryTemplate) <> "fail" Then
																strSQL_IEE = strQueryTemplate
																' Perform replacements
													            	strSQL_IEE = replace ( strSQL_IEE , "<NMI>", str_NMI, 1, -1, vbTextCompare)
													            	
													            	'[KS] replace meter_number 
													            	str_MeterNumber =  objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Meter_Number")).value 
													            	If trim(str_MeterNumber) <> "" Then
													            		strSQL_IEE = replace ( strSQL_IEE , "<METER_NUMBER>",str_MeterNumber , 1, -1, vbTextCompare)
													            	End If
													            														            	
													            	PrintMessage "p", "SQL Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing IEE SQL ("& strSQL_IEE &") "
													            	
													            	' Append the query in SQL Populated Coulmn
									            			        	set cellScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated"))
									            			        	strScratch = cellScratch.value
														        	cellScratch.formula = strScratch & vbCrLf & " IEE QUERY : - " & vbCrLf & strSQL_IEE
														        	cellScratch.WrapText = False
														        	
																' Execute IEE SQL
																strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, strSQL_IEE, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListIEEMeterDataSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListIEEMeterDataSqlColumn_Values"), "")
																
																' strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, strSQL_IEE, intRowNr_CurrentScenario, "2",  "", "", "", "")
																
																' PENDING, merge the ourput of IEE and MTS in a single CELL
																If strScratch = "PASS" Then

																' On error resume next
																	' $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
																	' The below code would cater to a requirement where there is more than 1 meter and 1 or many registers returned against a NMI. We need to update the XML on the basis of what the query returns
																	' $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
																	
																	temp_strSqlCols_NamesList 	= "" 
																	temp_strSqlCols_ValuesList = ""
																      temp_strSql_Suffix = ","  ' the list is 1-based
																	temp_IEE_MeterNumber = ""
																	temp_IEE_Meter_Counter = 0 
																	temp_IEE_Register_Counter = 0 
																	objDBRcdSet_TestData_IEE.MoveFirst
																	temp_XML_Tag_CSVIntervalData_runtimeUpdated = ""
																	temp_XML_Tag_CSVIntervalData_FinalTag = "" 
																	temp_XML_Tag_ConcatenatedRegisters = ""
																	temp_bln_scratch = true
																	
																	temp_XML_Tag_MeterDetails = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_MeterDetails"))
																	temp_XML_Tag_RegisterDetails = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_RegisterDetails"))
																	temp_XML_Tag_CSVIntervalData = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_CSVIntervalData"))
																	temp_XML_Tag_Tampering_Meter_Or_Register = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_Tampering_Meter_Or_Register"))
																	temp_No_of_Required_Meters = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Meter_Restriction"))
																	' If XML_Meter_Restriction is blank, we treat blank as "all"
																	If trim(temp_No_of_Required_Meters) = "" Then temp_No_of_Required_Meters = 999999999
																	If trim(temp_No_of_Required_Meters) = "all" Then temp_No_of_Required_Meters = 999999999
																	
																	
																	If trim(temp_No_of_Required_Meters) <> 0  Then
															
																		For int_Total_Recs  = 0 To objDBRcdSet_TestData_IEE.RecordCount
																		
																			temp_XML_Tag_RegisterDetails_runtimeUpdated = ""
																			temp_XML_Tag_RegisterDetails_runtimeUpdated = temp_XML_Tag_RegisterDetails
																			
																			
																			if temp_IEE_MeterNumber = "" Then 
																				temp_IEE_MeterNumber = objDBRcdSet_TestData_IEE.Fields("METERNUMBER").value
																				'Updating MeterNumber column
																				objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Meter_Number")) = temp_IEE_MeterNumber
																				temp_IEE_Meter_Counter = temp_IEE_Meter_Counter + 1
																				temp_IEE_Register_Counter = 1
																				strScratch = "METERNUMBER" & temp_IEE_Meter_Counter
																				temp_XML_Tag_MeterDetails_runtimeUpdated = temp_XML_Tag_MeterDetails
																				temp_XML_Tag_CSVIntervalData_runtimeUpdated = temp_XML_Tag_CSVIntervalData
																				temp_XML_Tag_RegisterDetails_FinalTag = ""
																				' Perform all Meter related replacements
																				
																			  ' ################################################################################################################################################
																			  	' ###############################################                         Meter Serial                               ############################################################
																			  	' ################################################################################################################################################
																				  	Select Case temp_XML_Tag_Tampering_Meter_Or_Register
																					  	Case "OneMeterTag", "TwoMeterTags", "OneMeter+OneRegister", "MeterandStatus"
																					  		If temp_IEE_Meter_Counter = 1 Then
																					  			temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_SerialNumber^", Tamper_string(temp_IEE_MeterNumber))
																							ElseIf temp_IEE_Meter_Counter = 2 and temp_XML_Tag_Tampering_Meter_Or_Register = "TwoMeterTags" Then
																								temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_SerialNumber^", Tamper_string(temp_IEE_MeterNumber))
																							Else
																								temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_SerialNumber^", temp_IEE_MeterNumber)
																								temp_XML_Tag_CSVIntervalData_runtimeUpdated = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcSQL_SerialNumber^", temp_IEE_MeterNumber)
																					  		End If
																					  	Case else
																						        temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_SerialNumber^", temp_IEE_MeterNumber)
																						        temp_XML_Tag_CSVIntervalData_runtimeUpdated = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcSQL_SerialNumber^", temp_IEE_MeterNumber)
																				  	End Select
																				
																				
																				temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_MANUFACTURER^", objDBRcdSet_TestData_IEE.Fields("MANUFACTURER").value)
																				temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_MODEL^", objDBRcdSet_TestData_IEE.Fields("MODEL").value)
																				temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_ConstantCAT^", objDBRcdSet_TestData_IEE.Fields("CONSTANTCAT").value)
																			  	' ################################################################################################################################################
																			  	' ###############################################                         STATUS Tag                               ############################################################
																			  	' ################################################################################################################################################
																				  	Select Case temp_XML_Tag_Tampering_Meter_Or_Register
																					  	Case "OneStatus", "MeterandStatus", "OneStatusC", "OneStatusD", "RegisterandStatus", "ControlledLoadandStatus", "ControlledLoadandStatusC", "ControlledLoadandStatusD"
																					  		If temp_IEE_Meter_Counter = 1 Then
																					  			If temp_XML_Tag_Tampering_Meter_Or_Register = "OneStatusC" or temp_XML_Tag_Tampering_Meter_Or_Register = "ControlledLoadandStatusC"  Then
																						  				temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_STATUS^", "C")
																						  			ElseIf temp_XML_Tag_Tampering_Meter_Or_Register =  "OneStatusD"  or temp_XML_Tag_Tampering_Meter_Or_Register = "ControlledLoadandStatusD"  Then
																						  				temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_STATUS^", "D")
																						  		End If
																							Else
																								temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_STATUS^", objDBRcdSet_TestData_IEE.Fields("STATUS").value)
																					  		End If
																					  	Case else
																						        temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_STATUS^", objDBRcdSet_TestData_IEE.Fields("STATUS").value)
																				  	End Select
		
																				If instr(1, temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_Program^") > 0  Then
																					temp_XML_Tag_MeterDetails_runtimeUpdated  = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_Program^", objDBRcdSet_TestData_IEE.Fields("PROGRAMID").value)
																				End If
																				
																				' temp_XML_Tag_MeterDetails_FinalTag
																				
																			Else
																				If trim(temp_IEE_MeterNumber) <> trim(objDBRcdSet_TestData_IEE.Fields("METERNUMBER").value) Then
																					
																					temp_XML_Tag_MeterDetails_FinalTag = temp_XML_Tag_MeterDetails_FinalTag & temp_XML_Tag_MeterDetails_runtimeUpdated & vbCrLf
																					temp_XML_Tag_MeterDetails_FinalTag = replace(temp_XML_Tag_MeterDetails_FinalTag, "^data_srcSQL_RegisterDetails^", temp_XML_Tag_RegisterDetails_FinalTag)
																					temp_XML_Tag_CSVIntervalData_FinalTag = replace(temp_XML_Tag_CSVIntervalData_FinalTag, "^data_SrcSQL_Concatenated_RegisterId_egE1Q1^", temp_XML_Tag_ConcatenatedRegisters)
																					temp_XML_Tag_CSVIntervalData_runtimeUpdated = temp_XML_Tag_CSVIntervalData
																					temp_IEE_Meter_Counter = temp_IEE_Meter_Counter + 1
																					
																					If temp_No_of_Required_Meters < temp_IEE_Meter_Counter Then 
																						temp_bln_scratch = false
																						int_Total_Recs  = objDBRcdSet_TestData_IEE.RecordCount + 1
																					End If 
																					
																					If temp_bln_scratch Then
																						temp_XML_Tag_RegisterDetails_FinalTag = ""
																						temp_XML_Tag_ConcatenatedRegisters = ""
																						temp_IEE_MeterNumber = objDBRcdSet_TestData_IEE.Fields("METERNUMBER").value
																						strScratch = "METERNUMBER" & temp_IEE_Meter_Counter
																						temp_IEE_Register_Counter = 1
																						temp_XML_Tag_MeterDetails_runtimeUpdated = temp_XML_Tag_MeterDetails
																						
																							'If temp_IEE_Meter_Counter <= 2 Then
																								' When the meter is changing, perform all Meter related replacements
																								' Perform all Meter related replacements
																						  	' ################################################################################################################################################
																						  	' ###############################################                         Meter Serial                               ############################################################
																						  	' ################################################################################################################################################
																							  	Select Case temp_XML_Tag_Tampering_Meter_Or_Register
																								  	Case "OneMeterTag", "TwoMeterTags", "OneMeter+OneRegister", "MeterandStatus"
																								  		If temp_IEE_Meter_Counter = 1 Then
																								  			temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_SerialNumber^", Tamper_string(temp_IEE_MeterNumber))
																										ElseIf temp_IEE_Meter_Counter = 2 and temp_XML_Tag_Tampering_Meter_Or_Register = "TwoMeterTags" Then
																											temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_SerialNumber^", Tamper_string(temp_IEE_MeterNumber))
																										Else
																											temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_SerialNumber^", temp_IEE_MeterNumber)
																											temp_XML_Tag_CSVIntervalData_runtimeUpdated = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcSQL_SerialNumber^", temp_IEE_MeterNumber)
																								  		End If
																								  	Case else
																									        temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_SerialNumber^", temp_IEE_MeterNumber)
																									        temp_XML_Tag_CSVIntervalData_runtimeUpdated = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcSQL_SerialNumber^", temp_IEE_MeterNumber)
																							  	End Select
																								
																								temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_MANUFACTURER^", objDBRcdSet_TestData_IEE.Fields("MANUFACTURER").value)
																								temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_MODEL^", objDBRcdSet_TestData_IEE.Fields("MODEL").value)
																								temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_ConstantCAT^", objDBRcdSet_TestData_IEE.Fields("CONSTANTCAT").value)
																							  	' ################################################################################################################################################
																							  	' ###############################################                         STATUS Tag                               ############################################################
																							  	' ################################################################################################################################################
																								Select Case temp_XML_Tag_Tampering_Meter_Or_Register
																								  	Case "OneStatus", "MeterandStatus", "OneStatusC", "OneStatusD", "RegisterandStatus", "ControlledLoadandStatus", "ControlledLoadandStatusC", "ControlledLoadandStatusD"
																								  		If temp_IEE_Meter_Counter = 1 Then
																								  			If temp_XML_Tag_Tampering_Meter_Or_Register = "OneStatusC" or temp_XML_Tag_Tampering_Meter_Or_Register = "ControlledLoadandStatusC"  Then
																								  				temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_STATUS^", "C")
																								  			ElseIf temp_XML_Tag_Tampering_Meter_Or_Register =  "OneStatusD"  or temp_XML_Tag_Tampering_Meter_Or_Register = "ControlledLoadandStatusD"  Then
																								  				temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_STATUS^", "D")
																								  			End If
																										Else
																											temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_STATUS^", objDBRcdSet_TestData_IEE.Fields("STATUS").value)
																								  		End If
																								  	Case else
																									        temp_XML_Tag_MeterDetails_runtimeUpdated = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_STATUS^", objDBRcdSet_TestData_IEE.Fields("STATUS").value)
																							  	End Select
																						  	
				
																								If instr(1, temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_Program^") > 0  Then
																									temp_XML_Tag_MeterDetails_runtimeUpdated  = replace(temp_XML_Tag_MeterDetails_runtimeUpdated, "^data_SrcSQL_Program^", objDBRcdSet_TestData_IEE.Fields("PROGRAMID").value)
																								End If
																								
																							'End If
																					End If	' end of If temp_bln_scratch Then
																					
																				Else
																					temp_XML_Tag_CSVIntervalData_runtimeUpdated = temp_XML_Tag_CSVIntervalData
																					temp_XML_Tag_CSVIntervalData_runtimeUpdated = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcSQL_SerialNumber^", temp_IEE_MeterNumber)
																				End If ' end of If trim(temp_IEE_MeterNumber) <> trim(objDBRcdSet_TestData_IEE.Fields("METERNUMBER").value) Then
																			End If

																			If temp_bln_scratch Then

																								  For int_Total_Fields = 0 To 	objDBRcdSet_TestData_IEE.Fields.Count -1
																								  	
																								  	If lcase(objDBRcdSet_TestData_IEE.Fields(int_Total_Fields).Name) =  "meternumber" Then
																								  		' temp_strSqlCols_NamesList 	= temp_strSqlCols_NamesList  & temp_strSql_Suffix & strScratch
																								  		temp_strSqlCols_NamesList 	= temp_strSqlCols_NamesList  & temp_strSql_Suffix & "METERNUMBER" & temp_IEE_Meter_Counter
																								  	ElseIf  lcase(objDBRcdSet_TestData_IEE.Fields(int_Total_Fields).Name) =  "registerid" Then
																								  		temp_strSqlCols_NamesList 	= temp_strSqlCols_NamesList  & temp_strSql_Suffix & "REGISTERID" & temp_IEE_Register_Counter
																								  	Else
																								  		temp_strSqlCols_NamesList 	= temp_strSqlCols_NamesList  	& temp_strSql_Suffix & objDBRcdSet_TestData_IEE.Fields(int_Total_Fields).Name & temp_IEE_Meter_Counter
																								  	End If
																								  	
																								        temp_strSqlCols_ValuesList		= temp_strSqlCols_ValuesList 	& temp_strSql_Suffix & objDBRcdSet_TestData_IEE.Fields(int_Total_Fields).Value
																								        temp_strSql_Suffix = ","
																								   Next
																								   
																								  	
																								  	' Here we need to check if we need to tamper some specific tags (to test negative scenarios)
																								  	
																								  	  ' For a meter, perform all register related replacements
																								  	
																								  	' ################################################################################################################################################
																								  	' ###############################################                         REGISTER ID                               ############################################################
																								  	' ################################################################################################################################################
																									  	Select Case temp_XML_Tag_Tampering_Meter_Or_Register
																										  	Case "OneRegisterTag", "TwoRegisterTags", "OneMeter+OneRegister", "RegisterandStatus"
																										  		If temp_IEE_Register_Counter = 1 and temp_IEE_Meter_Counter = 1 Then
																										  			temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_RegisterId^", Tamper_string(objDBRcdSet_TestData_IEE.Fields("RegisterId").value))
																												ElseIf temp_IEE_Meter_Counter = 1 and temp_IEE_Register_Counter = 2 and temp_XML_Tag_Tampering_Meter_Or_Register = "TwoRegisterTags" Then
																													temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_RegisterId^", Tamper_string(objDBRcdSet_TestData_IEE.Fields("RegisterId").value))
																												Else
																													temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_RegisterId^", objDBRcdSet_TestData_IEE.Fields("RegisterId").value)
																													temp_XML_Tag_CSVIntervalData_runtimeUpdated  = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcSQL_RegisterId^", objDBRcdSet_TestData_IEE.Fields("RegisterId").value)
																													temp_XML_Tag_ConcatenatedRegisters = temp_XML_Tag_ConcatenatedRegisters & objDBRcdSet_TestData_IEE.Fields("RegisterId").value
																										  		End If
																										  	Case else
																											        temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_RegisterId^", objDBRcdSet_TestData_IEE.Fields("RegisterId").value)
																											        temp_XML_Tag_CSVIntervalData_runtimeUpdated  = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcSQL_RegisterId^", objDBRcdSet_TestData_IEE.Fields("RegisterId").value)
																											        temp_XML_Tag_ConcatenatedRegisters = temp_XML_Tag_ConcatenatedRegisters & objDBRcdSet_TestData_IEE.Fields("RegisterId").value
																									  	End Select
																								   
																								   
																								   temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_NetworkTariffCode^", objDBRcdSet_TestData_IEE.Fields("NetworkTariffCode").value)
																								   temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_UnitOfMeasure^", objDBRcdSet_TestData_IEE.Fields("UnitOfMeasure").value)
																								   temp_XML_Tag_CSVIntervalData_runtimeUpdated  = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcSQL_UnitOfMeasure^", objDBRcdSet_TestData_IEE.Fields("UnitOfMeasure").value)
				   														   temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_MULTIPLIER^", objDBRcdSet_TestData_IEE.Fields("MULTIPLIER").value)

																		   temp_XML_Tag_CSVIntervalData_runtimeUpdated  = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcSQL_DSIdentifier^", objDBRcdSet_TestData_IEE.Fields("DS_IDENTIFIER").value)
																		   temp_XML_Tag_CSVIntervalData_runtimeUpdated  = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcSQL_IntervalLength^", objDBRcdSet_TestData_IEE.Fields("INT_LENGTH").value)
																		   
																		   ' Update XML_Tag_CSVIntervalData column date formats as below 
																		   temp_XML_DATE_formatYYYYMMDDHHmmss = fnUniqueNrFromTS(now(), "YYYYMMDDHHmmss")
																		   temp_XML_DATE_formatYYYYMMDDHHmm = fnUniqueNrFromTS(now(), "YYYYMMDDHHmm")
																		   temp_XML_DATE_formatYYYYMMDD = fnUniqueNrFromTS(now(), "YYYYMMDD")
																		   temp_XML_NMI = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI") ).value
																		   
																		   temp_XML_Tag_CSVIntervalData_runtimeUpdated  = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^dataNMI_SrcWsDP_withoutChecksum^", left(temp_XML_NMI,10) )
																		   temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_Suffix^", objDBRcdSet_TestData_IEE.Fields("Suffix").value)
																		   temp_XML_Tag_CSVIntervalData_runtimeUpdated  = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcFWR_DATE_formatYYYYMMDDHHmmss^", temp_XML_DATE_formatYYYYMMDDHHmmss)
																		   temp_XML_Tag_CSVIntervalData_runtimeUpdated  = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcFWR_DATE_formatYYYYMMDDHHmm^", temp_XML_DATE_formatYYYYMMDDHHmm)
																		   temp_XML_Tag_CSVIntervalData_runtimeUpdated  = replace(temp_XML_Tag_CSVIntervalData_runtimeUpdated, "^data_SrcFWR_DATE_formatYYYYMMDD^", temp_XML_DATE_formatYYYYMMDD)
																		   
																		   
												
																								  	' ################################################################################################################################################
																								  	' ###############################################                         ControlledLoad                               ############################################################
																								  	' ################################################################################################################################################
																									  	Select Case temp_XML_Tag_Tampering_Meter_Or_Register
																										  	Case "OneControlledLoad", "ControlledLoadandStatus", "OneControlledLoadTag", "ControlledLoadandStatusC", "ControlledLoadandStatusD"
																										  		If temp_IEE_Register_Counter = 1 and temp_IEE_Meter_Counter = 1 Then
																										  			temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_ControlledLoad^", Tamper_string(objDBRcdSet_TestData_IEE.Fields("ControlledLoad").value))
																												ElseIf temp_IEE_Meter_Counter = 1 and temp_IEE_Register_Counter = 2 and temp_XML_Tag_Tampering_Meter_Or_Register = "TwoRegisterTags" Then
																													temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_ControlledLoad^", Tamper_string(objDBRcdSet_TestData_IEE.Fields("ControlledLoad").value))
																												Else
																													temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_ControlledLoad^", objDBRcdSet_TestData_IEE.Fields("ControlledLoad").value)
																										  		End If
																										  	Case else
																											        temp_XML_Tag_RegisterDetails_runtimeUpdated  = replace(temp_XML_Tag_RegisterDetails_runtimeUpdated, "^data_SrcSQL_ControlledLoad^", objDBRcdSet_TestData_IEE.Fields("ControlledLoad").value)
																									  	End Select
						
																								   
																								   
																								   If temp_XML_Tag_RegisterDetails_FinalTag = "" Then
																								   	temp_XML_Tag_RegisterDetails_FinalTag = trim(temp_XML_Tag_RegisterDetails_runtimeUpdated)
																								   Else
																								   	temp_XML_Tag_RegisterDetails_FinalTag = trim(temp_XML_Tag_RegisterDetails_FinalTag) & vbCrLf & trim(temp_XML_Tag_RegisterDetails_runtimeUpdated)
																								   End If
																								   
																								   
																								   If temp_XML_Tag_CSVIntervalData_FinalTag = "" Then
																								   	temp_XML_Tag_CSVIntervalData_FinalTag = trim(temp_XML_Tag_CSVIntervalData_runtimeUpdated)
																								   Else
																								   	temp_XML_Tag_CSVIntervalData_FinalTag = trim(temp_XML_Tag_CSVIntervalData_FinalTag) & "," & vbCrLf & trim(temp_XML_Tag_CSVIntervalData_runtimeUpdated)
																								   End If
																								   
																								   objDBRcdSet_TestData_IEE.MoveNext	
																								   temp_IEE_Register_Counter = temp_IEE_Register_Counter + 1
																								   If objDBRcdSet_TestData_IEE.EOF  Then
																								   	int_Total_Recs = objDBRcdSet_TestData_IEE.RecordCount + 1
																								   End If
																			End if ' end of If temp_bln_scratch Then
 
																		Next ' end of For int_Total_Fields = 0 To 	objDBRcdSet_TestData_IEE.Fields.Count -1
	
																		If temp_bln_scratch Then
																		  temp_XML_Tag_MeterDetails_FinalTag = temp_XML_Tag_MeterDetails_FinalTag & temp_XML_Tag_MeterDetails_runtimeUpdated
																		  temp_XML_Tag_MeterDetails_FinalTag = replace(temp_XML_Tag_MeterDetails_FinalTag, "^data_srcSQL_RegisterDetails^", temp_XML_Tag_RegisterDetails_FinalTag)
																		  temp_XML_Tag_CSVIntervalData_FinalTag = replace(temp_XML_Tag_CSVIntervalData_FinalTag, "^data_SrcSQL_Concatenated_RegisterId_egE1Q1^", temp_XML_Tag_ConcatenatedRegisters)
																		Else
																		  temp_XML_Tag_MeterDetails_FinalTag = replace(temp_XML_Tag_MeterDetails_FinalTag, "^data_srcSQL_RegisterDetails^", temp_XML_Tag_RegisterDetails_FinalTag)
																		  temp_XML_Tag_CSVIntervalData_FinalTag = replace(temp_XML_Tag_CSVIntervalData_FinalTag, "^data_SrcSQL_Concatenated_RegisterId_egE1Q1^", temp_XML_Tag_ConcatenatedRegisters)
																		End If
																		  
																		' objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_MeterDetails")).value = temp_XML_Tag_MeterDetails_FinalTag
																		
																		set cellScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_MeterDetails"))
																        	cellScratch.formula = temp_XML_Tag_MeterDetails_FinalTag
																        	cellScratch.WrapText = False
	
																		set cellScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_CSVIntervalData"))
																        	cellScratch.formula = temp_XML_Tag_CSVIntervalData_FinalTag
																        	cellScratch.WrapText = False
	
																        	
																        	temp_XML_Tag_MeterDetails_FinalTag = ""
																        	temp_XML_Tag_MeterDetails = ""
																        	temp_XML_Tag_MeterDetails_runtimeUpdated = ""
																        	temp_XML_Tag_MeterDetails_FinalTag = ""
																        	temp_XML_Tag_RegisterDetails = ""
																        	temp_XML_Tag_RegisterDetails_runtimeUpdated = ""
																        	temp_XML_Tag_RegisterDetails_FinalTag	= ""														
																		temp_IEE_MeterNumber = ""
																		temp_IEE_Meter_Counter = 0 
																		temp_XML_Tag_CSVIntervalData_FinalTag = ""
																		temp_XML_Tag_ConcatenatedRegisters = ""
																		
		
																		' $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
																		' The above code would cater to a requirement where there is more than 1 meter and 1 or many registers returned against a NMI. We need to update the XML on the basis of what the query returns
																		' $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
																	
																		' concatenate names
																		strScratch = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names") ).value
																		'strScratch = strScratch  & objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListIEEMeterDataSqlColumn_Names") ).value
																		strScratch = strScratch  & temp_strSqlCols_NamesList
																		objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names") ).value = strScratch
																		' concatenate values
																		strScratch = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values") ).value
																		' strScratch = strScratch  & objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListIEEMeterDataSqlColumn_Values") ).value
																		strScratch = strScratch  & temp_strSqlCols_ValuesList
																		objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values") ).value = strScratch
																		
																		temp_strSqlCols_NamesList 	= "" 
																		temp_strSqlCols_ValuesList = ""

																	End If ' end of If trim(temp_No_of_Required_Meters) <> 0  Then

'																	If err.number <> 0  Then
'																		PrintMessage "f", "Error during IEE query execution to fetch meter,register information", "Error ("& err.description &") during IEE query execution and fetching meter, register information"
'																		fn_set_error_flags true, "Error ("& err.description &") during IEE query execution and fetching meter, register information"
'																	End If
'																	err.clear
'																	On error goto 0
																	
																	
																End If ' end of If strScratch = "PASS" Then
																
															End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
														
														Else
															PrintMessage "f", "IEE Query Execution", "Skipping execution of IEE query as there is no NMI found for record ("& intRowNr_CurrentScenario &"). Marking the row as fail and moving to next one"
															fn_set_error_flags true, "IEE query is provided however there is no NMI available for replacement. Skipping this row"
														End If ' end of If str_NMI <> "" Then
														
													End If ' end of If strSQL_IEE <> ""  Then
													
											End If	' end of If (lcase(str_ScenarioRow_FlowName) = "flow2" or lcase(str_ScenarioRow_FlowName) = "flow3")  and (lcase(gFWbln_ExitIteration) <> "y" or gFWbln_ExitIteration <> true) Then ' IEE Meter related information is minned only for flow 2
											
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f", "Problem with SQL, moving to the next row", "Query didn't return any data"
												' create a function to dump error in a file
												strScratch = gFWstr_RunFolder & "Error_Dump_" & fnTimeStamp(now, "YYYY`MM`DD_HH`mm`ss") & ".txt"
												sb_File_WriteContents  strScratch, gfsoForAppending, true, gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "MILESTONE - Problem with SQL, moving to the next row", "Error with SQL logged in file ("& strScratch &")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Generate_Request_ID"
												PrintMessage "p", "MILESTONE - Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
			            					
											strSQL_MTS = ""
											
									Case "Generate_Request_ID"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if

										
											If trim(lcase(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Request_ID_Same_or_Different") ).value)) <> ""  Then
										            int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
										            
												' For all Roles in scope, generate request ID's
												 For iRole = 1 To intArSz_Role
												 
											            objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(strAr_acrossA_Role(iRole)) & objWS_DataParameter.range("rgWS_RequestID").formula) ).value = int_UniqueReferenceNumberforXML
													' ####################################################################################################################################################################################
													' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
													' ####################################################################################################################################################################################
													fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", str_NMI,  int_UniqueReferenceNumberforXML,  strAr_acrossA_Role(iRole), "", "", ""
											            PrintMessage "p", "Generating Request ID","Scenario row("& intRowNr_CurrentScenario &") - Request ID for role ("& strAr_acrossA_Role(iRole) &") is ("& int_UniqueReferenceNumberforXML &") "
											            
												 	If lcase(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Request_ID_Same_or_Different") ).value) = "d"  Then
												 		wait 1
												            int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
												 	End If
												 	
												 Next ' end of For iRole = 1 To intArSz_Role
											End If
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at Generate_Request_ID because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f", "MILESTONE - Error during Request ID generation, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Request ID generation complete"
												'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML_Creation"
												PrintMessage "p", "MILESTONE - Request ID generation complete", "Generation of Request ID complete for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
										
									Case "XML_Creation"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										' Rules for XML Creation
										' 1) XML;s are created for all stages, left to right (INIT,PVAL,REQ,OBJ,REQ^2,OBJ^2,PEND,CAN,REJ,COM)
										' 2) For every inscope stage, next would be role sequence (for all eligible roles) i.e. which role needs to go first
										' 3) XML's are dropped in respective gateway

											 For ctr_Stage = 1 To intArSz_Stages
											 	 ' to get stage
												If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(strAr_Stages(ctr_Stage))).value) <> ""  Then ' check if the stage is in scope
													 ' msgbox strAr_Stages(ctr_Stage) & "Inscope"
													 For iRole = 1 To intArSz_Role
															For tempInt_RoleIdentificationCounter = 1 To intArSz_Role
																strScratch = strPrefix_RoleIsRelevant & strAr_acrossA_Role(tempInt_RoleIdentificationCounter)
																If  objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr(strScratch)).Value = iRole Then
																	Str_Role = strAr_acrossA_Role(tempInt_RoleIdentificationCounter)' "LNSP" ' Role parameter for specific scenario
																	Exit For
																Else
																	Str_Role = ""
																End If
															Next	' end of For tempInt_RoleIdentificationCounter = 1 To intArSz_Role 
															
															'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
																	' If the correct role was eastablished, generate the XML for that role
															'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
															
															If Str_Role <> "" Then 
																' msgbox "role sequence is " & 	Str_Role
																' strCaseGroup = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ObjectionCD")).value
																strCaseGroup = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
																
																strCaseNr    = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CATS_CR")).value
																str_ChangeStatusCode = strAr_Stages(ctr_Stage)
																
																'If lcase(str_ScenarioRow_FlowName) = "flow6" Then 
																'strXML_TemplateName_Current = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("MDN_xmlTemplate_Name")).value
																
																'else
																strXML_TemplateName_Current = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
																'End If
																
																Date_TransactionDate = now
																int_UniqueReferenceNumberforXML = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Str_Role) & objWS_DataParameter.range("rgWS_RequestID").formula)).value
																
																If lcase(str_ScenarioRow_FlowName) = "flow3" Then ' we need a wrong NMI in case of Flow 3
																	str_NMI = fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "WRONG_NMI")
																else											
																	str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
																End If	' If lcase(str_ScenarioRow_FlowName) = "flow3" Then ' we need a wrong NMI in case of Flow 3
											
																' strFolderNameWithSlashSuffix_Scratch = gFWstr_RunFolder ' already set on the top
																
																If trim(strXML_TemplateName_Current) <> "" and trim(strCaseGroup) <> "" Then
																	' ####################################################################################################################################################################################
																	' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
																	' ####################################################################################################################################################################################
																	fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", str_NMI,  int_UniqueReferenceNumberforXML,  Str_Role,  str_ChangeStatusCode, "", ""

																	PrintMessage "p", "Starting XML creation","Scenario row("& intRowNr_CurrentScenario &") - starting XML creation for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &")"
																	str_DataParameter_PopulatedXML_FqFileName = _
																		fnCreateCATSXML_V2(  _
																			Environment.Value("COMPANY"), strCaseGroup & "_" & strCaseNr, str_ChangeStatusCode, BASE_XML_Template_DIR,  _
																		       strXML_TemplateName_Current, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter, objWS_DataParameter, _
																		       dictWSDataParameter_KeyName_ColNr, 	intRowNr_CurrentScenario, _
																		       	gvntAr_RuleTable, _
																		       g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Date_TransactionDate,  _
																		       int_UniqueReferenceNumberforXML,  Str_Role, str_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values" )
																	
																	
																	' If there were no errors during XML creation, drop in gateway else exit the current row
															            If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
															            	PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
															            	iRole = intArSz_Role + 1
															            	ctr_Stage = intArSz_Stages + 1
															            Else
																		
																		strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")	
																		PrintMessage "p", "XML File","Scenario row("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
																		
																		' objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_CATSChangeDate" )) = fnTimeStamp(gXML_CATSChangeDate,"DD/MM/YYYY")
																		objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_CATSChangeDate" )) = "'" & gXML_CATSChangeDate
																		
																            ' ZIP XML File
																            ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
																            PrintMessage "p", "Zippng XML File","Scenario row("& intRowNr_CurrentScenario &") - Zipping XML file for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &")"
																                    
																            ' GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "CATS", Str_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  
																            GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "CATS", Str_Role, "", ""
										
																			If Environment.Value("CATSDestinationFolder") = "<notfound>" or Environment.Value("CATSDestinationFolder") = "" Then
																				PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																				gFWbln_ExitIteration = "y"
																			      gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																			Else
																			
																				If ctr_Stage > 1 Then ' Incase of multistage, REQ gets processed first hence we need to induce a specific wait
																					wait(Cint_wait_time_MultiStageXML_Difference)
																				End If
	 																			copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
																				PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																				objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Str_Role) & objWS_DataParameter.range("rgWS_Gateway_Destination_Folder").formula)).value =  Environment.Value("CATSDestinationFolder") ' & str_DataParameter_PopulatedXML_FqFileName
																			End If
															            End If
																Else
																	' PrintMessage "P", "XML_Creation SKIPPING", "No XML filename, is this intentional? skipping for row("& intRowNr_CurrentScenario &")-role("& Str_Role &")"
																	PrintMessage "F", "XML_Creation filename or replacement key missing", "XML filename or tags replacement key missing for row("& intRowNr_CurrentScenario &")-role("& Str_Role &")"
																	gFWbln_ExitIteration = true
																	gFWstr_ExitIteration_Error_Reason = "XML filename or tags replacement key missing for row("& intRowNr_CurrentScenario &")-role("& Str_Role &")"
																	iRole = intArSz_Role + 1
																	ctr_Stage = intArSz_Stages + 1
																	
																End If ' end of If trim(strXML_TemplateName_Current) <> "" Then
																
																
															End If ' end of If Str_Role <> "" Then 
													 Next ' end of For iRole = 1 To intArSz_Role
													 
												End If ' end of If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(strAr_Stages(ctr_Stage))).value)  Then ' check if the stage is in scope
										
											 Next ' end of For ctr_Stage = 1 To intArSz_Stages ' end of For ctr_Stage = 1 To intArSz_Stages


											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f", "MILESTONE - XML creation failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												If lcase(str_ScenarioRow_FlowName) = "flow7a" Then 
												
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "IEE_Query_Validation"
												'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
												 PrintMessage "p", "MILESTONE - XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_TwoMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												
													
												Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_CATS_Screen_Verification"
												'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
												 PrintMessage "p", "MILESTONE - XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_TwoMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												
												End If 'End of lcase(str_ScenarioRow_FlowName) = "flow7a"
												
			            					End If
			            					
									Case "MDN_XML_Creation"
									
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
										
										' Rules for XML Creation
										' 1) XML;s are created for all stages, left to right (INIT,PVAL,REQ,OBJ,REQ^2,OBJ^2,PEND,CAN,REJ,COM)
										' 2) For every inscope stage, next would be role sequence (for all eligible roles) i.e. which role needs to go first
										' 3) XML's are dropped in respective gateway

											 For ctr_Stage = 1 To intArSz_Stages
											 	 ' to get stage
												If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(strAr_Stages(ctr_Stage))).value) <> ""  Then ' check if the stage is in scope
													 ' msgbox strAr_Stages(ctr_Stage) & "Inscope"
													 For iRole = 1 To intArSz_Role
															For tempInt_RoleIdentificationCounter = 1 To intArSz_Role
																strScratch = strPrefix_RoleIsRelevant & strAr_acrossA_Role(tempInt_RoleIdentificationCounter)
																If  objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr(strScratch)).Value = iRole Then
																	Str_Role = strAr_acrossA_Role(tempInt_RoleIdentificationCounter)' "LNSP" ' Role parameter for specific scenario
																	Exit For
																Else
																	Str_Role = ""
																End If
															Next	' end of For tempInt_RoleIdentificationCounter = 1 To intArSz_Role 
															
															'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
																	' If the correct role was eastablished, generate the XML for that role
															'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
															
															If Str_Role <> "" Then 
																' msgbox "role sequence is " & 	Str_Role
																' strCaseGroup = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ObjectionCD")).value
																strCaseGroup = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
																
																strCaseNr    = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CATS_CR")).value
																str_ChangeStatusCode = strAr_Stages(ctr_Stage)
																
																strXML_TemplateName_Current = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("MDN_xmlTemplate_Name")).value
																
																Date_TransactionDate = now
																 ' Creating a new unique number for MDN xml to avoid the CATS xml replacement
																int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss") 
																'int_UniqueReferenceNumberforXML = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Str_Role) & objWS_DataParameter.range("rgWS_RequestID").formula)).value
											
																objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("MDN_RequestID"))= int_UniqueReferenceNumberforXML
											
																str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
																
											
																' strFolderNameWithSlashSuffix_Scratch = gFWstr_RunFolder ' already set on the top
																
																If trim(strXML_TemplateName_Current) <> "" and trim(strCaseGroup) <> "" Then
																	' ####################################################################################################################################################################################
																	' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
																	' ####################################################################################################################################################################################
																	fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", str_NMI,  int_UniqueReferenceNumberforXML,  Str_Role,  str_ChangeStatusCode, "", ""

																	PrintMessage "p", "Starting MDN XML creation","Scenario row("& intRowNr_CurrentScenario &") - starting MDN XML creation for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &")"
																	str_DataParameter_PopulatedXML_FqFileName = _
																		fnCreateCATSXML_V2(   _
																			Environment.Value("COMPANY"), strCaseGroup & "_" & strCaseNr, str_ChangeStatusCode, BASE_XML_Template_DIR,  _
																		       strXML_TemplateName_Current, strFolderNameWithSlashSuffix_Scratch, objWS_DataParameter, objWS_DataParameter, _
																		       dictWSDataParameter_KeyName_ColNr, 	intRowNr_CurrentScenario, _
																		       	gvntAr_RuleTable, _
																		       g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Date_TransactionDate,  _
																		       int_UniqueReferenceNumberforXML,  Str_Role, str_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values" )
																	
																	
																	' If there were no errors during XML creation, drop in gateway else exit the current row
															            If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
															            	PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
															            	iRole = intArSz_Role + 1
															            	ctr_Stage = intArSz_Stages + 1
															            Else
																		
																		strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")	
																		PrintMessage "p", "XML File","Scenario row("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
																		
																		' objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_CATSChangeDate" )) = fnTimeStamp(gXML_CATSChangeDate,"DD/MM/YYYY")
																		objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_CATSChangeDate" )) = "'" & gXML_CATSChangeDate
																		
																            ' ZIP XML File
																            ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
																            PrintMessage "p", "Zippng XML File","Scenario row("& intRowNr_CurrentScenario &") - Zipping XML file for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &")"
																                    
																            ' GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", Str_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
																            GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", Str_Role, "",""
										
																			If Environment.Value("CATSDestinationFolder") = "<notfound>" or Environment.Value("CATSDestinationFolder") = "" Then
																				PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																				gFWbln_ExitIteration = "y"
																			      gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																			Else
																			
																				If ctr_Stage > 1 Then ' Incase of multistage, REQ gets processed first hence we need to induce a specific wait
																					wait(Cint_wait_time_MultiStageXML_Difference)
																				End If
	 																			copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
																				PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																				objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Str_Role) & objWS_DataParameter.range("rgWS_Gateway_Destination_Folder").formula)).value =  Environment.Value("CATSDestinationFolder") ' & str_DataParameter_PopulatedXML_FqFileName
																			End If
															            End If
																Else
																	' PrintMessage "P", "XML_Creation SKIPPING", "No XML filename, is this intentional? skipping for row("& intRowNr_CurrentScenario &")-role("& Str_Role &")"
																	PrintMessage "F", "XML_Creation filename or replacement key missing", "XML filename or tags replacement key missing for row("& intRowNr_CurrentScenario &")-role("& Str_Role &")"
																	gFWbln_ExitIteration = true
																	gFWstr_ExitIteration_Error_Reason = "XML filename or tags replacement key missing for row("& intRowNr_CurrentScenario &")-role("& Str_Role &")"
																	iRole = intArSz_Role + 1
																	ctr_Stage = intArSz_Stages + 1
																	
																End If ' end of If trim(strXML_TemplateName_Current) <> "" Then
																
																
															End If ' end of If Str_Role <> "" Then 
													 Next ' end of For iRole = 1 To intArSz_Role
													 
												End If ' end of If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(strAr_Stages(ctr_Stage))).value)  Then ' check if the stage is in scope
										
											 Next ' end of For ctr_Stage = 1 To intArSz_Stages ' end of For ctr_Stage = 1 To intArSz_Stages


											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MDN XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f", "MILESTONE - XML creation failed, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Meter_Data_Notification_Import_Screen_Check"
												'bjWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_CATS_Screen_Verification"
												 PrintMessage "p", "MILESTONE - XML creation and placing on gateway complete", "MDN XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_TwoMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
										
										
									case "MTS_CATS_Screen_Verification"
										
											' fn_compare_Current_and_expected_execution_time
											If lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7" or lcase(str_ScenarioRow_FlowName) = "flow7b"  or lcase(str_ScenarioRow_FlowName) = "flow8" or lcase(str_ScenarioRow_FlowName) = "flow9" Then 
												'wait(180)
																					
											End If	
													tsNow = now()
													tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
													IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
														Exit do
													End if
													
												' There would be a dictionary which would capture all actuals....
		
		 										For ctr_Stage = 1 To intArSz_Stages
														If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(strAr_Stages(ctr_Stage))).value) <> ""  Then ' check if the stage is in scope
															 For iRole = 1 To intArSz_Role
																	For tempInt_RoleIdentificationCounter = 1 To intArSz_Role
																		strScratch = strPrefix_RoleIsRelevant & strAr_acrossA_Role(tempInt_RoleIdentificationCounter)
																		If  objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr(strScratch)).Value = iRole Then
																			Str_Role = trim(strAr_acrossA_Role(tempInt_RoleIdentificationCounter))' "LNSP" ' Role parameter for specific scenario
																			Exit For
																		Else
																			Str_Role = ""
																		End If
																	Next	' end of For tempInt_RoleIdentificationCounter = 1 To intArSz_Role 
																	
																	'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
																			' If the correct role was eastablished, capture actuals for that STAGE - ROLE combination from the GUI
																	'  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
																	
																	If Str_Role <> "" Then 
																		' strCaseGroup = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ObjectionCD")).value
																		strCaseGroup = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
																		
																		strCaseNr    = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CATS_CR")).value
																		str_Stage = strAr_Stages(ctr_Stage)
																		strXML_TemplateName_Current = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
																		Date_TransactionDate = now
																		int_UniqueReferenceNumberforXML = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Str_Role) & objWS_DataParameter.range("rgWS_RequestID").formula)).value
																		' str_NMI = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Str_Role) & objWS_DataParameter.range("rgws_NMIdata").formula)).value
																		str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
																		' strFolderNameWithSlashSuffix_Scratch = gFWstr_RunFolder ' already set on the top
																		
																		gDict_FieldName_ValueActual_allForSingleScenarioStageRole.RemoveAll
		
		
																		' Fetch verification string 
																		strVerification_String = ""
																		strVerification_String = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(Str_Role & temp_range_rg_VerificationString_Suffix)).value
		
																		
																		if instr(1, strVerification_String, str_Stage) > 0 Then 
																		
																			' Grab verification string for this stage/role
																			' ####################################################################################################################################################################################
																			' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
																			' ####################################################################################################################################################################################
																			fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", str_NMI,  int_UniqueReferenceNumberforXML,  Str_Role,   str_Stage, "", ""
																			
																			strVerification_String = mid(UCASE(strVerification_String),  (instr(1, strVerification_String, str_Stage & "_START")+len(str_Stage & "_START"))  , (instr(1, strVerification_String, str_Stage & "_END") - instr(1, strVerification_String, str_Stage & "_START") - len(str_Stage & "_START") ) )																	
																			PrintMessage "i", "Verification string","Scenario row("& intRowNr_CurrentScenario &") - Verification string for stage ("& str_Stage &") - role ("& Str_Role &") is ("& strVerification_String &") "
																			
																			PrintMessage "p", "MTS Verification","Scenario row("& intRowNr_CurrentScenario &") - Starting MTS verification for Stage ("& str_ChangeStatusCode &") - Role ("& Str_Role &")"
		
																			str_MTS_Txn_Status  = fnCheckCatsNotificationReceived_MTS_vn04 ( intRowNr_CurrentScenario, str_Stage, str_NMI, strCaseNr, int_UniqueReferenceNumberforXML, Str_Role, "", "", strVerification_String, "y", gFW_strRunLog_ScreenCapture_fqFileName)
																			
																			If lcase(str_MTS_Txn_Status) <> "fail"  Then ' Incase records are not found on MTS or row is missing
																				' msgbox strVerification_String
																				' varAr_Verification_String = split(strVerification_String,",")
																				varAr_Verification_String = split(strVerification_String,"^")
																				PrintMessage "p", "MTS Verification","Data captured from MTS screen, staring with validation against expected strings which would be performed using fn_compare_array_with_dictionary function"
																				
																				' Once the data is there in the dictionary, verify with expected values against the array
																				
																				strScratch = fn_compare_array_with_dictionary(varAr_Verification_String, gDict_FieldName_ValueActual_allForSingleScenarioStageRole) 
																				
																				If strScratch <> "PASS" Then
																					fn_set_error_flags true, "Stage("& str_Stage &")-Role("& Str_Role &")-" & strScratch
																					iRole = intArSz_Role + 1
																					ctr_Stage = intArSz_Stages + 1
																				End If ' end of If strScratch <> "PASS" Then
																				
																			Else
																				PrintMessage "f", "MTS Verification - FAILURE","Unable to capture from MTS screen because of some error. Skipping validation of expected VS actual results"
																				iRole = intArSz_Role + 1
																				ctr_Stage = intArSz_Stages + 1
																			End If ' end of If lcase(str_MTS_Txn_Status) <> "fail"  Then
																			
																		Else
																			PrintMessage "i", "Verification string not provided","Scenario row("& intRowNr_CurrentScenario &") - Verification string not provided for stage ("& str_Stage &") - role ("& Str_Role &"). Moving on without checking MTS"
																		End If ' end of if instr(1, strVerification_String, str_Stage) > 0 Then 
																		
																		
																	End If ' end of If Str_Role <> "" Then 
																	
															 Next ' end of For iRole = 1 To intArSz_Role
															 
														End If ' end of If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(strAr_Stages(ctr_Stage))).value)  Then ' check if the stage is in scope
												
													 Next ' end of For ctr_Stage = 1 To intArSz_Stages ' end of For ctr_Stage = 1 To intArSz_Stages
		
													' At the end, check if any error was there and write in corresponding column
													If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row("&intRowNr_CurrentScenario&") failed at MTS_CATS_Screen_Verification because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
														PrintMessage "f","MILESTONE - MTS_CATS_Screen_Verification function", "Row("&intRowNr_CurrentScenario&") failed at MTS_CATS_Screen_Verification because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
														int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
														Exit do
													Else
													
														If lcase(str_ScenarioRow_FlowName) = "flow4" or lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7" or lcase(str_ScenarioRow_FlowName) = "flow7b" or lcase(str_ScenarioRow_FlowName) = "flow9" Then ' 'flow 7 is for SIT purpose only
															objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_CATS_Screen_Verification complete"
																
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "IEE_Query_Validation"
															PrintMessage "p", "MILESTONE - MTS_CATS_Screen_Verification complete", "MTS_CATS_Screen_Verification complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
															' Function to write timestamp for next process
															fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
														
														ElseIf lcase(str_ScenarioRow_FlowName) = "flow8" Then
															objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_CATS_Screen_Verification complete"
														
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SQT_INTERFACE_OUT_RESPONSE_CHECK"
															
															PrintMessage "p", "MILESTONE - MTS_CATS_Screen_Verification complete", "MTS_CATS_Screen_Verification complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
															' Function to write timestamp for next process
															fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ThreeMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
															
												
														ElseIf lcase(str_ScenarioRow_FlowName) = "flow5" Then 
															objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_CATS_Screen_Verification complete"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "AQ_ALL_SCREEN_CHECK"
															PrintMessage "p", "MILESTONE - MTS_CATS_Screen_Verification complete", "MTS_CATS_Screen_Verification complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
															' Function to write timestamp for next process
															fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		
														Else
															objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_CATS_Screen_Verification complete"
															PrintMessage "p", "MILESTONE - MTS_CATS_Screen_Verification complete", "MTS_CATS_Screen_Verification complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
															' Function to write timestamp for next process
															fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
															int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
															int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
															
															' We need to change the status of NMI as available in KDR
															PrintMessage "p", "Reserve NMI - AVAILABLE","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& str_NMI &") in KDR as available"															
															fnKDR_Insert_Data_V1 "n", "n",str_NMI, temp_Scenario_ID, "available"
															
															
															Exit Do
														End If
					            					End If

											
											
								case "IEE_Query_Validation" 
                                                                                                                                                                
                                                            ' fn_compare_Current_and_expected_execution_time
                                                            
                                                            tsNow = now() 
                                                            tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
                                                            IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
                                                                            Exit do
                                                            End if
                                                            
                                            ' capture IEE query to fetch Meter Data
                                                            strSQL_IEE = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_Verification_SQL_Template_Name")).value 

                                                            If trim(strSQL_IEE) <> ""  Then
                                                                            str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))                                                                                                                                                                                                                       
                                                                            
                                                                            If str_NMI <> "" Then
                                                                              PrintMessage "p", "IEE SQL template","Scenario row("& intRowNr_CurrentScenario &") - IEE SQL Template for verification ("& strSQL_IEE &") "
                                                                    strQueryTemplate = fnRetrieve_SQL(strSQL_IEE)
                                                                    
                                                                    If lcase(strQueryTemplate) <> "fail" Then
                                                                    
                                                                                                            strSQL_IEE = strQueryTemplate
                                                                                                            ' Perform replacements
                                                                                                            temp_XML_Date = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("XML_CATSChangeDate" ))
                                                                                                            
                                                                            strSQL_IEE = replace ( strSQL_IEE , "<NMI>", str_NMI, 1, -1, vbTextCompare)
                                                                            strSQL_IEE = replace ( strSQL_IEE , "<XML_DATE>", temp_XML_Date, 1, -1, vbTextCompare)
                                                                            'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												'adding due to the defect- jumping into next function
												'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
												
                                                                            PrintMessage "p", "IEE verification Query Execution","Scenario row("& intRowNr_CurrentScenario &") - Executing IEE Verification SQL ("& strSQL_IEE &") "
                                                                                                            ' Execute IEE SQL
                                                                                                            strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_IEE, objDBRcdSet_TestData_IEE, strSQL_IEE, intRowNr_CurrentScenario, "2",  "", dictWSDataParameter_KeyName_ColNr("csListIEEVerificationSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListIEEVerificationSqlColumn_Values"), "")
                                                                                                            wait(60)
                                                                                                            ' Strategy
                                                                                                            ' 1) Recordset would have 2 records
                                                                                                            ' 2) First record need to have an enddate equal to temp_XML_Date
                                                                                                            ' 3) Second record should have start date equal to temp_XML_Date
                                                                                                            ' 4) Check the status of second record - PENDING
                                                                                                            
                                                                                                            
                                                                                                            
                                                                                                            If objDBConn_TestData_IEE.State = 1 and objDBRcdSet_TestData_IEE.RecordCount >=2  Then
                                                                                                                            objDBRcdSet_TestData_IEE.MoveFirst
                                                                                                                            ' If trim(objDBRcdSet_TestData_IEE.Fields("METERSTARTDATE")) = trim(temp_XML_Date) Then
																															If (trim(objDBRcdSet_TestData_IEE.Fields("METERSTARTDATE")) = trim(temp_XML_Date)) AND (trim(objDBRcdSet_TestData_IEE.Fields("METERSTATUSFULL")) =  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verification_IEE_MeterStatusFULL" )).value )Then
                                                                                                                                                                                                                                  
                                                                                                                           	PrintMessage "p", "IEE verification Query Execution","Expected start date for new meter version ((" & temp_XML_Date &") MATCH expected start date (" & trim(objDBRcdSet_TestData_IEE.Fields("METERENDDATE")) &")  in DB/Table"
                                                                                                                            Else
                                                                                                                                            PrintMessage "f", "IEE verification Query Execution","Expected start date for new meter version ((" & temp_XML_Date &") DOES NOT MATCH expected start date (" & trim(objDBRcdSet_TestData_IEE.Fields("METERENDDATE")) &")  in DB/Table"
                                                                                                                            End If
                                                                                                                            
                                                                                                                            objDBRcdSet_TestData_IEE.MoveNext
                                                                                                 
                                                                                                                            
                                                                                                                            If trim(objDBRcdSet_TestData_IEE.Fields("METERENDDATE")) = trim(temp_XML_Date) Then
                                                                                                                                            PrintMessage "p", "IEE verification Query Execution","Expected end date for old meter version ((" & temp_XML_Date &") MATCH expected end date (" & trim(objDBRcdSet_TestData_IEE.Fields("METERENDDATE")) &")  in DB/Table"
                                                                                                                            Else
                                                                                                                                            PrintMessage "f", "IEE verification Query Execution","Expected end date for old meter version ((" & temp_XML_Date &") DOES NOT MATCH expected end date (" & trim(objDBRcdSet_TestData_IEE.Fields("METERENDDATE")) &") in DB/Table"
                                                                                                                            End If
                                                                                                                            
                                                                                                            Else
                                                                                                                            PrintMessage "f", "IEE verification Query Execution","Failed during execution of IEE Query"
                                                                                                                            fn_set_error_flags true, "Failed during execution of IEE Query"
                                                                                                            End If
                                                                                                            
                                                                                                            ' Perform verifications of actual value returned by SQL
                                                                                                            
                                                                                                            ' Strategy for comparison
                                                                                                            '''''''''''''''''''''''''''''''''''''''''''''''''''''
                                                                                                            ' As the values are already there in some cell, as they went in XML
                                                                                                            ' Pick those values and compare then with the one returned from executing the SQL
                                                                                                            
                                                                                                            ' you need to check...for the NMI, Meter, Channel...the last two status fields is updated as what has come thru in the xml
                                                                                                            
                                                                                            End If ' end of  If lcase(strQueryTemplate) <> "fail" Then
                                                                            Else
                                                                                            PrintMessage "f", "IEE verification Query Execution", "Skipping execution of IEE verification query as there is no NMI found for record ("& intRowNr_CurrentScenario &"). Marking the row as fail and moving to next one"
                                                                                            fn_set_error_flags true, "IEE verification query is provided however there is no NMI available for replacement. Skipping this row"
                                                                            End If ' end of If str_NMI <> "" Then
                                                            End If ' end of If strSQL_IEE <> ""  Then

                                                            ' At the end, check if any error was there and write in corresponding column
                                                            If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
                                                                            objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
                                                                            objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row("&intRowNr_CurrentScenario&") failed at IEE_Query_Validation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
                                                                            objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
                                                                            PrintMessage "f","MILESTONE - IEE_Query_Validation function", "Row("&intRowNr_CurrentScenario&") failed at IEE_Query_Validation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
                                                                            objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
                                                                            int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
                                                                            Exit do
                                                            Else
                                                            
                                                            If lcase(str_ScenarioRow_FlowName) = "flow9" Then
																	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
																	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "IEE_Query_Validation complete"
																	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
																	PrintMessage "p", "MILESTONE - IEE_Query_Validation complete", "IEE_Query_Validation complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
																	' Function to write timestamp for next process
																	fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
																
                                                            Else
																If lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7" or lcase(str_ScenarioRow_FlowName) = "flow7b"  or lcase(str_ScenarioRow_FlowName) = "flow8" Then
																	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
																	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "IEE_Query_Validation complete"
																	'Removing this temp as the SQT not returning any results
																	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SQT_INTERFACE_OUT_RESPONSE_CHECK"
																	'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
																	PrintMessage "p", "MILESTONE - IEE_Query_Validation complete", "IEE_Query_Validation complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
																	' Function to write timestamp for next process
																	fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ThreeMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
																
																Else
																	' If Flow is flow 7a then step 1 is complete
	                                                                objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
	                                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
	                                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "IEE_Query_Validation complete"
	                                                                PrintMessage "p", "MILESTONE - IEE_Query_Validation complete", "IEE_Query_Validation complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
	                                                                objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
	                                                                ' Function to write timestamp for next process
	                                                                fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
	                                                                int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
	                                                                int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
	                                                                
	                                                                ' We need to change the status of NMI as available in KDR
	                                                                PrintMessage "p", "Reserve NMI","Scenario row("& intRowNr_CurrentScenario &") - Reserving NMI  ("& str_NMI &") in KDR as available"                                                                                                                                                                                                                                    
	                                                                fnKDR_Insert_Data_V1 "n", "n",str_NMI, temp_Scenario_ID, "available"
	                                                                Exit Do
                                                            		
                                                            	End If ' end of If lcase(str_ScenarioRow_FlowName) = "flow6" Then
                                                            	
                                                            'End If
                                                            
                                                            End If	
            End If

			            					
			            					
			            					
			            		case "SQT_INTERFACE_OUT_RESPONSE_CHECK"
										
										If objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("SQT_INT_OUT_RESPONSE_Query")).value <> "" Then	
											
											If lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7" or lcase(str_ScenarioRow_FlowName) = "flow7b"  or lcase(str_ScenarioRow_FlowName) = "flow8" Then
										
												tsNow = now() 
												tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
												IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
													Exit do
												End if
												
												str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
																																		
												var_ScheduledDate = fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("XML_CATSChangeDate")).value,"YYYY-MM-DD")
												var_ScheduledDate_DDMmMYYYY = fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("XML_CATSChangeDate")).value,"DD-MmM-YYYY")
												
												strSQL_MTS = ""
												strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("SQT_INT_OUT_RESPONSE_Query")).value 
												
												If strSQL_MTS <>"" Then
												
												PrintMessage "p", "SQT_INTERFACE_OUT_RESPONSE_CHECK","Scenario row("& intRowNr_CurrentScenario &") - Executing SQT INTERFACE OUT Verification SQL ("& strSQL_MTS &") "
												strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
												
							                	strSQL_MTS = replace ( strSQL_MTS  , "<NMI>", str_NMI,  1, -1, vbTextCompare)
							                	strSQL_MTS = replace ( strSQL_MTS  , "<ScheduledDate>", var_ScheduledDate,  1, -1, vbTextCompare)
							                	strSQL_MTS = replace ( strSQL_MTS  , "<ScheduledDate_DD-MMM-YYYY>", var_ScheduledDate_DDMMMYYYY,  1, -1, vbTextCompare)

												'KS: Adding a 6min delay due to sqt out currently takes 4-6min. will monitor and update accordingly.
												wait (600)
												' Execute CIS SQL
												Execute_SQL_Populate_DataSheet    objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2", dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", "",""	
											
												If objDBRcdSet_TestData_B.State > 0 Then 													
											
													If lcase(trim(objDBRcdSet_TestData_B.RecordCount)) = 1 Then
													
														str_CorrelationId =trim(objDBRcdSet_TestData_B.Fields("CORRELATIONID").Value)
											
														If lcase(trim(objDBRcdSet_TestData_B.Fields("ProposedStatus").Value)) = lcase(trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQT_INT_OUT_Verify_ProposedStatus")).value)) and _
															lcase(trim(objDBRcdSet_TestData_B.Fields("Reason").Value)) = lcase(trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQT_INT_OUT_Verify_Reason")).value)) Then 
																									
															gFWbln_ExitIteration = "n"
															
														Else
															temp_error_message =  "Issue with SQT_INTERFACE_OUT_RESPONSE_CHECK. Actual value: ProposedStatus:" & objDBRcdSet_TestData_B.Fields ("ProposedStatus") & "; Reason:"  & objDBRcdSet_TestData_B.Fields ("Reason") & "; No of Records:"  & lcase(trim(objDBRcdSet_TestData_B.RecordCount)) 
															gFWbln_ExitIteration = "y"
														   	gFWstr_ExitIteration_Error_Reason = "SQT_INTERFACE_OUT_RESPONSE_CHECK function - " & temp_error_message														
														End If
													
													Else
															temp_error_message =  "SQT_INTERFACE_OUT_RESPONSE_CHECK: Number of SQL Query record(s) not matched: Expecting 1 record; Actual: " & objDBRcdSet_TestData_B.RecordCount & " Records" 
															gFWbln_ExitIteration = "y"
														   	gFWstr_ExitIteration_Error_Reason = "SQT_INTERFACE_OUT_RESPONSE_CHECK function - " & temp_error_message		
													End if
												
												Else
													temp_error_message =  "Issue with SQT_INTERFACE_OUT_RESPONSE_CHECK. No row Returned."
													gFWbln_ExitIteration = "y"
													gFWstr_ExitIteration_Error_Reason = "\SQT_INTERFACE_OUT_RESPONSE_CHECK function - " & temp_error_message		
												End If	
												
												Else
													gFWbln_ExitIteration = "n" ' incase sql is empty do not exit but jump to next function
											End If ' end if of strSQL_MTS <>""

												
										End If ' end of lcase(str_ScenarioRow_FlowName) = "flow6"

											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at SQT_INTERFACE_OUT_RESPONSE_CHECK because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												'adding due to the defect- jumping into next function
												'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
												
												Reporter.ReportEvent micFail, "SQT_INTERFACE_OUT_RESPONSE_CHECK Failed", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
												
											ELSE
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SQT_INTERFACE_OUT_RESPONSE_CHECK complete"
												Reporter.ReportEvent micPass, "SQT_INTERFACE_OUT_RESPONSE_CHECK complete", "SQT_INTERFACE_OUT_RESPONSE_CHECK complete for row ("  & intRowNr_CurrentScenario &  ")" 
												
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")

												Exit Do
			            					End If
									 else
									 
									 	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SQT_INTERFACE_OUT_RESPONSE is empty"
										PrintMessage "f", "SQT_INTERFACE_OUT_RESPONSE","Query is empty"
										fn_set_error_flags true, "SQT_INTERFACE_OUT_RESPONSE is empty"
										
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")


									 End If 
									 
									 					

								
Case "CIS_API_QUERY_SO_VERIFICATION"
                        
                        If objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_QuerySO_API_Template")).value <> "" Then
																	

										Wait(5)
									
										strSQL_CIS = ""
										strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Find_CIS_SO_Num_DataPrep_Query")).value 													
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
																
					                	' perform all replacements
					                	strSQL_CIS = replace ( strSQL_CIS  , "<NMI>", str_NMI,  1, -1, vbTextCompare)
					                	wait(30)
					                	strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_CIS, objDBRcdSet_TestData_CIS, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", "", "")
					                	If strScratch = "PASS" Then
					                	
						                	If lcase(str_ScenarioRow_FlowName) = "flow8" or lcase(str_ScenarioRow_FlowName) = "flow9" Then
						                	
						                			If trim(lcase(objDBRcdSet_TestData_CIS.Fields("PARAM_SONUM").Value)) = "" Then
														PrintMessage "p", "CIS_SO_DataPrep_VERIFICATION","CIS SO not Created"
													ELse
														PrintMessage "f", "CIS_SO_DataPrep_VERIFICATION","CIS SO Created"
														fn_set_error_flags true, "CIS_SO_DataPrep_VERIFICATION-CIS SO Created"
													End If
						                	
						                	Else
						                	
						                	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SO_Number")).value =trim(lcase(objDBRcdSet_TestData_CIS.Fields("PARAM_SONUM").Value))
						                	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Find_CIS_SO_Num_DataPrep_Query completed"
						                	
						                	End If
					                	
					                	End If 'end of strScratch = "PASS"
					                	
					                	wait(5)
                        
                        End If
                        
                        If LCase(str_ScenarioRow_FlowName) = "flow7" Then
                            
                            tsNow = Now() 
                            tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
                            If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
                                Exit Do
                            End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
                            
                            PrintMessage "i", "CASE Start", "[CIS_Check_SOstatus_isRaised] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
                            
                            int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
                            'var_ScheduledDate = fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("ScheduledDate")).value,"DD/MM/YYYY")
                            Dim WORKORDERTYPE, WORKORDERTASK, WORKORDERSTATUS, WORKORDERREASON, WORKCOMPLETEDFLAG, WAIVEREASON
                                WORKORDERTYPE = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("WORKORDERTYPE")).value
                                WORKORDERTASK = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("WORKORDERTASK")).value
                                WORKORDERSTATUS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("WORKORDERSTATUS")).value
                                WORKORDERREASON = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("WORKORDERREASON")).value
                                WORKCOMPLETEDFLAG = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("WORKCOMPLETEDFLAG")).value
                                WAIVEREASON =objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("WAIVEREASON")).value
                            '######## CIS_SO_Check_Query #########################
                            strSQL_CIS = ""
                            strSQL_CIS = "CATS_CISOV_ServiceOrderVerification"
                            
                            If Trim(strSQL_CIS) <> ""  Then                                            
                                PrintMessage "p", "CIS_Check_SOstatus_isRaised","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing CIS_SO_Check_Query ("& strSQL_CIS &") "
                                strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
                                
                                ' perform all replacements
                              
                                strSQL_CIS = Replace ( strSQL_CIS  , "<NMI>", int_NMI,  1, -1, vbTextCompare)
                                'strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_CIS_SO_NUM>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber" )),  1, -1, vbTextCompare)
								wait(30)
                                ' Execute CIS SQL
                                'Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", "", ""
                                'strScratch = Execute_SQL_Populate_DataSheet_Hatseperator(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",   "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  "")
					            strScratch = ""
					            strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_CIS, objDBRcdSet_TestData_CIS, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", "", "")
                                wait(30)
                                If lcase(strScratch) <> "fail" Then

                                
                                 If objDBConn_TestData_CIS.State = 1 and objDBRcdSet_TestData_CIS.RecordCount >0  Then
                                                                                                                            objDBRcdSet_TestData_CIS.MoveFirst
                                                                                                                            If trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERTYPE")) = trim(WORKORDERTYPE) Then
                                                                                                                                            PrintMessage "p", "CIS OV verification Query Execution","Expected Work Order Type ((" & WORKORDERTYPE &") MATCH expected Work Order Type (" & trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERTYPE")) &")  in DB/Table"
                                                                                                                            Else
                                                                                                                                            PrintMessage "f", "CIS OV verification Query Execution","Expected SO number ((" & temp_XML_Date &") DOES NOT MATCH expected SO (" & trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERTYPE")) &") in DB/Table"
                                                                                                                            End If
                                                                                                                            If trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERTASK")) = trim(WORKORDERTASK) Then
                                                                                                                                            PrintMessage "p", "CIS OV verification Query Execution","Expected SO Number ((" & strSO_Number &") MATCH expected SO (" & trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERTASK")) &")  in DB/Table"
                                                                                                                            Else
                                                                                                                                            PrintMessage "f", "CIS OV verification Query Execution","Expected SO number ((" & temp_XML_Date &") DOES NOT MATCH expected SO (" & trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERTASK")) &") in DB/Table"
                                                                                                                            End If
                                                                                                                            
                                                                                                                            If trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERSTATUS")) = trim(WORKORDERSTATUS) Then
                                                                                                                                            PrintMessage "p", "CIS OV verification Query Execution","Expected WORKORDERSTATUS ((" & WORKORDERSTATUS &") MATCH expected Work Order Type (" & trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERSTATUS")) &")  in DB/Table"
                                                                                                                            Else
                                                                                                                                            PrintMessage "f", "CIS OV verification Query Execution","Expected WORKORDERSTATUS ((" & WORKORDERSTATUS &") DOES NOT MATCH expected SO (" & trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERSTATUS")) &") in DB/Table"
                                                                                                                            End If
                                                                                                                            If trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERREASON")) = trim(WORKORDERREASON) Then
                                                                                                                                            PrintMessage "p", "CIS OV verification Query Execution","Expected WORKORDERREASON ((" & WORKORDERREASON &") MATCH expected SO (" & trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERREASON")) &")  in DB/Table"
                                                                                                                            Else
                                                                                                                                            PrintMessage "f", "CIS OV verification Query Execution","Expected WORKORDERREASON ((" & WORKORDERREASON &") DOES NOT MATCH expected SO (" & trim(objDBRcdSet_TestData_CIS.Fields("WORKORDERREASON")) &") in DB/Table"
                                                                                                                            End If
                                                                                                                            
                                                                                                                            If trim(objDBRcdSet_TestData_CIS.Fields("WORKCOMPLETEDFLAG")) = trim(WORKCOMPLETEDFLAG) Then
                                                                                                                                            PrintMessage "p", "CIS OV verification Query Execution","Expected WORKCOMPLETEDFLAG ((" & WORKCOMPLETEDFLAG &") MATCH expected WORKCOMPLETEDFLAG (" & trim(objDBRcdSet_TestData_CIS.Fields("WORKCOMPLETEDFLAG")) &")  in DB/Table"
                                                                                                                            Else
                                                                                                                                            PrintMessage "f", "CIS OV verification Query Execution","Expected WORKCOMPLETEDFLAG ((" & WORKCOMPLETEDFLAG &") DOES NOT MATCH expected SO (" & trim(objDBRcdSet_TestData_CIS.Fields("WORKCOMPLETEDFLAG")) &") in DB/Table"
                                                                                                                            End If
                                                                                                                            If trim(objDBRcdSet_TestData_CIS.Fields("WAIVEREASON")) = trim(WAIVEREASON) Then
                                                                                                                                            PrintMessage "p", "CIS OV verification Query Execution","Expected WAIVEREASON ((" & WAIVEREASON &") MATCH expected SO (" & trim(objDBRcdSet_TestData_CIS.Fields("WAIVEREASON")) &")  in DB/Table"
                                                                                                                            Else
                                                                                                                                            PrintMessage "f", "CIS OV verification Query Execution","Expected WAIVEREASON ((" & WAIVEREASON &") DOES NOT MATCH expected SO (" & trim(objDBRcdSet_TestData_CIS.Fields("WAIVEREASON")) &") in DB/Table"
                                                                                                                            End If
                                                                                                                             
                                                                                                                            
                                                                                                                            
                                                                                                            Else
                                                                                                                            PrintMessage "f", "CIS verification Query Execution","Failed during execution of CIS Check Query"
                                                                                                                            fn_set_error_flags true, "Failed during execution of CIS SO Check Query"
                                                                                                            End If
                                                                                                            
                                                                                                            ' Perform verifications of actual val
                                    
                                        
                                End If    'end of If lcase(strScratch) <> "fail" Then                            
                                
                                
'                                                    Else
                                PrintMessage "i", "CIS_SO_Check_Query check - SKIPPING", "Skipping CIS_SO_Check_Query as the CIS_SO_Check_Query is blank"
                            End If 'If Trim(strSQL_CIS) <> ""  Then                                            
                            
                        End If ' If lcase(str_ScenarioRow_FlowName) = "flow7" Then
                        
                        ' ####################################### TO BE REMOVED ONCE THE DEFECT RELATED TO CIS IS RESOLVED <<<<<<<<<<<<<<<<<===========================
                        ' ####################################### TO BE REMOVED ONCE THE DEFECT RELATED TO CIS IS RESOLVED <<<<<<<<<<<<<<<<<===========================
                        fn_reset_error_flags
                        ' #########################################################################################################################################
                        ' #########################################################################################################################################
                        
                        
    						If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SO Query error because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
								Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
								int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
								Exit do
							Else
							 	If lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7" or lcase(str_ScenarioRow_FlowName) = "flow7b"  Then
								objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MDN_XML_Creation"
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS API Query SO is completed"
								'Reporter.ReportEvent micPass, "XML", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ") Continue manual check now" 
								'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
								' Function to write timestamp for next process
'													fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
'													int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
'													int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
								'Exit Do
								Else
									If lcase(str_ScenarioRow_FlowName) = "flow8" then													
								 	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SQT_IN_Query_Verification"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS API Query SO is empty"
									PrintMessage "f", "CIS_API_QUERY_SO_VERIFICATION","Query is empty"
									fn_set_error_flags true, "CIS API Query SO is empty - No Service Order created - Pass"

									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									
									Else
										If lcase(str_ScenarioRow_FlowName) = "flow9" then
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MDN_XML_Creation"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS API Query SO is empty"
										PrintMessage "f", "CIS_API_QUERY_SO_VERIFICATION","Query is empty"
										fn_set_error_flags true, "CIS API Query SO is empty - No Service Order created - Pass"

										End If 'End of lcase(str_ScenarioRow_FlowName) = "flow8"
									End If'end of lcase(str_ScenarioRow_FlowName) = "flow9"
								End If 'End of lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7"
				 
				 			End If 'lcase(gFWbln_ExitIteration) = "y" 






						Case  "CIS_API_QUERY_SO_VERIFICATION_OLD"
									
								If objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Find_CIS_SO_Num_DataPrep_Query")).value <> "" Then
									
									If objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_QuerySO_API_Template")).value <> "" Then
																	

										Wait(5)
									
										strSQL_CIS = ""
										strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Find_CIS_SO_Num_DataPrep_Query")).value 													
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
																
					                	' perform all replacements
					                	strSQL_CIS = replace ( strSQL_CIS  , "<NMI>", str_NMI,  1, -1, vbTextCompare)
					                	wait(30)
					                	strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_CIS, objDBRcdSet_TestData_CIS, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", "", "")
					                	If strScratch = "PASS" Then
					                	
						                	If lcase(str_ScenarioRow_FlowName) = "flow8" or lcase(str_ScenarioRow_FlowName) = "flow9" Then
						                	
						                			If trim(lcase(objDBRcdSet_TestData_CIS.Fields("PARAM_SONUM").Value)) = "" Then
														PrintMessage "p", "CIS_SO_DataPrep_VERIFICATION","CIS SO not Created"
													ELse
														PrintMessage "f", "CIS_SO_DataPrep_VERIFICATION","CIS SO Created"
														fn_set_error_flags true, "CIS_SO_DataPrep_VERIFICATION-CIS SO Created"
													End If
						                	
						                	Else
						                	
						                	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SO_Number")).value =trim(lcase(objDBRcdSet_TestData_CIS.Fields("PARAM_SONUM").Value))
						                	objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Find_CIS_SO_Num_DataPrep_Query completed"
						                	
						                	End If
					                	
					                	End If 'end of strScratch = "PASS"
					                	
					                	wait(5)
					                	
					                If lcase(str_ScenarioRow_FlowName) = "flow7" or lcase(str_ScenarioRow_FlowName) = "flow7b" Then
					                	
					                	If strScratch = "PASS"  Then
'										' execute the Query API to retreive data
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_QuerySO_API_Query started"
										strxml_CIS = ""
										strxml_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_QuerySO_API_Template")).value 
										strxml_CIS = fnRetrieve_xml(strxml_CIS)
'		
'					                	' perform all replacements
'										strxml_CIS = replace ( strxml_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
'										strxml_CIS = replace ( strxml_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
'																Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
'										strxml_CIS = replace ( strxml_CIS  , "<PARAM_SONUM>", objDBRcdSet_TestData_A.Fields ("PARAM_SONUM"), 1, -1, vbTextCompare)
'					                	
'					                	Dim url
'					                	url = "http://test-cisapi.corp.chedha.net/citi/RESTfulWS/rest/query"
'					                	fnRetrieve_Response_CIS_api  strxml_CIS,url	
'					                	
'										End If 'objDBRcdSet_TestData_A.RecordCount > 0
'										
'										
										
										
										'--------------
										
										'temp_XML_DATE_formatYYYYMMDDHHmmss = fnUniqueNrFromTS(now(), "YYYYMMDDHHmmss")
												input_file = "C:\_data\XMLs\" & temp_XML_DATE_formatYYYYMMDDHHmmss & ".xml "
												'Output_file = "C:\_data\response_2.xml"
												
												'strxml_CIS = fnRetrieve_xml("C:\Project\TestSuite-MC\meter_contestability\Templates\XML\tpXML_Request_Query_SO")
												
												' perform all replacements
												strxml_CIS = replace ( strxml_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
												'strxml_CIS = replace ( strxml_CIS  , "^param_user_name_eg_RECONECT^", "RECONECT",  1, -1,vbTextCompare)	
												strxml_CIS = replace ( strxml_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
																Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)
												strSO_Number = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SO_Number")).value
												strxml_CIS = replace ( strxml_CIS  , "<PARAM_SONUM>", strSO_Number, 1, -1, vbTextCompare)
												
												'msgbox strxml_CIS
												
												sb_File_WriteContents  input_file, gfsoForAppending, true, strxml_CIS
												wait(1)
												url = "http://test-cisapi.corp.chedha.net/citi/RESTfulWS/rest/query"
												str_response = fnRetrieve_Response_CIS_api (input_file,url)
												'str_response = fnRetrieve_Response_CIS_api (strxml_CIS,url)
												
												'sb_File_WriteContents  Output_file, gfsoForAppending, true, str_response
												wait(1)
												
												set XmlDom1 = CreateObject("Microsoft.XMLDOM")
												XmlDom1.load(str_response)

												
												'Msgbox XMLDOM1.SelectSingleNode("//TaskSubType").text
												
												Response_XML_string= "^SO_Type=" & XMLDOM1.SelectSingleNode("//SOType").text & "^SO_Description=" & XMLDOM1.SelectSingleNode("//SODescription").text & "^SO_TaskType=" & XMLDOM1.SelectSingleNode("//TaskSubType").text & "^SO_Status=" & XMLDOM1.SelectSingleNode("//SOStatusDesc").text _
												& "^SO_Reason=" & XMLDOM1.SelectSingleNode("//SOReasonDescription").text & "^Work_Completed=" & XMLDOM1.SelectSingleNode("//WorkCompleted").text & "^WaiveFee=" & XMLDOM1.SelectSingleNode("//WaiveFee").text & "^"
												

												'Msgbox XMLDOM1.SelectSingleNode("//NetworkServiceOrder").text
												    Dim Response_XML_string
										
												'Update below according to number of tags want to verify 
												 
												If lcase(Response_XML_string) <> ""  Then ' Incase records are not found on xml or row is missing
													strVerification_String = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Response_XML_Tag_Verification")).value
													If trim(lcase(strVerification_String)) = trim(lcase(Response_XML_string)) Then
														PrintMessage "p", "CIS_API_QUERY_SO_VERIFICATION","Actual XML ("& Response_XML_string &") captured from response xml	 MATCH expected SO description ("& strVerification_String &")"
													ELse
														PrintMessage "f", "CIS_API_QUERY_SO_VERIFICATION","Actual XML ("& Response_XML_string &") captured from response xml are DIFFERENT from expected  SO description ("& strVerification_String &")"
														fn_set_error_flags true, "Actual XML ("& Response_XML_string &") captured from  response xml are DIFFERENT from expected SO description ("& strVerification_String &")"
													End If
												End If ' end of  lcase(Response_XML_string) <> ""  
												    
										Else
													temp_error_message =  "CIS_Data_Prep_Query: Number of SQL Query record(s) not matched: Expecting 1 or more record; Actual: " & objDBRcdSet_TestData_CIS.RecordCount & " Records" 
													gFWbln_ExitIteration = "y"
												   	gFWstr_ExitIteration_Error_Reason = "CIS_Data_Prep_Query_CHECK function - " & temp_error_message		
											
										End If ' End of strScratch = "Pass"				                 
																	            	
								End If ' End of flow 7
									' At the end, check if any error was there and write in corresponding column
												If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SO Query error because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
													Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
													int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
													Exit do
												Else
												 	If lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7" or lcase(str_ScenarioRow_FlowName) = "flow7b"  Then
													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MDN_XML_Creation"
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS API Query SO is completed"
													'Reporter.ReportEvent micPass, "XML", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ") Continue manual check now" 
													'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
													' Function to write timestamp for next process
'													fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
'													int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
'													int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
													'Exit Do
													Else
														If lcase(str_ScenarioRow_FlowName) = "flow8" then													
													 	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SQT_IN_Query_Verification"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS API Query SO is empty"
														PrintMessage "f", "CIS_API_QUERY_SO_VERIFICATION","Query is empty"
														fn_set_error_flags true, "CIS API Query SO is empty - No Service Order created - Pass"
				
														fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
														
														Else
															If lcase(str_ScenarioRow_FlowName) = "flow9" then
															objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MDN_XML_Creation"
															objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS API Query SO is empty"
															PrintMessage "f", "CIS_API_QUERY_SO_VERIFICATION","Query is empty"
															fn_set_error_flags true, "CIS API Query SO is empty - No Service Order created - Pass"
				
															End If 'End of lcase(str_ScenarioRow_FlowName) = "flow8"
														End If'end of lcase(str_ScenarioRow_FlowName) = "flow9"
													End If 'End of lcase(str_ScenarioRow_FlowName) = "flow6" or lcase(str_ScenarioRow_FlowName) = "flow7"
									 
									 			End If 'lcase(gFWbln_ExitIteration) = "y" 
										End If 'End of Find_CIS_SO_Num_DataPrep_Query <> ""
									End If


								case "AQ_ALL_SCREEN_CHECK" 
										
											' fn_compare_Current_and_expected_execution_time
											
											tsNow = now() 
											tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
											IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
												Exit do
											End if
											
											'PbWindow("w_frame_4").PbWindow("w_all_activity_queue_srch").PbDataWindow("dw_slct").SelectCell "#1","activity_queue.id" @@ hightlight id_;_219087702_;_script infofile_;_ZIP::ssf13.xml_;_
 @@ hightlight id_;_131754_;_script infofile_;_ZIP::ssf17.xml_;_
											fn_MTS_MenuNavigation "transactions;all aqs"
											
											str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
											int_UniqueReferenceNumberforXML = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("LNSP" & objWS_DataParameter.range("rgWS_RequestID").formula)).value
											
											' Fetch verification string from work sheet
											strVerification_String = ""
											fn_MTS_Search_All_AQ str_NMI,fnTimeStamp(now, "DD/MM/YYYY"), fnTimeStamp(now, "DD/MM/YYYY"), "initTxnID_"& int_UniqueReferenceNumberforXML,  "y", gFW_strRunLog_ScreenCapture_fqFileName
 											temp_str_aqs = fn_MTS_Search_All_AQ_Capture_All_AQs
 											
											If lcase(temp_str_aqs) <> "fail"  Then ' Incase records are not found on MTS or row is missing
												strVerification_String = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("GUI_Verification_CATS_AQ")).value
												If trim(lcase(strVerification_String )) = trim(lcase(temp_str_aqs)) Then
													PrintMessage "p", "AQ_ALL_SCREEN_CHECK","Actual AQs ("& temp_str_aqs &") captured from application MATCH expected Aqs ("& strVerification_String &")"
												ELse
													PrintMessage "f", "AQ_ALL_SCREEN_CHECK","Actual AQs ("& temp_str_aqs &") captured from application are DIFFERENT from expected Aqs ("& strVerification_String &")"
													fn_set_error_flags true, "Actual AQs ("& temp_str_aqs &") captured from application are DIFFERENT from expected Aqs ("& strVerification_String &")"
												End If
											End If ' end of If lcase(temp_str_aqs) <> "fail"  Then
												
										     'End If ' End of strVerification_String = ""  - Remove this after new column
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row("&intRowNr_CurrentScenario&") failed at AQ_ALL_SCREEN_CHECK because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f","MILESTONE - AQ_ALL_SCREEN_CHECK function", "Row("&intRowNr_CurrentScenario&") failed at AQ_ALL_SCREEN_CHECK because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CATS_AQ_SCREEN_CHECK"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "AQ_ALL_SCREEN_CHECK complete"
												PrintMessage "p", "MILESTONE - AQ_ALL_SCREEN_CHECK complete", "AQ_ALL_SCREEN_CHECK complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												
												Exit Do
			            					End If	
			            					

            					case "Meter_Data_Notification_Import_Screen_Check" 
									
										' fn_compare_Current_and_expected_execution_time
										dim temp_str_mdn
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
 @@ hightlight id_;_219087702_;_script infofile_;_ZIP::ssf13.xml_;_
										
										
										' obtaining the MDN_RequestID from worksheet 
										int_UniqueReferenceNumberforXML = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MDN_RequestID")).value
										'Adding NEM_ for MDN requestID
										int_UniqueReferenceNumberforXML = "NEM_"& int_UniqueReferenceNumberforXML
										' Fetch verification string from work sheet
										strVerification_String = ""
										fn_MTS_Search_Meter_Data_Notification_Import fnTimeStamp(now, "DD/MM/YYYY"), fnTimeStamp(now, "DD/MM/YYYY"), int_UniqueReferenceNumberforXML,  "y", gFW_strRunLog_ScreenCapture_fqFileName
											temp_str_mdn = fn_MTS_Meter_Data_Notification_Import_Capture (int_UniqueReferenceNumberforXML)
											
										If lcase(temp_str_mdn) <> "fail"  Then ' Incase records are not found on MTS or row is missing
											strVerification_String = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("GUI_Verification_Meter_Data")).value
											If trim(lcase(strVerification_String )) = trim(lcase(temp_str_mdn)) Then
												PrintMessage "p", "MDN_SCREEN_CHECK","Actual MDNs ("& temp_str_mdn &") captured from application MATCH expected MDNs ("& strVerification_String &")"
											ELse
												PrintMessage "f", "MDN_SCREEN_CHECK","Actual MDNs ("& temp_str_mdn &") captured from application are DIFFERENT from expected MDNs ("& strVerification_String &")"
												fn_set_error_flags true, "Actual MDNs ("& temp_str_mdn &") captured from application are DIFFERENT from expected MDNs ("& strVerification_String &")"
											End If
										End If ' end of If lcase(temp_str_aqs) <> "fail"  Then
											
									     'End If ' End of strVerification_String = ""  - Remove this after new column
										
										' At the end, check if any error was there and write in corresponding column
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row("&intRowNr_CurrentScenario&") failed at Meter_Data_Notification_Import_Screen_Check because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f","MILESTONE - Meter_Data_Notification_Import_Screen_Check function", "Row("&intRowNr_CurrentScenario&") failed at Meter_Data_Notification_Import_Screen_Check because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit do
										Else
										
										
												If lcase(str_ScenarioRow_FlowName) = "flow7" or lcase(str_ScenarioRow_FlowName) = "flow7b" or lcase(str_ScenarioRow_FlowName) = "flow9" Then
												
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In Progress"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SQT_IN_Query_Verification"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Meter_Data_Notification_Import_Screen_Check complete"
														PrintMessage "p", "MILESTONE - AQ_ALL_SCREEN_CHECK complete", "Meter_Data_Notification_Import_Screen_Check complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
														'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
													' Function to write timestamp for next process
													fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											
												
												Else									
																					
														objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Meter_Data_Notification_Import_Screen_Check complete"
														PrintMessage "p", "MILESTONE - AQ_ALL_SCREEN_CHECK complete", "Meter_Data_Notification_Import_Screen_Check complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
														objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
													' Function to write timestamp for next process
													fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
													int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
													int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												
													Exit Do
												End If
		            					End If
		            					
		            					
		            				case "SQT_IN_Query_Verification" 
		            				
		            					
		            					If objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("SQT_IN_Verification_Query")).value <> "" Then	
		            							            															
											If lcase(str_ScenarioRow_FlowName) = "flow7"  or lcase(str_ScenarioRow_FlowName) = "flow7b" or lcase(str_ScenarioRow_FlowName) = "flow8" Then
										
												tsNow = now() 
												tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
												IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
													Exit do
												End if
												
																		
												strSQL_MTS = ""
												'hard coding SQT_In - add this to worksheet once its ready
												'strSQL_MTS = "SQT_IN_RESPONSE_CATS"
												strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("SQT_IN_Verification_Query")).value 
												
												If strSQL_MTS <>"" Then
												
												PrintMessage "p", "SQT_IN_RESPONSE_CHECK","Scenario row("& intRowNr_CurrentScenario &") - Executing SQT IN Verification SQL ("& strSQL_MTS &") "												
												strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
												
							                	strSQL_MTS = replace ( strSQL_MTS  , "<Correlation_id>", str_CorrelationId,  1, -1, vbTextCompare)
							                	

												'KS: Adding a 6min delay due to sqt out currently takes 4-6min. will monitor and update accordingly.
												'wait (300)
												' Execute CIS SQL
												Execute_SQL_Populate_DataSheet    objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2", "", "", "",""	
											
											
												If objDBRcdSet_TestData_B.State > 0 Then 													
											
													If lcase(trim(objDBRcdSet_TestData_B.RecordCount)) = 1 Then
														If lcase(trim(objDBRcdSet_TestData_B.Fields("USBRESPONSE").Value)) = lcase(trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQT_IN_Verification_String")).value)) Then 
																									
															gFWbln_ExitIteration = "n"
															
														Else
															temp_error_message =  "Issue with SQT_INTERFACE_IN_RESPONSE_CHECK. Actual value: USBRESPONSE:" & objDBRcdSet_TestData_B.Fields ("USBRESPONSE") & "; No of Records:"  & lcase(trim(objDBRcdSet_TestData_B.RecordCount)) 
															gFWbln_ExitIteration = "y"
														   	gFWstr_ExitIteration_Error_Reason = "SQT_IN RESPONSE_CHECK function - " & temp_error_message														
														End If
													
													Else
															temp_error_message =  "SQT_IN RESPONSE_CHECK: Number of SQL Query record(s) not matched: Expecting 1 record; Actual: " & objDBRcdSet_TestData_B.RecordCount & " Records" 
															gFWbln_ExitIteration = "y"
														   	gFWstr_ExitIteration_Error_Reason = "SQT_IN RESPONSE_CHECK function - " & temp_error_message		
													End if
												
												Else
													temp_error_message =  "Issue with SQT_IN_RESPONSE_CHECK. No row Returned."
													gFWbln_ExitIteration = "y"
													gFWstr_ExitIteration_Error_Reason = "SQT_IN_RESPONSE_CHECK function - " & temp_error_message		
												End If	
												
												Else
													gFWbln_ExitIteration = "n" ' incase sql is empty do not exit but jump to next function
											End If ' end if of strSQL_MTS <>""

												
										End If ' 

											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at SQT_IN_RESPONSE_CHECK because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												'adding due to the defect- jumping into next function
												'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_QUERY_SO_VERIFICATION"
												
												Reporter.ReportEvent micFail, "SQT_IN_RESPONSE_CHECK Failed", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
												
											ELSE
													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SQT_IN_Check complete"
													PrintMessage "p", "MILESTONE - AQ_ALL_SCREEN_CHECK complete", "Meter_Data_Notification_Import_Screen_Check complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
												' Function to write timestamp for next process
													fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
													int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
													int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												
													Exit Do
												
											
												
			            					End If
									 else
									 
								 End If 

		            					
		            					
		            					
		            					
		            					
		            					
		            					
		            					
		            					
		            					
		            					
		            					
										case "CATS_AQ_SCREEN_CHECK" 
										
											' fn_compare_Current_and_expected_execution_time
											
											tsNow = now() 
											tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
											IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
												Exit do
											End if
											
											fn_MTS_MenuNavigation "Transactions;CATS;Search CATS AQs"
											
											str_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
											int_UniqueReferenceNumberforXML = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("LNSP" & objWS_DataParameter.range("rgWS_RequestID").formula)).value
											
											
											' You need to call the function related to AQ screen
											
											fn_MTS_Search_Cats_AQ str_NMI, fnTimeStamp(now, "DD/MM/YYYY"), fnTimeStamp(now, "DD/MM/YYYY"), int_UniqueReferenceNumberforXML,  "y", gFW_strRunLog_ScreenCapture_fqFileName
 @@ hightlight id_;_526126_;_script infofile_;_ZIP::ssf28.xml_;_
 @@ hightlight id_;_4393158_;_script infofile_;_ZIP::ssf14.xml_;_
 											' fn_MTS_Search_All_AQ_SelectandRecord_All_Results  "first", "",int_NMI, "y", gFW_strRunLog_ScreenCapture_fqFileName
 											temp_str_aqs = fn_MTS_Search_Cats_AQ_Capture_Cats_AQs
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row("&intRowNr_CurrentScenario&") failed at AQ_ALL_SCREEN_CHECK because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f","MILESTONE - AQ_ALL_SCREEN_CHECK function", "Row("&intRowNr_CurrentScenario&") failed at AQ_ALL_SCREEN_CHECK because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "AQ_ALL_SCREEN_CHECK complete"
												PrintMessage "p", "MILESTONE - AQ_ALL_SCREEN_CHECK complete", "AQ_ALL_SCREEN_CHECK complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												
												Exit Do
			            					End If											
				
										Case "WaitingForPrevStep"
										
										temp_last_row_status = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value
										
										If LCase(temp_last_row_status ) = "pass" Then
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start-SQL"
											
										ElseIf LCase(temp_last_row_status ) = "fail" Then
											fn_set_error_flags true, "Step1/previous step of this row failed, marking this as fail"
											
											If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed as Previous Step row failed"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f", "Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											
										Else
											Exit Do
										End If 'end of If LCase(temp_last_row_status ) = "pass" Then
										


										case "FAIL"
											Exit Do
										case else ' incase there is no function (which would NEVER happen) name
											Exit Do
								End Select
					Loop While  (lcase(gFWbln_ExitIteration) = "n" or gFWbln_ExitIteration = false)

				' ################################################################# END of first flow ####################################################################################################
				' ################################################################# END of first flow ####################################################################################################


				Case else ' Incase there is no elaboration for a flow, we need to report in datasheet and move on to next row
				    gFWbln_ExitIteration = "Y"
				    gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
					PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = gFWstr_ExitIteration_Error_Reason
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
			End Select ' end of 	Select Case str_ScenarioRow_FlowName
		
		End If ' end of If  (	( strInScope = cY)  and str_ScenarioRow_FlowName <> "" and (trim(str_Next_Function) = "" or lcase(str_Next_Function) <> "fail" or lcase(str_Next_Function) <> "end") ) Then

		' ####################################################################################################################################################################################
		' #####################################################                 LOGGING STEP 3 - Clear data before moving to next row #########################################################################
		' ####################################################################################################################################################################################
		fnTestExeLog_ClearRuntimeValues
		
		' Reset error flags
		fn_reset_error_flags
		
		' clear scratch dictionary
		clear_scratch_Dictionary gdict_scratch

	Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	

	' objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows

	' Reset error flags
	fn_reset_error_flags

	i = 2
	objWB_Master.save ' Save the workbook 
Loop While int_ctr_no_of_Eligible_rows > 0

PrintMessage "i", "MILESTONE - Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & ") - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= now

objWB_Master.save ' Save the workbook 
Printmessage "i", "MILESTONE - End of Execution", "Finished execution of CATS_WIGS - CATS_4pt2_with_Verification scripts"

' Close MTS
fnMTS_WinClose

' Once the test is complete, move the results to network drive
Printmessage "i", "MILESTONE - Copy to network drive", "Copying results to network drive ("& Cstr_CATS_WIGS_Final_Result_Location &")"
copyfolder  gFWstr_RunFolder , Cstr_CATS_WIGS_Final_Result_Location

ExitAction