﻿



Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong


Dim intScratch
'Added by MK
Dim piValue
Dim piParticipantID
Dim lvoWshNetwork
Dim CurrentNetworkUser

Set lvoWshNetwork = CreateObject("WScript.Network")

CurrentNetworkUser = UCASE(lvoWshNetwork.UserName)





gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

' Dim bln_Scratch :  bln_Scratch = cbool(true)

dim intColNr_InScope, intColNr_RowCompletionStatus
intColNr_InScope = cint(-1) : intColNr_RowCompletionStatus = cint(-1)

Dim str_rowScenarioID, intColNR_ScenarioID

Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = true

Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
Dim thisCell
Dim strPhase, strStage
Dim gFWcell_xferNMI 
Dim strBarText, strBarText_Save, strMsg
Dim cStr_BarText_Format
Dim temp_DataPrep_CIS_SOType, temp_DataPrep_CIS_SOReason , temp_DataPrep_CIS_TaskType, str_Required_SP_Status_MTS, temp_NMI_List

' BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from GIT
BASE_XML_Template_DIR = cBASE_XML_Template_DIR

cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^RequestID`{4}^NMI`{5} ."

Dim strFolderNameWithSlashSuffix_Scratch
' strFolderNameWithSlashSuffix_Scratch = "C:\_data\XMLs\" ' MC\"


' push the RunStats to the DriverWS report-area
Dim iTS, iTS_Max, rowTL, colTL

Dim tsOfRun : tsOfRun = DateAdd("d", -0, now()) ' qq

gDt_Run_TS = tsOfRun

Dim BASE_GIT_DIR ' : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"
BASE_GIT_DIR =   cBASE_GIT_DIR

Dim strCaseBaseAutomationDir 
strCaseBaseAutomationDir  = BASE_GIT_DIR


' ####################################################################################  UFT configurations at the start of test  - START ####################################################################################  

	Dim Temp_Execution_Results_Location  : Temp_Execution_Results_Location = "C:\_data\Test_Execution_Results\"        'Share drive for AMI Energisation End to End project
	Dim strRunLog_Folder
	
	dim qtTest, qtResultsOpt, qtApp
	
	
	Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
	If qtApp.launched <> True then
	    qtApp.Launch
	End If
	
	qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	 qtApp.Options.Run.RunMode = "Fast"
	qtApp.Options.Run.ViewResults = False
	
	
	Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
	Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539


	Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
	str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
	If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
		str_AnFw_FocusArea = ""
	else
		str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
	End If
	
	strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", array(fnAttachStr(Temp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
	    gQtTest.Name                                 , _
	    Parameter.Item("Environment")                       , _
	    Parameter.Item("Company")                           , _
	    fnTimeStamp(gDt_Run_TS, "YYYY`MM`DD_HH`mm`ss")))
	    ' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))


	Path_MultiLevel_CreateComplete strRunLog_Folder
	gFWstr_RunFolder = strRunLog_Folder
	
	qtResultsOpt.ResultsLocation = gFWstr_RunFolder
	gQtResultsOpt.ResultsLocation = strRunLog_Folder
	

	qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
	qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
	qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
	qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
	qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
	qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
	qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
	qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
	qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
	qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
	qtApp.Options.Run.ScreenRecorder.RecordSound = False
	qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True
	
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
	'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
	
	gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
	' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
	gQtApp.Options.Run.ViewResults                = False

	strFolderNameWithSlashSuffix_Scratch = strRunLog_Folder


' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  

' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  

initialize_scratch_Dictionary

'Set gdict_scratch = CreateObject("Scripting.Dictionary")
'gdict_scratch.CompareMode = vbTextCompare
'
clear_scratch_Dictionary gdict_scratch

' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  


 
	'  Description:-  Function to replace arguments in a string format
	'   Parameters:-    1) str_StringFormat - a template string format. An e.g. of template is 
	'						cStr_BarText_Format = "Executing Case`{0}^Envt`{1}^Coy`{2}^Row`{3}^CRCode`{4}^Phase`{5}^Role`{6}^Stage`{7}^RequestID`{8}^NMI`{9} ."
	'						The template should have arguments within {} and the numbering should s
	
	

					
	If 1 = 1  Then		
					
					
					Environment.Value("COMPANY")=Parameter.Item("Company")
					Environment.Value("ENV")=Parameter.Item("Environment")
					    
					' Load the MasterWorkBook objWB_Master
					Dim strWB_noFolderContext_onlyFileName  
					strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
					
					Dim wbScratch
					Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
					dim  dictWSDataParameter_KeyName_ColNr
					
					fnExcel_CreateAppInstance  objXLapp, true
					
					LoadData_RunContext_V3  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
					
					Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
					Set wbScratch = nothing
								
					objXLapp.Calculation = xlManual
					'objXLapp.screenUpdating=False
					
					Dim objWS_DataParameter 						 
					set objWS_DataParameter           					= objWB_Master.worksheets("CIS_NB") ' wsMsDT_mrCntestbty_Objectn") ' qq this must become a parm
					objWS_DataParameter.activate
					
					Dim objWS_templateXML_TagDataSrc_Tables 	
					set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
					set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
					
					Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
					' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
					gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

				'	Get headers of table in a dictionary
					Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
					gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
					
					
					gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
					Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
					gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
					' Load the first/title row in a dictionary
					int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
					fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
					int_MaxColumns = UBound(gvntAr_RuleTable,2)

					objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
					objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
					objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
					objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
					' objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
					objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = now 

					gDt_Run_TS = now
					gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 

					Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE
					
					'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
					Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
					dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
					Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
					intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
					intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
					intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
					fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
					
					rowTL = 1 : colTL = 1
					
					' =========
					' =========  FrameworkPhase 0b   - Technical Setup for Running the SingleScenario Rows   =============
					' =========
					
					Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
					
					dim r
					Dim objDBConn_TestData_A, objDBRcdSet_TestData_A
					
'					On error resume next
		'	add more of these as-required, for querying databases
		'	add more of these as-required, for querying databases
					Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
					r = 0
					set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")
		'	add more of these as-required, for querying databases
		
		
					Dim objDBConn_TestData_B, objDBRcdSet_TestData_B
					Set objDBConn_TestData_B    = CreateObject("ADODB.Connection") : set objDBRcdSet_TestData_B  = CreateObject("ADODB.Recordset")
		'added for CIS database			
					Dim objDBConn_TestData_C, objDBRcdSet_TestData_C
					Set objDBConn_TestData_C    = CreateObject("ADODB.Connection") : set objDBRcdSet_TestData_C  = CreateObject("ADODB.Recordset")


'working here - added for checking something		
					Dim objDBConn_TestData_D, objDBRcdSet_TestData_D
					Set objDBConn_TestData_D    = CreateObject("ADODB.Connection") : set objDBRcdSet_TestData_D  = CreateObject("ADODB.Recordset")

'			Dim	DB_CONNECT_STR_MTS
'					DB_CONNECT_STR_MTS = _
'							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
'						Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
'						Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
'						Environment.Value("USERNAME") 	& 	";Password=" & _
'						Environment.Value("PASSWORD") 	& ";"
'
			Dim	strEnvCoy
					strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")
					
			Dim	DB_CONNECT_STR_CIS
					DB_CONNECT_STR_CIS = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
						Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
						Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
						Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
						Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"

			Dim	DB_CONNECT_STR_IEE
					DB_CONNECT_STR_IEE = _
							"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
						Environment.Value( strEnvCoy & 	"_IEE_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
						Environment.Value( strEnvCoy & 	"_IEE_SERVICENAME"		) 	& "))); User ID=" & _
						Environment.Value( strEnvCoy & 	"_IEE_USERNAME"			) 	& ";Password=" & _
						Environment.Value( strEnvCoy & 	"_IEE_PASSWORD"			)  	& ";"


					Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
					Dim strQueryTemplate, dtD, Hyph, strNMI
					
					Dim intTestData_NMI_ColNr, intTestData_RowIsInScope_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
					Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
					intTestData_RowIsInScope_ColNr  = dictWSDataParameter_KeyName_ColNr("InScopeRowCt_Outbound") ' "InScope"
					
					Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
					
					Dim intSQL_Pkg_ColNr, strSQL_RunPackage
					
					
					Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn
					
					
						intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
						intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
						
						intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
					
								
							'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
						intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
						sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
					
						Dim strColName
						objWS_DataParameter.calculate	'	to ensure the filename is updated
									
									
									
					Dim  str_xlBitNess : str_xlBitNess = ""
									
					sb_File_WriteContents  _
						gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , true, _
						fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
						Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
					
				    Dim intColNr_ScenarioStatus, intColNr_ExitTestIteration_NY, intColNr_ExitTestIteration_ReasonContextEtc
						  
						  intColNr_ExitTestIteration_NY 					= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_NY")
						  intColNr_ExitTestIteration_ReasonContextEtc 	= dictWSDataParameter_KeyName_ColNr.Item("ExitTestIteration_ReasonContextEtc")
					
						Dim blnColumnSet : blnColumnSet = cbool(true)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
						If IsEmpty(intColNr_InScope) then	
							blnColumnSet =false
						ElseIf intColNr_InScope = -1 then
							blnColumnSet =false
						End if
						If not blnColumnSet then 
					'		qq log this	
					'		objXLapp.Calculation = xlManual
							objXLapp.screenUpdating=True
							PrintMessage "f","Unable to locate InScope column", "Unable to locate inscope column( " & Parameter("InScope_Column_Name")  &" )"
							exittest
						End If
					
						objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
						objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
						objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
						objWS_DataParameter.calculate
						objxlapp.screenupdating = true
					
						' objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
					
					' =========
					' =========  FrameworkPhase 0c   - Setup the MultiStage Array                                           =============
					' =========
					
					Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
					Dim  intMasterZoomLevel 
					
					Dim rowStart, rowEnd, colStart, colEnd
					Dim RangeAr 
					
					
					'    ===>>>   Enable HotKeys
					' NOT NEEDED objWB_Master.Application.Run "HotKeys_Set"
					
					'    ===>>>   Make the RunBook Un-Shared, so that all later processes can run, esp the next, Expand, process
					' NOT NEEDED objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
					
					
	End if

				objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = Parameter("InScope_Column_Name")
					
dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = cint(0)	: intFirst= cint(0)	: intSecond = cint(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = cint(1)	' qq


sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , true, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf

	

'	determine the number of in-scope rows
Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun

tsStartOfRun = now()


'	qq     intNrOfRows_Inscope_and_Unfinished

sb_File_WriteContents   	_
	str_Fq_LogFileName, gfsoForAppending , false, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf



dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow

'	now setup to do process the DriverSheet


Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_Scenario : intRowNr_CurrentScenario = cInt(0) ' formerly cLng
Dim cInt_MinutesRequired_toComplete_eachScenario, cDbl_ExtraFactor_ToWait_for_RunToComplete, tsStopRunWhenRunningTooLong
Dim cInt_MinutesPerDay : cInt_MinutesPerDay = 24 * 60
Dim cInt_SecondsPerDay : cInt_SecondsPerDay = 60 * cInt_MinutesPerDay 
Dim dt_rgWS_RunWillCeaseAt_TimeStamp : dt_rgWS_RunWillCeaseAt_TimeStamp = now()-7
cInt_MinutesRequired_toComplete_eachScenario = cInt(5)
cDbl_ExtraFactor_ToWait_for_RunToComplete = cdbl(1.2)

	                
Dim str_ScenarioRow_FlowName 
Dim cStr_FlowNumbers 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= cInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= cInt(-1)

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = false

Dim intColNr_Request_ID 
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
 intColNr_ScenarioStatus 			= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")
 Dim strRole, strInitiator_Role, var_ScheduledDate

Dim strInScope, strRowCompletionStatus

objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=true
objWB_Master.save


	
	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
		' Get headers of table in a dictionary
	Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	

	
		' Load the first/title row in a dictionary
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	
'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
	fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue
	

'	qq	the code below was in the CatsWigs_MultiStage test case, but can be found nowhere in this one , is this significant ? 		<<===  BrianM 2017`03Mar`w11`15Wed
		'		fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table
		'	int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)



' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch applications at startup - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
'
' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
''	
' Reset error flags
fn_reset_error_flags
Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID
Dim str_Company_Name, ctr_Stage, tempInt_RoleIdentificationCounter, Str_Role
Dim strSQL_IEE
Dim strSQL_CIS

int_ctr_total_Eligible_rows = 0
int_ctr_no_of_passed_rows = 0

i = 1

' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PREP - Update company related information in driver sheet %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

' set the state to run the SingleScenarios over, based on the company being tested
  Select Case ucase(Parameter.Item("Company"))
    Case "CITI", "PCOR"
      str_Company_Name = "VIC"
    Case "SAPN"
      str_Company_Name = "SA"
    Case else
    	' Reporter.ReportEvent micFail,"Invalid company name passed as parameter", "COmpany name passed as parameter ("& Parameter.Item("Company") &") is not handled in script...., exiting test"
    	PrintMessage "f","Invalid company name passed as parameter", "COmpany name passed as parameter ("& Parameter.Item("Company") &") is not handled in script...., exiting test"
      ExitAction ' exit the test case
  End Select



Dim strCaseGroup, strCaseNr, strXML_TemplateName_Current, Date_TransactionDate
Dim str_DataParameter_PopulatedXML_FqFileName, str_MTS_Txn_Status
Dim strVerification_String, varAr_Verification_String









' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
' %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SCRIPT EXECUTION STARTS HERE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




Do
	
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
	
									'	intRowNr_LoopWS_RowNr_StartOn   qq
	
		'	if the scenarioRow is InScope-and-Incomplete                                                                                                                 
		strInScope 							= 	ucase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
		str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
		str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
		temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value
		
		' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
		
			If   ucase(strInScope) = ucase(cY)  and str_ScenarioRow_FlowName <> "" and lcase(str_Next_Function) <> "fail" and lcase(str_Next_Function) <> "end" Then
			
				If i = 1 Then
					If ucase(Parameter.Item("RunMode")) = "N" Then ' Value can be N- New or R - restart
						objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start-SQL"
						str_Next_Function = "start-SQL"
					End If
					fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
					int_ctr_total_Eligible_rows = int_ctr_no_of_Eligible_rows
					' Update the company / state name in worksheet for an eligible row
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("list_tState") ).value = str_Company_Name
				End If
			
				' Generate a filename for screenshot file
				gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, left(temp_Scenario_ID,10)))
				
				
			Select Case lcase(str_ScenarioRow_FlowName)
				
				' ################################################################# START of first flow ####################################################################################################
				' ################################################################# START of first flow ####################################################################################################
				Case "flow1":
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
								Select Case str_Next_Function
									Case "start-SQL" ' this is datamine
										' First function
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if

											' capture IEE query
											strSQL_IEE = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_SQL_Template_Name")).value 
											
											If strSQL_IEE = ""  Then
												fn_set_error_flags true, "IEE query template (IEE_SQL_Template_Name) is blank"
											Else
												strQueryTemplate = fnRetrieve_SQL(strSQL_IEE)
												If lcase(strQueryTemplate) <> "fail" Then
													strSQL_IEE = strQueryTemplate
													
													' Perform replacements
										           strSQL_IEE = replace ( strSQL_IEE , "<tariff_type>",  objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Parameter.Item("Company")) & "_Tariff_Type")).value, 1, -1, vbTextCompare)
										           
													' Execute IEE SQL
													strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_IEE, intRowNr_CurrentScenario, "1",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),"")
													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value  = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
										        End If ' end of If lcase(strQueryTemplate) <> "fail" Then
											            	
												
											End if ' end of If strSQL_IEE = ""  Then
											
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												' Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining continued"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Filter_NMI_List_in_CIS"
												' Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												PrintMessage "p", "Data mining continued", "Data mining continued for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
			            					
											strSQL_IEE = ""

									Case "Filter_NMI_List_in_CIS"
									
									tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if


											' capture CIS query
											strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SQL_Template_Name")).value 
											
											If strSQL_CIS = ""  Then
												fn_set_error_flags true, "CIS query template (CIS_SQL_Template_Name) is blank"
											Else
												strQueryTemplate = fnRetrieve_SQL(strSQL_CIS)
												If lcase(strQueryTemplate) <> "fail" Then
													strSQL_CIS = strQueryTemplate
													
													' Perform replacements
										           strSQL_CIS = replace ( strSQL_CIS , "<tariff_type>",  objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Parameter.Item("Company")) & "_tariff_type")).value, 1, -1, vbTextCompare)
										           strSQL_CIS = replace ( strSQL_CIS , "<Nmi_List>", mid((objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value),2,len(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value)), 1, -1, vbTextCompare)
													' Execute CIS SQL
													strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_CIS, intRowNr_CurrentScenario, "1",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),"")
													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value  = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
'													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value  = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
										        End If ' end of If lcase(strQueryTemplate) <> "fail" Then
											            	
												
											End if ' end of If strSQL_CIS = ""  Then
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at Filter_NMI_List_in_CIS because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												' Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Datamine continued"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Get_Details_From_CIS"
												' Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												PrintMessage "p", "Data mining continued", "Data mining continued for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
			            					
			            					strSQL_CIS = ""
										
									Case "Get_Details_From_CIS"
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
											' capture CIS query
											strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Get_Details_From_CIS")).value 
											
											If strSQL_CIS = ""  Then
												fn_set_error_flags true, "CIS query template (Get_Details_From_CIS) is blank"
											Else
												strQueryTemplate = fnRetrieve_SQL(strSQL_CIS)
												If lcase(strQueryTemplate) <> "fail" Then
													strSQL_CIS = strQueryTemplate
													
													' Perform replacements
										           strSQL_CIS = replace ( strSQL_CIS , "<Tariff_Type>",  objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Parameter.Item("Company")) & "_tariff_type")).value, 1, -1, vbTextCompare)
										           strSQL_CIS = replace ( strSQL_CIS , "<Nmi_List>", mid((objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value),2,len(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value)), 1, -1, vbTextCompare)
													' Execute CIS SQL
													strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_CIS, intRowNr_CurrentScenario, "1",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),dictWSDataParameter_KeyName_ColNr("NMI"))
													fnKDR_Insert_Data_CIS_V1 "n", "n",objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value , str_rowScenarioID, "available"
													
													fnKDR_Insert_Data_IEE_V1 "n", "n",objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value , str_rowScenarioID, "available"
													
													'objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value  = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
'													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value  = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
										        End If ' end of If lcase(strQueryTemplate) <> "fail" Then
											            	
												
											End if ' end of If strSQL_CIS = ""  Then
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at Get_Details_From_CIS because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												' Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Datamine continued"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Get_Aggs_Start_Date"
												' Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												PrintMessage "p", "Data mining continued", "Data mining continued for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
										
										strSQL_CIS = ""

									Case "Get_Aggs_Start_Date"
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
											' capture CIS query
											strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Get_Aggs_Start_Date")).value 
											
											If strSQL_CIS = ""  Then
												fn_set_error_flags true, "CIS query template (Get_Aggs_Start_Date) is blank"
											Else
												strQueryTemplate = fnRetrieve_SQL(strSQL_CIS)
												If lcase(strQueryTemplate) <> "fail" Then
													strSQL_CIS = strQueryTemplate
													
													' Perform replacements
										           strSQL_CIS = replace ( strSQL_CIS , "<Tariff_Start>",  objDBRcdSet_TestData_B.Fields ("TARIFF_START"), 1, -1, vbTextCompare)
										           strSQL_CIS = replace ( strSQL_CIS , "<Nmi>", objDBRcdSet_TestData_B.Fields ("NMI"), 1, -1, vbTextCompare)
													' Execute CIS SQL
													strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_C, objDBRcdSet_TestData_C, strSQL_CIS, intRowNr_CurrentScenario, "1",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", dictWSDataParameter_KeyName_ColNr("Aggs_Start_Date"),"")
													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Aggs_Start_Date")).value  = "'" & fnTimeStamp(objDBRcdSet_TestData_C.Fields("AGG_END_DT").Value,"DD/MM/YYYY")
'													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value  = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
										        End If ' end of If lcase(strQueryTemplate) <> "fail" Then
											            	
												
											End if ' end of If strSQL_CIS = ""  Then
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at Get_Aggs_Start_Date because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												' Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Datamine continued"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Get_Aggs_End_Date"
												' Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												PrintMessage "p", "Data mining continued", "Data mining continued for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
										
										strSQL_CIS = ""

									Case "Get_Aggs_End_Date"
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
											' capture CIS query
											strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Get_Aggs_End_Date")).value 
											
											
											If strSQL_CIS = ""  Then
												fn_set_error_flags true, "CIS query template (Get_Aggs_End_Date) is blank"
											Else
												strQueryTemplate = fnRetrieve_SQL(strSQL_CIS)
												If lcase(strQueryTemplate) <> "fail" Then
													strSQL_CIS = strQueryTemplate
													
													' Perform replacements
										           strSQL_CIS = replace ( strSQL_CIS , "<Today>", mid(now(),1,10), 1, -1, vbTextCompare)
										    
										          ' strSQL_CIS = replace ( strSQL_CIS , "<Aggs_Start_date>", mid((objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Aggs_Start_Date")).value),2,10), 1, -1, vbTextCompare)
										           
										           strSQL_CIS = replace ( strSQL_CIS , "<Aggs_Start_date>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Aggs_Start_Date")).value, 1, -1, vbTextCompare)
										           strSQL_CIS = replace ( strSQL_CIS , "<Route_No>",  objDBRcdSet_TestData_B.Fields ("ROUTE_NO"), 1, -1, vbTextCompare)
										           
													' Execute CIS SQL
													strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_D, objDBRcdSet_TestData_D, strSQL_CIS, intRowNr_CurrentScenario, "1",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"),"","","")
													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Aggs_End_Date")).formula  = "'" & fnTimeStamp(objDBRcdSet_TestData_D.Fields ("NEXT_BILL_DATE").Value, "DD/MM/YYYY")
													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Read_Date")).formula  = "'" & fnTimeStamp(objDBRcdSet_TestData_D.Fields ("NEXT_READ_DATE").value, "YYYYMMDD")
													
									
'													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value  = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
										        End If ' end of If lcase(strQueryTemplate) <> "fail" Then
											            	
												
											End if ' end of If strSQL_CIS = ""  Then
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at Get_Aggs_End_Date because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												' Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data Mine Continued"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Get_Interval_Length"
												' Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												PrintMessage "p", "Data mining continued", "Data mining continued for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
										
										strSQL_CIS = ""										

									Case "Get_Interval_Length"
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
											' capture IEE query
											strSQL_IEE = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Get_Interval_Length")).value 
											
											
											If strSQL_IEE = ""  Then
												fn_set_error_flags true, "IEE query template (Get_Interval_Length) is blank"
											Else
												strQueryTemplate = fnRetrieve_SQL(strSQL_IEE)
												If lcase(strQueryTemplate) <> "fail" Then
													strSQL_IEE = strQueryTemplate
													
													' Perform replacements
										           strSQL_IEE = replace ( strSQL_IEE , "<Meter_Id>", objDBRcdSet_TestData_B.Fields ("METER_ID"), 1, -1, vbTextCompare)
										           
													' Execute IEE SQL
													strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_IEE, intRowNr_CurrentScenario, "1",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", dictWSDataParameter_KeyName_ColNr("Interval_Length"),"")
													'objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Aggs_Start_Date")).value  = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value
'													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("IEE_NMI_LIST")).value  = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value
										        End If ' end of If lcase(strQueryTemplate) <> "fail" Then
											            	
												
											End if ' end of If strSQL_CIS = ""  Then
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at Get_Interval_Length because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												' Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data Mine Continued"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Generate_NEM12_Data_File"
												' Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												PrintMessage "p", "Data mining continued", "Data mining continued for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
										
										strSQL_IEE = ""	
										
									Case "Generate_NEM12_Data_File"
										tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if
								
									piParticipantID=objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Parameter.Item("Company")) & "_MDP")).value
									piValue= mid(objDBRcdSet_TestData_B.Fields ("NMI"),1,10) &  "," & objDBRcdSet_TestData_B.Fields ("DATASTREAM") &  "," & objDBRcdSet_TestData_B.Fields ("METER_ID") &  "," & mid((objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Interval_Length")).value),2,len(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Interval_Length")).value)) & "," & objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Read_Date")).value & "," & objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Aggs_Start_Date")).value & "," & objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Aggs_End_Date")).value & "," & "A,,," & piParticipantID 
						

									strScratch=NEM12Data( piValue)
									
									
									'working here
									GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "IEE","All", gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
										
																			If Environment.Value("CATSDestinationFolder") = "<notfound>" or Environment.Value("CATSDestinationFolder") = "" Then
																				' Reporter.ReportEvent micFail,"Gateway destination folder missing", "Not able to locate destination folder for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																				PrintMessage "f","NEM12 Destination folder missing", "Not able to locate destination folder for NEM12 Import - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																				gFWbln_ExitIteration = "y"
																			      gFWstr_ExitIteration_Error_Reason  = "NEM12 destination folder missing - Not able to locate destination folder for NEM12 Import - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																			Else
	 																		call	copyfile (NEM12FileName, cTemp_Execution_Results_Location  ,Environment.Value("CATSDestinationFolder")) ' qq fix this length hack
																				' Reporter.ReportEvent micPass, "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& Str_Role &") - Application (CATS) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																				PrintMessage "p", "NEM12 file placed on IEE server", "NEM12 File placed on ("& Environment.Value("CATSDestinationFolder")&") - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
																				'objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr(trim(Str_Role) & objWS_DataParameter.range("rgWS_Gateway_Destination_Folder").formula)).value =  Environment.Value("CATSDestinationFolder") ' & str_DataParameter_PopulatedXML_FqFileName
																			End If					
																			

											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												' Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "NEM12 creation complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Validate_Interval_Reads"
												' Reporter.ReportEvent micPass, "XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
												 PrintMessage "p", "NEM12 creation and transfer to IEE complete", "NEM12 creation and transfer to IEE complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If																			
									
									
									
										
									case "Validate_Interval_Reads"
									tsNow = now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit do
										End if

											' capture IEE query
											strSQL_IEE = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Validate_Interval_Reads")).value 
											
											If strSQL_IEE = ""  Then
												fn_set_error_flags true, "IEE query template (Validate_Interval_Reads) is blank"
											Else
												strQueryTemplate = fnRetrieve_SQL(strSQL_IEE)
												If lcase(strQueryTemplate) <> "fail" Then
													strSQL_IEE = strQueryTemplate
													
													' Perform replacements
										           
										           strSQL_IEE = replace ( strSQL_IEE , "<NMI>", objDBRcdSet_TestData_B.Fields ("NMI") , 1, -1, vbTextCompare)
										           strSQL_IEE = replace ( strSQL_IEE , "<Aggs_start_Date>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Aggs_Start_Date")).value , 1, -1, vbTextCompare)
										           strSQL_IEE = replace ( strSQL_IEE , "<Aggs_End_Date>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Aggs_End_Date")).value , 1, -1, vbTextCompare)
										      
													' Execute IEE SQL
													strScratch = Execute_SQL_Populate_DataSheet(objWS_DataParameter, DB_CONNECT_STR_IEE, objDBConn_TestData_C, objDBRcdSet_TestData_C, strSQL_IEE, intRowNr_CurrentScenario, "1",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", dictWSDataParameter_KeyName_ColNr("IEE_Aggs_Value"),"")
													
										        End If ' end of If lcase(strQueryTemplate) <> "fail" Then
											            	
												
											End if ' end of If strSQL_IEE = ""  Then
											
											
											' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data prep because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												' Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data prep continued"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Raise_Aggs_Request"
												' Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												PrintMessage "p", "Data mining continued", "Data mining continued for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If
			            					
											strSQL_IEE = ""	
											
											
											case "Raise_Aggs_Request"
											
											fnCIS_Login "y", CurrentNetworkUser, Parameter("Password"), Parameter("Server")
											fn_CIS_MenuNavigation "File;Open Explorer	Ctrl+O"
											fnCIS_Legal_Entry_Property_Search_Other_Search_Criteria  objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI")).value, "NMI Number.", "All Roles","y", "y", gFW_strRunLog_ScreenCapture_fqFileName
											fn_CIS_MenuNavigation "Account;Tariff Charges;Detail" @@ hightlight id_;_7473144_;_script infofile_;_ZIP::ssf38.xml_;_
											fn_CIS_MenuNavigation "Edit;Off Cycle Bill"
											wait(1)
											PbWindow("CIS").PbWindow("Services_to_be_Billed").PbDataWindow("dw_bill_list").SetCellData "#1","fg_select","ON" @@ hightlight id_;_10618890_;_script infofile_;_ZIP::ssf40.xml_;_
											PbWindow("CIS").PbWindow("Services_to_be_Billed").PbButton("btn_OK").Click @@ hightlight id_;_14287736_;_script infofile_;_ZIP::ssf41.xml_;_
											PbWindow("CIS").PbWindow("Services_to_be_Billed").PbWindow("Aggregation_Method").PbDataWindow("dw_1").SetCellData "#1","end_date",objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Aggs_End_Date")).value @@ hightlight id_;_12126298_;_script infofile_;_ZIP::ssf42.xml_;_
											Wait(1)
											PbWindow("CIS").PbWindow("Services_to_be_Billed").PbWindow("Aggregation_Method").PbButton("btn_OK").Click
											Wait(1)
											fn_CIS_MenuNavigation "Account;Aggregation Request"
											
												' At the end, check if any error was there and write in corresponding column
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at Raise_Aggs_Request because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												' Reporter.ReportEvent micFail, "Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "Problem with raising Aggs request", gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "Completed"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data Prep done"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = ""
												' Reporter.ReportEvent micPass, "Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												PrintMessage "p", "Data Prep is complete", "Aggs request successfully raised for ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
			            					End If




'											PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_header").SelectCell "#1","ts_aggr_start" @@ hightlight id_;_1705530_;_script infofile_;_ZIP::ssf57.xml_;_

'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_header").SelectCell "#1","ts_aggr_end" @@ hightlight id_;_1705530_;_script infofile_;_ZIP::ssf58.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_header").SelectCell "#1","ts_aggregation" @@ hightlight id_;_1705530_;_script infofile_;_ZIP::ssf59.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_header").SelectCell "#1","st_aggregation_desc" @@ hightlight id_;_1705530_;_script infofile_;_ZIP::ssf60.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_header").SelectCell "#1","ind_billed_169" @@ hightlight id_;_1705530_;_script infofile_;_ZIP::ssf61.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_header").SelectCell "#1","ts_status" @@ hightlight id_;_1705530_;_script infofile_;_ZIP::ssf62.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_header").SelectCell "#1","tp_aggregation_desc" @@ hightlight id_;_1705530_;_script infofile_;_ZIP::ssf63.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_header").SelectCell "#1","NO_EMP_AGGR_ST_UPDT_DS" @@ hightlight id_;_1705530_;_script infofile_;_ZIP::ssf64.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_data").SelectCell "#1","dt_period_start" @@ hightlight id_;_1181146_;_script infofile_;_ZIP::ssf65.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_data").SelectCell "#1","dt_period_end" @@ hightlight id_;_1181146_;_script infofile_;_ZIP::ssf66.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_data").SelectCell "#1","cd_tpr" @@ hightlight id_;_1181146_;_script infofile_;_ZIP::ssf67.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_data").Click 325,39 @@ hightlight id_;_1181146_;_script infofile_;_ZIP::ssf68.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_data").SelectCell "#1","cd_unit_of_measure" @@ hightlight id_;_1181146_;_script infofile_;_ZIP::ssf69.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").PbDataWindow("dw_aggr_data").SelectCell "#1","pc_substitution" @@ hightlight id_;_1181146_;_script infofile_;_ZIP::ssf70.xml_;_
'PbWindow("CIS").PbWindow("Aggregation_Request").Close
'PbWindow("CIS").PbWindow("ws_070606_bill_history").Move -350,22 @@ hightlight id_;_1705426_;_script infofile_;_ZIP::ssf71.xml_;_
'PbWindow("CIS").PbWindow("ws_070606_bill_history").Close
'PbWindow("CIS").PbWindow("search_results").Close
'	    									PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").SelectCell "#1","user_id"	
'											PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").SetCellData "#1","user_id",strUserName
'	 
'	
'	    								
'	    									PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").SelectCell "#1","password"	
'											PbWindow("CIS").PbWindow("Login").PbDataWindow("panel_Login_Details").SetCellData "#1","password",strPassword
			
											
											
																
			            			
										case "FAIL"
											Exit Do
											case""
											gFWbln_ExitIteration = True
											Exit Do
										case else ' incase there is no function (which would NEVER happen) name
										gFWbln_ExitIteration = True
											Exit Do
								End Select
					Loop While  (lcase(gFWbln_ExitIteration) = "n" or gFWbln_ExitIteration = false)

				' ################################################################# END of first flow ####################################################################################################
				' ################################################################# END of first flow ####################################################################################################


				Case else ' Incase there is no elaboration for a flow, we need to report in datasheet and move on to next row
				    gFWbln_ExitIteration = "Y"
				    gFWstr_ExitIteration_Error_Reason = "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
					' Reporter.ReportEvent micFail, "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
					PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
					objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
					int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
			End Select ' end of 	Select Case str_ScenarioRow_FlowName
		
		End If ' end of If  (	( strInScope = cY)  and str_ScenarioRow_FlowName <> "" and (trim(str_Next_Function) = "" or lcase(str_Next_Function) <> "fail" or lcase(str_Next_Function) <> "end") ) Then

'		' Before resetting the flags, write the last error in comments column
'		If gFWstr_ExitIteration_Error_Reason <> "" Then
'			strScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value 
'			strScratch = strScratch & " : " & gFWstr_ExitIteration_Error_Reason
'			objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value = strScratch 
'		End If
'		
		' Reset error flags
		fn_reset_error_flags
		
		' first write the data of scratch dictionary in excel
		
		' clear scratch dictionary
		clear_scratch_Dictionary gdict_scratch

	Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	

	objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows

	' Reset error flags
	fn_reset_error_flags

	'i = 2
	objWB_Master.save ' Save the workbook 
Loop While int_ctr_no_of_Eligible_rows > 0

' Reporter.ReportEvent micDone, "Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & " ) - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
PrintMessage "i", "Execution Stats", "Total inscope rows ("& int_ctr_total_Eligible_rows & " ) - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= now

objWB_Master.save ' Save the workbook 

fnCIS_WinClose
' Once the test is complete, move the results to network drive
copyfolder  gFWstr_RunFolder , Cstr_CIS_NB_Final_Result_Location

ExitAction

 @@ hightlight id_;_8127086_;_script infofile_;_ZIP::ssf49.xml_;_

 @@ hightlight id_;_7407898_;_script infofile_;_ZIP::ssf37.xml_;_







 @@ hightlight id_;_29688912_;_script infofile_;_ZIP::ssf43.xml_;_
 @@ hightlight id_;_262406_;_script infofile_;_ZIP::ssf21.xml_;_
 @@ hightlight id_;_9700754_;_script infofile_;_ZIP::ssf32.xml_;_