﻿Extern.Declare micLong,"MessageBeep","User32","MessageBeep",micLong
gfwint_DiagnosticLevel = 0 ' CHanging diagnostic level for debugging a particular row

Dim intScratch, cellScratch
gblnStringBuilderReplace = "Y" ' to eliminate calling of stringbuilder function

gstr_Field_Seperator = "^"

Dim intColNr_InScope, intColNr_RowCompletionStatus
intColNr_InScope = CInt(-1) : intColNr_RowCompletionStatus = CInt(-1)

Dim str_rowScenarioID, intColNR_ScenarioID, temp_Next_ScenarioFunction_canStart_atOrAfter_TS

Dim blnDataReservationIsActive_DefaultTrue : blnDataReservationIsActive_DefaultTrue = True

Dim strScratch_dictionary_TemplateKey, strScratch_dictionary_FieldName, strScratch
Dim thisCell
Dim strPhase, strStage
Dim gFWcell_xferNMI 
Dim strBarText, strBarText_Save, strMsg
Dim temp_DataPrep_CIS_SOType, temp_DataPrep_CIS_SOReason , temp_DataPrep_CIS_TaskType, str_Required_SP_Status_MTS, temp_NMI_List

'BASE_XML_Template_DIR = "\\corp\it\MKT\INTER-DEPT\BIP\IT Projects - 2016\16-057 Meter Contestability\Templates\XML\" ' qq hardcoded but need to come from GIT
BASE_XML_Template_DIR = cBASE_XML_Template_DIR

Set hFWobj_SB = DotNetFactory.CreateInstance( "System.Text.StringBuilder" )
Set gFWobj_SystemDate = DotNetFactory.CreateInstance( "System.DateTime" )

' push the RunStats to the DriverWS report-area
Dim cellTopLeft, iTS, iTS_Max, rowTL, colTL

Dim tsOfRun : tsOfRun = DateAdd("d", -0, Now()) ' qq

gDt_Run_TS = tsOfRun

Dim BASE_GIT_DIR : BASE_GIT_DIR =  "C:\Project\TestSuite-MC\meter_contestability\"
Dim strCaseBaseAutomationDir 
' strCaseBaseAutomationDir = BASE_AUTOMATION_DIR
strCaseBaseAutomationDir  = BASE_GIT_DIR

' ####################################################################################  UFT configurations at the start of test  - START ####################################################################################  

Dim Temp_Execution_Results_Location  : Temp_Execution_Results_Location = "C:\_data\Test_Execution_Results\"        'Share drive for AMI Energisation End to End project
Dim strRunLog_Folder

Dim qtTest, qtResultsOpt, qtApp


Set qtApp = CreateObject("QuickTest.Application")    ' http://automated-360.com/qtpuft/automation-object-model-quicktest-professional/
If qtApp.launched <> True Then
	qtApp.Launch
End If 'end of If qtApp.launched <> True Then

qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
qtApp.Options.Run.RunMode = "Fast"
qtApp.Options.Run.ViewResults = False


Set qtTest = qtApp.Test                                            ' http://www.geekinterview.com/question_details/64501
Set qtResultsOpt = CreateObject("QuickTest.RunResultsOptions")     ' https://community.hpe.com/t5/Unified-Functional-Testing/How-to-create-a-Result-Folder-where-QTP-will-store-results/td-p/4205539

Dim str_AnFw_FocusArea ' AutomationFramework_FocusArea
str_AnFw_FocusArea = fnGetEnvtVar("user", "AnFw_FocusArea")
If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 
	str_AnFw_FocusArea = ""
Else
	str_AnFw_FocusArea = "@" & str_AnFw_FocusArea & "_"
End If 'end of If InStr ( 1, str_AnFw_FocusArea, "`NotFound`", vbTextCompare) > 0 Then 

strRunLog_Folder = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}\", Array(fnAttachStr(Temp_Execution_Results_Location,"\","R", gcLibBln_OnlyIfNotAlreadyThere)  , _
gQtTest.Name                                 , _
Parameter.Item("Environment")                       , _
Parameter.Item("Company")                           , _
fnTimeStamp(gDt_Run_TS, "YYYYMMDD_HHmmss")))
'fnTimeStamp(gDt_Run_TS, "YYYY`MM`DD_HH`mm`ss")))
' fnTimeStamp(gDt_Run_TS, "yyyy`MMMmM`wkWW`ddDdD_HH`mm`ss")))

Path_MultiLevel_CreateComplete strRunLog_Folder
gFWstr_RunFolder = strRunLog_Folder

qtResultsOpt.ResultsLocation = gFWstr_RunFolder
gQtResultsOpt.ResultsLocation = strRunLog_Folder


'                                        
'                                        qtApp.Options.Run.ImageCaptureForTestResults = "OnError"
'                                        qtApp.Options.Run.MovieCaptureForTestResults = "Never"
'                                        qtApp.Options.Run.MovieSegmentSize = 2048
qtApp.Options.Run.RunMode = "Fast"
'                                        qtApp.Options.Run.SaveMovieOfEntireRun = False
'                                        qtApp.Options.Run.StepExecutionDelay = 0
'                                        qtApp.Options.Run.ViewResults = False
qtApp.Options.Run.AutoExportReportConfig.AutoExportResults = True
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReport = True
qtApp.Options.Run.AutoExportReportConfig.DataTableReport = True
qtApp.Options.Run.AutoExportReportConfig.LogTrackingReport = False
qtApp.Options.Run.AutoExportReportConfig.ScreenRecorderReport = False
qtApp.Options.Run.AutoExportReportConfig.SystemMonitorReport = True
qtApp.Options.Run.AutoExportReportConfig.ExportLocation = gFWstr_RunFolder
qtApp.Options.Run.AutoExportReportConfig.UserDefinedXSL = ""
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportType = "HTML"
qtApp.Options.Run.AutoExportReportConfig.StepDetailsReportFormat = "Detailed"
qtApp.Options.Run.AutoExportReportConfig.ExportForFailedRunsOnly = False
qtApp.Options.Run.ScreenRecorder.DeactivateShowWindowContents = True
qtApp.Options.Run.ScreenRecorder.RecordSound = False
qtApp.Options.Run.ScreenRecorder.SetPlainWallpaper = True

'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html
gQtApp.Test.Settings.Launchers("Windows Applications").Active = False
'	QTP recognizes Powerbuilder objects as Windows objects  <<==  http://eyeontesting.com/questions/2226/qtp-11-recognizes-powerbuilder-125-objects-as-wind.html

gQtApp.Options.Run.ImageCaptureForTestResults = "OnError"
' gQtApp.Options.Run.RunMode                    = "Fast"  '  <<===== Noooo  qqqq
gQtApp.Options.Run.ViewResults                = False

' ####################################################################################  UFT configurations at the start of test  - END ####################################################################################  

' ####################################################################################  Scratch dictionary to be used across applications - START ####################################################################################  

initialize_scratch_Dictionary
clear_scratch_Dictionary gdict_scratch

' ####################################################################################  Scratch dictionary to be used across applications - End  ####################################################################################  


If 1 = 1  Then		
	
	
	Set gFWoDnf_SysDiags = Dotnetfactory.CreateInstance("System.Diagnostics.StackFrame")
	
	Dim MethodName
	On Error Resume Next
	' MethodName = new oDnf_SysDiags.StackTrace().GetFrame(0).GetMet hod().Name
	MethodName = oDnf_SysDiags.GetMethod().Name
	On Error Goto 0
	Dim BASE_XML_Template_DIR
	
	Environment.Value("COMPANY")=Parameter.Item("Company")
	Environment.Value("ENV")=Parameter.Item("Environment")
	
	Dim intColNr_ScenarioStatus
	
	' Load the MasterWorkBook objWB_Master
	Dim strWB_noFolderContext_onlyFileName  
	strWB_noFolderContext_onlyFileName = Parameter.Item("Plan_Book_Name_With_Ext")
	
	Dim wbScratch
	
	Dim objXLapp, objWS_TestRunContext, dictWSTestRunContext_KeyName_ColNr
	Dim  int_NrOf_NMI_rows_required, dictWSDataParameter_KeyName_ColNr
	
	fnExcel_CreateAppInstance  objXLapp, True
	LoadData_RunContext_V3  objXLapp, wbScratch , objWS_TestRunContext, "TESTDATA",  strCaseBaseAutomationDir & "DataSheets\" & strWB_noFolderContext_onlyFileName, dictWSTestRunContext_KeyName_ColNr, tsOfRun, strRunLog_Folder
	
	If LCase(Environment.Value("COMPANY")) = "sapn" Then
		Environment.Value("COMPANY_CODE")="ETSA"
	Else
		Environment.Value("COMPANY_CODE")=Environment.Value("COMPANY")
	End If 'end of If LCase(Environment.Value("COMPANY")) = "sapn" Then
	
	' ####################################################################################################################################################################################
	' #####################################################                 LOGGING STEP 1 - SETUP      #################################################################################################
	' ####################################################################################################################################################################################
	fn_setup_Test_Execution_Log Parameter.Item("Company"), Parameter.Item("Environment"), "N", "mcB2B_SO_01", GstrRunFileName, "ws_B2B_SO", strRunLog_Folder, gObjNet.ComputerName
	Printmessage "i", "Start of Execution", "Starting execution of mcB2B_SO_01 scripts"
	
	' qq 2017`01Jan`24Tue_16`33`22 BrianM Workaround
	Set objWB_Master = wbScratch 	'	qq does this result in an excessive consumption of resources ?  If so, should this be phoenixed ?
	Set wbScratch = Nothing
	
	objXLapp.Calculation = xlManual
	objXLapp.screenUpdating=False
	
	Dim objWS_DataParameter 						 
	Set objWS_DataParameter           					= objWB_Master.worksheets(Parameter("Worksheet")) ' wsMsDT_mrCntestbty_Objectn") ' qq this must become a parm
	objWS_DataParameter.activate
	
	Dim objWS_templateXML_TagDataSrc_Tables 	
	Set objWS_templateXML_TagDataSrc_Tables 	= objWB_Master.worksheets("templateXML_TagDataSrc_Tables") ' reference the  XML Template WS name
	' Set objWS_CATS_Destination_Folders = objWB_Master.worksheets("CATS_Destination_Folders") ' store DestinationFolders worksheet reference
	' Dim objWS_ScenarioOutComesValidations		: set objWS_ScenarioOutComeValidations 		= objWB_Master.worksheets("wsSpec_ScenarioOutcomeVerify") ' reference the Scenario ValidationRules WS
	
	Dim int_MaxColumns, str_ChangeStatusCode ' Change status code is a TAG in XML and would have the value of the running Stage
	Dim tempInt_RoleIdentificationCounter
	' qq move this assignment inside the function that uses it,   use    ==>>>    if   gVntAr is nothing then instantiate and initialize it, as used in   fn      fnScenarioOutcome_Verify_verNN
	gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array
	
	'	Get headers of table in a dictionary
	Set gdictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
	gdictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare
	
	
	'gvntAr_CATS_Destination_Folder_Table = objWS_CATS_Destination_Folders.Range("rgWsDyn_CATS_Destination_Folder_Table") ' capture the cats destination folder information in an array
	Set gdictWSCATSDestinationFolder_Table_ColValue  = CreateObject("Scripting.Dictionary")
	gdictWSCATSDestinationFolder_Table_ColValue.CompareMode = vbTextCompare
	' Load the first/title row in a dictionary
	'int_MaxColumns = UBound(gvntAr_CATS_Destination_Folder_Table,2)
	' fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow gvntAr_CATS_Destination_Folder_Table, 1, int_MaxColumns, gdictWSCATSDestinationFolder_Table_ColValue
	
	int_MaxColumns = UBound(gvntAr_RuleTable,2)
	
	Dim intLogCol_Start, intLogCol_End,  intLogRow_Start, intLogRow_End, temp_last_row_NextFunctionName
	
	'	if logging is being done to an array, get the size of the range to be loaded to the array, and load it
	If gcFw_intLogMethod    =     gcFw_intLogMethod_RangeToWS Then	
		
		intLogRow_Start        =    gfwobjWS_LogSheet.range("rgWS_RowNr_Data_FirstMinusOne").row        +    1
		intLogRow_End         =    gfwobjWS_LogSheet.range("rgWS_RowNr_Data_LastPlusOne").row        -    1
		intLogCol_Start        =     gfwobjWS_LogSheet.range("rgWS_ColNr_Data_First_MinusOne").column    +    1
		intLogCol_End        =     gfwobjWS_LogSheet.range("rgWS_ColNr_Data_Last_PlusOne").column    -    1
		
		gfwAr_DriverSheet_Log_rangeFor_PullPush = _
		gfwobjWS_LogSheet.range ( gfwobjWS_LogSheet.cells ( intLogRow_Start, intLogCol_Start), gfwobjWS_LogSheet.cells(intLogRow_End, intLogCol_End) )
		intLogRow_End = intLogRow_End         ' qq for debugging    '        else            '    =    gcFw_intLogMethod_WSdirect
		
	End If 'end of If gcFw_intLogMethod    =     gcFw_intLogMethod_RangeToWS Then	
	
	Dim vntNull, mt
	
	MethodName = gFWoDnf_SysDiags.GetMethod().Name		
	objWS_DataParameter.range("rgWS_RunConfig_ENV").cells(1,1).formula 		= "'" & Parameter.Item("Environment")
	objWS_DataParameter.range("rgWS_runConfig_COY").formula 					= "'" & Parameter.Item("Company")
	'objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula 		= "'" & gObjNet.ComputerName
	'objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula	= "'" & gObjNet.UserName
	objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= "0"
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= "0"
	objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula = Now 
	
	
	' setAll_RequestIDs_Unique objWS_DataParameter
	
	'objWS_DataParameter.range("rgWS_cellReportStatus_runPC").formula = "'" & fnWindows_UserName(false)'
	'objWS_DataParameter.range("rgWS_cellReportStatus_runUserId").formula = "'" & fnFW_GetComputerName()
	
	
	gDt_Run_TS = Now
	gFwTsRequestID_Common = fnTimeStamp( gDt_Run_TS, cFWstrRequestID_StandardFormat) 
	
	Dim cellReportStatus_FwkProcessStage, cellReportStatus_ROLE, cellReportStatus_SingleScenario
	'		set cellReportStatus_FwkProcessStage = objWS_DataParameter.range("rgWS_cellReportStatus_FwkProcessStage")
	'		set cellReportStatus_ROLE            = objWS_DataParameter.range("rgWS_cellReportStatus_ROLE")
	
	'      and the dictionary that contains it's columnNames, dictWSDataParameter_KeyName_ColNr
	Set dictWSDataParameter_KeyName_ColNr = CreateObject("Scripting.Dictionary")
	dictWSDataParameter_KeyName_ColNr.CompareMode = vbTextCompare
	Dim intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd
	intWsRowNr_ColumNames = objWS_DataParameter.range("rgWS_ColumName_Row").row
	intWsColNr_ParmsStart = objWS_DataParameter.range("rgWS_DataCol_First").column
	intWsColNr_ParmsEnd   = objWS_DataParameter.range("rgWS_DataCol_Last").column
	fnWS_LoadKey_ColOffsets_toDictionary objWS_DataParameter, intWsRowNr_ColumNames, intWsColNr_ParmsStart, intWsColNr_ParmsEnd-intWsColNr_ParmsStart+1, "", dictWSDataParameter_KeyName_ColNr '' qq chg nrOfCols to a str that accepts "all" or "allCols"
	
	'set cellTopLeft = objWS_DataParameter.range("rgWS_RunReport_TopLeftCell_Stage0_StartTS")
	' rowTL = cellTopLeft.row : colTL = cellTopLeft.column
	rowTL = 1 : colTL = 1
	
	Dim int_NrOf_InScope_Rows, int_NrOf_InScope_Rows_A, int_NrOf_InScope_Rows_B
	
	Dim r
	Dim objDBConn_TestData_A, objDBRcdSet_TestData_A
	
	Set objDBConn_TestData_A    = CreateObject("ADODB.Connection")
	r = 0
	Set objDBRcdSet_TestData_A  = CreateObject("ADODB.Recordset")
	
	
	Dim objDBConn_TestData_B, objDBRcdSet_TestData_B
	Set objDBConn_TestData_B    = CreateObject("ADODB.Connection") : Set objDBRcdSet_TestData_B  = CreateObject("ADODB.Recordset") ': set objDBRcdSet_TestData_C  = CreateObject("ADODB.Recordset")
	Dim	DB_CONNECT_STR_MTS
	DB_CONNECT_STR_MTS = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" 	&	_
	Environment.Value("HOSTNAME") 	&	")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" & _
	Environment.Value("SERVICENAME") 	&	"))); User ID=" & _
	Environment.Value("USERNAME") 	& 	";Password=" & _
	Environment.Value("PASSWORD") 	& ";"
	
	Dim objDBConn_TestData_C, objDBRcdSet_TestData_C
	Set objDBConn_TestData_C    = CreateObject("ADODB.Connection")
	Set objDBRcdSet_TestData_C  = CreateObject("ADODB.Recordset")
	
	
	Dim	strEnvCoy
	strEnvCoy 		= Parameter.Item("Environment") & "_" & Parameter.Item("Company")
	
	Dim	DB_CONNECT_STR_CIS
	DB_CONNECT_STR_CIS = _
	"Provider=OraOLEDB.Oracle; Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" & _
	Environment.Value( strEnvCoy & 	"_CIS_HOSTNAME"			) 	& ")(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=" 		& 	_
	Environment.Value( strEnvCoy & 	"_CIS_SERVICENAME"		) 	& "))); User ID=" & _
	Environment.Value( strEnvCoy & 	"_CIS_USERNAME"			) 	& ";Password=" & _
	Environment.Value( strEnvCoy & 	"_CIS_PASSWORD"			)  	& ";"
	
	Dim intSQL_ColNr, strSQL_FindActiveNmi_ofSize
	Dim strQueryTemplate, dtD, Hyph, strNMI
	Dim intTestData_NMI_ColNr, intCatsCR_ColNr, strCatsCR, str_dlData, str_dlSuffix, str_dlLine, sep :  sep = "`"
	Dim intColNR_BaseDate, intColNR_DateOffset, intColNr_sizeOfNMI
	Dim str_NMI_ColumnName , intReserveDataRC, strReserveData_ResultDesc : intReserveDataRC = 0
	
	Dim intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn

	If Parameter.Item("RowNr_StartOn") = 0 and Parameter.Item("RowNr_FinishOn") = 0 Then
		intRowNr_LoopWS_RowNr_StartOn 	= objWS_DataParameter.Range("rgWS_DataRow_First_MinusOne").Row + 1
		intRowNr_LoopWS_RowNr_FinishOn	= objWS_DataParameter.range("rgWS_DataRow_Last_PlusOne").row - 1
	Else
		intRowNr_LoopWS_RowNr_StartOn 	= Parameter.Item("RowNr_StartOn")
		intRowNr_LoopWS_RowNr_FinishOn	= Parameter.Item("RowNr_FinishOn")
	End If
	
	intColNR_ScenarioID 					= dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") 
	
	'	===>>>   Setting the   intColNr_InScope   colulmn - \EXECUTION flow
	intColNr_InScope = dictWSDataParameter_KeyName_ColNr(Parameter("InScope_Column_Name"))
	sbFW_setRun_Scope  intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn, intRowNr_LoopWS_RowNr_FinishOn, dictWSDataParameter_KeyName_ColNr, objWS_DataParameter  ' set the Run Scope  i.e. Scenarios to run, etc
	
	Dim strColName
	objWS_DataParameter.calculate	'	to ensure the filename is updated
	
	Dim  str_xlBitNess : str_xlBitNess = objWS_DataParameter.range("rgWS_XL_BitNess_x32x64").value
	
	sb_File_WriteContents  _
	gFWstr_RunFolder & "RunContextAndPurpose.txt", gfsoForAppending , True, _
	fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
	Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, str_xlBitNess, objWS_DataParameter.Name, strColName, intColNr_InScope, intRowNr_LoopWS_RowNr_StartOn	, intRowNr_LoopWS_RowNr_FinishOn) )  
	
	Dim blnColumnSet : blnColumnSet = CBool(True)		'	the following statement was tried, but evaluated to empty  ==>>   if ((IsEmpy(intColNr_InScope)) or (intColNr_InScope=-1))
	If IsEmpty(intColNr_InScope) Then	
		blnColumnSet =False
	ElseIf intColNr_InScope = -1 Then
		blnColumnSet =False
	End If 'end of If IsEmpty(intColNr_InScope) Then	
	If Not blnColumnSet Then 
		'		qq log this	
		'		objXLapp.Calculation = xlManual
		objXLapp.screenUpdating=True
		exittest
	End If 'end of If Not blnColumnSet Then 
	
	objXLapp.Calculation = xlManual ' BrianM for the remaining duration of phase_I only, as a test - BrianM_2017`02Feb`w09`28Tue_10`01  
	'objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope
	'objWS_DataParameter.range("rgWS_Stage_Config_LocalOrGlobal") = "Local"			'		<<<===   should this always be the way the run-scope is managed ?
	objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").formula = "'" & intRowNr_LoopWS_RowNr_StartOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowStart").value = intRowNr_LoopWS_RowNr_StartOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).formula = "'" & intRowNr_LoopWS_RowNr_FinishOn
	objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd"  ).value = intRowNr_LoopWS_RowNr_FinishOn
	'		objWS_DataParameter.cells ( intRowNr_LoopWS_RowNr_StartOn  , intColNr_InScope ).select   '   qq reInstate ?? 
	objWS_DataParameter.calculate
	objxlapp.screenupdating = True
	'	objWS_DataParameter.Calculate
	
	objXLapp.ScreenUpdating = False	'	qq want to see if this persists the statusbar info display, if so, we have the minimum visibity that the test is running (i.e. not locked)
	
	
	
	Dim strAr_ProcessStage, intArSz_ProcessStage, iSingleProcess
	Dim  intMasterZoomLevel 
	
	Dim rowStart, rowEnd, colStart, colEnd
	Dim RangeAr 
	
	objWB_Master.Application.Run "WorkBook_UnShare_soUseBy_SingleUser"
	
	
End If 'end of If 1 = 1  Then	

objWS_DataParameter.range("rgWS_Selected_InScopeCol_ColNr") = intColNr_InScope

Dim str_Fq_LogFileName : str_Fq_LogFileName = gFWstr_RunFolder & "aRunLog_" & gQtTest.Name & ".log" ' qq add a 
Dim int_thisFlowFunction_nrOfParms, intFirst, intSecond :  int_thisFlowFunction_nrOfParms = CInt(0)	: intFirst= CInt(0)	: intSecond = CInt(0)
Dim z : z = "z"
Dim intWS_RowNr : intWS_Scenario_RowNr = CInt(1)	' qq



sb_File_WriteContents   	_
str_Fq_LogFileName, gfsoForAppending , True, _
fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )   & vbCrLf

Dim intNrOfRows_Inscope_and_Unfinished, tsStartOfRun

tsStartOfRun = Now()

sb_File_WriteContents   	_
str_Fq_LogFileName, gfsoForAppending , False, _
fn_StringFormatReplacement ("TestName={0},RunOnPC={1},LanUserName={2},UFT_Version={3},MsExcel_Version={4},RunParms=[SheetName/InScopeColName/Nr={5}/{6}/{7},RowStart/End={8}/{9}],RunPurpose=QQ", _
Array(gQtTest.Name, gobjNet.ComputerName, gobjNet.UserName, gqtapp.version, z, z, z, z, z	, z)  )  & vbCrLf

Dim strRunScenario_PreScenarioRow_keyTemplate , strRunScenario_PreScenarioRow_Key
strRunScenario_PreScenarioRow_keyTemplate = _
"^RunNr`{0}^TestName`{1}^Envt`{2}^Coy`{3}^ScenarioList`^ScenarioChunk`^DriverSheetName`{4}^" ' 
'	Examples   1 2 3                              DEVMC      CITI                                                                                                              

strRunScenario_PreScenarioRow_Key  = fn_StringFormatReplacement ( _
strRunScenario_PreScenarioRow_keyTemplate , _
Array ( fnTimeStamp( Now(), 	cFWstrRequestID_StandardFormat) , gQtTest.Name  , Parameter.Item("Environment") , Parameter.Item("Company")  , objWS_DataParameter.name ) )

' the keys below must be set from inside the DataSheet-ScenarioRow processing loop
Dim strRunScenario_ScenarioRow_keyTemplate, strRunScenario_ScenarioRow_Key
strRunScenario_ScenarioRow_keyTemplate = "ScenarioRowNr`{0}^InfoType`FieldValue_Actual^FlowName`{1}^"
' examples                                                                                  1001 1100                                                                   Flow02
Dim str_FlowFunction_Seq_Name_templateKey , str_FlowFunction_Seq_Name_Key 
str_FlowFunction_Seq_Name_templateKey  = "FlowFunction_Seq_Name`{0}_{1}^"
'                                                                                                                                 1   DataMine
Dim str_DataContext_FieldName_templateKey,  str_DataContext_FieldName_Key
str_DataContext_FieldName_templateKey = "DataContext_System_AccessMethod_AccessPoint`{0}_{1}_{2}^FieldName`{3}^Field_ActualValue`inItem^"
'                                                                                                                      dcCIS_SQL_ap1  dcMTS_GUI_ServiceOrder                       NMI


Dim strRunScenario_ActualResults_keyTemplate_BeforeScenarioRow, strRunScenario_ActualResults_Template_forScenarioRow


Dim int_rgWS_cellReportStatus_rowStart : int_rgWS_cellReportStatus_rowStart 	= objWS_DataParameter.range("rgWS_cellReportStatus_rowStart")
Dim int_rgWS_cellReportStatus_rowEnd	: int_rgWS_cellReportStatus_rowEnd 		= objWS_DataParameter.range("rgWS_cellReportStatus_rowEnd")
Const cIncomplete = "Incomplete"
Dim intRowNr_Scenario : intRowNr_CurrentScenario = CInt(0) ' formerly cLng
Dim cInt_MinutesRequired_toComplete_eachScenario, cDbl_ExtraFactor_ToWait_for_RunToComplete, tsStopRunWhenRunningTooLong
Dim cInt_MinutesPerDay : cInt_MinutesPerDay = 24 * 60
Dim cInt_SecondsPerDay : cInt_SecondsPerDay = 60 * cInt_MinutesPerDay 
Dim int_rgWS_NumberOfIncompleteScenarios
Dim dt_rgWS_RunWillCeaseAt_TimeStamp : dt_rgWS_RunWillCeaseAt_TimeStamp = Now()-7
cInt_MinutesRequired_toComplete_eachScenario = CInt(5)
cDbl_ExtraFactor_ToWait_for_RunToComplete = CDbl(1.2)


Dim str_ScenarioRow_FlowName 
'Dim int_FlowHasNotStartedYet : int_FlowHasNotStartedYet =  objWS_DataParameter.range("rgWS_TestHasNotStarted").value
Dim cStr_FlowNumbers 
Dim intColNr_ExitTestIteration : intColNr_ExitTestIteration = dictWSDataParameter_KeyName_ColNr("ExitTestIteration_NY")
Dim  intFlowNr_Current 						:	intFlowNr_Current 						= CInt(0)
Dim int_ScenarioRow_Flow_CurrentStepNr 	:	int_ScenarioRow_Flow_CurrentStepNr	= CInt(-1)
Dim temp_desc_ack, temp_desc_code_ack, temp_explanation_ack

Dim bln_All_InScope_ScenarioRows_HaveFinished_Executing : bln_All_InScope_ScenarioRows_HaveFinished_Executing = False

Dim intColNr_Request_ID 
' intColNr_Request_ID 				= dictWSDataParameter_KeyName_ColNr("Request_ID")
intColNr_Request_ID 				= dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")
intColNr_RowCompletionStatus 	= dictWSDataParameter_KeyName_ColNr("RowCompletionStatus")
intColNr_ScenarioStatus 			= dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail")
Dim intColNr_Flow_CurrentStepNr : intColNr_Flow_CurrentStepNr = dictWSDataParameter_KeyName_ColNr("Flow_CurrentStepNr") 
Dim strRole, strInitiator_Role, var_ScheduledDate, temp_INITIATOR, temp_INITIATOR_NAME

Dim strInScope, strRowCompletionStatus, var_App_Restart_Time
objXLapp.Calculation = xlAutomatic
objXLapp.screenUpdating=True
objWB_Master.save

gvntAr_RuleTable = objWS_templateXML_TagDataSrc_Tables.Range("rgWsDyn_KeyMatch_XMLTag_Replacement_RuleTable") ' before generating XML, pick the XML replacement table in an array

' Get headers of table in a dictionary
Set g2dictWStemplateXML_TagDataSrc_Tables_ColValue = CreateObject("Scripting.Dictionary")
g2dictWStemplateXML_TagDataSrc_Tables_ColValue.CompareMode = vbTextCompare

' Load the first/title row in a dictionary
int_MaxColumns = UBound(										gvntAr_RuleTable,2)

'	g2dictWStemplateXML_TagDataSrc_Tables_ColValue.RemoveAll
fnWS_LoadKey_ColOffsets_toDictionary_fromArray_SingleRow 	gvntAr_RuleTable, 1, int_MaxColumns, g2dictWStemplateXML_TagDataSrc_Tables_ColValue

' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - START xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
' fnMTS_WinClose
'OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")
var_App_Restart_Time = fn_CurrentTimebySeconds(Cint_MTS_Application_Restart_Time)
initialize_performance_Dictionary

' xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx - Launch of applications at startup - ENDS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
''	
' Reset error flags
fn_reset_error_flags
Dim int_Unique_SO_Number, temp_save_SchDate_Original_Value, temp_array_SpecialCOnditions
Dim int_ctr_no_of_Eligible_rows : int_ctr_no_of_Eligible_rows = 0
Dim int_ctr_total_Eligible_rows, int_ctr_no_of_passed_rows, temp_Scenario_ID
Dim temp_no_of_times, int_Ctr_No_Of_Times

int_ctr_no_of_Eligible_rows = 0
int_ctr_total_Eligible_rows = 0
int_ctr_no_of_passed_rows = 0
int_Ctr_No_Of_Times = 0 

i = 1
Do
	
	
	For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart To int_rgWS_cellReportStatus_rowEnd	' Umesh - This need to be reviewed during execution
		
		
		strInScope 							= 	UCase( 	objWS_DataParameter.cells( intRowNr_CurrentScenario, intColNr_InScope  				).value )
		str_ScenarioRow_FlowName 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Scenario_FlowName") ).value 
		str_Next_Function 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value
		temp_Scenario_ID 			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Unique_Scenario_ID") ).value
		temp_no_of_times			= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NO_OF_TIMES") ).value
		
		If temp_no_of_times = "" Then
			temp_no_of_times = 1
		End If
		
		For int_Ctr_No_Of_Times = 1 To temp_no_of_times
			
				
				' MostRecent_Function_Status - this holds the name of most recent function or FAIL, incase the row has failed. The failure reason would be there in comments cell
				
				If   strInScope = cY  And str_ScenarioRow_FlowName <> "" And LCase(str_Next_Function) <> "fail" And LCase(str_Next_Function) <> "end" Then
					
					If i = 1 Then
						PrintMessage "i", "Flow information", "Flow for row ("& intRowNr_CurrentScenario  &") is ("&  str_ScenarioRow_FlowName &")"
						If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 Then
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "WaitingForPrevStep"
							' Remove references (formula) with static value 
							strScratch = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value 
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
		
							strScratch = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value 
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value = strScratch
		
							strScratch = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value 
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value = strScratch
		
						Else
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start"
							str_Next_Function = "start"
							fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
						End If 'end of If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 Then
						
						int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows + 1
						int_ctr_total_Eligible_rows = int_ctr_total_Eligible_rows + 1			
						objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows					
					End If ' end of If i = 1 Then
					
					' Generate a filename for screenshot file
					gFW_strRunLog_ScreenCapture_fqFileName = fn_StringFormatReplacement("{0}{1}_{2}_{3}_{4}.png", Array(gFWstr_RunFolder, Parameter.Item("Environment"), Parameter.Item("Company"), intRowNr_CurrentScenario, Left(temp_Scenario_ID,10)))
					
					' ####################################################################################################################################################################################
					' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
					' ####################################################################################################################################################################################
					fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", "", "", "", "", "", ""
					
					' We need to clear NMI and SQL columns for performance runs
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = ""
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )) = ""
					objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )) = ""
					
					
					Select Case LCase(str_ScenarioRow_FlowName)
						
						Case "flow1", "flow2", "flow3", "flow17","flow1a","flow1b", "flow0",  "flow20", "flow1x":
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
							Select Case str_Next_Function
								Case "start" ' this is datamine
									' First function
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
										
										PrintMessage "i", "CASE Start", "[Data mining] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
										
										fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_SQL_Template_Name", "retrieve", "optional"
										fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_Details_Query", "retrieve", "optional"
										If LCase(str_ScenarioRow_FlowName) = "flow0" Then
											fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "MTS_SQL_Template_Name", "retrieve", "optional"
										else
											If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 Then
												fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "MTS_SQL_Template_Name", "retrieve", "optional"
											Else
												fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "MTS_SQL_Template_Name", "retrieve", "mandatory"
											End If
										End If
										
										If LCase(gFWbln_ExitIteration) <> "y" Or gFWbln_ExitIteration <> True Then
																			
										'retrive nmi
										int_NMI = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI")
																		
										If instr(1,int_NMI, "TECHFAIL-Unable-to-locate") <=0 and int_NMI <> ""  and gFWbln_ExitIteration <> true Then
											
											'calculate schedule date based on offset
											fn_determine_B2B_ScheduleDate objWS_DataParameter, intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr
										
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = int_NMI
											' ********************** Reserve NMI - START ************************
											If blnDataReservationIsActive_DefaultTrue Then
												fnKDR_Insert_Data_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
												fnKDR_Insert_Data_CIS_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												' ####################################################################################################################################################################################
												' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
												' ####################################################################################################################################################################################
												fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", int_NMI, "", "", "", "", ""						                  		
												PrintMessage "p", "Reserve NMI - CANDIDATE","Scenario row ("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR as candidate"	
											End If 'end of If blnDataReservationIsActive_DefaultTrue Then
											' ********************** Reserve NMI - END  ************************
			
											'==========================================================================================================================================
											'KS: ALT_FRMP replace with correct details
											
											If TRIM(UCASE(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator") ).value )) =  "ROLE_ALTFRMP" OR TRIM(UCASE(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator") ).value )) = "ROLE_ALT_FRMP" Then
												'[KS] where or not if alt_frmp has a value, run below function and get a proper value
												'sqlALTFRMP = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "ALT_FRMP")	
													'If TRIM(sqlALTFRMP) = "" Then
												'		'execute cis query 		
												'	End If
												PrintMessage "i", "CASE Start", "[fn_Fetch_ALTFRMP_Value - replacing ALT_FRMP with correct value.]"
												fn_Fetch_ALTFRMP_Value objWS_DataParameter, intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A,DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B
											End If									
											'==========================================================================================================================================
		
		
											' ############################# BELOW CODE WOULD BE EXECUTED ONLY INCASE NMI IS NOT BLANK #######################################################
											
											' Check Required_SP_Status_MTS = D and create service order in CIS
											str_Required_SP_Status_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_MTS") ).value 
											str_Required_Deen_Method_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_Deen_Method_MTS") ).value 
											
											''Note - check if below is required or not ###########
											' ############ DATA PREP via API #####################
											' Koki : We need to sort str_Required_Deen_Method_MTS <> "6" And str_Required_Deen_Method_MTS <> "7" And str_Required_Deen_Method_MTS = ""
											' If (LCase(str_Required_SP_Status_MTS) = "d" And str_Required_Deen_Method_MTS <> "6" And str_Required_Deen_Method_MTS <> "7" And str_Required_Deen_Method_MTS = "" ) And LCase(str_ScenarioRow_FlowName) <> "flow0" And LCase(str_ScenarioRow_FlowName) <> "flow20" Then
											' If (LCase(str_Required_SP_Status_MTS) = "d" And str_Required_Deen_Method_MTS <> "6" And str_Required_Deen_Method_MTS <> "7" And str_Required_Deen_Method_MTS = "" ) And LCase(str_ScenarioRow_FlowName) <> "flow20" Then
											
											Create_Find_CompleteSO str_ScenarioRow_FlowName, objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, str_Required_SP_Status_MTS, str_Required_Deen_Method_MTS, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A									
											
											If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS")).value ) <> "" Then
												fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_API_ModifyServiceProvisionRequest", "insert", "optional"	
												gOverrideTime = Cint_wait_time_TenMin
											End if 'end of If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS")).value ) <> "" Then 										
											
											If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Manual_CIS_SOType") ).value ) <> "" Then
												fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_CreateManualSO_API_Query", "insert", "optional"	
												fn_wait(2) ' Koki: Why this wait ?????
											End If 'end of If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Manual_CIS_SOType") ).value ) <> "" Then
											
											' ###################   Code to take care of multiple special conditions - START ##############################################################
												strScratch	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Special_Condition_Type") ).value 
												If strScratch <> "" Then
													PrintMessage "p", "Special_Condition_Type","Scenario row ("& intRowNr_CurrentScenario &") - Special Condition Requested ("& strScratch &") "
													if gOverrideTime < Cint_wait_time_ThreeMin Then gOverrideTime = Cint_wait_time_ThreeMin
													temp_array_SpecialCOnditions = Split(strScratch,";")
													temp_array_count_items = UBound(temp_array_SpecialCOnditions)
													
													For intctr = 0 To temp_array_count_items 
														If Trim(temp_array_SpecialCOnditions(intctr)) <> "" Then
															gstr_SpecialCondition = Trim(temp_array_SpecialCOnditions(intctr))
															
															' ############ CIS_Property_Service_Provision_Query #####################
															' capture CIS_API_Query to fetch the property ID and Service Providion No
															
															fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_Property_Service_Provision_Query", "retrieve", "mandatory"	
															fn_wait(2) ' Koki: why this wait ????
															fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_API_Special_Condition", "Insert", "mandatory"	
															
														End If ' end of If Trim(temp_array_SpecialCOnditions(intctr)) <> "" Then
													Next ' end of For intctr = 0 To temp_array_count_items 
												End If ' end of If strScratch <> "" Then
											' ###################   Code to take care of multiple special conditions - END ##############################################################								
											
											Create_Service_Provision_Existing_Property str_ScenarioRow_FlowName, objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, str_Required_SP_Status_MTS, str_Required_Deen_Method_MTS, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A
											
										ElseIf  gFWbln_ExitIteration <> true  Then
											fn_set_error_flags true, "NMI not found as some query might have failed. Existing test"
											PrintMessage "f", "NMI not found", gFWstr_ExitIteration_Error_Reason
										End if ' end of If int_NMI <> "" Then	
										End if 'If LCase(gFWbln_ExitIteration) <> "y" Or gFWbln_ExitIteration <> True Then
		
										' At the end, check if any error was there and write in corresponding column
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ") - while executing the query("&g_str_QueryName&")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"								
											'Printmessage "f", "FLOW (" & str_ScenarioRow_FlowName & " datamine error), moving to the next row", gFWstr_ExitIteration_Error_Reason
											' create a function to dump error in a file 
											'strScratch = gFWstr_RunFolder & "Error_Dump_" & fnTimeStamp(now, "YYYY`MM`DD_HH`mm`ss") & ".txt"
											'sb_File_WriteContents  strScratch, gfsoForAppending, true, gFWstr_ExitIteration_Error_Reason
											PrintMessage "f", "MILESTONE - Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed."									
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit Do
										Else
											
											If LCase(str_ScenarioRow_FlowName) = "flow0" Then
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS SO is completed"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												Printmessage "p", "MILESTONE - Data mining", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Data mining complete." 
												Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												Exit Do
												
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML"
												Printmessage "p", "MILESTONE - Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")																		
											End If ''end of If LCase(str_ScenarioRow_FlowName) = "flow0" Then
										End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										strSQL_CIS = ""
										strSQL_MTS = ""
									
									End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
								'										
								Case "XML"
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
									
									If temp_XML_Template  <> "" Then
										
										PrintMessage "i", "CASE Start", "[XML] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ") - starting B2B XML creation with template ("& temp_XML_Template &")"
										
										int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
										objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")).value = int_UniqueReferenceNumberforXML
										fn_wait(1)
										
										'objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss")  ' This would NOT be same as int_UniqueReferenceNumberforXML
										'objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = fnUniqueNrFromTS(now(), "CCYYMMDDHHmmss")  ' This would NOT be same as int_UniqueReferenceNumberforXML
										strScratch = objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("WhetherGenerate_Retailer_Service_OrderNo_YN")).value  
										If Trim(strScratch ) = "" Or Trim(LCase(strScratch)) = "y" Then
											objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss")  ' This would NOT be same as int_UniqueReferenceNumberforXML
										Else
											'retrive so from same retailer
											fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "mts_find_b2bsonum_same_ret", "retrieve", "optional"
											
											'strScratch = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "SAME_RETAILER_SONUM")
											strScratch = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "SAME_RETAILER_SONUM")
											
											If strScratch <> "" Then
												objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = strScratch
											Else	
												'if not found then use any ret so num in the system									
												objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "RET_SERVICEORDERNUMBER")
											End If
											
										End If 'end of If Trim(strScratch ) = "" Or Trim(LCase(strScratch)) = "y" Then
										
										temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
										temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
										int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
										str_ChangeStatusCode = "B2BSO"
										
										' ####################################################################################################################################################################################
										' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
										' ####################################################################################################################################################################################
										fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", int_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""
										PrintMessage "p", "Populate XML","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for ("& str_ChangeStatusCode &") - NMI ("& int_NMI &") - Role ("& temp_Role &")"
										
										str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
										temp_XML_Template, gFWstr_RunFolder, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
										intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now,  int_UniqueReferenceNumberforXML, _
										temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
		
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								           	 	PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
								        Else
											strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
											PrintMessage "p", "XML File","Scenario row ("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
																				
											' ZIP XML File
											ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
											PrintMessage "p", "Zippng XML File","Scenario row ("& intRowNr_CurrentScenario &") - Zipping XML file for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &")"									            
											
											GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
											
											If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
												PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												gFWbln_ExitIteration = "y"
												gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
											Else
												' copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
												store_value_performance_Dictionary gdict_XMLFilename_DestinationFolder, Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", Environment.Value("CATSDestinationFolder")
												PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
											End If 'end of If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
											
											'################# flow1b START ##################################################												
											If LCase(str_ScenarioRow_FlowName) = "flow1b" Then
												
												fn_wait(1)
												
												strSQL_MTS = ""
												strSQL_MTS = "MTS_EVENT_INSERT"
												PrintMessage "p", "MTS_EVENT_INSERT", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_MTS  &")"
												strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
												
												temp_INITIATOR = objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator")).value
												temp_INITIATOR_NAME = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, temp_INITIATOR)
												
												strSQL_MTS = Replace ( strSQL_MTS  , "<INITIATOR>", temp_INITIATOR_NAME ,  1, -1, vbTextCompare)
												strSQL_MTS = Replace ( strSQL_MTS  , "<INIT_TRANSACTION_ID>", objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")) ,  1, -1, vbTextCompare)
												strSQL_MTS = Replace ( strSQL_MTS  , "<PARAM_EVENT_GROUP>", objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("DataPrep_CIS_Action")) ,  1, -1, vbTextCompare)
			
												' Update query in SQL Query cell
												strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
												strScratch = strScratch & vbCrLf & "MTS_EVENT_INSERT Query:" & vbCrLf & strSQL_MTS
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
			
												Execute_Insert_SQL   DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS
												
											End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow1b" Then
											'################# flow1b END ##################################################	
										End If ' end of If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
		
		
									Else
										'PrintMessage "i", "XML Template cell blank", "No XML template located in cell (xmlTemplate_Name) for row (" & intRowNr_CurrentScenario & ")" 
										fn_set_error_flags true, "XML Template cell blank - No XML template located in cell (xmlTemplate_Name)." 
										PrintMessage "f", "XML failure", gFWstr_ExitIteration_Error_Reason
									End If ' end of If temp_XML_Template  <> "" Then
		
		
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " XML creation error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else											
										
										If int_Ctr_No_Of_Times = temp_no_of_times  Then
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Else
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "start"
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										End If
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
										PrintMessage "p", "MILESTONE - XML creation complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										Exit Do
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
		
								Case "B2B_Gateway_Ack_verify"						
								
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[B2B_Gateway_Ack_verify] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "B2B_Gateway_Ack_verify", "retrieve", "mandatory"
									
									strScratch = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_TRANSACTION_STATUS")
									' Verify what the query has returned
									If trim(strScratch ) = "REJECTED" Then
										temp_desc_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESCC")
										temp_desc_code_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESC_CODE")
										temp_explanation_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_EXPLANATION")
										fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - Description ("& temp_desc_ack &") - Desc Code ("& temp_desc_code_ack &") - Explanation ("& temp_explanation_ack &")"
										PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason
									Else
										If trim(strScratch ) = "ACCEPTED" Then
											PrintMessage "p", "B2B_Gateway_Ack_verify check pass", "B2B_ACKV_TRANSACTION_STATUS is ACCEPTED"
										Else
											fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - B2B_ACKV_TRANSACTION_STATUS ("& strScratch &")"
											PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason
											'PrintMessage "p", "B2B_Gateway_Ack_verify check pass", "B2B_Gateway_Ack_verify query has not returned any error and hence moving to next steps"
										End If	'If trim(strScratch ) = "ACCEPTED" Then							
									End If 'If trim(strScratch ) = "REJECTED" Then
									
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at B2B_Gateway_Ack_verify because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " B2B_Gateway_Ack_verify error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else											
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "B2B_Gateway_Ack_verify complete"
										
										If LCase(str_ScenarioRow_FlowName) = "flow17" Then 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Gateway_Response_Check"
										Else
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SOstatus_isRaised"
										End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow17" Then 
																				
										PrintMessage "p", "MILESTONE - B2B_Gateway_Ack_verify complete", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - B2B_Gateway_Ack_verify complete"										
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")													
									End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow1b" Then
									
		
							Case  "CIS_API_SO_Complete"								
							
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
		
								PrintMessage "i", "CASE Start", "[CIS_API_SO_Complete] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
		
								If objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_CompleteSO_API_Query")).value <> "" Then															
									
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_CompleteSO_API_Query started"															
																
									fn_wait(5)
									
									strSQL_CIS = ""
									strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Find_CIS_SO_Num_DataPrep_Query")).value 													
									PrintMessage "p", "Starting Find_CIS_SO_Num_DataPrep_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing Query ("& strSQL_CIS &")"
									strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
									
									' perform all replacements
									strSQL_CIS = Replace ( strSQL_CIS  , "<NMI>", int_NMI,  1, -1, vbTextCompare)
									fn_wait(5)
									
									' Update query in SQL Query cell
									strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
									strScratch = strScratch & vbCrLf & "Find_CIS_SO_Num_DataPrep_Query Query:" & vbCrLf & strSQL_CIS
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
									
									Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", "", ""
									
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Find_CIS_SO_Num_DataPrep_Query completed"
									
									fn_wait(5)
									If objDBRcdSet_TestData_A.RecordCount > 0  Then
										' execute the API to create data
										strSQL_CIS = ""
										strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_CompleteSO_API_Query")).value 
										PrintMessage "p", "Execute CIS_CompleteSO_API_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing Query ("& strSQL_CIS &")"
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										' perform all replacements
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
										Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_SONUM>", objDBRcdSet_TestData_A.Fields ("PARAM_SONUM"), 1, -1, vbTextCompare)
										
										If InStr(1, strSQL_CIS, "<param_ DataPrep_CIS_Action>")>0 Then 
											strSQL_CIS = Replace ( strSQL_CIS  , "<param_ DataPrep_CIS_Action>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_SO_Action" )), 1, -1, vbTextCompare)
										End If 'end of If InStr(1, strSQL_CIS, "<param_ DataPrep_CIS_Action>")>0 Then 
										
										If InStr(1, strSQL_CIS, "<NEW_METER>")>0 Then
											temp_NEW_METER = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "NEW_METER")
											strSQL_CIS = Replace ( strSQL_CIS  , "<NEW_METER>", temp_NEW_METER  ,  1, -1, vbTextCompare)
										End If 'end of If InStr(1, strSQL_CIS, "<NEW_METER>")>0 Then
										
										If InStr(1, strSQL_CIS, "<PARAM_READINGMETHOD>")>0 Then												
											Select Case LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Meter_Type_CIS" )).value)												
												Case "e"
												strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Basic Meter",  1, -1, vbTextCompare)
												Case "e1"
												strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Manually Read Interval Meter",  1, -1, vbTextCompare)
												Case "e2"
												strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Comms4",  1, -1, vbTextCompare)
												Case "e4"
												strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Manually Read Interval Meter",  1, -1, vbTextCompare)													
											End Select 'end of Select Case LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("METER_TYPE_CIS" )).value)												
										End If 'end of If InStr(1, strSQL_CIS, "<PARAM_READINGMETHOD>")>0 Then												
										
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "CIS_CompleteSO_API_Query Query:" & vbCrLf & strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
										
										' Execute complete SO API Query
										Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_CompleteSO_API_Query completed"
										strSQL_CIS = ""
										
										' We need to execute additional API for ModifyServiceProvisionRequest If Required_SP_Status_CIS <> “”
										strScratch = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS")).value 
										
										If Trim(strScratch) <> "" Then
											strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_API_ModifyServiceProvisionRequest")).value 		
											PrintMessage "p", "Starting CIS_API_ModifyServiceProvisionRequest","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - API Modify SP Query ("& strSQL_CIS &")"
											strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
											
											' perform all replacements
											strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
											strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
											Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
											strSQL_CIS = Replace ( strSQL_CIS  , "<Param_NMI>", int_NMI, 1, -1, vbTextCompare)
											strSQL_CIS = Replace ( strSQL_CIS  , "<param_SP_Status>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS" )), 1, -1, vbTextCompare)
											
											fn_wait(5)
		
											' Update query in SQL Query cell
											strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
											strScratch = strScratch & vbCrLf & "CIS_API_ModifyServiceProvisionRequest Query:" & vbCrLf & strSQL_CIS
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		
											'Execute Query
											Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
											strSQL_CIS = ""
										End If 'end of If Trim(strScratch) <> "" Then
									End If 	'end of If objDBRcdSet_TestData_A.RecordCount > 0  Then
									
									'At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SO completion error because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "CIS_API_SO_Complete: FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else											
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "B2BSO is completed"
										PrintMessage "p", "MILESTONE - CIS_API_SO_Complete", "CIS_API_SO_Complete completed for row ("  & intRowNr_CurrentScenario &  ")" 
										Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Exit Do
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								End If 'end of If objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_CompleteSO_API_Query")).value <> "" Then															
		
		'--------------------------------------------------------------------------------------------------------------------------------------------------------------
		'koki working here (flow1x)
		
							Case  "CIS_API_SO_Complete_V2"								
							
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
		
								PrintMessage "i", "CASE Start", "[CIS_API_SO_Complete_v2] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								
								'determine cis_action_type by running the query
								strCISQuery = "CIS_Find_CIS_WO_Action"
								strCISQuery = fnRetrieve_SQL(strCISQuery)
								' perform all replacements						
								strCISQuery = Replace ( strCISQuery  , "<param_CIS_workOrderSubType>", objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType")).value ,  1, -1, vbTextCompare)
								Execute_SQL_Populate_DataSheet_Hatseperator objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strCISQuery, intRowNr_CurrentScenario, "2",   "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  ""					           
		
								strSOType = lcase(objWS_DataParameter.cells (intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("WorkType")).value)
		
								Select Case strSOType
									Case "re-energisation"
										strQuery = "CIS_CompleteSO_API"
									Case "de-energisation"
										' MRIM has 2 tasks							
										If lcase(objWS_DataParameter.cells (intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Meter_Type_CIS")).value) ="e1" Then
											strQuery = "CIS_CompleteSO_API_DEEN_MRIM"									
										'AMI & RRIM has 1 task	
										ElseIf lcase(objWS_DataParameter.cells (intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Meter_Type_CIS")).value) = "e4" or lcase(objWS_DataParameter.cells (intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Meter_Type_CIS")).value) = "e2"   Then
												strQuery = "CIS_CompleteSO_API"
										'Basic has 2 tasks however task 2 reading task required number or registers, so n/a for now. 
										Else
											strQuery = "ERROR"
										End If																
									'[KS] this need future fix
									'Case "metering service works"
									'	strQuery = "CIS_CompleteSO_IM_API"
									Case else
										strQuery = "ERROR"
								End Select
														
		
								If strQuery <> "ERROR" Then															
																
		'							strSQL_CIS = ""
		'							strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Find_CIS_SO_Num_DataPrep_Query")).value 													
		'														
		'							If strSQL_CIS = "" Then
		'								strSQL_CIS = "Find_CIS_SO_Num_DataPrep"
		'							End If
		'							
		'							PrintMessage "p", "Starting Find_CIS_SO_Num_DataPrep_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing Query ("& strSQL_CIS &")"
		'							strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
		'							
		'							' perform all replacements
		'							strSQL_CIS = Replace ( strSQL_CIS  , "<NMI>", int_NMI,  1, -1, vbTextCompare)
		'							fn_wait(5)
		'							
		'							' Update query in SQL Query cell
		'							strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
		'							strScratch = strScratch & vbCrLf & "Find_CIS_SO_Num_DataPrep_Query Query:" & vbCrLf & strSQL_CIS
		'							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
		'							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		'							
		'							Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", "", ""
		'							
		'							'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Find_CIS_SO_Num_DataPrep_Query completed"							
		'							fn_wait(5)							
		'														
		'							If objDBRcdSet_TestData_A.RecordCount > 0  Then
										' execute the API to create data
										strSQL_CIS = ""
										'strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_CompleteSO_API_Query")).value 
										strSQL_CIS = strQuery
										PrintMessage "p", "Execute CIS_CompleteSO_API_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing Query ("& strSQL_CIS &")"
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										' perform all replacements
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
										Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
										
										strCISSO = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value								
										strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_SONUM>", strCISSO, 1, -1, vbTextCompare)
										PrintMessage "i", "CIS_API_SO_Complete_V2","Replace <PARAM_SONUM> with (" & strCISSO & ")."
										
										If InStr(1, strSQL_CIS, "<param_ DataPrep_CIS_Action>")>0 Then 
											temp_value = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","DataPrep_CIS_Action"))
											strSQL_CIS = Replace ( strSQL_CIS  , "<param_ DataPrep_CIS_Action>", temp_value, 1, -1, vbTextCompare)
											PrintMessage "i", "CIS_API_SO_Complete_V2","Replace <param_ DataPrep_CIS_Action> with (" & temp_value & ")."
										End If 'end of If InStr(1, strSQL_CIS, "<param_ DataPrep_CIS_Action>")>0 Then 
										
										If InStr(1, strSQL_CIS, "<NEW_METER>")>0 Then
											temp_NEW_METER = fn_Retrieve_Value_SQL_Attribute(objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, "csListSqlColumn_Names", "csListSqlColumn_Values", intRowNr_CurrentScenario, "NEW_METER")
											strSQL_CIS = Replace ( strSQL_CIS  , "<NEW_METER>", temp_NEW_METER  ,  1, -1, vbTextCompare)
										End If 'end of If InStr(1, strSQL_CIS, "<NEW_METER>")>0 Then
										
										If InStr(1, strSQL_CIS, "<PARAM_READINGMETHOD>")>0 Then												
											Select Case LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Meter_Type_CIS" )).value)												
												Case "e"
												strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Basic Meter",  1, -1, vbTextCompare)
												Case "e1"
												strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Manually Read Interval Meter",  1, -1, vbTextCompare)
												Case "e2"
												strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Comms4",  1, -1, vbTextCompare)
												Case "e4"
												strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_READINGMETHOD>", "Manually Read Interval Meter",  1, -1, vbTextCompare)													
											End Select 'end of Select Case LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("METER_TYPE_CIS" )).value)												
										End If 'end of If InStr(1, strSQL_CIS, "<PARAM_READINGMETHOD>")>0 Then												
										
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "CIS_CompleteSO_API_Query Query:" & vbCrLf & strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
										
										' Execute complete SO API Query
										Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_CompleteSO_API_Query completed"
										strSQL_CIS = ""
																		
		'							End If 	'end of If objDBRcdSet_TestData_A.RecordCount > 0  Then
									
									'At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SO completion error because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "CIS_API_SO_Complete: FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else	
		
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "NMIREF_CHECK"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_API_SO_Complete complete"
										PrintMessage "p", "MILESTONE - CIS_API_SO_Complete complete", "CIS_API_SO_Complete complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_TenMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								End If 'end of If objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_CompleteSO_API_Query")).value <> "" Then															
						
		
							'koki working here
							Case  "NMIREF_CHECK"								
							
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
		
								PrintMessage "i", "CASE Start", "[NMIREF_CHECK] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
		
								'strVerificationString = "^MARKET=SUCCESS^CONFIGURATION=SUCCESS^RATE_ASSIGNMENT=SUCCESS^SP_INSTANCE=SUCCESS^"
								strExpectedVerificationString = "^MARKET=SUCCESS^MTR_INSTANCE=SUCCESS^RATE_ASSIGNMENT=SUCCESS^"
		
								strScratch = fn_Verify_B2B_NMIREF(objWS_DataParameter, intRowNr_CurrentScenario,dictWSDataParameter_KeyName_ColNr, strExpectedVerificationString, "^")
			
								If LCase(strScratch) <> "pass" Then
									fn_set_error_flags true , strScratch
									'gFWstr_ExitIteration_Error_Reason = strScratch
								End If
								
								' Reset error flags temp as AMI meter NMI Ref always fails.
								fn_reset_error_flags
		
								'At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "NMIREF_CHECK error because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "NMIREF_CHECK: FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else											
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SOstatus_isCompleted"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "NMIREF_CHECK complete"
									PrintMessage "p", "MILESTONE - NMIREF_CHECK Complete", "NMIREF_CHECK complete for row ("  & intRowNr_CurrentScenario &  ")" 
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
		
						Case "MTS_Check_SOstatus_isCompleted"
														
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
								
								PrintMessage "i", "CASE Start", "[MTS_Check_SOstatus_isCompleted] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & "), NMI ("& int_NMI &")."
								
		
								MTS_Check_SOstatus_isRaised_V2 objWS_DataParameter,intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr,_
																"",	int_NMI, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value, _
																"first",_														
																"COMPLETED", "", "", "", "", "","y", "n", "",""												
							
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Check_SOstatus_isCompleted because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isCompleted error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_SO_Response_Check"
									PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isCompleted complete", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - MTS_Check_SOstatus_isCompleted complete" 
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								End If ' end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
		
							Case "MTS_SO_Response_Check"
														
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[MTS_SO_Response_Check] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
														
								'Navigate to "Transactions;Service Orders;Service Order Request Search"
								fn_MTS_MenuNavigation "transactions;service orders;service order response search"	
								PrintMessage "i", "Case: MTS_SO_Response_Check", "service order response search navigation completed."
				
								pistr_Ret_SO_Num = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber" )).value
								pistr_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
								pistr_NMI = left(pistr_NMI,10)
								pistr_CIS_SO_Num = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber" )).value
								
								'---
								'determine cis_product code by running the query
								strCISQuery = "CIS_Find_CIS_WO_Charges"
								strCISQuery = fnRetrieve_SQL(strCISQuery)
								' perform all replacements						
								strCISQuery = Replace ( strCISQuery  , "<param_cis_so_num>", pistr_CIS_SO_Num ,  1, -1, vbTextCompare)
								Execute_SQL_Populate_DataSheet_Hatseperator objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strCISQuery, intRowNr_CurrentScenario, "2",   "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  ""					           
								pistr_Product_Code = Ucase(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","CIS_WO_CHG")))
								'pistr_Product_Code = "RCTBH"
								'---
				
								fn_MTS_Search_Service_Order_Response pistr_Ret_SO_Num, "", pistr_NMI, fnTimeStamp(now,"DD/MM/YYYY"), fnTimeStamp(now,"DD/MM/YYYY"), "", "", "" 
								'(pistr_SO_ID, pistr_Tnx_ID, pistr_NMI, pivar_Sent_From, pivar_Sent_To, pistr_Initiator_ID, pistr_Work_Completed_Status, pistr_Response_Status)		
				
								fn_wait (3)
								
								PbWindow("MTS").PbWindow("Search_Service_Order_Response_Screen").Activate
								
								PbWindow("MTS").PbWindow("Search_Service_Order_Response_Screen").PbDataWindow("panel_Service_Order_Response").SelectCell "#1","work_completed_status"
								PrintMessage "i", "Case: MTS_SO_Response_Check", "#1 record selected."
								
								fn_MTS_Click_button_any_screen "Search_Service_Order_Response_Screen", "btn_Details"
								PrintMessage "i", "Case: MTS_SO_Response_Check", "btn_Details clicked."
								
		'						fn_MTS_Service_Order_Response_Details pistr_NMI, _
		'																pistr_Ret_SO_Num, _
		'																pistr_Exception_code_cd, _
		'																pistr_Special_Notes, _
		'																pistr_Work_Completed_Status, _
		'																pistr_Product_Code, _
		'																pistr_SO_Type, _
		'																pistr_SO_Subtype, _
		'																pistr_CIS_WO_Type, _
		'																pistr_CIS_SO_Num, _
		'																pistr_Response_Status, _
		'																pibtnclick_SO_AQ_Details, _
		'																pibtnclick_Close, _
		'																WhetherCaptureScreenshot_YN
		
								fn_MTS_Service_Order_Response_Details pistr_NMI, _
																		pistr_Ret_SO_Num, _
																		"", _
																		"", _
																		"Completed", _
																		pistr_Product_Code, _
																		"", _
																		"", _
																		"", _
																		pistr_CIS_SO_Num, _
																		"SENT", _
																		"", _
																		"y", _
																		"y"
		
		
								fn_MTS_Close_any_screen "Search_Service_Order_Response_Screen"
								
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_SO_Response_Check because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_SO_Response_Check error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
		'							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
		'							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_SO_Response_Check complete"
		'							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "IEE_Query_Validation"
		'							PrintMessage "p", "MILESTONE - MTS_SO_Response_Check complete", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - MTS_SO_Response_Check complete" 
		'							' Function to write timestamp for next process
		'							fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		'							
		'							
									
								  	objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
		                            objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
		                            objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_SO_Response_Check complete"
		                            PrintMessage "p", "MILESTONE - MTS_SO_Response_Check complete", "MTS_SO_Response_Check complete for row ("  & intRowNr_CurrentScenario &  "). All verifications passed" 
		                            Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
		                            objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
		                            ' Function to write timestamp for next process
		                            fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		                            int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
		                            int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
									
								End If ' end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
		' koki working here ends. (for flow 1x)
		'--------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		
								'Need to write function for 'Read Time stamp and verify time has Passed'
								Case "Gateway_Response_Check"
								
								If LCase(str_ScenarioRow_FlowName) = "flow17" or LCase(str_ScenarioRow_FlowName) = "flow3" Then
									
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[Gateway_Response_Check] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									'[KS] new function created to handle multiple rejection error in gateway tnx response: fnVerify_B2B_Gateway_TnxResponse
									If isempty(dictWSDataParameter_KeyName_ColNr("Verify_GW_TnxResponse" )) Then
										strExpectedVerificationString = "" 
									Else
										strExpectedVerificationString = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Verify_GW_TnxResponse")).value
									End If
																
									If strExpectedVerificationString <> "" Then
										strSQL_MTS = "Gateway_Response_Check_v2" 
										strStratch = fnVerify_B2B_Gateway_TnxResponse(objWS_DataParameter, intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, strSQL_MTS, strExpectedVerificationString)
									
										If strStratch <> "PASS" Then								
											fn_set_error_flags true, strStratch
										End If						
									
									Else		
										
										
										
										
										
										
										strSQL_MTS = ""
										strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Gateway_Response_Check_Query")).value 		
										
										If Trim(strSQL_MTS) <> ""  Then
										
											temp_all_Error_Messages = ""
											PrintMessage "p", "Gateway_Response_Check_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - MTS SQL Template ("& strSQL_MTS &") "
											strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
											
											' perform all replacements
											strSQL_MTS = Replace ( strSQL_MTS  , "<INIT_TRANSACTION_ID>", "SOR_" & objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")),  1, -1, vbTextCompare)
			
											' Update query in SQL Query cell
											strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
											strScratch = strScratch & vbCrLf & "INIT_TRANSACTION_ID Query:" & vbCrLf & strSQL_MTS
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
			
											Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2", "", "", "",""	
											
											If objDBRcdSet_TestData_B.State > 0 Then	
												
												If objDBRcdSet_TestData_B.RecordCount > 0  Then
													
													' Verify output of query
													' check STATUS
													str_Expected = "" : str_Actual = ""
													str_Expected = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value))
													str_Actual = Trim(LCase(objDBRcdSet_TestData_B.Fields ("STATUS")))
													If str_Expected <> "" Then
														If str_Actual = str_Expected Then
															PrintMessage "p", "Gateway_Response_Check - STATUS Same", "Actual value of STATUS ("&  str_Actual  &") is same as expected value of Status ("&  str_Expected  &")"
														Else								
																fn_set_error_flags true, "Actual value of STATUS ("&  str_Actual &") is diffrent from Expected value ("&  str_Expected  &")"
																PrintMessage "f", "Gateway_Response_Check - STATUS DIFFERENT",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "											
														End If 'end of If Trim(LCase(objDBRcdSet_TestData_B.Fields ("STATUS"))) = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value)) Then
													Else
														PrintMessage "i", "Gateway_Response_Check - skipping STATUS checks", "As NewSO_Status_InMTS is blank, skipping status check"
													End If
													
													' check CODE
													str_Expected = "" : str_Actual = ""
													str_Expected = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason_Code")).value))
													str_Actual = Trim(LCase(objDBRcdSet_TestData_B.Fields ("CODE")))
												
													If str_Expected <> "" Then
														If str_Actual = str_Expected Then
															PrintMessage "p", "Gateway_Response_Check - CODE same", "Actual value of Rejection_CODE ("&  Trim(LCase(objDBRcdSet_TestData_B.Fields ("CODE")))  &") is same as expected value of Code ("&  Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason_Code")).value))  &")"
														Else								
																fn_set_error_flags true, "Actual value of Rejection_CODE ("&  str_Actual &") is diffrent from Expected value ("&  str_Expected  &")"
																PrintMessage "f", "Gateway_Response_Check - CODE DIFFERENT",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "	
														End If 'end of If Trim(LCase(objDBRcdSet_TestData_B.Fields ("STATUS"))) = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value)) Then
													Else
														PrintMessage "i", "Gateway_Response_Check - skipping Rejection_CODE checks", "As Rejection_Reason_Code is blank, skipping status check"
													End If
				
													'[KS] flow17 desc has desc & explanation concatinated in SQL & Driversheet. Flow3 is dealt separately
													' check Explanation (Rejection_code & explanation)
													 If LCase(str_ScenarioRow_FlowName) = "flow17" Then	
													 
														' check DESCRIPTION for flow17
														
														str_Expected = "" : str_Actual = ""
														str_Expected = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value))
														str_Actual = Trim(LCase(objDBRcdSet_TestData_B.Fields ("DESCRIPTION")))
												
														If str_Expected <> "" Then
															If str_Actual = str_Expected Then
																PrintMessage "p", "Gateway_Response_Check - DESCRIPTION same", "Actual value of Description ("&  Trim(LCase(objDBRcdSet_TestData_B.Fields ("DESCRIPTION")))  &") is same as expected value of Description ("&  Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value))  &")"
															Else								
																fn_set_error_flags true, "Actual value of DESCRIPTION ("&  str_Actual &") is diffrent from Expected value ("&  str_Expected  &")"
																PrintMessage "f", "Gateway_Response_Check - DESCRIPTION DIFFERENT",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "	
															End If 'end of If Trim(LCase(objDBRcdSet_TestData_B.Fields ("STATUS"))) = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value)) Then
														Else
															PrintMessage "i", "Gateway_Response_Check - skipping DESCRIPTION checks", "As Rejection_Explanation is blank, skipping status check"
														End If
																						
													 End If 'If LCase(str_ScenarioRow_FlowName) = "flow17" Then
				
														
													'Check for flow3
													 If LCase(str_ScenarioRow_FlowName) = "flow3" Then
													 
													 	' check Rejection_Reason
													 	
													 	str_Expected = "" : str_Actual = ""
														str_Expected = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value))
														str_Actual = Trim(LCase(objDBRcdSet_TestData_B.Fields ("REJECTION_REASON")))								
													 	
														If str_Expected <> "" Then
															If str_Actual = str_Expected Then
																PrintMessage "p", "Gateway_Response_Check - REJECTION_REASON same", "Actual value of REJECTION_REASON ("&  Trim(LCase(objDBRcdSet_TestData_B.Fields ("REJECTION_REASON")))  &") is same as expected value of Description ("&  Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value))  &")"
															Else								
																fn_set_error_flags true, "Actual value of REJECTION_REASON ("&  str_Actual &") is diffrent from Expected value ("&  str_Expected  &")"
																PrintMessage "f", "Gateway_Response_Check - REJECTION_REASON DIFFERENT",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "	
															End If 'end of If Trim(LCase(objDBRcdSet_TestData_B.Fields ("STATUS"))) = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value)) Then
														Else
															PrintMessage "i", "Gateway_Response_Check - skipping REJECTION_REASON checks", "As REJECTION_REASON is blank, skipping status check"
														End If
				
														' check Rejection_Explanation
														str_Expected = "" : str_Actual = ""
														str_Expected = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value))
														str_Actual = Trim(LCase(objDBRcdSet_TestData_B.Fields ("EXPLANATION")))																	 	
														
														If str_Expected <> "" Then
															If str_Expected <> "" Then
																PrintMessage "p", "Gateway_Response_Check - EXPLANATION same", "Actual value of EXPLANATION ("&  Trim(LCase(objDBRcdSet_TestData_B.Fields ("EXPLANATION")))  &") is same as expected value of Description ("&  Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value))  &")"
															Else								
																fn_set_error_flags true, "Actual value of EXPLANATION ("&  str_Actual &") is diffrent from Expected value ("&  str_Expected  &")"
																PrintMessage "f", "Gateway_Response_Check - EXPLANATION DIFFERENT",  gFWstr_ExitIteration_Error_Reason
																temp_all_Error_Messages = temp_all_Error_Messages & gFWstr_ExitIteration_Error_Reason & " ; "	
															End If 'end of If Trim(LCase(objDBRcdSet_TestData_B.Fields ("STATUS"))) = Trim(LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value)) Then
														Else
															PrintMessage "i", "Gateway_Response_Check - skipping EXPLANATION checks", "As Rejection_Explanation is blank, skipping status check"
														End If
				
													End if 'If LCase(str_ScenarioRow_FlowName) = "flow3" Then
													
													
													If gFWbln_ExitIteration Then
														' Incase there is any error, just exit
														gFWstr_ExitIteration_Error_Reason = temp_all_Error_Messages
														temp_all_Error_Messages = ""
													End If ' end of If gFWbln_ExitIteration Then
			
												Else
													' temp_error_message =  "No response file generated at gateway"
													temp_error_message =  "No response file generated at gateway. Please check INIT_TRANSACTION_ID Query appended in SQL_Populated column"
													fn_set_error_flags true, temp_error_message
												End If 'end of If objDBRcdSet_TestData_B.RecordCount > 0  Then
											Else
												fn_set_error_flags true, "Gateway_Response_Check: Issue with Gateway_Response_Check. No row Returned. Please check INIT_TRANSACTION_ID Query appended in SQL_Populated column"
												PrintMessage "f", "Gateway_Response_Check - no record found",gFWstr_ExitIteration_Error_Reason
											End If 'end of If objDBRcdSet_TestData_B.State > 0 Then	
											
										Else
											fn_set_error_flags true, "Gateway_Response_Check_Query missing - Gateway_Response_Check_Query is missing, which is a mandatory step"
											PrintMessage "f", "Gateway_Response_Check_Query missing",gFWstr_ExitIteration_Error_Reason
										End If 'If Trim(strSQL_MTS) <> ""  Then
																	
									End If 'If strExpectedVerificationString <> "" Then
									
		
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at Gateway_Response_Check because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										Printmessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " - gateway response check failure), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Gateway_Response_Check complete"
										Printmessage "p", "MILESTONE - Gateway_Response_Check complete", "Gateway_Response_Check complete for row ("  & intRowNr_CurrentScenario &  ")" 
										Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Exit Do
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								End If 'End of If LCase(str_ScenarioRow_FlowName) = "flow17" Then
								
								Case "MTS_Check_SOstatus_isRaised"
								
								' fn_compare_Current_and_expected_execution_time
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[MTS_Check_SOstatus_isRaised] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								 						
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value						
								
								PrintMessage "p", "MTS_Check_SOstatus_isRaised","Scenario row ("& intRowNr_CurrentScenario &") - MTS GUI Search & Verify for NMI (" & int_NMI & "), Ret_SONUM (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value & "), Status (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value &  "), AQ_Name (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value  &  "), AQ_Explaination (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value &  "), AQ_Status (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value &  "), Rejection_Reason (" & _														
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value &  "), Rejection_Explanation (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value	& ")"																
																			
								
								'verify MTS GUI
								'MTS_Check_SOstatus_isRaised int_NMI,
								
								MTS_Check_SOstatus_isRaised_V2 objWS_DataParameter,intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr,_
																"",	int_NMI, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"first",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,"y", "n", "",""
								
								
								If gFWbln_ExitIteration <> True AND (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow1a" Or LCase(str_ScenarioRow_FlowName) = "flow20")  And LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value) ="raised" Then
									If Trim(gdict_scratch("CIS_ServiceOrderNumber")) = "" Then
										gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & " & CIS SO not generated in MTS"
										gFWbln_ExitIteration = True
									Else
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value = gdict_scratch("CIS_ServiceOrderNumber")
									End If 'end of If Trim(gdict_scratch("CIS_ServiceOrderNumber")) = "" Then
									
		'							If Trim(gdict_scratch("Ret_ServiceOrderNumber")) = "" Then
		'								fn_set_error_flags true, gFWstr_ExitIteration_Error_Reason & " - Retailer Service Order not found in MTS"
		'							Else
		'								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = gdict_scratch("Ret_ServiceOrderNumber")
		'							End If 'end of If Trim(gdict_scratch("CIS_ServiceOrderNumber")) = "" Then
									
								End If 'end of If (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow1a" Or LCase(str_ScenarioRow_FlowName) = "flow20")  And LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value) ="raised" Then
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_Check_SOstatus_isRaised because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									Printmessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isRaised error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									
									If  (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow20")  And objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )) <> "" Then	
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_Check_SOstatus_isRaised"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SQT_INTERFACE_OUT_RESPONSE_CHECK"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
										Printmessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised complete", "MTS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										
									ElseIf  LCase(str_ScenarioRow_FlowName) = "flow1a" or  LCase(str_ScenarioRow_FlowName) = "flow1x" Then	
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_Check_SOstatus_isRaised"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
										Printmessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised complete", "MTS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										
									ElseIf  LCase(str_ScenarioRow_FlowName) = "flow2" Then	
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_ALL_AQS"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
										Printmessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised complete", "MTS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		
									ElseIf  LCase(str_ScenarioRow_FlowName) = "flow3" Then	
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Gateway_Response_Check"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
										Printmessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised complete", "MTS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		
										'												ElseIf lcase(str_ScenarioRow_FlowName) = "flow18" Then
										'													objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										'													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Raise_Manual_SO_in_CIS"
										'													objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
										'													Printmessage "p", "MTS_Check_SOstatus_isRaised complete", "MTS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
										'													fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
										PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised complete", "MTS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
										Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Exit Do
									End If 'end of If  (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow20")  And objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )) <> "" Then	
									
									
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
		
								Case "MTS_Check_ALL_AQS"
														
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									Printmessage "i", "CASE Start", "[MTS_Check_ALL_AQS] for Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - MTS GUI Verification ALL AQs for SONUM (" & _
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value  &  ")" 
									
									Dim strVerification_String
									strVerification_String = ""
									If isempty(dictWSDataParameter_KeyName_ColNr("GUI_Verification_SO_AQ" )) Then
										strVerification_String = "" 
									Else
										' Fetch verification string from work sheet
										strVerification_String = trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("GUI_Verification_SO_AQ" ))) 
									End If							
		
									If strVerification_String <> "" Then
										
										' Click on ALL-> AQ's
										fn_MTS_MenuNavigation "transactions;all aqs"
										int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
										int_UniqueReferenceNumberforXML = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("LNSP" & objWS_DataParameter.range("rgWS_RequestID").formula)).value
		
										fn_MTS_Search_All_AQ int_NMI,fnTimeStamp(now, "DD/MM/YYYY"), fnTimeStamp(now, "DD/MM/YYYY"), "initTxnID_"& int_UniqueReferenceNumberforXML,  "y",  gFW_strRunLog_ScreenCapture_fqFileName
																										
										clear_scratch_Dictionary gdict_scratch
										
										fn_Compare_Multiple_AQ_Search_Service_Order_AQ_Screen strVerification_String, "^"
									
										' Close ALL AQ screen
										fn_MTS_Close_any_screen "Search_All_Aqs"
										
									Else						
										PrintMessage "i", "MTS_Check_ALL_AQS checks","Skipping MTS_Check_ALL_AQS checks as the GUI_Verification_SO_AQ verification string is empty"
									End If
									
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_Check_ALL_AQS because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										Printmessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_ALL_AQS error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else
										
		'								If LCase(str_ScenarioRow_FlowName) = "flow2"  And lcase(trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verify_SOEvent_Publish_YN" )))) = "y" Then	
		'									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
		'									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SOEvent_Publish"
		'									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_ALL_AQS complete"
		'									Printmessage "p", "MILESTONE - MTS_Check_ALL_AQS complete", "MTS_Check_ALL_AQS complete for row ("  & intRowNr_CurrentScenario &  ")" 
		'									' Function to write timestamp for next process
		'									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		'									
		
										If LCase(str_ScenarioRow_FlowName) = "flow2" Then	
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "DB_Check_ALL_AQS"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_ALL_AQS complete"
											Printmessage "p", "MILESTONE - MTS_Check_ALL_AQS complete", "MTS_Check_ALL_AQS complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_ALL_AQS complete"
											PrintMessage "p", "MILESTONE - MTS_Check_ALL_AQS complete", "MTS_Check_ALL_AQS complete for row ("  & intRowNr_CurrentScenario &  ")" 
											Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
											Exit Do
										End If 'end of If  (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow20")  And objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )) <> "" Then	
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
		
								Case "DB_Check_ALL_AQS"
								
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							
									Printmessage "i", "CASE Start", "[DB_Check_ALL_AQS] for Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ")"
									
									Dim strExpectedVerificationString
									strExpectedVerificationString = ""
									If isempty(dictWSDataParameter_KeyName_ColNr("DB_Verification_SO_AQ" )) Then
										strExpectedVerificationString = "" 
									Else
										' Fetch verification string from work sheet
										strExpectedVerificationString = trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("DB_Verification_SO_AQ" )))
									End If			
									
									
									If strExpectedVerificationString <> "" Then
										temp_Expected_Verification_String_Array = split(strExpectedVerificationString, "^")	
									
										For int_Ctr = 0 To ubound(temp_Expected_Verification_String_Array)
											If temp_Expected_Verification_String_Array(int_Ctr) <> "" Then
												temp_Key = mid(temp_Expected_Verification_String_Array(int_Ctr),1,(instr(1,temp_Expected_Verification_String_Array(int_Ctr),"=")-1))
												temp_Value = mid(temp_Expected_Verification_String_Array(int_Ctr), instr(1,temp_Expected_Verification_String_Array(int_Ctr),"=") + 1, len(temp_Expected_Verification_String_Array(int_Ctr))-instr(1,temp_Expected_Verification_String_Array(int_Ctr),"=") + 1)
												temp_Value = replace(temp_Value, """","")
												fnLoadKey_toDictionary gdict_scratch, trim(temp_Key), trim(temp_Value)
												Printmessage "i", "DB_Check_ALL_AQS", "fnLoadKey_toDictionary arr(" & int_Ctr & ") key (" & temp_Key & "), value (" &  temp_Value &  ")"
											End If 'end of If temp_Expected_Verification_String_Array(int_Ctr) <> "" Then
										Next								
										
										Dim objDBConn_TestData_D, objDBRcdSet_TestData_D
										Set objDBConn_TestData_D    = CreateObject("ADODB.Connection")
										Set objDBRcdSet_TestData_D  = CreateObject("ADODB.Recordset")
									
										For intCtr = 0 To ubound(temp_Expected_Verification_String_Array)
											
											If temp_Expected_Verification_String_Array(intCtr) <> "" AND gdict_scratch("AQ_Data_Error_Reason_Code"&intCtr) <> "" Then
												
												temp_Verify_AQ_Name = gdict_scratch("AQ_Data_Error_Reason_Code"&intCtr)
												temp_Verify_AQ_Description = gdict_scratch("AQ_Change_Reason_Code"&intCtr)
												temp_Verify_AQ_Status = gdict_scratch("AQ_Data_Status"&intCtr)
												temp_Verify_AQ_EmailSent = gdict_scratch("AQ_EmailSent"&intCtr)
												
												PrintMessage "p", "DB_Check_ALL_AQS Query", "Verify Value for arr("& intCtr &"), AQ_Data_Error_Reason_Code ("& temp_Verify_AQ_Name &"), AQ_Change_Reason_Code ("& temp_Verify_AQ_Description &"), AQ_Data_Status (" & temp_Verify_AQ_Status & "), AQ_EmailSent(" & temp_Verify_AQ_EmailSent & ")"									
												
												' Read the SQL template once 			
												temp_SQL_Template = "MTS_B2BSO_FIND_ALL_AQS" 
												PrintMessage "p", "DB_Check_ALL_AQS Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& temp_SQL_Template  &")"
												
												If temp_SQL_Template <> ""  Then
													
													temp_SQL_Template = fnRetrieve_SQL(temp_SQL_Template)
										  
													'Replace Values in SQL
													If InStr(1, temp_SQL_Template,"<RET_SO_NUM>") > 0 Then
														str_ret_so_num = objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value
														temp_SQL_Template = Replace ( temp_SQL_Template , "<RET_SO_NUM>" , str_ret_so_num , 1, -1, vbTextCompare)
														Printmessage "i", "DB_Check_ALL_AQS", "SQL Replace: <RET_SO_NUM> with (" & str_ret_so_num & ")"
													End If 
													
													If InStr(1, temp_SQL_Template,"<AQ_NAME>") > 0 Then
														temp_SQL_Template = Replace ( temp_SQL_Template , "<AQ_NAME>" ,temp_Verify_AQ_Name , 1, -1, vbTextCompare)												
														Printmessage "i", "DB_Check_ALL_AQS", "SQL Replace: <AQ_NAME> with (" & temp_Verify_AQ_Name & ")"
													End If 
											
													' Run the query
													fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "DB_Check_ALL_AQS Query", strSQL_MTS
													strScratch = Execute_SQL_Populate_DataSheet (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_D, objDBRcdSet_TestData_D, temp_SQL_Template, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "")		
													
													' Perform verifications
													If lcase(strScratch) = "pass" Then									
														If objDBRcdSet_TestData_D.recordcount > 0  Then										
														
															temp_DB_AQ_Description = objDBRcdSet_TestData_D.Fields("DB_AQ_DESCRIPTION").value
															temp_DB_AQ_Status = objDBRcdSet_TestData_D.Fields("DB_AQ_STATUS").value
															temp_DB_AQ_EmailSent = objDBRcdSet_TestData_D.Fields("DB_AQ_EMAIL_SENT_YN").value												
														
															'AQ Description check
															If lcase(trim(temp_Verify_AQ_Description)) =  lcase(trim(temp_DB_AQ_Description)) Then
																PrintMessage "P", "DB_Check_ALL_AQS Validation pass", "Expected value ("&  temp_Verify_AQ_Description & ") is SAME as Actual value("&  temp_DB_AQ_Description & ")"
															Else
																PrintMessage "f", "DB_Check_ALL_AQS Validation fail", "Expected value ("&  temp_Verify_AQ_Description & ") is DIFFERENT than Actual value("&  temp_DB_AQ_Description & ")"
															End If
															
															'AQ Status check
															If lcase(trim(temp_Verify_AQ_Status)) =  lcase(trim(temp_DB_AQ_Status)) Then
																PrintMessage "P", "DB_Check_ALL_AQS Validation pass", "Expected value ("&  temp_Verify_AQ_Status & ") is SAME as Actual value("&  temp_DB_AQ_Status & ")"
															Else
																PrintMessage "f", "DB_Check_ALL_AQS Validation fail", "Expected value ("&  temp_Verify_AQ_Status & ") is DIFFERENT than Actual value("&  temp_DB_AQ_Status & ")"
															End If												
															
															'AQ Email Sent Flag check
															If ucase(trim(temp_Verify_AQ_EmailSent)) =  ucase(trim(temp_DB_AQ_EmailSent)) Then
																PrintMessage "P", "DB_Check_ALL_AQS Validation pass", "Expected value ("&  temp_Verify_AQ_EmailSent & ") is SAME as Actual value("&  temp_DB_AQ_EmailSent & ")"
															Else
																PrintMessage "f", "DB_Check_ALL_AQS Validation fail", "Expected value ("&  temp_Verify_AQ_EmailSent & ") is DIFFERENT than Actual value("&  temp_DB_AQ_EmailSent & ")"
															End If
															
														End If 'end of If objDBRcdSet_TestData_B.recordcount > 0  Then
													Else
														fn_set_error_flags true, "DB_Check_ALL_AQS Query has return no data"
													End If ' If lcase(strScratch) = "pass" Then									
												End If	'If temp_SQL_Template <> ""  Then
											'Else	
												'PrintMessage "i", "DB_Check_ALL_AQS checks","Skipping DB_Check_ALL_AQS checks as the temp_Expected_Verification_String_Array verification string is empty"
											End If	'If temp_Expected_Verification_String_Array(intctr) <> "" Then
											
										Next 'For intctr = 0 To ubound(temp_Expected_Verification_String_Array)
										
										clear_scratch_Dictionary gdict_scratch
									Else
										PrintMessage "i", "DB_Check_ALL_AQS checks","Skipping DB_Check_ALL_AQS checks as the DB_Check_ALL_AQS verification string is empty"
									End If 'If strExpectedVerificationString <> "" Then
		
								set objDBConn_TestData_D = nothing
								set objDBRcdSet_TestData_D = nothing
										
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at DB_Check_ALL_AQS because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									Printmessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & "DB_Check_ALL_AQS error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									
								If LCase(str_ScenarioRow_FlowName) = "flow2" Then	
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "SOEvent_Publish"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "DB_Check_ALL_AQS complete"
									Printmessage "p", "MILESTONE - DB_Check_ALL_AQS complete", "DB_Check_ALL_AQS complete for row ("  & intRowNr_CurrentScenario &  ")" 
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "DB_Check_ALL_AQS complete"
									PrintMessage "p", "MILESTONE - DB_Check_ALL_AQS complete", "DB_Check_ALL_AQS complete for row ("  & intRowNr_CurrentScenario &  ")" 
									Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
									Exit Do
								End If 'end of If  (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow20")  And objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )) <> "" Then	
							End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
		
		
								Case "SQT_INTERFACE_OUT_RESPONSE_CHECK"
								
								If (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow20") And objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )) <> "" Then
									
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[SQT_INTERFACE_OUT_RESPONSE_CHECK] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									
									var_ScheduledDate = fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("ScheduledDate")).value,"DD/MM/YYYY")
									
									strSQL_MTS = ""
									strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("SQT_INT_OUT_RESPONSE_Query")).value 
									
									If Trim(strSQL_MTS) <> ""  Then
										
										PrintMessage "p", "SQT_INT_OUT_RESPONSE_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing MTS SQL ("& strSQL_MTS &") "
										strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
										
										'replace
										int_Ret_ServiceOrderNumber = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber" )).value								
										PrintMessage "i", "SQT_INTERFACE_OUT_RESPONSE_CHECK", "Replace sql_tag <Ret_ServiceOrderNumber> with (" & int_Ret_ServiceOrderNumber & ")" 
										strSQL_MTS = Replace ( strSQL_MTS  , "<Ret_ServiceOrderNumber>", int_Ret_ServiceOrderNumber,  1, -1, vbTextCompare)
		
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "SQT_INT_OUT_RESPONSE_Query Query:" & vbCrLf & strSQL_MTS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		
										' Execute MTS SQL
										'Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2", "", "", "",""	
										strScratch = Execute_SQL_Populate_DataSheet_Hatseperator(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",   "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  "")
										
										If lcase(strScratch) <> "fail" Then 													
											
											objWB_Master.save ' Save the workbook
											strScratch = ""
											
											'--
											'--Verify WORKORDERTYPE
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType")).value))
											temp_actual_value =  lcase(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","WORKORDERTYPE")))
											If temp_expected_value <> "" Then									
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "SQT_INT_OUT_RESPONSE Verification",  "Actual value of WORKORDERTYPE (" & temp_actual_value & ") from SQT_INT_OUT_RESPONSE query is same as expected value ("& temp_expected_value &") mentioned in (CIS_workOrderType_Code) column in datasheet"
												Else
													fn_set_error_flags true, "Actual value of WORKORDERTYPE (" & temp_actual_value & ") from SQT_INT_OUT_RESPONSE query is DIFFERENT from expected value ("& temp_expected_value &") mentioned in (CIS_workOrderType) column in datasheet"
													PrintMessage "f", "SQT_INT_OUT_RESPONSE Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "SQT_INT_OUT_RESPONSE Verification",  "Skipping check related to WORKORDERTYPE as (CIS_workOrderType) column is blank in datasheet"
											End if
		
											'--Verify WORKORDERTASK
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType")).value))
											temp_actual_value =  lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","WORKORDERTASK")))									
											If temp_expected_value <> "" Then									
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "SQT_INT_OUT_RESPONSE Verification",  "Actual value of WORKORDERTASK (" & temp_actual_value & ") from SQT_INT_OUT_RESPONSE query is same as expected value ("& temp_expected_value &") mentioned in (CIS_workOrderSubType) column in datasheet"
												Else
													fn_set_error_flags true, "Actual value of WORKORDERTASK (" & temp_actual_value & ") from SQT_INT_OUT_RESPONSE query is DIFFERENT from expected value ("& temp_expected_value &") mentioned in (CIS_workOrderSubType) column in datasheet"
													PrintMessage "f", "SQT_INT_OUT_RESPONSE Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "SQT_INT_OUT_RESPONSE Verification",  "Skipping check related to WORKORDERTASK as (CIS_workOrderSubType) column is blank in datasheet"
											End if
											
											
											'--Verify Fee_applied	
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied")).value))
											temp_actual_value =  lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SERVICEORDERCHARGECODE")))									
											If temp_expected_value <> "" Then									
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "SQT_INT_OUT_RESPONSE Verification",  "Actual value of SERVICEORDERCHARGECODE (" & temp_actual_value & ") from SQT_INT_OUT_RESPONSE query is same as expected value ("& temp_expected_value &") mentioned in (Fee_Applied) column in datasheet"
												Else
													fn_set_error_flags true, "Actual value of SERVICEORDERCHARGECODE (" & temp_actual_value & ") from SQT_INT_OUT_RESPONSE query is DIFFERENT from expected value ("& temp_expected_value &") mentioned in (Fee_Applied) column in datasheet"
													PrintMessage "f", "SQT_INT_OUT_RESPONSE Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "SQT_INT_OUT_RESPONSE Verification",  "Skipping check related to SERVICEORDERCHARGECODE as (Fee_Applied) column is blank in datasheet"
											End if
											
											
											'--Verify NOTES_SENT	
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notes_Sent")).value))
											temp_actual_value =  lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","NOTE")))									
											If temp_expected_value <> "" Then									
												'If  temp_actual_value = temp_expected_value Then
												If instr(1,temp_actual_value, temp_expected_value) > 0 Then
													PrintMessage "p", "SQT_INT_OUT_RESPONSE Verification",  "Actual value of NOTE (" & temp_actual_value & ") from SQT_INT_OUT_RESPONSE query is same as expected value ("& temp_expected_value &") mentioned in (Notes_Sent) column in datasheet"
												Else
													fn_set_error_flags true, "Actual value of NOTE (" & temp_actual_value & ") from SQT_INT_OUT_RESPONSE query is DIFFERENT from expected value ("& temp_expected_value &") mentioned in (Notes_Sent) column in datasheet"
													PrintMessage "f", "SQT_INT_OUT_RESPONSE Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "SQT_INT_OUT_RESPONSE Verification",  "Skipping check related to NOTE as (Notes_Sent) column is blank in datasheet"
											End if
											
											'---
		''									
		''									'Verify WORKORDERTYPE & WORKORDERTASK 
		''									If  LCase(Trim(objDBRcdSet_TestData_B.Fields("WORKORDERTYPE").Value = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType")).value)) And _
		''										LCase(Trim(objDBRcdSet_TestData_B.Fields("WORKORDERTASK").Value)) = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType")).value)) Then 
		''										
		''										PrintMessage "p", "SQT_INT_OUT_RESPONSE","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Verification PASSED: Field matched: WORKORDERTYPE ("& _
		''														objDBRcdSet_TestData_B.Fields("WORKORDERTYPE").Value & "), WORKORDERTASK (" & objDBRcdSet_TestData_B.Fields("WORKORDERTASK").Value & ")"										
		''										gFWbln_ExitIteration = False
		''										
		''										'Verify Fee_applied										
		''										If Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied" )).value) <> "" Then
		''											If LCase(Trim(objDBRcdSet_TestData_B.Fields("SERVICEORDERCHARGECODE").Value)) = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied")).value)) Then
		''												gFWbln_ExitIteration = False
		''												PrintMessage "p", "SQT_INT_OUT_RESPONSE","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Verification PASSED: Field matched: Fee_Applied (" &_
		''															objDBRcdSet_TestData_B.Fields("SERVICEORDERCHARGECODE").Value & ")"
		''											Else
		''												temp_error_message =  "Issue with SQT_INTERFACE_OUT_RESPONSE_CHECK. Actual Charge value: " & objDBRcdSet_TestData_B.Fields ("SERVICEORDERCHARGECODE") & "Expected: " &  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied")).value
		''												gFWbln_ExitIteration = True
		''												gFWstr_ExitIteration_Error_Reason = "SQT_INTERFACE_OUT_RESPONSE_CHECK function - " & temp_error_message	
		''											End If 
		''										End If
		''																				
		''										'Verify NOTES_SENT
		''										If Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notes_Sent" )).value) <> "" Then
		''										
		''											If Instr (1, LCase(Trim(objDBRcdSet_TestData_B.Fields("NOTE").Value)), LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notes_Sent")).value))) > 0 Then
		''												gFWbln_ExitIteration = False
		''												PrintMessage "p", "SQT_INT_OUT_RESPONSE","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  _
		''																") -  Verification PASSED: Field matched: Notes_Sent matched (" & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notes_Sent")).value & ")"
		''											Else
		''												temp_error_message =  "Issue with SQT_INTERFACE_OUT_RESPONSE_CHECK. Actual Note sent is: " & objDBRcdSet_TestData_B.Fields ("NOTE") & "Expected: " & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notes_Sent")).value
		''												gFWbln_ExitIteration = True
		''												gFWstr_ExitIteration_Error_Reason = "SQT_INTERFACE_OUT_RESPONSE_CHECK function - " & temp_error_message	
		''											End If 
		''										End If 'If Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notes_Sent" )).value) <> "" Then
		''										
		''									Else													
		''										temp_error_message =  "Issue with SQT_INTERFACE_OUT_RESPONSE_CHECK. Actual value: " & objDBRcdSet_TestData_B.Fields ("WORKORDERTYPE") & "\"  & objDBRcdSet_TestData_B.Fields ("WORKORDERTASK")
		''										gFWbln_ExitIteration = True
		''										gFWstr_ExitIteration_Error_Reason = "SQT_INTERFACE_OUT_RESPONSE_CHECK function - " & temp_error_message														
		''										PrintMessage "f", "SQT_INT_OUT_RESPONSE_Query", temp_error_message
		''									End If ''Verify WORKORDERTYPE & WORKORDERTASK 
		''									
											
											If strScratch <> "" Then
												gFWstr_ExitIteration_Error_Reason = strScratch
											End If
											
										Else												
											temp_error_message =  "Issue with SQT_INTERFACE_OUT_RESPONSE_CHECK Query. No row Returned."
											gFWbln_ExitIteration = True
											gFWstr_ExitIteration_Error_Reason = "SQT_INTERFACE_OUT_RESPONSE_CHECK function - " & temp_error_message	
											PrintMessage "f", "SQT_INT_OUT_RESPONSE_Query", temp_error_message												
										End If 'lcase(strScratch) <> "fail" 
										
										'													CIS_Check_SOstatus_isRaised int_NMI, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )).value, _
										'													var_ScheduledDate, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InCIS" )).value, _
										'													objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value,"n"											
										
									End If 'end of If Trim(strSQL_MTS) <> ""  Then
									
								End If ' end of If (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow20") And objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )) <> "" Then
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at SQT_INTERFACE_OUT_RESPONSE_CHECK because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - SQT_INTERFACE_OUT_RESPONSE_CHECK Failed", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
									
								ElseIf (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow20") And objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("PreviousSO_Status_InMTS_Updated")).value <> "" Then 
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "Check_PreviousSO_Status_InMTS"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SQT_INTERFACE_OUT_RESPONSE_CHECK complete"
									PrintMessage "p", "MILESTONE - SQT_INTERFACE_OUT_RESPONSE_CHECK complete", "MTS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_CancelSO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									
									
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SQT_INTERFACE_OUT_RESPONSE_CHECK complete"
									PrintMessage "p", "MILESTONE - SQT_INTERFACE_OUT_RESPONSE_CHECK complete", "SQT_INTERFACE_OUT_RESPONSE_CHECK complete for row ("  & intRowNr_CurrentScenario &  ")" 
									Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
									Exit Do
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								
								Case "Check_PreviousSO_Status_InMTS"										
								' fn_compare_Current_and_expected_execution_time
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[Check_PreviousSO_Status_InMTS] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value												
								
								MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario - 1 ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"first",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("PreviousSO_Status_InMTS_Updated")).value, _
								objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
								objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,"n", "n", objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("SOResponse_Exception_Code")).value,""
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at Check_PreviousSO_Status_InMTS because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - Check_PreviousSO_Status_InMTS Fail", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
									
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Check_PreviousSO_Status_InMTS complete"
									PrintMessage "p", "Check_PreviousSO_Status_InMTS complete", "Check_PreviousSO_Status_InMTS complete for row ("  & intRowNr_CurrentScenario &  ")" 
									Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
									Exit Do
									
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								
								Case "CIS_Check_SOstatus_isRaised"
								
								If LCase(str_ScenarioRow_FlowName) = "flow1a" or LCase(str_ScenarioRow_FlowName) = "flow1x" Then
									
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[CIS_Check_SOstatus_isRaised] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
									var_ScheduledDate = fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("ScheduledDate")).value,"DD-MM-YYYY")
									
									'######## CIS_SO_Check_Query #########################
									strSQL_CIS = ""
									strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SO_Check_Query")).value 
									
									'Default value
									If Trim(strSQL_CIS) = ""  Then
										strSQL_CIS = "CIS_SO_Check"
									End If
									
									
									If Trim(strSQL_CIS) <> ""  Then											
										PrintMessage "p", "CIS_Check_SOstatus_isRaised","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing CIS_SO_Check_Query ("& strSQL_CIS &") "
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										' perform all replacements
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
										Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "<NMI>", int_NMI,  1, -1, vbTextCompare)
										strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_CIS_SO_NUM>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber" )),  1, -1, vbTextCompare)
		
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "CIS_SO_Check_Query Query:" & vbCrLf & strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		
										' Execute CIS SQL
										'Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", "", ""
										strScratch = Execute_SQL_Populate_DataSheet_Hatseperator(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",   "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  "")
										
										If lcase(strScratch) <> "fail" Then
										
											objWB_Master.save ' Save the workbook
											strScratch = ""
											
											' Verify output of query
											' WORKORDERTYPE
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType_Code" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","WORKORDERTYPE")))
											If temp_expected_value <> "" Then									
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Actual value of WORKORDERTYPE (" & temp_actual_value & ") from CIS_SO_Check_Query is same as expected value ("& temp_expected_value &") mentioned in CIS_workOrderType_Code column in datasheet"
												Else
													fn_set_error_flags true, "; Actual value of WORKORDERTYPE (" & temp_actual_value & ") from CIS_SO_Check_Query is DIFFERENT from expected value ("& temp_expected_value &") mentioned in CIS_workOrderType_Code column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to WORKORDERTYPE as CIS_workOrderType_Code column is blank in datasheet"
											End if
											
											' WORKORDERTASK
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","WORKORDERTASK")))
											If temp_expected_value <> "" Then
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Actual value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query is same as expected value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
												Else
													fn_set_error_flags true, "; Actual value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from expected value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to WORKORDERTASK as CIS_workOrderSubType column is blank in datasheet"
											End If
		
											' SERVICEORDERCHARGECODE
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SERVICEORDERCHARGECODE")))
											
											If temp_expected_value <> "" Then
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Actual value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query is same as expected value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
												Else
													fn_set_error_flags true, "; Actual value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from expected value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to SERVICEORDERCHARGECODE as Fee_Applied column is blank in datasheet"
											End if
											
											' RETAILERSONUMBER
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","RETAILERSONUMBER")))
											
											If temp_expected_value <> "" Then
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Actual value of RETAILERSONUMBER (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as expected value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
												Else
													fn_set_error_flags true, "; Actual value of RETAILERSONUMBER (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from expected value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to RETAILERSONUMBER as Fee_Applied column is blank in datasheet"
											End If
											
											' SOEFFECTIVEDATE
											temp_expected_value = var_ScheduledDate ' LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ScheduledDate" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SOEFFECTIVEDATE")))
											
											If temp_expected_value <> "" Then
												 'temp_expected_value = fnTimeStamp(temp_expected_value, "DD-MM-YYYY")
												If temp_actual_value <> "" Then
													temp_actual_value = fnTimeStamp(temp_actual_value, "DD-MM-YYYY")	
												End If
												
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Actual value of SOEFFECTIVEDATE (" & temp_actual_value & ") from CIS_SO_Check_Query is same as expected value ("& temp_expected_value &") mentioned in ScheduledDate column in datasheet"
												Else
													fn_set_error_flags true, "; Actual Value of SOEFFECTIVEDATE (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from expected value ("& temp_expected_value &") mentioned in ScheduledDate column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to SOEFFECTIVEDATE as ScheduledDate column is blank in datasheet"
											End If
										
										
											' Field Notes Sent
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Notes_Sent" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","FIELD_INSTRUCTIONS")))
																													
											If temp_expected_value <> "" Then									
												'If  temp_actual_value = temp_expected_value Then
												If  InStr(1, temp_actual_value,temp_expected_value) Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Actual Value of NOTE (" & temp_actual_value & ") from CIS_SO_Check_Query is same as expected value ("& temp_expected_value &") mentioned in (Notes_Sent) column in datasheet."
												Else
													fn_set_error_flags true, "Actual Value of NOTE (" & temp_actual_value & ") from CIS_SO_Check_Query is DIFFERENT from expected value ("& temp_expected_value &") mentioned in (Notes_Sent) column in datasheet."
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to NOTE as (Notes_Sent) column is blank in datasheet"
											End if
										
											If strScratch <> "" Then
												gFWstr_ExitIteration_Error_Reason = strScratch
											ElseIf (LCase(str_ScenarioRow_FlowName) = "flow1x") Then	
													PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Flow1x Modify CIS WO to Set Schedule Date to current date."
													fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "cis_modifyso_api", "insert", "optional"	
											End If
										
										 
										End If	'end of If lcase(strScratch) <> "fail" Then (CIS_SO_Check	exe)
										
										
		'								If LCase(Trim(objDBRcdSet_TestData_A.RecordCount)) > 0  And LCase(Trim(objDBRcdSet_TestData_A.Fields("WORKORDERTYPE").Value)) = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType_Code" )).value)) And _
		'									LCase(Trim(objDBRcdSet_TestData_A.Fields("WORKORDERTASK").Value)) = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value)) Then 
		'									gFWbln_ExitIteration = False
		'									
		'									If Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied" )).value) <> "" Then
		'										If LCase(Trim(objDBRcdSet_TestData_A.Fields("SERVICEORDERCHARGECODE").Value)) <> LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied")).value)) Then
		'											temp_error_message =  "CIS_Check_SOstatus_isRaised failed: Actual Charge value: " & objDBRcdSet_TestData_A.Fields ("SERVICEORDERCHARGECODE") & "Expected was: " & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied")).value
		'											gFWbln_ExitIteration = True
		'											gFWstr_ExitIteration_Error_Reason = temp_error_message
		'											PrintMessage "f", "CIS_SO_Check_Query", gFWstr_ExitIteration_Error_Reason											   			
		'										End If 'end of If LCase(Trim(objDBRcdSet_TestData_A.Fields("SERVICEORDERCHARGECODE").Value)) <> LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied")).value)) Then
		'									End If 'end of If Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied" )).value) <> "" Then
		'								Else
		'									temp_error_message =  "CIS_Check_SOstatus_isRaised failed: Actual CIS SO Type value: " & objDBRcdSet_TestData_A.Fields ("WORKORDERTYPE") & "\"  & objDBRcdSet_TestData_A.Fields ("WORKORDERTASK")
		'									gFWbln_ExitIteration = True
		'									gFWstr_ExitIteration_Error_Reason = "CIS_Check_SOstatus_isRaised function - " & temp_error_message														
		'									PrintMessage "f", "CIS_SO_Check_Query", gFWstr_ExitIteration_Error_Reason
		'								End If 'end of If LCase(Trim(objDBRcdSet_TestData_A.RecordCount)) > 0  And LCase(Trim(objDBRcdSet_TestData_A.Fields("WORKORDERTYPE").Value)) = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType_Code" )).value)) And _
		'								
										'											CIS_Check_SOstatus_isRaised int_NMI, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )).value, _
										'													var_ScheduledDate, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InCIS" )).value, _
										'													objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value,"n"											
									Else
										PrintMessage "i", "CIS_SO_Check_Query check - SKIPPING", "Skipping CIS_SO_Check_Query as the CIS_SO_Check_Query is blank"
									End If 'If Trim(strSQL_CIS) <> ""  Then	 (CIS_SO_Check)
									
								End If ' If lcase(str_ScenarioRow_FlowName) = "flow1" Then
								
								' ####################################### TO BE REMOVED ONCE THE DEFECT RELATED TO CIS IS RESOLVED <<<<<<<<<<<<<<<<<===========================
								' ####################################### TO BE REMOVED ONCE THE DEFECT RELATED TO CIS IS RESOLVED <<<<<<<<<<<<<<<<<===========================
								' KS: commenting this out as no one knows why this is here.
								' fn_reset_error_flags
								' #########################################################################################################################################
								' #########################################################################################################################################
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at CIS_Check_SOstatus_isRaised because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " CIS_Check_SOstatus_isRaised error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
								
									If LCase(str_ScenarioRow_FlowName) = "flow1x" Then
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_Check_SOstatus_isRaised complete"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_SO_Complete_V2"
										PrintMessage "p", "MILESTONE - CIS_Check_SOstatus_isRaised complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_TenMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_Check_SOstatus_isRaised complete"
										PrintMessage "p", "MILESTONE - CIS_Check_SOstatus_isRaised complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
										Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Exit Do
									End if
									
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								Case "SOEvent_Publish"										
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[SOEvent_Publish] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								
								Dim blnVerification_String
								blnVerification_String = false
								If isempty(dictWSDataParameter_KeyName_ColNr("Verify_SOEvent_Publish_YN" )) or LCase(Environment.Value("COMPANY")) = "sapn" Then
										blnVerification_String = false
								Else
									' Fetch verification string from work sheet
									If lcase(trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verify_SOEvent_Publish_YN" )))) = "y" then
										blnVerification_String = True 
									else
										blnVerification_String = false 
									End if
								End If							
														
								If blnVerification_String Then
																								
								' Get the query related to SOEvent_Publish
							    strQueryTemplate = fnRetrieve_SQL("SOEvent_Publish_Check")
									
								If LCASE(strQueryTemplate) <> "fail" Then
									strSQL_MTS = strQueryTemplate
									
									' Perform replacements
									strSQL_MTS = Replace ( strSQL_MTS  , "<INIT_TRANSACTION_ID>", "SOR_" & objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")),  1, -1, vbTextCompare)					            	
						            PrintMessage "p", "SOEvent_Publish", "Replace <INIT_TRANSACTION_ID> with (SOR_" & objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")) & ")"
						            
						            fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "SOEvent_Publish_Check", strSQL_MTS
						            strScratch =  Execute_SQL_Populate_DataSheet_Hatseperator(objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",   "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  ""	)					           
						            'PrintMessage "p", "SOEvent_Publish", "SQL Execution completed (SOEvent_Publish_Check)"
						            
					            	If lcase(strScratch) <> "fail" Then 
						            	strQueryTemplate = fnRetrieve_SQL("MTS_B2B_SO_XML_Extract")
							            PrintMessage "p", "SOEvent_Publish", "SQL Execution started (MTS_B2B_SO_XML_Extract)"
							            
										If LCASE(strQueryTemplate) <> "fail" Then
											strSQL_MTS = strQueryTemplate
		
											strSQL_MTS = Replace ( strSQL_MTS  , "<INIT_TRANSACTION_ID>", "SOR_" & objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")),  1, -1, vbTextCompare)					            	
								            PrintMessage "p", "SOEvent_Publish", "Replace <INIT_TRANSACTION_ID> with (SOR_" & objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")) & ")"
								            fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "MTS_B2B_SO_XML_Extract", strSQL_MTS
								            strScratch =  Execute_SQL_Populate_DataSheet_Hatseperator (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",   "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  ""	)
								            
								            If lcase(strScratch) <> "fail" Then 
									            PrintMessage "p", "SOEvent_Publish", "SQL Execution completed (MTS_B2B_SO_XML_Extract)"	
			
												objWB_Master.save
												
												' Verify output of query
												' Verify  SEP_FRMP = XML_EXTRAC_FROM
												If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_FRMP")) _
												= trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_FROM")) Then
													PrintMessage "p", "SOEvent_Publish Verification",  "Value of FRMP (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_FRMP")) & ") from SOEvent_Publish_Check query is same as value ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_FROM")) &") from MTS_B2B_SO_XML_Extract query"
												Else
													fn_set_error_flags true, "Value of FRMP (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_FRMP")) & ") from SOEvent_Publish_Check query is DIFFERENT from value ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_FROM")) &" from MTS_B2B_SO_XML_Extract query)"
													PrintMessage "f", "SOEvent_Publish Verification",  gFWstr_ExitIteration_Error_Reason
												End If
			
												'Verify SEP_SO_TYPE = XML_EXTRAC_SO_TYPE
												If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_SO_TYPE")) _
												= trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_SO_TYPE")) Then
													PrintMessage "p", "SOEvent_Publish Verification",  "Value of SO Type (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_SO_TYPE")) & ") from SOEvent_Publish_Check query is same as value ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_SO_TYPE")) &" from MTS_B2B_SO_XML_Extract query)"
												Else
													fn_set_error_flags true, "Value of SO Type (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_SO_TYPE")) & ") from SOEvent_Publish_Check query is DIFFERENT from value ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_SO_TYPE")) &" from MTS_B2B_SO_XML_Extract query)"
													PrintMessage "f", "SOEvent_Publish Verification",  gFWstr_ExitIteration_Error_Reason
												End If
			
												'Verify SEP_SO_SUBTYPE = XML_EXTRAC_SO_SUB_TYPE
												If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_SO_SUBTYPE")) _
												= trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_SO_SUB_TYPE")) Then
													PrintMessage "p", "SOEvent_Publish Verification",  "Value of SO Sub Type (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_SO_SUBTYPE")) & ") from SOEvent_Publish_Check query is same as value ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_SO_SUB_TYPE")) &" from MTS_B2B_SO_XML_Extract query)"
												Else
													fn_set_error_flags true, "Value of SO Sub Type (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_SO_SUBTYPE")) & ") from SOEvent_Publish_Check query is DIFFERENT from value ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_SO_SUB_TYPE")) &" from MTS_B2B_SO_XML_Extract query)"
													PrintMessage "f", "SOEvent_Publish Verification",  gFWstr_ExitIteration_Error_Reason
												End If
												
												'Verify SEP_SO_ACTION = XML_EXTRAC_ACTION
												If trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_SO_ACTION")) _
												= trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_ACTION")) Then
													PrintMessage "p", "SOEvent_Publish Verification",  "Value of Action (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_SO_ACTION")) & ") from SOEvent_Publish_Check query is same as value ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_ACTION")) &" from MTS_B2B_SO_XML_Extract query)"
												Else
													fn_set_error_flags true, "Value of Action (" & trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_SO_ACTION")) & ") from SOEvent_Publish_Check query is DIFFERENT from value ("& trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_ACTION")) &" from MTS_B2B_SO_XML_Extract query)"
													PrintMessage "f", "SOEvent_Publish Verification",  gFWstr_ExitIteration_Error_Reason
												End If
			
												'Verify Schedule/Effective date
												var_SEP_Event_Date = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SEP_Event_Date"))
												var_SEP_Event_Date = fnTimeStamp(var_SEP_Event_Date,"YYYY/MM/DD")
												var_XMLEvent_Date = trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","XML_EXTRAC_Date"))
												var_XMLEvent_Date = fnTimeStamp(var_XMLEvent_Date,"YYYY/MM/DD")
			
												If var_SEP_Event_Date = var_XMLEvent_Date Then
													PrintMessage "p", "SOEvent_Publish Verification",  "Value of Event Date (" & var_SEP_Event_Date & ") from SOEvent_Publish_Check query is same as value ("& var_XMLEvent_Date & ") from MTS_B2B_SO_XML_Extract query."
												Else
													fn_set_error_flags true, "Value of Event Date (" & var_SEP_Event_Date & ") from SOEvent_Publish_Check query is DIFFERENT from value ("& var_XMLEvent_Date & ") from MTS_B2B_SO_XML_Extract query."
													PrintMessage "f", "SOEvent_Publish Verification",  gFWstr_ExitIteration_Error_Reason
												End If
						            		
						            		End If 'If lcase(strScratch) <> "fail" Then  (execution MTS_B2B_SO_XML_Extract)
						            
						            	End If ' If LCASE(strQueryTemplate) <> "fail" Then (retrive MTS_B2B_SO_XML_Extract) 
		
									End If ' If lcase(strScratch) <> "fail" Then  (execution of sql: SOEvent_Publish_Check)
									
								End If ' end of If LCASE(strQueryTemplate) <> "fail" Then (sql: SOEvent_Publish_Check)
									
								else						
									PrintMessage "i", "SOEvent_Publish Verification",  "skipping SOEvent_Publish Verification."
								End If 'If blnVerification_String Then
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at SOEvent_Publish because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									'PrintMessage "f", "MILESTONE - SOEvent_Publish Fail", gFWstr_ExitIteration_Error_Reason
									
									' create a function to dump error in a file 
									'strScratch = gFWstr_RunFolder & "Error_Dump_" & fnTimeStamp(now, "YYYY`MM`DD_HH`mm`ss") & ".txt"
									'sb_File_WriteContents  strScratch, gfsoForAppending, true, gFWstr_ExitIteration_Error_Reason
									PrintMessage "f", "MILESTONE - SOEvent_Publish Fail", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed."																	
									
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
								
									If isempty(dictWSDataParameter_KeyName_ColNr("GUI_Verification_CDRS_Reason" )) Then
										flagCDRs = "" 
									Else
										flagCDRs = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("GUI_Verification_CDRS_Reason")).value
									End If
									
									If lcase(trim(flagCDRs)) = "y" Then
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Search_CDR_Checks"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SOEvent_Publish complete"
										Printmessage "p", "MILESTONE - SOEvent_Publish complete", "MTS_Check_ALL_AQS complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "SOEvent_Publish complete"
										PrintMessage "p", "MILESTONE - SOEvent_Publish complete", "SOEvent_Publish complete for row ("  & intRowNr_CurrentScenario &  ")" 
										Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Exit Do
									End If ' end of If LCase(str_ScenarioRow_FlowName) = "flow2"  And lcase(trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Verify_SOEvent_Publish_YN" )))) = "y" Then	
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								Case "MTS_Search_CDR_Checks"										
								' fn_compare_Current_and_expected_execution_time
								
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[MTS_Search_CDR_Checks] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									dim blnCDRVerification
									blnCDRVerification = false
									
									If isempty(dictWSDataParameter_KeyName_ColNr("GUI_Verification_CDRS_Reason" )) Then
										blnCDRVerification = false
									Else
										' Fetch verification string from work sheet
										If lcase(trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("GUI_Verification_CDRS_Reason" )))) = "y" then
											blnCDRVerification = True 
										else
											blnCDRVerification = false 
										End if
									
									End if
									
									
									If blnCDRVerification Then
																	
											int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
											strScratch =  trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",","XML_EXTRAC_FROM"))
											
											If instr(1,strScratch, "TECHFAIL-Unable-to-locate") <=0 and strScratch <> ""  and gFWbln_ExitIteration <> true Then
											
											PrintMessage "p", "MTS_Search_CDR_Checks","For NMI ("& int_NMI &") "
											
											fn_MTS_MenuNavigation "Transactions;Customer / Site Details;Search Customer Details Request Sent"
											
											fn_MTS_Search_Customer_Details_Request_Sent int_NMI,"", "", fnTimeStamp(now,"DD/MM/YYYY"), fnTimeStamp(now,"DD/MM/YYYY"), "", "", "y", "y",  gFW_strRunLog_ScreenCapture_fqFileName
											
											fn_MTS_Search_Customer_Details_Request_Sent_SelectandVerify_Record_Results "first","","","","",int_NMI,strScratch,"Confirm Life Support","SENT","","","","","","","","","","y",gFW_strRunLog_ScreenCapture_fqFileName
											
											fn_MTS_Click_button_any_screen "Search_Customer_Details_Request_Sent", "btn_Display"
											
											fn_MTS_Search_Customer_Details_Request_Details_Screen "","","",int_NMI, strScratch,"Confirm Life Support","","SENT","","","","","","","","","","","","","","","","","y",gFW_strRunLog_ScreenCapture_fqFileName
											
											fn_MTS_Close_any_screen "Customer_Details_Request_Details"
											
											fn_MTS_Close_any_screen "Search_Customer_Details_Request_Sent"														
									
										Else  
											fn_set_error_flags true, "XML_EXTRAC_FROM value not found as some query might have failed. Existing test."
											PrintMessage "f", "MTS_Search_CDR_Checks", gFWstr_ExitIteration_Error_Reason
										End if ' end of If int_NMI <> "" Then	
									
									Else
										PrintMessage "i", "MTS_Search_CDR_Checks","CDR Check is not applicable to this Scenario row."
									End if 'If blnCDRVerification Then
									
									
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at MTS_Search_CDR_Checks because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - MTS_Search_CDR_Checks Fail", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
										
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Search_CDR_Checks complete"
										PrintMessage "p", "MILESTONE - MTS_Search_CDR_Checks complete", "Check_PreviousSO_Status_InMTS complete for row ("  & intRowNr_CurrentScenario &  ")" 
										Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Exit Do
										
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
															
								
								
								Case "WaitingForPrevStep"
								
								temp_last_row_status = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value
								temp_last_row_NextFunctionName = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("Next_Function" )).value
								
								If LCase(temp_last_row_status ) = "pass" Then
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start"
									
								ElseIf LCase(temp_last_row_status ) = "fail" Then
									gFWbln_ExitIteration = True
									gFWstr_ExitIteration_Error_Reason =  "Row failed as Previous Step row failed"
									
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed as Previous Step row failed"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									
								ElseIf LCase(temp_last_row_status ) = "" and temp_last_row_NextFunctionName = "" Then
									gFWbln_ExitIteration = True
									gFWstr_ExitIteration_Error_Reason =  "Step1 row not included in execution, marking this row as failed"
									
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = gFWstr_ExitIteration_Error_Reason
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								Else
									Exit Do
								End If 'end of If LCase(temp_last_row_status ) = "pass" Then
								
								
								Case "FAIL"
									Exit Do
								case else ' incase there is no function (which would NEVER happen) name
									Exit Do						
							End Select 'end of Select Case str_Next_Function
							
							
							' Loop While  (lcase(gFWbln_ExitIteration) <> "n" or gFWbln_ExitIteration <> false)
						Loop While  (LCase(gFWbln_ExitIteration) = "n" Or gFWbln_ExitIteration = False)
						
						Case "flow5","flow9","flow16","flow19","flow7","flow21":
						
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
							
							Select Case str_Next_Function
								
								Case "start" ' this is datamine
								
										' First function
										tsNow = Now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit Do
										End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										
										If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
											
											PrintMessage "i", "CASE Start", "[Data mining] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
											
											fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_SQL_Template_Name", "retrieve", "optional"
											
											If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 Then
												fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "MTS_SQL_Template_Name", "retrieve", "optional"
											Else
												fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "MTS_SQL_Template_Name", "retrieve", "mandatory"
											End If 
												
											int_NMI = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI")
																			
											If instr(1,int_NMI, "TECHFAIL-Unable-to-locate") <=0 and int_NMI <> ""  and gFWbln_ExitIteration <> true Then
												
												'calculate schedule date based on offset
												fn_determine_B2B_ScheduleDate objWS_DataParameter, intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr
												
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = int_NMI
												' ********************** Reserve NMI - START ************************
												If blnDataReservationIsActive_DefaultTrue Then
													fnKDR_Insert_Data_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
													fnKDR_Insert_Data_CIS_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
													fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
													' ####################################################################################################################################################################################
													' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
													' ####################################################################################################################################################################################
													fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", int_NMI, "", "", "", "", ""						                  		
													PrintMessage "p", "Reserve NMI - CANDIDATE","Scenario row ("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR as candidate"	
												End If 'end of If blnDataReservationIsActive_DefaultTrue Then
												' ********************** Reserve NMI - END  ************************
				
				
				
													'==========================================================================================================================================
											'KS: ALT_FRMP replace with correct details
											
											If TRIM(UCASE(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator") ).value )) =  "ROLE_ALTFRMP" OR TRIM(UCASE(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator") ).value )) = "ROLE_ALT_FRMP" Then
												'[KS] where or not if alt_frmp has a value, run below function and get a proper value
												'sqlALTFRMP = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "ALT_FRMP")	
													'If TRIM(sqlALTFRMP) = "" Then
												'		'execute cis query 		
												'	End If
												PrintMessage "i", "CASE Start", "[fn_Fetch_ALTFRMP_Value - replacing ALT_FRMP with correct value.]"
												fn_Fetch_ALTFRMP_Value objWS_DataParameter, intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A,DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B
											End If									
											'==========================================================================================================================================
				
				
												' ############################# BELOW CODE WOULD BE EXECUTED ONLY INCASE NMI IS NOT BLANK #######################################################
												
												' Check Required_SP_Status_MTS = D and create service order in CIS
												str_Required_SP_Status_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_MTS") ).value 
												str_Required_Deen_Method_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_Deen_Method_MTS") ).value 
												
												''Note - check if below is required or not ###########
												' ############ DATA PREP via API #####################
												
										        Create_Find_CompleteSO str_ScenarioRow_FlowName, objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, str_Required_SP_Status_MTS, str_Required_Deen_Method_MTS, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A									
												
												If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS")).value ) <> "" Then
													fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_API_ModifyServiceProvisionRequest", "insert", "optional"	
													gOverrideTime = Cint_wait_time_TenMin
												End if 'end of If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS")).value ) <> "" Then 										
												
												' ###################   Code to take care of multiple special conditions - START ##############################################################
													strScratch	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Special_Condition_Type") ).value 
													If strScratch <> "" Then
														if gOverrideTime < Cint_wait_time_ThreeMin Then gOverrideTime = Cint_wait_time_ThreeMin
														PrintMessage "p", "Special_Condition_Type","Scenario row ("& intRowNr_CurrentScenario &") - Special Condition Requested ("& strScratch &") "
														temp_array_SpecialCOnditions = Split(strScratch,";")
														temp_array_count_items = UBound(temp_array_SpecialCOnditions)
														
														For intctr = 0 To temp_array_count_items 
															If Trim(temp_array_SpecialCOnditions(intctr)) <> "" Then
																gstr_SpecialCondition = Trim(temp_array_SpecialCOnditions(intctr))
																
																' ############ CIS_Property_Service_Provision_Query #####################
																' capture CIS_API_Query to fetch the property ID and Service Providion No
																
																fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_Property_Service_Provision_Query", "retrieve", "mandatory"	
																fn_wait(2) ' Koki: why this wait ????
																fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_API_Special_Condition", "Insert", "mandatory"	
																
															End If ' end of If Trim(temp_array_SpecialCOnditions(intctr)) <> "" Then
														Next ' end of For intctr = 0 To temp_array_count_items 
													End If ' end of If strScratch <> "" Then
												' ###################   Code to take care of multiple special conditions - END ##############################################################								
												
												Create_Service_Provision_Existing_Property str_ScenarioRow_FlowName, objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, str_Required_SP_Status_MTS, str_Required_Deen_Method_MTS, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A
												
											ElseIf  gFWbln_ExitIteration <> true  Then
												fn_set_error_flags true, "NMI not found as some query might have failed. Existing test"
												PrintMessage "f", "NMI not found", gFWstr_ExitIteration_Error_Reason
											End if ' end of If int_NMI <> "" Then	
			
										' At the end, check if any error was there and write in corresponding column
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"									
											PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " data mine error), moving to the next row", gFWstr_ExitIteration_Error_Reason
											' create a function to dump error in a file 
											'strScratch = gFWstr_RunFolder & "Error_Dump_" & fnTimeStamp(now, "YYYY`MM`DD_HH`mm`ss") & ".txt"
											'sb_File_WriteContents  strScratch, gfsoForAppending, true, gFWstr_ExitIteration_Error_Reason
											PrintMessage "f", "MILESTONE - Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit Do
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML"
											PrintMessage "p", "MILESTONE - Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")											
										End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										
										strSQL_CIS = ""
										strSQL_MTS = ""
										
									End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								Case "XML"
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
									PrintMessage "i", "CASE Start", "[XML] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
									
									If temp_XML_Template  <> "" Then
										
										PrintMessage "p", "Starting XML creation","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation with ("& temp_XML_Template &")"
										int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
										objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")).value = int_UniqueReferenceNumberforXML
										fn_wait(1)
										objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss")  ' This would NOT be same as int_UniqueReferenceNumberforXML
										
										If LCase(str_ScenarioRow_FlowName) = "flow7" Then
											temp_save_SchDate_Original_Value = objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("ScheduledDate")).value 
											objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("ScheduledDate")).value = fnTimeStamp(Now-1, "YYYY-MM-DD") 
										End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow7" Then
										
										temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
										temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
										int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
										str_ChangeStatusCode = "B2BSO"
										
										' ####################################################################################################################################################################################
										' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
										' ####################################################################################################################################################################################
										fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", int_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""
										
										PrintMessage "p", "Populate XML","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for ("& str_ChangeStatusCode &") - NMI ("& int_NMI &") - Role ("& temp_Role &")"
										str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
										temp_XML_Template, gFWstr_RunFolder, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
										intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now, int_UniqueReferenceNumberforXML, _
										temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
		
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								            		PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
								            Else
											strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
											PrintMessage "p", "XML File","Scenario row ("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
																				
											' ZIP XML File
											ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
											PrintMessage "p", "Zippng XML File","Scenario row ("& intRowNr_CurrentScenario &") - Zipping XML file  ("& str_DataParameter_PopulatedXML_FqFileName &")"									            
											
											GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
											
											If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
												PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												gFWbln_ExitIteration = "y"
												gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
											Else
												' copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
												store_value_performance_Dictionary gdict_XMLFilename_DestinationFolder, Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", Environment.Value("CATSDestinationFolder")
												PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
											End If 'end of If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
										End If ' end of If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
										
									Else
										fn_set_error_flags true, "XML Template cell blank - No XML template located in cell (xmlTemplate_Name)." 
										PrintMessage "f", "XML failure", gFWstr_ExitIteration_Error_Reason
									End If ' end of If temp_XML_Template  <> "" Then
									
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " XML creation error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else
										If int_Ctr_No_Of_Times = temp_no_of_times  Then
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Else
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "start"
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										End If
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
										PrintMessage "p", "MILESTONE - XML creation complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										Exit Do
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									
									
								End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
		
		
								Case "B2B_Gateway_Ack_verify"
								
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[B2B_Gateway_Ack_verify] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "B2B_Gateway_Ack_verify", "retrieve", "mandatory"
									
									strScratch = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_TRANSACTION_STATUS")
									' Verify what the query has returned
									
		'							If trim(strScratch ) <> "" Then
		'								temp_desc_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESCC")
		'								temp_desc_code_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESC_CODE")
		'								temp_explanation_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_EXPLANATION")
		'								fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - Description ("& temp_desc_ack &") - Desc Code ("& temp_desc_code_ack &") - Explanation ("& temp_explanation_ack &")"
		'								PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason
		'							Else
		'								PrintMessage "p", "B2B_Gateway_Ack_verify check pass", "B2B_Gateway_Ack_verify query has not returned any error and hence moving to next steps"
		'							End If
		
									If trim(strScratch ) = "REJECTED" Then
										temp_desc_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESCC")
										temp_desc_code_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESC_CODE")
										temp_explanation_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_EXPLANATION")
										fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - Description ("& temp_desc_ack &") - Desc Code ("& temp_desc_code_ack &") - Explanation ("& temp_explanation_ack &")"
										PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason
									Else
										If trim(strScratch ) = "ACCEPTED" Then
											PrintMessage "p", "B2B_Gateway_Ack_verify check pass", "B2B_ACKV_TRANSACTION_STATUS is ACCEPTED"
										Else
											fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - B2B_ACKV_TRANSACTION_STATUS ("& strScratch &")"
											PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason									
										End If	'If trim(strScratch ) = "ACCEPTED" Then							
									End If 'If trim(strScratch ) = "REJECTED" Then
		
								' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during B2B_Gateway_Ack_verify because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " B2B_Gateway_Ack_verify error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "B2B_Gateway_Ack_verify complete"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SOstatus_isRaised"
										PrintMessage "p", "MILESTONE - B2B_Gateway_Ack_verify complete", "B2B_Gateway_Ack_verify complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										temp_save_SchDate_Original_Value = ""
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								Case "MTS_Check_SOstatus_isRaised"
								
								' fn_compare_Current_and_expected_execution_time
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[MTS_Check_SOstatus_isRaised] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
								
								PrintMessage "p", "MTS_Check_SOstatus_isRaised","Verify MTS_Check_SOstatus_isRaised for NMI ("& int_NMI &") "
		
		'						MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"first",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value, _
		'						objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
		'						objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,"y", "n", "",""
		'						
								'[KS] flow7 new scenario raised where second xml also get rejected. Since there are only 1 column existing for rejection reason/exp in the driversheet, we will not check rej_reason for 1st xml but will check for 2nd xml
								If LCase(str_ScenarioRow_FlowName) = "flow7" or LCase(str_ScenarioRow_FlowName) = "flow21"Then						
									MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value, _
																									"first",_
																									objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value, _
																									"", _
																									"", _
																									"", _														
																									"", _
																									"","y", "n", "",""
								Else						
									MTS_Check_SOstatus_isRaised_V2 objWS_DataParameter,intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr,_
																"",	int_NMI, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value, _
																"first",_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,"y", "n", "",""												
								End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow7" Then
								
								'If trim(gdict_scratch("CIS_ServiceOrderNumber")) = "" Then
								'gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & " - CIS SO not generated in MTS"
								'gFWbln_ExitIteration = true
								'Else
								'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value = gdict_scratch("CIS_ServiceOrderNumber")
								'End If
								
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Check_SOstatus_isRaised because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isRaised error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_Check_SOstatus_isRaised"
									PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised complete", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - MTS_Check_SOstatus_isRaised complete" 
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								End If ' end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								
								Case "CIS_Check_SOstatus_isRaised"
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[CIS_Check_SOstatus_isRaised] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								
								If LCase(str_ScenarioRow_FlowName) <> "flow7" and  LCase(str_ScenarioRow_FlowName) <> "flow21" Then								
									int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
									var_ScheduledDate = fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("ScheduledDate")).value,"DD-MM-YYYY")
									
									'############## CIS_SO_Check_Query #################
									strSQL_CIS = ""
									strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SO_Check_Query")).value 
									If strSQL_CIS <> "" Then
		
										PrintMessage "p", "CIS SQL template: CIS_SO_Check_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - CIS SQL Template ("& strSQL_CIS &") "
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										' perform all replacements
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
										Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "<NMI>", int_NMI,  1, -1, vbTextCompare)
										strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_CIS_SO_NUM>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber" )),  1, -1, vbTextCompare)
			
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "CIS_SO_Check_Query Query:" & vbCrLf & strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
			
										' Execute CIS SQL
										'Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", "", ""
										
										
										'If LCase(Trim(objDBRcdSet_TestData_A.RecordCount)) > 0  And LCase(Trim(objDBRcdSet_TestData_A.Fields("WORKORDERTYPE").Value = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )).value)) And _
										'	LCase(Trim(objDBRcdSet_TestData_A.Fields("WORKORDERTASK").Value)) = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value)) Then 
										'	gFWbln_ExitIteration = False
										'Else
										'	temp_error_message =  "Issue with CIS SO raised. Actual value: " & objDBRcdSet_TestData_A.Fields ("WORKORDERTYPE") & "\"  & objDBRcdSet_TestData_A.Fields ("WORKORDERTASK")
										'	gFWbln_ExitIteration = True
										'	gFWstr_ExitIteration_Error_Reason = "CIS_Check_SOstatus_isRaised function - " & temp_error_message														
										'End If 'end of If LCase(Trim(objDBRcdSet_TestData_A.RecordCount)) > 0  And LCase(Trim(objDBRcdSet_TestData_A.Fields("WORKORDERTYPE").Value = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )).value)) And _
		
										strScratch = Execute_SQL_Populate_DataSheet_Hatseperator(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",   "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  "")
										
										If lcase(strScratch) <> "fail" Then
										
											objWB_Master.save ' Save the workbook
											strScratch = ""
											
											' Verify output of query
											' WORKORDERTYPE
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType_Code" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","WORKORDERTYPE")))
											If temp_expected_value <> "" Then									
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Value of WORKORDERTYPE (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as value ("& temp_expected_value &") mentioned in CIS_workOrderType_Code column in datasheet"
												Else
													fn_set_error_flags true, "Value of WORKORDERTYPE (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from value ("& temp_expected_value &") mentioned in CIS_workOrderType_Code column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to WORKORDERTYPE as CIS_workOrderType_Code column is blank in datasheet"
											End if
											
											' WORKORDERTASK
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","WORKORDERTASK")))
											If temp_expected_value <> "" Then
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
												Else
													fn_set_error_flags true, "Value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to WORKORDERTASK as CIS_workOrderSubType column is blank in datasheet"
											End If
		
											' SERVICEORDERCHARGECODE
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SERVICEORDERCHARGECODE")))
											
											If temp_expected_value <> "" Then
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
												Else
													fn_set_error_flags true, "Value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to SERVICEORDERCHARGECODE as Fee_Applied column is blank in datasheet"
											End if
											
											' RETAILERSONUMBER
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","RETAILERSONUMBER")))
											
											If temp_expected_value <> "" Then
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Value of RETAILERSONUMBER (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
												Else
													fn_set_error_flags true, "Value of RETAILERSONUMBER (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to RETAILERSONUMBER as Fee_Applied column is blank in datasheet"
											End If
											
											' SOEFFECTIVEDATE
											temp_expected_value = var_ScheduledDate ' LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ScheduledDate" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SOEFFECTIVEDATE")))
											
											If temp_expected_value <> "" Then
												 'temp_expected_value = fnTimeStamp(temp_expected_value, "DD-MM-YYYY")
												If temp_actual_value <> "" Then
													temp_actual_value = fnTimeStamp(temp_actual_value, "DD-MM-YYYY")	
												End If
												
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Value of SOEFFECTIVEDATE (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as value ("& temp_expected_value &") mentioned in ScheduledDate column in datasheet"
												Else
													fn_set_error_flags true, "Value of SOEFFECTIVEDATE (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from value ("& temp_expected_value &") mentioned in ScheduledDate column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to SOEFFECTIVEDATE as ScheduledDate column is blank in datasheet"
											End If
		
											If strScratch <> "" Then
												gFWstr_ExitIteration_Error_Reason = strScratch
											End If
											
										End If	'end of If lcase(strScratch) <> "fail" Then
										
									End If 'end of If strSQL_CIS <> "" Then
									
									'############## CIS_ModifySO_API_Query #################
									strSQL_CIS = ""
									strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_ModifySO_API_Query")).value 
									If strSQL_CIS <> "" Then
										PrintMessage "p", "CIS SQL template: CIS_ModifySO_API_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - CIS SQL Template ("& strSQL_CIS &") "
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										' perform all replacements
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
										Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
										
										strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_SONUM>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber" )),  1, -1, vbTextCompare)
										strSQL_CIS = Replace ( strSQL_CIS  , "<param_DateToday-eg-2017-02-28>", fnTimeStamp ( Now ,  "YYYY-MM-DD" ),  1, -1, vbTextCompare)
			
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "CIS_ModifySO_API_Query Query:" & vbCrLf & strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
			
										' Execute CIS SQL
										Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
									End If 'end of If strSQL_CIS <> "" Then
									
									
									'											If objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Meter_Type_CIS" )).value = "E" Then
									'													CIS_Check_SOstatus_isRaised int_NMI, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )).value, _
									'														var_ScheduledDate, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InCIS" )).value, _
									'															objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value, "n"
									'											Else
									'													CIS_Check_SOstatus_isRaised int_NMI, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )).value, _
									'														var_ScheduledDate, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InCIS" )).value, _
									'															objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value, "y"
									'												
									'											End If
									
									If LCase(str_ScenarioRow_FlowName) = "flow9" Then
										
										'############## CIS_API_Cancel_SO_Query #################
										strSQL_CIS = ""
										strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_API_Cancel_SO_Query")).value 
										PrintMessage "p", "CIS SQL template: CIS_API_Cancel_SO_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - CIS SQL Template ("& strSQL_CIS &") "
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										' perform all replacements
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
										Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_SONUM>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber" )), 1, -1, vbTextCompare)
		
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "CIS_API_Cancel_SO_Query Query:" & vbCrLf & strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		
										' Execute Query
										Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
										strSQL_CIS = ""
										
									End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow9" Then
									
									'New SO wait for 30 min 
									If LCase(str_ScenarioRow_FlowName) = "flow19" Then
										
										'############## CIS_API_New_SO_Query #################
										strSQL_CIS = ""
										strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_API_New_SO_Query")).value 
										PrintMessage "p", "CIS SQL template: CIS_API_New_SO_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - CIS SQL Template ("& strSQL_CIS &") "
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										' perform all replacements
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
										Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_SONUM>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber" )), 1, -1, vbTextCompare)
		
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "CIS_API_New_SO_Query Query:" & vbCrLf & strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		
										' Execute Query
										Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
										strSQL_CIS = ""
										
									End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow19" Then
								End If ' end of If LCase(str_ScenarioRow_FlowName) <> "flow7" Then								
								
								
								' ####################################### TO BE REMOVED ONCE THE DEFECT RELATED TO CIS IS RESOLVED <<<<<<<<<<<<<<<<<===========================
								' ####################################### TO BE REMOVED ONCE THE DEFECT RELATED TO CIS IS RESOLVED <<<<<<<<<<<<<<<<<===========================
									' KS: commenting this out as no one knows why this is here.
								'fn_reset_error_flags
								' #########################################################################################################################################
								' #########################################################################################################################################
								
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during CIS_Check_SOstatus_isRaised because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " CIS_Check_SOstatus_isRaised error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML_Second"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_Check_SOstatus_isRaised complete"
									PrintMessage "p", "MILESTONE - CIS_Check_SOstatus_isRaised complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								' ------------------------------------------------------------------------------------------------------------------
								
								Case "XML_Second"
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[XML_Second] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
									
									If temp_XML_Template  <> "" Then
										
										objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("actionType")).value = "Cancel"
										int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
										temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
										temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
										int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
										str_ChangeStatusCode = "B2BSO"
										
										
										' ####################################################################################################################################################################################
										' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
										' ####################################################################################################################################################################################
										fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", int_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""
										
										PrintMessage "p", "Starting XML creation","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &")"
										
										str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
										temp_XML_Template, gFWstr_RunFolder, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
										intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now, int_UniqueReferenceNumberforXML, _
										temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								            		PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
								            Else
											strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
											PrintMessage "p", "XML File","Scenario row ("& intRowNr_CurrentScenario &") - XML file for ("& str_ChangeStatusCode &") - Role ("& temp_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
											
											' ZIP XML File
											ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
											PrintMessage "p", "Zippng XML File","Scenario row ("& intRowNr_CurrentScenario &") - Zipping XML file ("& str_DataParameter_PopulatedXML_FqFileName &")"
											
											GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
											
											If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
												PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												gFWbln_ExitIteration = "y"
												gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
											Else
												' copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
												store_value_performance_Dictionary gdict_XMLFilename_DestinationFolder, Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", Environment.Value("CATSDestinationFolder")
												PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
											End If 'end of If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
										End If ' end of If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
										
									Else
										fn_set_error_flags true, "XML Template cell blank - No XML template located in cell (xmlTemplate_Name)." 
										PrintMessage "f", "XML failure", gFWstr_ExitIteration_Error_Reason
									End If ' end of If temp_XML_Template  <> "" Then
																
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during cancel XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " XML creation error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Cancel XML creation and placing on gateway complete"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SOstatus_isRaised_Second"
										PrintMessage "p", "MILESTONE - XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
									
										' Function to write timestamp for next process
										'fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										If LCase(str_ScenarioRow_FlowName) = "flow1" or LCase(str_ScenarioRow_FlowName) = "flow1a" or LCase(str_ScenarioRow_FlowName) = "flow5" or LCase(str_ScenarioRow_FlowName) = "flow12" or LCase(str_ScenarioRow_FlowName) = "flow16" or LCase(str_ScenarioRow_FlowName) = "flow18" or LCase(str_ScenarioRow_FlowName) = "flow19" Then
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										Else
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_AQREJECT, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										End If
											
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
																
								End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								
								Case "MTS_Check_SOstatus_isRaised_Second" 
								
								' fn_compare_Current_and_expected_execution_time
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[MTS_Check_SOstatus_isRaised_Second] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
								
								
								'[KS] flow7 new scenario raised where second xml also get rejected. Since there are only 1 column existing for rejection reason/exp in the driversheet, we will not check rej_reason for 1st xml but will check for 2nd xml
								If LCase(str_ScenarioRow_FlowName) = "flow7"  or LCase(str_ScenarioRow_FlowName) = "flow21" Then
										
										Dim temp_last_ret_so, temp_last_status
										
										temp_last_ret_so = objWS_DataParameter.cells(intRowNr_CurrentScenario,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value
										temp_last_status = objWS_DataParameter.cells(intRowNr_CurrentScenario,dictWSDataParameter_KeyName_ColNr("CancelSO_Status")).value
										
										If trim(temp_last_ret_so) <> "" AND trim(temp_last_status) <> ""  Then
										
											PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Scenario row ("& intRowNr_CurrentScenario &") - Search Verify Last SO (CancelSO). Ret_SONUM (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value & "), Status (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CancelSO_Status")).value &  "), AQ_Name (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value  &  "), AQ_Explaination (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value &  "), AQ_Status (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value &  "), Rejection_Reason (" & _														
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value &  "), Rejection_Explanation (" & _
																				objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value	& ")"																
																				
			
											'MTS_Check_SOstatus_isRaised 
											MTS_Check_SOstatus_isRaised_V2 objWS_DataParameter,intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr,_
																		"Cancel", int_NMI, _ 
																		objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,_
																		"",_
																		objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CancelSO_Status")).value, _
																		objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, _
																		objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, _
																		objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
																		objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, _
																		objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value, _
																		"n", "n", "",""
										Else								
											PrintMessage "i", "MTS_Check_SOstatus_isRaised_Second","Search & Verify First SO NOT done due to USER not provided verification string, Ret_ServiceOrderNumber (" & temp_last_ret_so & _
														"CancelSO_Status (" & temp_last_status & ")"
										End If
										
										
										Dim temp_first_ret_so, temp_first_status
										
										temp_first_ret_so = objWS_DataParameter.cells(intRowNr_CurrentScenario,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value
										temp_first_status = objWS_DataParameter.cells(intRowNr_CurrentScenario,dictWSDataParameter_KeyName_ColNr("PreviousSO_Status_InMTS_Updated")).value
										
										If trim(temp_first_ret_so) <> "" AND trim(temp_first_status) <> ""  Then
											
											PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Scenario row ("& intRowNr_CurrentScenario &") - Search Verify First SO. Ret_SONUM (" & _
																		temp_first_ret_so & "), Status (" &_
																		temp_first_status & "), and NO other "
											
											
											MTS_Check_SOstatus_isRaised_V2 objWS_DataParameter,intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr,_
																		"New", int_NMI, _ 
																		temp_first_ret_so,_
																		"",_
																		temp_first_status, _
																		"", "", "",	"", "",_
																		"n", "n", "",""
										Else								
											PrintMessage "i", "MTS_Check_SOstatus_isRaised_Second","Search & Verify First SO NOT done due to USER not provided verification string, Ret_ServiceOrderNumber (" & temp_first_ret_so & _
														"PreviousSO_Status_InMTS_Updated (" & temp_first_status & ")"
										End If
										
									
								Else						
										PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Scenario row ("& intRowNr_CurrentScenario &") - Last SO."
										MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CancelSO_Status")).value, _
										objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
										objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,"n", "n", "",""
										
										PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Scenario row ("& intRowNr_CurrentScenario &") - First SO."
										MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("PreviousSO_Status_InMTS_Updated")).value, _
										objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
										objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,"n", "n", "",""
								
								End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow7" Then
		
								
		'						PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Scenario row ("& intRowNr_CurrentScenario &") - Last SO."
		'						MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"last",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CancelSO_Status")).value, _
		'						objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
		'						objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,"n", "n", "",""
		'						
		'						PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Scenario row ("& intRowNr_CurrentScenario &") - First SO."
		'						MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"first",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("PreviousSO_Status_InMTS_Updated")).value, _
		'						objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
		'						objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,"n", "n", "",""
		'						
		
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Check_SOstatus_isRaised_Second because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isRaised_Second error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								
								ElseIf LCase(str_ScenarioRow_FlowName) = "flow16" or LCase(str_ScenarioRow_FlowName) = "flow19" Then
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised_Second complete"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML_third"
									PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
									' Function to write timestamp for next process
									If LCase(str_ScenarioRow_FlowName) = "flow19" Then	                                            
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ThirtyMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									Else
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow19" Then	   
									'Exit Do
								
								ElseIf LCase(str_ScenarioRow_FlowName) = "flow5" AND LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CancelSO_Status")).value) = "processed" Then
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised_Second complete"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_Check_SOstatus_isCancelled"
									PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
																
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised_third complete"
									PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
									Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
									
									'Koki to work on this. this will never work
									' Function to write timestamp for next process (flow 19 will require to wait for 30 mins)
								'	If LCase(str_ScenarioRow_FlowName) = "flow19" Then	                                            
								'		fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ThirtyMin, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								'	Else
								'		fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								'	End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow19" Then	                                            
									
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1	                                           
									Exit Do
								'End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow16" Then
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								
								Case "XML_third"
									
									If LCase(str_ScenarioRow_FlowName) <> "flow16" Then
										Exit Do
									End If 'end of If LCase(str_ScenarioRow_FlowName) <> "flow16" Then
									
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
										
										PrintMessage "i", "CASE Start", "[XML_third] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
										
										temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
										
										If temp_XML_Template  <> "" Then
											'Flow 19 with New third XML creation
											If LCase(str_ScenarioRow_FlowName) = "flow19" Then
												objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("actionType")).value = "New"                                    		
											Else
												objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("actionType")).value = "Cancel"                                           	
											End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow19" Then
											
											int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
											temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
											temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
											int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
											str_ChangeStatusCode = "B2BSO"
											
											' ####################################################################################################################################################################################
											' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
											' ####################################################################################################################################################################################
											fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", int_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""
											PrintMessage "p", "Starting XML creation","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &")"
											
											str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
											temp_XML_Template, gFWstr_RunFolder, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
											intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now, int_UniqueReferenceNumberforXML, _
											temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
									            		PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
									            Else
												
												strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
												PrintMessage "p", "XML File","Scenario row ("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
																						
												' ZIP XML File
												ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
												PrintMessage "p", "Zippng XML File","Scenario row ("& intRowNr_CurrentScenario &") - Zipping XML file for Stage ("& str_ChangeStatusCode &")"
												
												GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
												
												If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
													PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													gFWbln_ExitIteration = "y"
													gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												Else
													' copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
													store_value_performance_Dictionary gdict_XMLFilename_DestinationFolder, Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", Environment.Value("CATSDestinationFolder")
													PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
												End If 'end of If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
											End If ' end of If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											
										Else
											fn_set_error_flags true, "XML Template cell blank - No XML template located in cell (xmlTemplate_Name)." 
											PrintMessage "f", "XML failure", gFWstr_ExitIteration_Error_Reason
										End If ' end of If temp_XML_Template  <> "" Then
										
										' At the end, check if any error was there and write in corresponding column
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during cancel XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " XML creation error), moving to the next row", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit Do
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Second Cancel XML creation and placing on gateway complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SOstatus_isRaised_third"
											PrintMessage "p", "MILESTONE - Second XML (cancel) creation and placing on gateway complete", "Second XML (cancel) creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
											
											' Function to write timestamp for next process
											'fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											If LCase(str_ScenarioRow_FlowName) = "flow1" or LCase(str_ScenarioRow_FlowName) = "flow1a" or LCase(str_ScenarioRow_FlowName) = "flow5" or LCase(str_ScenarioRow_FlowName) = "flow12" or LCase(str_ScenarioRow_FlowName) = "flow16" or LCase(str_ScenarioRow_FlowName) = "flow18" or LCase(str_ScenarioRow_FlowName) = "flow19" Then
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											Else
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_AQREJECT, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											End If										
											
										End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
																			
									End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
									
								
								Case "MTS_Check_SOstatus_isRaised_third" 
								
									If LCase(str_ScenarioRow_FlowName) <> "flow16" and LCase(str_ScenarioRow_FlowName) <> "flow19" Then
										Exit Do
									End If 'end of If LCase(str_ScenarioRow_FlowName) <> "flow16" Then
									
									' fn_compare_Current_and_expected_execution_time
									
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[MTS_Check_SOstatus_isRaised_third] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
									
									If LCase(str_ScenarioRow_FlowName) = "flow16" Then
									
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CancelSO_Status" )).value = "REJECTED"
										
										PrintMessage "p", "MTS_Check_SOstatus_isRaised","Verify MTS_Check_SOstatus_isRaised_third for nmi ("& int_NMI &") "
										
										MTS_Check_SOstatus_isRaised int_NMI,_
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,_
																	"last",_
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CancelSO_Status")).value, _
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value,_
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value,_
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _                                                        
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value,_
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,_
																	"n", "n", "",""
									
									Else
										'flow19
										PrintMessage "p", "MTS_Check_SOstatus_isRaised","Verify MTS_Check_SOstatus_isRaised_third for nmi ("& int_NMI &") "
										
										MTS_Check_SOstatus_isRaised int_NMI,_
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,_
																	"last",_
																	"RAISED", _
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value,_
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, _
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _                                                        
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value,_
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,_
																	"n", "n", "",""
		
									End if
									
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Check_SOstatus_isRaised_Second because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isRaised_Second error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised_third complete"
										PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
										Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Exit Do					
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
		
		'---- KS: CIS_Check_SOstatus_isCancelled added to flow5
					Case "CIS_Check_SOstatus_isCancelled"
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[CIS_Check_SOstatus_isCancelled] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								
								'KS: flow5 AQ processed successfully therefore need to verify CIS SO Status.
								If LCase(str_ScenarioRow_FlowName) = "flow5" Then								
									int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value						
									
									'############## CIS_SO_Check_Status_Query #################
									strSQL_CIS = ""
									strSQL_CIS = "CIS_SO_Check_Status"
									If strSQL_CIS <> "" Then
		
										PrintMessage "p", "CIS SQL template: CIS_SO_Check_Status Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - CIS SQL Template ("& strSQL_CIS &") "
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										' perform all replacements							
										strSQL_CIS = Replace ( strSQL_CIS  , "<NMI_WITH_CHECKSUM>", int_NMI,  1, -1, vbTextCompare)
										strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_CIS_SO_NUM>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber" )),  1, -1, vbTextCompare)
			
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "CIS_SO_Check_Query Query:" & vbCrLf & strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
			
										strScratch = Execute_SQL_Populate_DataSheet_Hatseperator(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",   "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  "")
										
										If lcase(strScratch) <> "fail" Then
										
											objWB_Master.save ' Save the workbook
											strScratch = ""
											
											' Verify output of query
											' CIS_SO_STATUS
											temp_expected_value = "cancelled"
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","CIS_SO_STATUS")))
											
											If  trim(temp_actual_value) = trim(temp_expected_value) Then
												PrintMessage "p", "CIS_Check_SOstatus_isCancelled Verification",  "Actual value of CIS_SO_STATUS (" & temp_actual_value & ") from CIS_SO_Check_Status Query is same as expected value ("& temp_expected_value &")"
											Else
												fn_set_error_flags true, "Actual value of CIS_SO_STATUS (TVP109.ST_WORK_ORDER) is (" & temp_actual_value & ") from CIS_SO_Check_Status Query is DIFFERENT from expected value ("& temp_expected_value &")"
												PrintMessage "f", "CIS_Check_SOstatus_isCancelled Verification",  gFWstr_ExitIteration_Error_Reason
												strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
											End If 'If  trim(temp_actual_value) = trim(temp_expected_value) Then
										
											' CIS_TASK_STATUS
											temp_expected_value = "cancelled"
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","CIS_TASK_STATUS")))
																				
											If  trim(temp_actual_value) = trim(temp_expected_value) Then
												PrintMessage "p", "CIS_Check_SOstatus_isCancelled Verification",  "Value of CIS_TASK_STATUS (" & temp_actual_value & ") from CIS_SO_Check_Status Query is same as value ("& temp_expected_value &")"
											Else
												fn_set_error_flags true, "Actual value of CIS_TASK_STATUS (TVP523.ST_WRK_ORD_DET_112) is (" & temp_actual_value & ") from CIS_SO_Check_Status Query is DIFFERENT from expected value ("& temp_expected_value &")"
												PrintMessage "f", "CIS_Check_SOstatus_isCancelled Verification",  gFWstr_ExitIteration_Error_Reason
												strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
											End If 'If  trim(temp_actual_value) = trim(temp_expected_value) Then
											
											
											If strScratch <> "" Then
												gFWstr_ExitIteration_Error_Reason = strScratch
											End If '	If strScratch <> "" Then
											
										End If	'end of If lcase(strScratch) <> "fail" Then
										
									End If 'end of If strSQL_CIS <> "" Then		
								
								End If 'If LCase(str_ScenarioRow_FlowName) = "flow5" Then	
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during CIS_Check_SOstatus_isCancelled because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " CIS_Check_SOstatus_isCancelled error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_Check_SOstatus_isCancelled complete"
									PrintMessage "p", "MILESTONE - CIS_Check_SOstatus_isCancelled complete", "CIS_Check_SOstatus_isCancelled complete for row ("  & intRowNr_CurrentScenario &  ")" 
									Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
									Exit Do					
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
			
		'-----
		
								Case "WaitingForPrevStep"
									temp_last_row_status = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value
									temp_last_row_NextFunctionName = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("Next_Function" )).value						
									
									If LCase(temp_last_row_status ) = "pass" Then
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start"
									ElseIf LCase(temp_last_row_status ) = "fail" Then
										gFWbln_ExitIteration = True
										gFWstr_ExitIteration_Error_Reason =  "Row failed as Previous Step row failed"
									
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed as Previous Step row failed"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										
									ElseIf LCase(temp_last_row_status ) = "" and temp_last_row_NextFunctionName = "" Then
										gFWbln_ExitIteration = True
										gFWstr_ExitIteration_Error_Reason =  "Step1 row not included in execution, marking this row as failed"
										
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = gFWstr_ExitIteration_Error_Reason
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										
									Else
										Exit Do
									End If 'end of If LCase(temp_last_row_status ) = "pass" Then
								
								
								Case "FAIL"
									Exit Do
								case else ' incase there is no function (which would NEVER happen) name
									Exit Do						
								' ------------------------------------------------------------------
							End Select 'end of Select Case str_Next_Function
							' Loop While  (lcase(gFWbln_ExitIteration) <> "n" or gFWbln_ExitIteration <> false)
						Loop While  (LCase(gFWbln_ExitIteration) = "n" Or gFWbln_ExitIteration = False)
						
						
						' ###################### FLOW 11 START ################################################
						
						Case "flow11":
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
							Select Case str_Next_Function
								Case "start" ' this is datamine
								
										' First function
										tsNow = Now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit Do
										End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										
										If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
											
											PrintMessage "i", "CASE Start", "[Gateway_Response_Check] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
											fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_SQL_Template_Name", "retrieve", "optional"
											If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 Then
												fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "MTS_SQL_Template_Name", "retrieve", "optional"
											Else
												fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "MTS_SQL_Template_Name", "retrieve", "mandatory"
											End If
												
											int_NMI = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI")
																			
											If instr(1,int_NMI, "TECHFAIL-Unable-to-locate") <=0 and int_NMI <> ""  and gFWbln_ExitIteration <> true Then
												
												'calculate schedule date based on offset
												fn_determine_B2B_ScheduleDate objWS_DataParameter, intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr
												
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = int_NMI
												' ********************** Reserve NMI - START ************************
												If blnDataReservationIsActive_DefaultTrue Then
													fnKDR_Insert_Data_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
													fnKDR_Insert_Data_CIS_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
													fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
													' ####################################################################################################################################################################################
													' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
													' ####################################################################################################################################################################################
													fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", int_NMI, "", "", "", "", ""						                  		
													PrintMessage "p", "Reserve NMI - CANDIDATE","Scenario row ("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR as candidate"	
												End If 'end of If blnDataReservationIsActive_DefaultTrue Then
												' ********************** Reserve NMI - END  ************************
		
		
											'==========================================================================================================================================
											'KS: ALT_FRMP replace with correct details
											
											If TRIM(UCASE(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator") ).value )) =  "ROLE_ALTFRMP" OR TRIM(UCASE(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator") ).value )) = "ROLE_ALT_FRMP" Then
												'[KS] where or not if alt_frmp has a value, run below function and get a proper value
												'sqlALTFRMP = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "ALT_FRMP")	
													'If TRIM(sqlALTFRMP) = "" Then
												'		'execute cis query 		
												'	End If
												PrintMessage "i", "CASE Start", "[fn_Fetch_ALTFRMP_Value - replacing ALT_FRMP with correct value.]"
												fn_Fetch_ALTFRMP_Value objWS_DataParameter, intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A,DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B
											End If									
											'==========================================================================================================================================
		
												' ############################# BELOW CODE WOULD BE EXECUTED ONLY INCASE NMI IS NOT BLANK #######################################################
												
												' Check Required_SP_Status_MTS = D and create service order in CIS
												str_Required_SP_Status_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_MTS") ).value 
												str_Required_Deen_Method_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_Deen_Method_MTS") ).value 
												
												''Note - check if below is required or not ###########
												' ############ DATA PREP via API #####################
												
												Create_Find_CompleteSO str_ScenarioRow_FlowName, objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, str_Required_SP_Status_MTS, str_Required_Deen_Method_MTS, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A										
												
												If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS")).value ) <> "" Then
													fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_API_ModifyServiceProvisionRequest", "insert", "optional"	
													gOverrideTime = Cint_wait_time_TenMin
												End if 'end of If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS")).value ) <> "" Then 										
												
												
												' ###################   Code to take care of multiple special conditions - START ##############################################################
													strScratch	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Special_Condition_Type") ).value 
													If strScratch <> "" Then
														PrintMessage "p", "Special_Condition_Type","Scenario row ("& intRowNr_CurrentScenario &") - Special Condition Requested ("& strScratch &") "
														if gOverrideTime < Cint_wait_time_ThreeMin Then gOverrideTime = Cint_wait_time_ThreeMin
														temp_array_SpecialCOnditions = Split(strScratch,";")
														temp_array_count_items = UBound(temp_array_SpecialCOnditions)
														
														For intctr = 0 To temp_array_count_items 
															If Trim(temp_array_SpecialCOnditions(intctr)) <> "" Then
																gstr_SpecialCondition = Trim(temp_array_SpecialCOnditions(intctr))
																
																' ############ CIS_Property_Service_Provision_Query #####################
																' capture CIS_API_Query to fetch the property ID and Service Providion No
																
																fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_Property_Service_Provision_Query", "retrieve", "mandatory"	
																fn_wait(2) ' Koki: why this wait ????
																fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_API_Special_Condition", "Insert", "mandatory"	
																
															End If ' end of If Trim(temp_array_SpecialCOnditions(intctr)) <> "" Then
														Next ' end of For intctr = 0 To temp_array_count_items 
													End If ' end of If strScratch <> "" Then
												' ###################   Code to take care of multiple special conditions - END ##############################################################								
												
												Create_Service_Provision_Existing_Property str_ScenarioRow_FlowName, objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, str_Required_SP_Status_MTS, str_Required_Deen_Method_MTS, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A
												
											ElseIf  gFWbln_ExitIteration <> true  Then
												fn_set_error_flags true, "NMI not found as some query might have failed. Existing test"
												PrintMessage "f", "NMI not found", gFWstr_ExitIteration_Error_Reason
											End if ' end of If int_NMI <> "" Then	
			
												' At the end, check if any error was there and write in corresponding column
											If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"										
												'PrintMessage "f", "FLOW (" & str_ScenarioRow_FlowName & " datamine error), Query did not return any data, moving to the next row", gFWstr_ExitIteration_Error_Reason
												' create a function to dump error in a file 
												'strScratch = gFWstr_RunFolder & "Error_Dump_" & fnTimeStamp(now, "YYYY`MM`DD_HH`mm`ss") & ".txt"
												'sb_File_WriteContents  strScratch, gfsoForAppending, true, gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "MILESTONE - Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit Do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML"
												PrintMessage "p", "MILESTONE - Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 										
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")	
											End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											strSQL_CIS = ""
											strSQL_MTS = ""
									
								End If ' end of IF tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								Case "XML"
									
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
										PrintMessage "i", "CASE Start", "[XML] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
										
										temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
										
										If temp_XML_Template  <> "" Then
											PrintMessage "p", "Starting B2BSO XML creation","Scenario row ("& intRowNr_CurrentScenario &") - starting B2B XML creation with template ("& temp_XML_Template &")"
											int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
											objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")).value = int_UniqueReferenceNumberforXML
											fn_wait(1)
											objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss")  ' This would NOT be same as int_UniqueReferenceNumberforXML
											
											temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
											temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
											int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
											str_ChangeStatusCode = "B2BSO"
											
											' ####################################################################################################################################################################################
											' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
											' ####################################################################################################################################################################################
											fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", int_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""
											PrintMessage "p", "Populate XML","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for ("& str_ChangeStatusCode &") - NMI ("& int_NMI &") - Role ("& temp_Role &")"
											
											str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
											temp_XML_Template, gFWstr_RunFolder, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
											intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now, int_UniqueReferenceNumberforXML, _
											temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
											
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
									            		PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
									            Else
												strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
												PrintMessage "p", "XML File","Scenario row ("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
																						
												' ZIP XML File
												ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
												PrintMessage "p", "Zippng XML File","Scenario row ("& intRowNr_CurrentScenario &") - Zipping XML file  ("& str_DataParameter_PopulatedXML_FqFileName &")"	
												
												GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
												
												If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
													PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													gFWbln_ExitIteration = "y"
													gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												Else
													' copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
													store_value_performance_Dictionary gdict_XMLFilename_DestinationFolder, Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", Environment.Value("CATSDestinationFolder")
													PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
												End If 'end of If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
											End If ' end of If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
											
										Else
											fn_set_error_flags true, "XML Template cell blank - No XML template located in cell (xmlTemplate_Name)." 
											PrintMessage "f", "XML failure", gFWstr_ExitIteration_Error_Reason
										End If ' end of If temp_XML_Template  <> "" Then
				
										' At the end, check if any error was there and write in corresponding column
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " XML creation error), moving to the next row", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit Do
										Else
											If int_Ctr_No_Of_Times = temp_no_of_times  Then
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
											Else
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "start"
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
											End If
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
											PrintMessage "p", "MILESTONE - XML creation complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											Exit Do
										End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									
									End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
		
		
								Case "B2B_Gateway_Ack_verify"
								
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[B2B_Gateway_Ack_verify] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "B2B_Gateway_Ack_verify", "retrieve", "mandatory"
									
									strScratch = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_TRANSACTION_STATUS")
									' Verify what the query has returned
									
									If trim(strScratch ) = "REJECTED" Then
										temp_desc_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESCC")
										temp_desc_code_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESC_CODE")
										temp_explanation_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_EXPLANATION")
										fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - Description ("& temp_desc_ack &") - Desc Code ("& temp_desc_code_ack &") - Explanation ("& temp_explanation_ack &")"
										PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason
									Else
										If trim(strScratch ) = "ACCEPTED" Then
											PrintMessage "p", "B2B_Gateway_Ack_verify check pass", "B2B_ACKV_TRANSACTION_STATUS is ACCEPTED"
										Else
											fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - B2B_ACKV_TRANSACTION_STATUS ("& strScratch &")"
											PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason									
										End If	'If trim(strScratch ) = "ACCEPTED" Then							
									End If 'If trim(strScratch ) = "REJECTED" Then							
									
									
		'							If trim(strScratch ) <> "" Then
		'								temp_desc_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESCC")
		'								temp_desc_code_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESC_CODE")
		'								temp_explanation_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_EXPLANATION")
		'								fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - Description ("& temp_desc_ack &") - Desc Code ("& temp_desc_code_ack &") - Explanation ("& temp_explanation_ack &")"
		'								PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason
		'							Else
		'								PrintMessage "p", "B2B_Gateway_Ack_verify check pass", "B2B_Gateway_Ack_verify query has not returned any error and hence moving to next steps"
		'							End If
									
									' At the end, check if any error was there and write in corresponding column
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during B2B_Gateway_Ack_verify because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " B2B_Gateway_Ack_verify error), moving to the next row", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit Do
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "B2B_Gateway_Ack_verify complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SOstatus_isRaised"
											PrintMessage "p", "MILESTONE - B2B_Gateway_Ack_verify complete", "B2B_Gateway_Ack_verify complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									
		
		
								Case "MTS_Check_SOstatus_isRaised"
								
								' fn_compare_Current_and_expected_execution_time
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[MTS_Check_SOstatus_isRaised] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
								
								PrintMessage "p", "MTS_Check_SOstatus_isRaised","Verify MTS_Check_SOstatus_isRaised for NMI ("& int_NMI &") "
								
								MTS_Check_SOstatus_isRaised int_NMI, _
															objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,_
															"first",_
															objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value, _
															objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value,_
															objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value,_
															objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
															objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value,_
															objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,_
															"y", "n", "",""
								
								'koki to correct this flow issue
								If (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow20") And LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value) ="raised" Then
									If Trim(gdict_scratch("CIS_ServiceOrderNumber")) = "" Then
										gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & " - CIS SO not generated in MTS"
										gFWbln_ExitIteration = True
									Else
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value = gdict_scratch("CIS_ServiceOrderNumber")
										
										If LCase(str_ScenarioRow_FlowName) = "flow20" Then
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_API_SO_Complete"
										End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow20" Then
									End If 'end of If Trim(gdict_scratch("CIS_ServiceOrderNumber")) = "" Then
								End If 'end of If (LCase(str_ScenarioRow_FlowName) = "flow1" Or LCase(str_ScenarioRow_FlowName) = "flow20") And LCase(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value) ="raised" Then
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Check_SOstatus_isRaised because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isRaised error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_Create_Second_SO"
									PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised complete", "MTS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								Case "CIS_Create_Second_SO"
								
								' fn_compare_Current_and_expected_execution_time
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[CIS_Create_Second_SO] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								
								'############### CIS_fetch_Retailer_Details ##############################
								strSQL_CIS = ""
								strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_fetch_Retailer_Details")).value 													
								PrintMessage "p", "CIS SQL template: CIS_fetch_Retailer_Details","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - CIS SQL Template ("& strSQL_CIS &") "
								strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
								
								strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value
								' perform all replacements
								strSQL_CIS = Replace ( strSQL_CIS  , "<CIS_ServiceOrderNumber>", strScratch ,  1, -1, vbTextCompare)
		
								' Update query in SQL Query cell
								strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
								strScratch = strScratch & vbCrLf & "CIS_ServiceOrderNumber Query:" & vbCrLf & strSQL_CIS
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
								objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		
								Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", "", ""
								
								If objDBRcdSet_TestData_A.RecordCount > 0  Then
									
									'############### CIS_CreateSO_WithAdditional_Data_API_Query ##############################
									strSQL_CIS = ""
									strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_CreateSO_WithAdditional_Data_API_Query")).value 
									PrintMessage "p", "CIS SQL template: CIS_CreateSO_WithAdditional_Data_API_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - CIS SQL Template ("& strSQL_CIS &") "
									strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
									
									' perform all replacements
									strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
									strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
									Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
									
									strSQL_CIS = Replace ( strSQL_CIS  , "<param_NMI>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value, 1, -1, vbTextCompare)
									strSQL_CIS = Replace ( strSQL_CIS  , "<param_CIS_workOrderType>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )).value, 1, -1, vbTextCompare)
									strSQL_CIS = Replace ( strSQL_CIS  , "<param_scheduledate_plus_two_days>",  fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ScheduledDate" )).value + 2 , "YYYY-MM-DD"), 1, -1, vbTextCompare)
									strSQL_CIS = Replace ( strSQL_CIS  , "<param_Retailer_code>", objDBRcdSet_TestData_A.Fields ("Retailer_code"), 1, -1, vbTextCompare)
									strSQL_CIS = Replace ( strSQL_CIS  , "<param_RetailerName>", objDBRcdSet_TestData_A.Fields ("RetailerName"), 1, -1, vbTextCompare)
									strSQL_CIS = Replace ( strSQL_CIS  , "<param_ RET_SO_NUM>", objDBRcdSet_TestData_A.Fields ("RET_SO_NUM"), 1, -1, vbTextCompare)
		
									' Update query in SQL Query cell
									strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
									strScratch = strScratch & vbCrLf & "CIS_CreateSO_WithAdditional_Data_API_Query Query:" & vbCrLf & strSQL_CIS
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		
									' Execute Query
									Execute_Insert_SQL   DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
									strSQL_CIS = ""
									
								Else
									' Generate error as not able to create and comeplete SO in CIS												
									gFWbln_ExitIteration = True
									gFWstr_ExitIteration_Error_Reason = "Unable to complete SO because the query (" & strSQL_CIS & ") didn't return any row"
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at CIS_Create_Second_SO because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - Unable to create specal condition" , gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								End If ' end of If objDBRcdSet_TestData_A.RecordCount > 0  Then
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during CIS_Create_Second_SO because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " CIS_Create_Second_SO error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_Create_Second_SO complete"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML_Second"
									PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised complete", "MTS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								Case "XML_Second"
								
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
										PrintMessage "i", "CASE Start", "[XML_Second] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
										temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
										
										If temp_XML_Template  <> "" Then
											PrintMessage "p", "Starting XML creation","Starting XML creation with ("& temp_XML_Template &")"
											objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("actionType")).value = "Cancel"
											int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
											temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
											temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
											int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
											str_ChangeStatusCode = "B2BSO"
											
											' ####################################################################################################################################################################################
											' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
											' ####################################################################################################################################################################################
											fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", int_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""								        
											PrintMessage "p", "Populate XML","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for ("& str_ChangeStatusCode &") - NMI ("& int_NMI &") - Role ("& temp_Role &")"									
											
											str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
											temp_XML_Template, gFWstr_RunFolder, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
											intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now, int_UniqueReferenceNumberforXML, _
											temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
											If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
									            		PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
									            Else
												strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
												PrintMessage "p", "XML File","Scenario row ("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
																						
												' ZIP XML File
												ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
												PrintMessage "p", "Zippng XML File","Scenario row ("& intRowNr_CurrentScenario &") - Zipping XML file  ("& str_DataParameter_PopulatedXML_FqFileName &")"	
												
												GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
												
												If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
													PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													gFWbln_ExitIteration = "y"
													gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												Else
													' copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
													store_value_performance_Dictionary gdict_XMLFilename_DestinationFolder, Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", Environment.Value("CATSDestinationFolder")
													PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
													objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
												End If 'end of If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
											End If ' end of If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
			
										Else
											fn_set_error_flags true, "XML Template cell blank - No XML template located in cell (xmlTemplate_Name)." 
											PrintMessage "f", "XML failure", gFWstr_ExitIteration_Error_Reason
										End If ' end of If temp_XML_Template  <> "" Then
			
										' At the end, check if any error was there and write in corresponding column
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during cancel XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " XML creation error), moving to the next row", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit Do
										Else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Cancel XML creation and placing on gateway complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SOstatus_isRaised_Second"
											PrintMessage "p", "MILESTONE - XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											'fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											If LCase(str_ScenarioRow_FlowName) = "flow1" or LCase(str_ScenarioRow_FlowName) = "flow1a" or LCase(str_ScenarioRow_FlowName) = "flow5" or LCase(str_ScenarioRow_FlowName) = "flow12" or LCase(str_ScenarioRow_FlowName) = "flow16" or LCase(str_ScenarioRow_FlowName) = "flow18" or LCase(str_ScenarioRow_FlowName) = "flow19" Then
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											Else
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_AQREJECT, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											End If
										End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								
								Case "MTS_Check_SOstatus_isRaised_Second" 						
								' fn_compare_Current_and_expected_execution_time						
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[MTS_Check_SOstatus_isRaised_Second] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
								
								PrintMessage "p", "MTS_Check_SOstatus_isRaised","Verify MTS_Check_SOstatus_isRaised for NMI ("& int_NMI &") "
								MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"last",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CancelSO_Status")).value, _
								objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
								objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,"n", "n", "",""
								
								MTS_Check_SOstatus_isRaised int_NMI,objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,"first",objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("PreviousSO_Status_InMTS_Updated")).value, _
								objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
								objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,"n", "n", "",""
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Check_SOstatus_isRaised_Second because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isRaised_Second error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised_Second complete"
									PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
									Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
									Exit Do							
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								Case "WaitingForPrevStep"
									temp_last_row_status = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value
									temp_last_row_NextFunctionName = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("Next_Function" )).value
									
									If LCase(temp_last_row_status ) = "pass" Then
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start"							
									ElseIf LCase(temp_last_row_status ) = "fail" Then
										gFWbln_ExitIteration = True
										gFWstr_ExitIteration_Error_Reason =  "Row failed as Previous Step row failed"
										
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed as Previous Step row failed"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									
									ElseIf LCase(temp_last_row_status ) = "" and temp_last_row_NextFunctionName = "" Then
										gFWbln_ExitIteration = True
										gFWstr_ExitIteration_Error_Reason =  "Step1 row not included in execution, marking this row as failed"
										
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = gFWstr_ExitIteration_Error_Reason
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
																	
									Else
										Exit Do
									End If 'end of If LCase(temp_last_row_status ) = "pass" Then
								
								
								Case "FAIL"
									Exit Do
								case else ' incase there is no function (which would NEVER happen) name
									Exit Do							
								' --------------------------------------------------------------------
							End Select 'end of Select Case str_Next_Function
							' Loop While  (lcase(gFWbln_ExitIteration) <> "n" or gFWbln_ExitIteration <> false)
						Loop While  (LCase(gFWbln_ExitIteration) = "n" Or gFWbln_ExitIteration = False)
						
						' ###################### FLOW 11 End ##################################################
						
						
						' ###################### FLOW 12 START ################################################
						
						Case "flow12","flow12a","flow18":
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
							Select Case str_Next_Function
								Case "start" ' this is datamine
		
										' First function
										tsNow = Now() 
										tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
										If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
											Exit Do
										End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										
										If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
											
											PrintMessage "i", "CASE Start", "[Data mining] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
											fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_SQL_Template_Name", "retrieve", "optional"
											If InStr(1,LCase(temp_Scenario_ID),"step2") > 0 Then
												fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "MTS_SQL_Template_Name", "retrieve", "optional"
											else
												fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "MTS_SQL_Template_Name", "retrieve", "mandatory"
											End If
												
											int_NMI = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "NMI")
																			
											If instr(1,int_NMI, "TECHFAIL-Unable-to-locate") <=0 and int_NMI <> ""  and gFWbln_ExitIteration <> true Then
												
												'calculate schedule date based on offset
												fn_determine_B2B_ScheduleDate objWS_DataParameter, intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr
										
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )) = int_NMI
												' ********************** Reserve NMI - START ************************
												If blnDataReservationIsActive_DefaultTrue Then
													fnKDR_Insert_Data_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
													fnKDR_Insert_Data_CIS_V1 "n", "n",int_NMI, temp_Scenario_ID, "available"
													fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
													' ####################################################################################################################################################################################
													' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
													' ####################################################################################################################################################################################
													fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", int_NMI, "", "", "", "", ""						                  		
													PrintMessage "p", "Reserve NMI - CANDIDATE","Scenario row ("& intRowNr_CurrentScenario &") - Reserving NMI  ("& int_NMI &") in KDR as candidate"	
												End If 'end of If blnDataReservationIsActive_DefaultTrue Then
												' ********************** Reserve NMI - END  ************************
		
											'==========================================================================================================================================
											'KS: ALT_FRMP replace with correct details
											
											If TRIM(UCASE(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator") ).value )) =  "ROLE_ALTFRMP" OR TRIM(UCASE(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Initiator") ).value )) = "ROLE_ALT_FRMP" Then
												'[KS] where or not if alt_frmp has a value, run below function and get a proper value
												'sqlALTFRMP = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "ALT_FRMP")	
													'If TRIM(sqlALTFRMP) = "" Then
												'		'execute cis query 		
												'	End If
												PrintMessage "i", "CASE Start", "[fn_Fetch_ALTFRMP_Value - replacing ALT_FRMP with correct value.]"
												fn_Fetch_ALTFRMP_Value objWS_DataParameter, intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A,DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B
											End If									
											'==========================================================================================================================================
		
												' ############################# BELOW CODE WOULD BE EXECUTED ONLY INCASE NMI IS NOT BLANK #######################################################
												
												' Check Required_SP_Status_MTS = D and create service order in CIS
												str_Required_SP_Status_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_MTS") ).value 
												str_Required_Deen_Method_MTS		= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_Deen_Method_MTS") ).value 
												
												''Note - check if below is required or not ###########
												' ############ DATA PREP via API #####################
												Create_Find_CompleteSO str_ScenarioRow_FlowName, objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, str_Required_SP_Status_MTS, str_Required_Deen_Method_MTS, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A
												
												If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS")).value ) <> "" Then
													fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_API_ModifyServiceProvisionRequest", "insert", "optional"	
													gOverrideTime = Cint_wait_time_TenMin
												End if 'end of If trim(objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Required_SP_Status_CIS")).value ) <> "" Then 										
												
												' ###################   Code to take care of multiple special conditions - START ##############################################################
													strScratch	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Special_Condition_Type") ).value 
													If strScratch <> "" Then
														PrintMessage "p", "Special_Condition_Type","Scenario row ("& intRowNr_CurrentScenario &") - Special Condition Requested ("& strScratch &") "
														if gOverrideTime < Cint_wait_time_ThreeMin Then gOverrideTime = Cint_wait_time_ThreeMin
														temp_array_SpecialCOnditions = Split(strScratch,";")
														temp_array_count_items = UBound(temp_array_SpecialCOnditions)
														
														For intctr = 0 To temp_array_count_items 
															If Trim(temp_array_SpecialCOnditions(intctr)) <> "" Then
																gstr_SpecialCondition = Trim(temp_array_SpecialCOnditions(intctr))
																
																' ############ CIS_Property_Service_Provision_Query #####################
																' capture CIS_API_Query to fetch the property ID and Service Providion No
																
																fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_Property_Service_Provision_Query", "retrieve", "mandatory"	
																fn_wait(2) ' Koki: why this wait ????
																fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, LCase(str_ScenarioRow_FlowName), "CIS_API_Special_Condition", "Insert", "mandatory"	
																
															End If ' end of If Trim(temp_array_SpecialCOnditions(intctr)) <> "" Then
														Next ' end of For intctr = 0 To temp_array_count_items 
													End If ' end of If strScratch <> "" Then
												' ###################   Code to take care of multiple special conditions - END ##############################################################								
												
												Create_Service_Provision_Existing_Property str_ScenarioRow_FlowName, objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, str_Required_SP_Status_MTS, str_Required_Deen_Method_MTS, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A
												
											ElseIf  gFWbln_ExitIteration <> true  Then
												fn_set_error_flags true, "NMI not found as some query might have failed. Existing test"
												PrintMessage "f", "NMI not found", gFWstr_ExitIteration_Error_Reason
											End if ' end of If int_NMI <> "" Then	
			
											' At the end, check if any error was there and write in corresponding column
											If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at data mine because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"										
												'PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " data mine error), moving to the next row", gFWstr_ExitIteration_Error_Reason
												' create a function to dump error in a file 
												'strScratch = gFWstr_RunFolder & "Error_Dump_" & fnTimeStamp(now, "YYYY`MM`DD_HH`mm`ss") & ".txt"
												'sb_File_WriteContents  strScratch, gfsoForAppending, true, gFWstr_ExitIteration_Error_Reason
												PrintMessage "f", "MILESTONE - Problem with SQL, moving to the next row", gFWstr_ExitIteration_Error_Reason
												Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												Exit Do
											Else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Data mining complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML"
												PrintMessage "p", "MILESTONE - Data mining complete", "Data mining complete for row ("  & intRowNr_CurrentScenario &  ")" 
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")								
												' umesh to update
											End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											strSQL_CIS = ""
											strSQL_MTS = ""
									
								End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								Case "XML"
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
									PrintMessage "i", "CASE Start", "[XML] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
									
									If temp_XML_Template  <> "" Then
										PrintMessage "p", "Starting XML creation","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation with ("& temp_XML_Template &")" 
										int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
										objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("INIT_TRANSACTION_ID")).value = int_UniqueReferenceNumberforXML									          
										fn_wait(1)									            
										int_Unique_SO_Number = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss")  ' This would NOT be same as int_UniqueReferenceNumberforXML
										objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = int_Unique_SO_Number
										temp_save_SchDate_Original_Value = objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("ScheduledDate")).value 
										objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("ScheduledDate")).value = fnTimeStamp(Now-1, "YYYY-MM-DD") 
										
										temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
										temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
										int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
										str_ChangeStatusCode = "B2BSO"
										
										' ####################################################################################################################################################################################
										' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
										' ####################################################################################################################################################################################
										fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", int_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""									        
										PrintMessage "p", "Populate XML","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for ("& str_ChangeStatusCode &") - NMI ("& int_NMI &") - Role ("& temp_Role &")"
										
										str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
										temp_XML_Template, gFWstr_RunFolder, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
										intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now, int_UniqueReferenceNumberforXML, _
										temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
										
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								            		PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
								            Else
											strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
											PrintMessage "p", "XML File","Scenario row ("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
											
											' ZIP XML File
											ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
											PrintMessage "p", "Zippng XML File","Scenario row ("& intRowNr_CurrentScenario &") - Zipping XML file  ("& str_DataParameter_PopulatedXML_FqFileName &")"									            
											
											GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
											
											If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
												PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												gFWbln_ExitIteration = "y"
												gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
											Else
												' copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
												store_value_performance_Dictionary gdict_XMLFilename_DestinationFolder, Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", Environment.Value("CATSDestinationFolder")
												PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
											End If 'end of If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
										End If ' end of If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
										
									Else
										fn_set_error_flags true, "XML Template cell blank - No XML template located in cell (xmlTemplate_Name)." 
										PrintMessage "f", "XML failure", gFWstr_ExitIteration_Error_Reason
									End If ' end of If temp_XML_Template  <> ""  Then
									
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " XML creation error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else
										If int_Ctr_No_Of_Times = temp_no_of_times  Then
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Else
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "start"
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										End If
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "XML creation and placing on gateway complete"
										PrintMessage "p", "MILESTONE - XML creation complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										Exit Do
		
									End If 'end of  If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results						
							End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
		
		
		
							Case "B2B_Gateway_Ack_verify"
								
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[B2B_Gateway_Ack_verify] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									fn_Query_Flow_B2B_SO  objWS_DataParameter,  intRowNr_CurrentScenario,  dictWSDataParameter_KeyName_ColNr, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, LCase(str_ScenarioRow_FlowName), "B2B_Gateway_Ack_verify", "retrieve", "mandatory"
									
									strScratch = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_TRANSACTION_STATUS")
									
									' Verify what the query has returned
									If trim(strScratch ) = "REJECTED" Then
										temp_desc_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESCC")
										temp_desc_code_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESC_CODE")
										temp_explanation_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_EXPLANATION")
										fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - Description ("& temp_desc_ack &") - Desc Code ("& temp_desc_code_ack &") - Explanation ("& temp_explanation_ack &")"
										PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason
									Else
										If trim(strScratch ) = "ACCEPTED" Then
											PrintMessage "p", "B2B_Gateway_Ack_verify check pass", "B2B_ACKV_TRANSACTION_STATUS is ACCEPTED"
										Else
											fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - B2B_ACKV_TRANSACTION_STATUS ("& strScratch &")"
											PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason									
										End If	'If trim(strScratch ) = "ACCEPTED" Then							
									End If 'If trim(strScratch ) = "REJECTED" Then							
									
		'							If trim(strScratch ) <> "" Then
		'								temp_desc_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESCC")
		'								temp_desc_code_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_DESC_CODE")
		'								temp_explanation_ack = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values" )).value, "^", "B2B_ACKV_EXPLANATION")
		'								fn_set_error_flags true, "B2B_Gateway_Ack_verify check failure - Description ("& temp_desc_ack &") - Desc Code ("& temp_desc_code_ack &") - Explanation ("& temp_explanation_ack &")"
		'								PrintMessage "f", "B2B_Gateway_Ack_verify check failure", gFWstr_ExitIteration_Error_Reason
		'							Else
		'								PrintMessage "p", "B2B_Gateway_Ack_verify check pass", "B2B_Gateway_Ack_verify query has not returned any error and hence moving to next steps"
		'							End If
		'							
									' At the end, check if any error was there and write in corresponding column
										If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during B2B_Gateway_Ack_verify because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
											PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " B2B_Gateway_Ack_verify error), moving to the next row", gFWstr_ExitIteration_Error_Reason
											Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											Exit Do
										Else
											' Revert schedule date to original value
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "B2B_Gateway_Ack_verify complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SOstatus_isRaised"
											PrintMessage "p", "MILESTONE - B2B_Gateway_Ack_verify complete", "B2B_Gateway_Ack_verify complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										End If 'end of  If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									
		
								Case "MTS_Check_SOstatus_isRaised"
								
								' fn_compare_Current_and_expected_execution_time
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[MTS_Check_SOstatus_isRaised] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
								
								If LCase(str_ScenarioRow_FlowName) = "flow12" or LCase(str_ScenarioRow_FlowName) = "flow12a" Then										
									PrintMessage "p", "MTS_Check_SOstatus_isRaised","Verify MTS_Check_SOstatus_isRaised for NMI ("& int_NMI &") "									
		
									'set autorise replace 
									MTS_Check_SOstatus_isRaised_V2 objWS_DataParameter,intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr,_
																"", int_NMI, _ 
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,_
																"first",_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value,_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value,_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
																"","","", "y", "",""
								End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow12" or LCase(str_ScenarioRow_FlowName) = "flow12a" Then										
								
								'Don't set autorise replace 
								If LCase(str_ScenarioRow_FlowName) = "flow18" Then			
									PrintMessage "p", "MTS_Check_SOstatus_isRaised","Scenario row ("& intRowNr_CurrentScenario &") - MTS_Check_SOstatus_isRaised for NMI ("& int_NMI &") "											
									
									MTS_Check_SOstatus_isRaised int_NMI,_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,_
																"first",_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InMTS")).value, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value,_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value,_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
																"","","y", "n", "",""
								End If 'end of If LCase(str_ScenarioRow_FlowName) = "flow18" Then			
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Check_SOstatus_isRaised because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isRaised error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised complete"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "XML_Second"
									PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised complete", "MTS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
								
								
								Case "XML_Second"
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
									PrintMessage "i", "CASE Start", "[XML_Second] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									temp_XML_Template = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("xmlTemplate_Name")).value
									
									If temp_XML_Template  <> "" Then
										PrintMessage "p", "Starting XML creation","Starting XML creation with ("& temp_XML_Template &")"
										objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("actionType")).value = "Replace"
										
										int_UniqueReferenceNumberforXML = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss") ' GetRandomNumber(1300000001, 2147483647) ' "txnID_<dataTxnId_Numeric_AnFwkMSN_eg`1368906681>"  sample "txnID_1811821464" ' qq - need 110's to prevent collision during parallel execution
										fn_wait(1)
										
										int_Unique_SO_Number = fnUniqueNrFromTS(Now(), "CCYYMMDDHHmmss")  ' This would NOT be same as int_UniqueReferenceNumberforXML
										objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value = int_Unique_SO_Number
										
										temp_XML_Template_Uniq_Ref =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("XML_Tag_Case_Group")).value
										temp_Role =  objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Role_for_Gateway_Queue")).value
										int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" ))
										str_ChangeStatusCode = "B2BSO"
										
										' ####################################################################################################################################################################################
										' #####################################################                 LOGGING STEP 2 - Update variables which change at runtime     #########################################################################
										' ####################################################################################################################################################################################
										fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "",  "", int_NMI,  int_UniqueReferenceNumberforXML,  temp_Role,  str_ChangeStatusCode, "", ""									        
										PrintMessage "p", "Populate XML","Scenario row ("& intRowNr_CurrentScenario &") - starting XML creation for ("& str_ChangeStatusCode &") - NMI ("& int_NMI &") - Role ("& temp_Role &")"
										
										
										str_DataParameter_PopulatedXML_FqFileName = fnCreateCATSXML_V2(Environment.Value("COMPANY"), temp_XML_Template_Uniq_Ref, str_ChangeStatusCode, BASE_XML_Template_DIR, _
										temp_XML_Template, gFWstr_RunFolder, objWS_DataParameter,objWS_DataParameter, dictWSDataParameter_KeyName_ColNr, _ 
										intRowNr_CurrentScenario, gvntAr_RuleTable, g2dictWStemplateXML_TagDataSrc_Tables_ColValue, Now, int_UniqueReferenceNumberforXML, _
										temp_Role, int_NMI, "csListSqlColumn_Names", "csListSqlColumn_Values")
										
										If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
								            		PrintMessage "f","Unreplaced tags in XML", "There seems to be some unreplaced tags in XML (" & str_DataParameter_PopulatedXML_FqFileName & "). Exiting iteration"
								            Else
											strScratch = fnElement(str_DataParameter_PopulatedXML_FqFileName, "\", 0, -1, "R")
											PrintMessage "p", "XML File","Scenario row ("& intRowNr_CurrentScenario &") - XML file for Stage ("& str_ChangeStatusCode &") - Role ("& temp_Role &") is ("& str_DataParameter_PopulatedXML_FqFileName &")"
																				
											' ZIP XML File
											ZipFile str_DataParameter_PopulatedXML_FqFileName, gFWstr_RunFolder ' Environment.Value("TESTFOLDER")
											PrintMessage "p", "Zippng XML File","Scenario row ("& intRowNr_CurrentScenario &") - Zipping XML file  ("& str_DataParameter_PopulatedXML_FqFileName &")"		
											
											GetCATSDestinationFolder_V2 Environment.Value("COMPANY"),Environment.Value("ENV"), "B2B", temp_Role, gvntAr_CATS_Destination_Folder_Table, gdictWSCATSDestinationFolder_Table_ColValue  ' WIP
											
											If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
												PrintMessage "f","Gateway destination folder missing", "Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												gFWbln_ExitIteration = "y"
												gFWstr_ExitIteration_Error_Reason  = "Gateway destination folder missing - Not able to locate destination folder for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
											Else
												' copyfile Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip",gFWstr_RunFolder ,Environment.Value("CATSDestinationFolder") ' qq fix this length hack
												store_value_performance_Dictionary gdict_XMLFilename_DestinationFolder, Left(str_DataParameter_PopulatedXML_FqFileName, Len(str_DataParameter_PopulatedXML_FqFileName)-4) &".zip", Environment.Value("CATSDestinationFolder")
												PrintMessage "p", "XML placed on Gateway", "XML placed on gateway ("& Environment.Value("CATSDestinationFolder")&") for Role ("& temp_role &") - Application (B2B) - Environemnt ("& Environment.Value("ENV") &") - Company ("& Environment.Value("COMPANY") &")"
												objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Gateway_Destination_Folder")).value = Environment.Value("CATSDestinationFolder")
											End If 'end of If Environment.Value("CATSDestinationFolder") = "<notfound>" Or Environment.Value("CATSDestinationFolder") = "" Then
										End If ' end of If lcase(gFWbln_ExitIteration) = "y" or gFWbln_ExitIteration = true Then ' write an error in the results
		
									Else
										fn_set_error_flags true, "XML Template cell blank - No XML template located in cell (xmlTemplate_Name)." 
										PrintMessage "f", "XML failure", gFWstr_ExitIteration_Error_Reason
									End If ' end of If temp_XML_Template  <> "" Then
		
		
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during cancel XML creation because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " XML creation error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Replace XML creation and placing on gateway complete"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Check_SOstatus_isRaised_Second"
										PrintMessage "p", "MILESTONE - XML creation and placing on gateway complete", "XML creation and placing on gateway complete for row ("  & intRowNr_CurrentScenario &  ")" 
										' Function to write timestamp for next process
										'fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
		
										If LCase(str_ScenarioRow_FlowName) = "flow1" or LCase(str_ScenarioRow_FlowName) = "flow1a" or LCase(str_ScenarioRow_FlowName) = "flow5" or LCase(str_ScenarioRow_FlowName) = "flow12" or LCase(str_ScenarioRow_FlowName) = "flow16" or LCase(str_ScenarioRow_FlowName) = "flow18" or LCase(str_ScenarioRow_FlowName) = "flow19" Then
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										Else
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_AQREJECT, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										End If
										
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
																								
								End If ' end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								
								Case "MTS_Check_SOstatus_isRaised_Second" 
								
								' fn_compare_Current_and_expected_execution_time
								
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								PrintMessage "i", "CASE Start", "[MTS_Check_SOstatus_isRaised_Second] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
								int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
								
								If objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("ReplaceSO_Status")).value <> "" Then
									
									PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Verify MTS_Check_SOstatus_isRaised for LAST SO for NMI ("& int_NMI &"), Ret_ServiceOrderNumber ("  & _
														objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value &  "), ReplaceSO_Status (" & _
														objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("ReplaceSO_Status")).value & "), AQ_Name (" & _
														objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value & "), AQ_Explaination (" & _
														objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value & "), AQ_Status (" & _
														objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value & "), Rejection_Reason (" & _														
														objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value & "), Rejection_Explanation (" & _
														objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value & ")."
							
									MTS_Check_SOstatus_isRaised int_NMI,_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value,_
																"last",_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("ReplaceSO_Status")).value, _
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Name")).value,_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Explaination")).value,_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("AQ_Status")).value, _														
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Reason")).value,_
																objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("Rejection_Explanation")).value,_
																"n", "n", "",""
								
								Else
									PrintMessage "i", "MTS_Check_SOstatus_isRaised_Second","ReplaceSO_Status column is blank, therefore skipping check."
								End If 'ReplaceSO_Status <> ""
								
								
								If objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("PreviousSO_Status_InMTS_Updated")).value <> "" Then
								
									PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Verify MTS_Check_SOstatus_isRaised for FIRST SO for NMI ("& int_NMI &") "
									
									'------------------------------------------------------------------------------------------------------------------------------------------
									' MTS_Find_Ret_ServiceOrderNumber query to find Ret SO Num for the first SO
										strSQL_MTS = "MTS_Find_Ret_ServiceOrderNumber" 
										If strSQL_MTS <> ""  Then
											PrintMessage "p", "MTS_Find_Ret_ServiceOrderNumber", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& strSQL_MTS  &")"
											strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
											' perform all replacements
											strSQL_MTS = Replace ( strSQL_MTS  , "<sqltag_action_type>", "New", 1, -1, vbTextCompare)
											strSQL_MTS = Replace ( strSQL_MTS  , "<sqltag_nmi>", _
											objWS_DataParameter.cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("NMI") ).value, 1, -1, vbTextCompare)
			
			
											' Update query in SQL Query - SQL_Populated cell
											strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
											strScratch = strScratch & vbCrLf & "MTS_Find_Ret_ServiceOrderNumber Query:" & vbCrLf & strSQL_MTS
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
											
											Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), ""
										End If ' end of If strSQL_CIS <> ""  Then
									
									int_SQL_RET_SONUM = fn_Fetch_SpecificArrayAttributeValue (objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,",", "SQL_RET_SONUM")
									'------------------------------------------------------------------------------------------------------------------------------------------
									
									If int_SQL_RET_SONUM <> "" Then
										
										PrintMessage "p", "MTS_Check_SOstatus_isRaised_Second","Verify MTS_Check_SOstatus_isRaised for FIRST SO for NMI ("& int_NMI &"), int_SQL_RET_SONUM ("  & _
															int_SQL_RET_SONUM &  ") PreviousSO_Status_InMTS_Updated (), AQ_Name (), AQ_Explaination (), AQ_Status (), Rejection_Reason (), Rejection_Explanation ()"
				
										MTS_Check_SOstatus_isRaised int_NMI,_
																	int_SQL_RET_SONUM,_
																	"first",_
																	objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("PreviousSO_Status_InMTS_Updated")).value, _
																	"",_
																	"",_
																	"", _														
																	"",_
																	"",_
																	"n", "n", "",""
									
									ELSE
										gFWbln_ExitIteration = "y"
										gFWstr_ExitIteration_Error_Reason  = "Unable to retrieve retailer service order number for the first SO (int_SQL_RET_SONUM)"
										PrintMessage "f","MTS_Check_SOstatus_isRaised_Second", gFWstr_ExitIteration_Error_Reason
									End If 'If int_SQL_RET_SONUM <> "" Then
		
								Else
									PrintMessage "i", "MTS_Check_SOstatus_isRaised_Second","PreviousSO_Status_InMTS_Updated column is blank, therefore skipping check."
								End If 'PreviousSO_Status_InMTS_Updated <> ""
			
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Check_SOstatus_isRaised_Second because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isRaised_Second error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									Select Case LCase(str_ScenarioRow_FlowName)
										Case "flow12a"
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised_Second complete"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Search_Serivice_Order_AQ"
											PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "MTS_Check_SOstatus_isRaised_Second complete for row ("  & intRowNr_CurrentScenario &  ")" 
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										
										Case "flow12"
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised_Second complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "DB_Check_ALL_AQS"
												PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "MTS_Check_SOstatus_isRaised_Second complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										
										Case else
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Check_SOstatus_isRaised_Second complete"
											PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "MTS_Check_SOstatus_isRaised_Second complete for row ("  & intRowNr_CurrentScenario &  ")" 
											Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
											' Function to write timestamp for next process
											fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
											Exit Do
									End Select
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
								'Search Replace AQ and assign to User and close AQ for auto raise
								Case "MTS_Search_Serivice_Order_AQ" 
								
									' fn_compare_Current_and_expected_execution_time
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[MTS_Search_Serivice_Order_AQ] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
									strScratch = ""
									strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber" )).value
									
									If strScratch  <> "" Then
										PrintMessage "p", "MTS_Search_Serivice_Order_AQ screen checks", "Scenario row ("& intRowNr_CurrentScenario &") - MTS_Search_Serivice_Order_AQ for Retailer Service Order No ("& strScratch &"), NMI ("& int_NMI &") "
										
										fn_MTS_MenuNavigation "Transactions;Service Orders;Service Order AQ Search"
										fn_MTS_Search_Service_Order_AQ  strScratch, "","","","","", int_NMI,"","", fnTimeStamp(now,"DD/MM/YYYY"), fnTimeStamp(now,"DD/MM/YYYY"), "", "", "", "","","","Y","Y", gFW_strRunLog_ScreenCapture_fqFileName
																		
										If fn_Fetch_Total_Records_Results_Grid("Search_Service_Order_AQ_Screen") Then
											PrintMessage "p", "MTS_Search_Serivice_Order_AQ screen checks", "Checking records on Search_Service_Order_AQ_Screen"
											fn_MTS_Search_Service_Order_AQ_SelectandVerify_Record_Results "first","","","","","","","","","","","","","","","y",gFW_strRunLog_ScreenCapture_fqFileName
											
											PrintMessage "p", "MTS_Search_Serivice_Order_AQ screen", "Clicking Details button on Search_Service_Order_AQ_Screen"
											fn_MTS_Click_button_any_screen "Search_Service_Order_AQ", "btn_Details"
											
											' Reassign AQ to Logged in user
											PrintMessage "p", "MTS_Search_Serivice_Order_Detail screen", "Re-assigning to user (" & ucase(gobjNet.UserName) & ")"
											fn_MTS_Service_Order_AQ_Detail_Reassign ucase(gobjNet.UserName), "","","", "y","",""
											
											' Select action as Auto Raise
											PrintMessage "p", "MTS_Search_Serivice_Order_Detail screen", "Setting action to (Auto-raise)"
											fn_MTS_Service_Order_AQ_Detail_Reassign "", "Auto-raise","","", "y","y", gFW_strRunLog_ScreenCapture_fqFileName
											
											' Click close button
											PrintMessage "p", "MTS_Search_Serivice_Order_Detail screen", "Clicking Close button"
											fn_MTS_Close_any_screen "Service_Order_AQ_Details"
											' fn_MTS_Click_button_any_screen "Service_Order_AQ_Details", "btn_Close"
										
										ELSE
											fn_set_error_flags true, "fn_MTS_Search_Service_Order_AQ step - No AQ found and hence cannot proceed with function fn_MTS_Search_Service_Order_AQ"
											PrintMessage "p", "MTS_Search_Serivice_Order_AQ screen checks", gFWstr_ExitIteration_Error_Reason
										End If
										fn_MTS_Close_any_screen "Search_Service_Order_AQ"
									Else
										fn_set_error_flags true, "MTS_Search_Serivice_Order_AQ step - unable to find Retailer Service Order No and hence cannot proceed with function fn_MTS_Search_Service_Order_AQ"
										PrintMessage "p", "MTS_Search_Serivice_Order_AQ screen checks", gFWstr_ExitIteration_Error_Reason
									End If
									
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Check_SOstatus_isRaised_Second because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Check_SOstatus_isRaised_Second error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else
										Select Case LCase(str_ScenarioRow_FlowName)
											Case "flow12a"
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Search_Serivice_Order_AQ complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "MTS_Search_Serivice_Order_Request"
												PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "MTS_Check_SOstatus_isRaised_Second complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_XML_Process, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
																		
											Case else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Search_Serivice_Order_AQ complete"
												PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "MTS_Check_SOstatus_isRaised_Second complete for row ("  & intRowNr_CurrentScenario &  ")"
												Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 										
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												Exit Do
										End Select 'end of  Select Case LCase(str_ScenarioRow_FlowName)
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
							Case "MTS_Search_Serivice_Order_Request" 
								
									' fn_compare_Current_and_expected_execution_time
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[MTS_Search_Serivice_Order_Request] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
									strScratch = ""
									strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber" )).value
									
									If strScratch  <> "" Then
										PrintMessage "p", "MTS_Search_Serivice_Order_Request screen checks", "Scenario row ("& intRowNr_CurrentScenario &") - MTS_Search_Serivice_Order_Request for Retailer Service Order No ("& strScratch &"), NMI ("& int_NMI &") "
										
										fn_MTS_MenuNavigation "Transactions;Service Orders;Service Order Request Search"
										fn_MTS_Serach_Service_Order_Request strScratch,"","",int_NMI,"","","","","","",""
										fn_MTS_Serach_Service_Order_Request_SelectandVerifyRecord "first",  objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber" )).value,"","","","","","","","RAISED","y","y"
										fn_MTS_Close_any_screen "Search_Service_Order_Request_Screen"
										
										If Trim(gdict_scratch("CIS_ServiceOrderNumber")) = "" Then
											gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & " - CIS SO not generated in MTS"
											gFWbln_ExitIteration = True
										Else
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value = gdict_scratch("CIS_ServiceOrderNumber")
										End If 'end of If Trim(gdict_scratch("CIS_ServiceOrderNumber")) = "" Then
		
										
									Else
										fn_set_error_flags true, "MTS_Search_Serivice_Order_Request step - unable to find Retailer Service Order No and hence cannot proceed with function MTS_Search_Serivice_Order_Request"
										PrintMessage "p", "MTS_Search_Serivice_Order_Request screen checks", gFWstr_ExitIteration_Error_Reason
									End If
									
									' At the end, check if any error was there and write in corresponding column
									If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed during MTS_Search_Serivice_Order_Request because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
										PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " MTS_Search_Serivice_Order_Request error), moving to the next row", gFWstr_ExitIteration_Error_Reason
										Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										Exit Do
									Else
										Select Case LCase(str_ScenarioRow_FlowName)
											Case "flow12a"
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "In-Progress"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Search_Serivice_Order_Request complete"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "CIS_Check_SOstatus_isRaised"
												PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "MTS_Check_SOstatus_isRaised_Second complete for row ("  & intRowNr_CurrentScenario &  ")" 
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
											Case else
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "MTS_Search_Serivice_Order_Request complete"
												PrintMessage "p", "MILESTONE - MTS_Check_SOstatus_isRaised_Second complete", "MTS_Check_SOstatus_isRaised_Second complete for row ("  & intRowNr_CurrentScenario &  ")" 
												Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
												' Function to write timestamp for next process
												fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
												int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
												Exit Do
										End Select 'end of  Select Case LCase(str_ScenarioRow_FlowName)
									End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
								Case "CIS_Check_SOstatus_isRaised"
								
								If LCase(str_ScenarioRow_FlowName) = "flow12a" Then
									
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									
									PrintMessage "i", "CASE Start", "[CIS_Check_SOstatus_isRaised] for Scenario ("& intRowNr_CurrentScenario &") Flow (" & LCase(str_ScenarioRow_FlowName) & ")"
									
									int_NMI = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("NMI" )).value
									var_ScheduledDate = fnTimeStamp(objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("ScheduledDate")).value,"DD-MM-YYYY")
									
									'######## CIS_SO_Check_Query #########################
									strSQL_CIS = ""
									strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SO_Check_Query")).value 
									
									If Trim(strSQL_CIS) <> ""  Then											
										PrintMessage "p", "CIS_Check_SOstatus_isRaised","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing CIS_SO_Check_Query ("& strSQL_CIS &") "
										strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
										
										' perform all replacements
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
										strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
										Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
										
										strSQL_CIS = Replace ( strSQL_CIS  , "<NMI>", int_NMI,  1, -1, vbTextCompare)
				'koki working here: handle multple xml. cis so num not got in dict because last checked was aq/rej
						
										If Trim(gdict_scratch("CIS_ServiceOrderNumber")) = "" or objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value = "" Then
											gFWstr_ExitIteration_Error_Reason = gFWstr_ExitIteration_Error_Reason & " - CIS SO not generated in MTS"
											gFWbln_ExitIteration = True
											PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason									
										Else
											'objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value = gdict_scratch("CIS_ServiceOrderNumber")
										End if	
										
										
								  	If (lcase(trim(gFWbln_ExitIteration)) <> "y" or  gFWbln_ExitIteration <> true) AND objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value <> "" Then
									
								
										strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_CIS_SO_NUM>", objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber" )).value,  1, -1, vbTextCompare)
			
		
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "CIS_SO_Check_Query Query:" & vbCrLf & strSQL_CIS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		
										' Execute CIS SQL
										'Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "", "", ""
										
		'								If LCase(Trim(objDBRcdSet_TestData_A.RecordCount)) > 0  And LCase(Trim(objDBRcdSet_TestData_A.Fields("WORKORDERTYPE").Value)) = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType_Code" )).value)) And _
		'									LCase(Trim(objDBRcdSet_TestData_A.Fields("WORKORDERTASK").Value)) = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value)) Then 
		'									gFWbln_ExitIteration = False
		'									
		'									If Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied" )).value) <> "" Then
		'										If LCase(Trim(objDBRcdSet_TestData_A.Fields("SERVICEORDERCHARGECODE").Value)) <> LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied")).value)) Then
		'											temp_error_message =  "CIS_Check_SOstatus_isRaised failed: Actual Charge value: " & objDBRcdSet_TestData_A.Fields ("SERVICEORDERCHARGECODE") & "Expected was: " & objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied")).value
		'											gFWbln_ExitIteration = True
		'											gFWstr_ExitIteration_Error_Reason = temp_error_message
		'											PrintMessage "f", "CIS_SO_Check_Query", gFWstr_ExitIteration_Error_Reason											   			
		'										End If 'end of If LCase(Trim(objDBRcdSet_TestData_A.Fields("SERVICEORDERCHARGECODE").Value)) <> LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied")).value)) Then
		'									End If 'end of If Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied" )).value) <> "" Then
		'								Else
		'									temp_error_message =  "CIS_Check_SOstatus_isRaised failed: Actual CIS SO Type value: " & objDBRcdSet_TestData_A.Fields ("WORKORDERTYPE") & "\"  & objDBRcdSet_TestData_A.Fields ("WORKORDERTASK")
		'									gFWbln_ExitIteration = True
		'									gFWstr_ExitIteration_Error_Reason = "CIS_Check_SOstatus_isRaised function - " & temp_error_message														
		'									PrintMessage "f", "CIS_SO_Check_Query", gFWstr_ExitIteration_Error_Reason
		'								End If 'end of If LCase(Trim(objDBRcdSet_TestData_A.RecordCount)) > 0  And LCase(Trim(objDBRcdSet_TestData_A.Fields("WORKORDERTYPE").Value)) = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType_Code" )).value)) And _
		
		
										strScratch = Execute_SQL_Populate_DataSheet_Hatseperator(objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2",   "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"),  "")
										
										If lcase(strScratch) <> "fail" Then
										
											objWB_Master.save ' Save the workbook
											strScratch = ""
											
											' Verify output of query
											' WORKORDERTYPE
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderType_Code" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","WORKORDERTYPE")))
											If temp_expected_value <> "" Then									
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Value of WORKORDERTYPE (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as value ("& temp_expected_value &") mentioned in CIS_workOrderType_Code column in datasheet"
												Else
													fn_set_error_flags true, "Value of WORKORDERTYPE (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from value ("& temp_expected_value &") mentioned in CIS_workOrderType_Code column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to WORKORDERTYPE as CIS_workOrderType_Code column is blank in datasheet"
											End if
											
											' WORKORDERTASK
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","WORKORDERTASK")))
											If temp_expected_value <> "" Then
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
												Else
													fn_set_error_flags true, "Value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to WORKORDERTASK as CIS_workOrderSubType column is blank in datasheet"
											End If
		
											' SERVICEORDERCHARGECODE
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Fee_Applied" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SERVICEORDERCHARGECODE")))
											
											If temp_expected_value <> "" Then
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
												Else
													fn_set_error_flags true, "Value of WORKORDERTASK (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to SERVICEORDERCHARGECODE as Fee_Applied column is blank in datasheet"
											End if
											
											' RETAILERSONUMBER
											temp_expected_value = LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","RETAILERSONUMBER")))
											
											If temp_expected_value <> "" Then
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Value of RETAILERSONUMBER (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
												Else
													fn_set_error_flags true, "Value of RETAILERSONUMBER (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from value ("& temp_expected_value &") mentioned in CIS_workOrderSubType column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to RETAILERSONUMBER as Fee_Applied column is blank in datasheet"
											End If
											
											' SOEFFECTIVEDATE
											temp_expected_value = var_ScheduledDate 
											'temp_expected_value = fnTimeStamp(var_ScheduledDate, "DD-MM-YYYY") ' LCase(Trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("ScheduledDate" )).value)) 
											temp_actual_value = lcase(trim(fn_Fetch_SpecificArrayAttributeValue_Ignore_previous_Errors( objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names")).value,objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values")).value,"^","SOEFFECTIVEDATE")))
											
											If temp_expected_value <> "" Then
												 'temp_expected_value = fnTimeStamp(temp_expected_value, "DD-MM-YYYY")
												If temp_actual_value <> "" Then
													temp_actual_value = fnTimeStamp(temp_actual_value, "DD-MM-YYYY")	
												End If
												
												If  temp_actual_value = temp_expected_value Then
													PrintMessage "p", "CIS_Check_SOstatus_isRaised Verification",  "Value of SOEFFECTIVEDATE (" & temp_actual_value & ") from CIS_SO_Check_Query query is same as value ("& temp_expected_value &") mentioned in ScheduledDate column in datasheet"
												Else
													fn_set_error_flags true, "Value of SOEFFECTIVEDATE (" & temp_actual_value & ") from CIS_SO_Check_Query query is DIFFERENT from value ("& temp_expected_value &") mentioned in ScheduledDate column in datasheet"
													PrintMessage "f", "CIS_Check_SOstatus_isRaised Verification",  gFWstr_ExitIteration_Error_Reason
													strScratch = strScratch & gFWstr_ExitIteration_Error_Reason
												End If
											Else
												PrintMessage "i", "CIS_Check_SOstatus_isRaised Verification",  "Skipping check related to SOEFFECTIVEDATE as ScheduledDate column is blank in datasheet"
											End If
		
											If strScratch <> "" Then
												gFWstr_ExitIteration_Error_Reason = strScratch
											End If
											
										End If	'end of If lcase(strScratch) <> "fail" Then
										
										'											CIS_Check_SOstatus_isRaised int_NMI, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CIS_workOrderType" )).value, _
										'													var_ScheduledDate, objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("NewSO_Status_InCIS" )).value, _
										'													objWS_DataParameter.cells(intRowNr_CurrentScenario ,dictWSDataParameter_KeyName_ColNr("CIS_workOrderSubType" )).value, objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("CIS_ServiceOrderNumber")).value,"n"											
																
									End If 'if CIS_SO_NUM <> ""
									
									
									End If 'If Trim(strSQL_CIS) <> ""  Then											
									
								End If ' If lcase(str_ScenarioRow_FlowName) = "flow1" Then
								
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at CIS_Check_SOstatus_isRaised because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " CIS_Check_SOstatus_isRaised error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "CIS_Check_SOstatus_isRaised complete"
									PrintMessage "p", "MILESTONE - CIS_Check_SOstatus_isRaised complete", "CIS_Check_SOstatus_isRaised complete for row ("  & intRowNr_CurrentScenario &  ")" 
									Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
									Exit Do
								End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
		
		'---ks: db_check_all_aq cases added to flow 12
								Case "DB_Check_ALL_AQS"
								
									tsNow = Now() 
									tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
									If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
										Exit Do
									End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
							
									Printmessage "i", "CASE Start", "[DB_Check_ALL_AQS] for Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ")"
									
									strExpectedVerificationString = trim(objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("DB_Verification_SO_AQ" )))
									
									If strExpectedVerificationString <> "" Then
										temp_Expected_Verification_String_Array = split(strExpectedVerificationString, "^")	
									
										For int_Ctr = 0 To ubound(temp_Expected_Verification_String_Array)
											If temp_Expected_Verification_String_Array(int_Ctr) <> "" Then
												temp_Key = mid(temp_Expected_Verification_String_Array(int_Ctr),1,(instr(1,temp_Expected_Verification_String_Array(int_Ctr),"=")-1))
												temp_Value = mid(temp_Expected_Verification_String_Array(int_Ctr), instr(1,temp_Expected_Verification_String_Array(int_Ctr),"=") + 1, len(temp_Expected_Verification_String_Array(int_Ctr))-instr(1,temp_Expected_Verification_String_Array(int_Ctr),"=") + 1)
												temp_Value = replace(temp_Value, """","")
												fnLoadKey_toDictionary gdict_scratch, trim(temp_Key), trim(temp_Value)
												Printmessage "i", "DB_Check_ALL_AQS", "fnLoadKey_toDictionary arr(" & int_Ctr & ") key (" & temp_Key & "), value (" &  temp_Value &  ")"
											End If 'end of If temp_Expected_Verification_String_Array(int_Ctr) <> "" Then
										Next								
										
										Dim objDBConn_TestData_E, objDBRcdSet_TestData_E
										Set objDBConn_TestData_E    = CreateObject("ADODB.Connection")
										Set objDBRcdSet_TestData_E  = CreateObject("ADODB.Recordset")
									
										For intCtr = 0 To ubound(temp_Expected_Verification_String_Array)
											
											If temp_Expected_Verification_String_Array(intCtr) <> "" AND gdict_scratch("AQ_Data_Error_Reason_Code"&intCtr) <> "" Then
												
												temp_Verify_AQ_Name = gdict_scratch("AQ_Data_Error_Reason_Code"&intCtr)
												temp_Verify_AQ_Description = gdict_scratch("AQ_Change_Reason_Code"&intCtr)
												temp_Verify_AQ_Status = gdict_scratch("AQ_Data_Status"&intCtr)
												temp_Verify_AQ_EmailSent = gdict_scratch("AQ_EmailSent"&intCtr)
												
												PrintMessage "p", "DB_Check_ALL_AQS Query", "Verify Value for arr("& intCtr &"), AQ_Data_Error_Reason_Code ("& temp_Verify_AQ_Name &"), AQ_Change_Reason_Code ("& temp_Verify_AQ_Description &"), AQ_Data_Status (" & temp_Verify_AQ_Status & "), AQ_EmailSent(" & temp_Verify_AQ_EmailSent & ")"									
												
												' Read the SQL template once 			
												temp_SQL_Template = "MTS_B2BSO_FIND_ALL_AQS" 
												PrintMessage "p", "DB_Check_ALL_AQS Query", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing query ("& temp_SQL_Template  &")"
												
												If temp_SQL_Template <> ""  Then
													
													temp_SQL_Template = fnRetrieve_SQL(temp_SQL_Template)
										  
													'Replace Values in SQL
													If InStr(1, temp_SQL_Template,"<RET_SO_NUM>") > 0 Then
														str_ret_so_num = objWS_DataParameter.Cells( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Ret_ServiceOrderNumber")).value
														temp_SQL_Template = Replace ( temp_SQL_Template , "<RET_SO_NUM>" , str_ret_so_num , 1, -1, vbTextCompare)
														Printmessage "i", "DB_Check_ALL_AQS", "SQL Replace: <RET_SO_NUM> with (" & str_ret_so_num & ")"
													End If 
													
													If InStr(1, temp_SQL_Template,"<AQ_NAME>") > 0 Then
														temp_SQL_Template = Replace ( temp_SQL_Template , "<AQ_NAME>" ,temp_Verify_AQ_Name , 1, -1, vbTextCompare)												
														Printmessage "i", "DB_Check_ALL_AQS", "SQL Replace: <AQ_NAME> with (" & temp_Verify_AQ_Name & ")"
													End If 
											
													' Run the query
													fn_append_Query_SQLPopulatedCol objWS_DataParameter, intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr, "DB_Check_ALL_AQS Query", strSQL_MTS
													strScratch = Execute_SQL_Populate_DataSheet (objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_E, objDBRcdSet_TestData_E, temp_SQL_Template, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), "")		
													
													' Perform verifications
													If lcase(strScratch) = "pass" Then									
														If objDBRcdSet_TestData_E.recordcount > 0  Then										
														
															temp_DB_AQ_Description = objDBRcdSet_TestData_E.Fields("DB_AQ_DESCRIPTION").value
															temp_DB_AQ_Status = objDBRcdSet_TestData_E.Fields("DB_AQ_STATUS").value
															temp_DB_AQ_EmailSent = objDBRcdSet_TestData_E.Fields("DB_AQ_EMAIL_SENT_YN").value												
														
															'AQ Description check
															If lcase(trim(temp_Verify_AQ_Description)) =  lcase(trim(temp_DB_AQ_Description)) Then
																PrintMessage "P", "DB_Check_ALL_AQS Validation pass", "Expected value ("&  temp_Verify_AQ_Description & ") is SAME as Actual value("&  temp_DB_AQ_Description & ")"
															Else
																PrintMessage "f", "DB_Check_ALL_AQS Validation fail", "Expected value ("&  temp_Verify_AQ_Description & ") is DIFFERENT than Actual value("&  temp_DB_AQ_Description & ")"
															End If
															
															'AQ Status check
															If lcase(trim(temp_Verify_AQ_Status)) =  lcase(trim(temp_DB_AQ_Status)) Then
																PrintMessage "P", "DB_Check_ALL_AQS Validation pass", "Expected value ("&  temp_Verify_AQ_Status & ") is SAME as Actual value("&  temp_DB_AQ_Status & ")"
															Else
																PrintMessage "f", "DB_Check_ALL_AQS Validation fail", "Expected value ("&  temp_Verify_AQ_Status & ") is DIFFERENT than Actual value("&  temp_DB_AQ_Status & ")"
															End If												
															
															'AQ Email Sent Flag check
															If ucase(trim(temp_Verify_AQ_EmailSent)) =  ucase(trim(temp_DB_AQ_EmailSent)) Then
																PrintMessage "P", "DB_Check_ALL_AQS Validation pass", "Expected value ("&  temp_Verify_AQ_EmailSent & ") is SAME as Actual value("&  temp_DB_AQ_EmailSent & ")"
															Else
																PrintMessage "f", "DB_Check_ALL_AQS Validation fail", "Expected value ("&  temp_Verify_AQ_EmailSent & ") is DIFFERENT than Actual value("&  temp_DB_AQ_EmailSent & ")"
															End If
															
														End If 'end of If objDBRcdSet_TestData_B.recordcount > 0  Then
													Else
														fn_set_error_flags true, "DB_Check_ALL_AQS Query has return no data"
													End If ' If lcase(strScratch) = "pass" Then									
												End If	'If temp_SQL_Template <> ""  Then
											'Else	
												'PrintMessage "i", "DB_Check_ALL_AQS checks","Skipping DB_Check_ALL_AQS checks as the temp_Expected_Verification_String_Array verification string is empty"
											End If	'If temp_Expected_Verification_String_Array(intctr) <> "" Then
											
										Next 'For intctr = 0 To ubound(temp_Expected_Verification_String_Array)
										
										clear_scratch_Dictionary gdict_scratch
									Else
										PrintMessage "i", "DB_Check_ALL_AQS checks","Skipping DB_Check_ALL_AQS checks as the DB_Check_ALL_AQS verification string is empty"
									End If 'If strExpectedVerificationString <> "" Then
		
								set objDBConn_TestData_E = nothing
								set objDBRcdSet_TestData_E = nothing
										
								' At the end, check if any error was there and write in corresponding column
								If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed at DB_Check_ALL_AQS because of error (" & gFWstr_ExitIteration_Error_Reason & ")"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
									Printmessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & "DB_Check_ALL_AQS error), moving to the next row", gFWstr_ExitIteration_Error_Reason
									Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									Exit Do
								Else
									objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "DB_Check_ALL_AQS complete"
									PrintMessage "p", "MILESTONE - DB_Check_ALL_AQS complete", "DB_Check_ALL_AQS complete for row ("  & intRowNr_CurrentScenario &  ")"
									Printmessage "p", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Passed & Completed." 										
									objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
									' Function to write timestamp for next process
									fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
									int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
									int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
									Exit Do	
								
							End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
		
		'----
		
								Case "WaitingForPrevStep"
										temp_last_row_status = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value
										temp_last_row_NextFunctionName = objWS_DataParameter.cells(intRowNr_CurrentScenario -1 , dictWSDataParameter_KeyName_ColNr("Next_Function" )).value
										
										If LCase(temp_last_row_status ) = "pass" Then
											objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value = "start"
										
										ElseIf LCase(temp_last_row_status ) = "fail" Then
											gFWbln_ExitIteration = True
											gFWstr_ExitIteration_Error_Reason =  "Row failed as Previous Step row failed"
											If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "Row failed as Previous Step row failed"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f", "MILESTONE - Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
												Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
										
										ElseIf LCase(temp_last_row_status ) = "" and temp_last_row_NextFunctionName = "" Then
											gFWbln_ExitIteration = True
											gFWstr_ExitIteration_Error_Reason =  "Step1 row not included in execution, marking this row as failed"
											
											If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
												objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = gFWstr_ExitIteration_Error_Reason
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
												PrintMessage "f", "MILESTONE - Row failed as Previous Step row failed", gFWstr_ExitIteration_Error_Reason
												Printmessage "f", "OverAll_TestStatus", "Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Failed & Completed." 
												objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "FAIL"
												int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
											End If 'end of If LCase(gFWbln_ExitIteration) = "y" Or gFWbln_ExitIteration = True Then ' write an error in the results
											
										Else
											Exit Do
										End If 'end of If LCase(temp_last_row_status ) = "pass" Then
								
								
								Case "FAIL"
									Exit Do
								case else ' incase there is no function (which would NEVER happen) name
									Exit Do							
								' --------------------------------------------------------------------
							End Select ' end of Select Case str_Next_Function
						Loop While  (LCase(gFWbln_ExitIteration) = "n" Or gFWbln_ExitIteration = False)
						
						' ###################### FLOW 12 End ##################################################
						
						' ###################### FLOW -B2B_CLEANUP START ################################################
						' 	CASE		: B2BSO_CLEANUP
						' 	Description	: Purging old records
						'	Actions		: 
						' 		If MTS Query defined in WS: (to cater for B2BSO raised via MTS and via B2B automation Script)
						'					i. Find all Open and Working B2BSO for nmis in KDR table 
						'				  	ii. Cancel SO in CIS via CISAPI	
						'				  	iii. delete reconds from KDR MTS table
						'					Note: created_ts < sysdate - 30
						'
						'		If CIS Query defined in WS (to cater for B2BSO's raised by others in CIS)
						'				  	i. Cancel SO in CIS via CISAPI	
						'				  	ii. delete reconds from KDR MTS table
						'					Note: currently restricted to created_dt > 100days old & 50 records (can modify sql if req)
						'
						''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
						Case LCase("B2BSO_CLEANUP"):
						Do
							str_Next_Function 	= objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("Next_Function") ).value 
							Select Case str_Next_Function
								Case "start" ' this is datamine
								' First function
								tsNow = Now() 
								tsNext_ScenarioFunction_canStart_atOrAfter = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")).value
								If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
									Exit Do
								End If 'end of If tsNow < tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
									'############### MTS_Purge_Find_B2B_CIS_SONUM ##################### 
									strSQL_MTS = ""									
									strSQL_MTS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("MTS_SQL_Template_Name")).value 
									
									If strSQL_MTS <> "" Then
										PrintMessage "p", "MTS SQL template: MTS_Purge_Find_B2B_CIS_SONUM","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing MTS Query to Find out CIS SO Numbers (" & strSQL_MTS & ")"
										strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
										
										' Perform replacements																			
		
										' Update query in SQL Query cell
										strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
										strScratch = strScratch & vbCrLf & "MTS_SQL_Template_Name Query:" & vbCrLf & strSQL_MTS
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		
										' Execute MTS SQL
										Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), ""
										
									ELSE
										' capture CIS query
										strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_SQL_Template_Name")).value 
										
										If strSQL_CIS <> ""  Then
											
											PrintMessage "p", "CIS SQL template: CIS_Purge_Find_B2B_CIS_SONUM","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - Executing CIS Query to Find out CIS SO Numbers (" & strSQL_CIS & ")"
											strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
																	
											' Execute CIS SQL
											Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_B, objDBRcdSet_TestData_B, strSQL_CIS, intRowNr_CurrentScenario, "2", "", dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Names"), dictWSDataParameter_KeyName_ColNr("csListSqlColumn_Values"), dictWSDataParameter_KeyName_ColNr("NMI")
										End If ' end of If strSQL_CIS <> ""  Then
								
								
									End If 'End of If strSQL_MTS <> "" Then
									
									If objDBRcdSet_TestData_B.State > 0 Then 
										PrintMessage "p", "B2BSO Purging Started","Scenario row ("& intRowNr_CurrentScenario &") - Purging NMI/CIS SO for total of (SONUM) (" & objDBRcdSet_TestData_B.RecordCount & ")"
										
										For Iterator = 1 To objDBRcdSet_TestData_B.RecordCount
											
											fnTestExeLog_UpdateRuntimeValues intRowNr_CurrentScenario, temp_Scenario_ID, "", "", Trim(LCase(objDBRcdSet_TestData_B.Fields ("NMI"))), Trim(LCase(objDBRcdSet_TestData_B.Fields ("SONUM"))), "", "", "", ""	
											
											'############### CIS_API_Cancel_SO #####################
											strSQL_CIS = ""
											
											strSQL_CIS = objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("CIS_API_Cancel_SO_Query")).value 
											
											If strSQL_CIS = "" Then
												strSQL_CIS = "CIS_API_Cancel_SO"
											End If
											
											PrintMessage "p", "CIS API template: CIS_API_Cancel_SO_Query","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName & _
																	") Record (" & Iterator & " of " & objDBRcdSet_TestData_B.RecordCount & ")- Executing CANCEL SO CIS API Query ("& strSQL_CIS & _
																	"), NMI (" & Trim(LCase(objDBRcdSet_TestData_B.Fields ("NMI"))) & "), SO_NUM: (" & Trim(LCase(objDBRcdSet_TestData_B.Fields ("SONUM"))) & "), WO TYPE (" &  Trim(LCase(objDBRcdSet_TestData_B.Fields ("WO_TYPE"))) & ")"
											strSQL_CIS = fnRetrieve_SQL(strSQL_CIS)
											
											' perform all replacements
											strSQL_CIS = Replace ( strSQL_CIS  , "^param_company_code_egCITI^", Environment.Value("COMPANY_CODE"),  1, -1,vbTextCompare)								
											strSQL_CIS = Replace ( strSQL_CIS  , "^param_user_name_eg_RECONECT^", Environment.Value( Parameter.Item("Environment") & "_" & _
											Environment.Value("COMPANY") & "_CIS_API_USERNAME"),  1, -1,vbTextCompare)								
											strSQL_CIS = Replace ( strSQL_CIS  , "<PARAM_SONUM>", Trim(LCase(objDBRcdSet_TestData_B.Fields ("SONUM"))), 1, -1, vbTextCompare)
											
											'											
											'											' Execute complete SO API Query
											
											' Update query in SQL Query cell
											strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
											strScratch = strScratch & vbCrLf & "CIS_API_Cancel_SO_Query Query:" & vbCrLf & strSQL_CIS
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
											
											'Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS, intRowNr_CurrentScenario, "2", "", "","",""
											Execute_Insert_SQL DB_CONNECT_STR_CIS, objDBConn_TestData_A, objDBRcdSet_TestData_A, strSQL_CIS
											fn_wait(5)
											
											'############### MTS_Purge_KDR #####################
											strSQL_MTS = ""									
											strSQL_MTS = "MTS_Purge_KDR"
											PrintMessage "p", "MTS SQL template: MTS_KDR_Purge","Scenario row ("& intRowNr_CurrentScenario &") - Flow (" & str_ScenarioRow_FlowName &  ") - MTS Purge SQL Template ("& strSQL_MTS &"), Removing KDR records for NMI (" & Trim(LCase(objDBRcdSet_TestData_B.Fields ("NMI"))) & ")"
											strSQL_MTS = fnRetrieve_SQL(strSQL_MTS)
											
											' perform all replacements
											strSQL_MTS = Replace ( strSQL_MTS  , "<int_nmi>", Trim(LCase(objDBRcdSet_TestData_B.Fields ("NMI"))),  1, -1,vbTextCompare)								
		
											' Update query in SQL Query cell
											strScratch = objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value
											strScratch = strScratch & vbCrLf & "MTS_Purge_KDR Query:" & vbCrLf & strSQL_MTS
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).value = strScratch
											objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("SQL_Populated")).WrapText = False
		
											' Execute MTS SQL
											'Execute_SQL_Populate_DataSheet   objWS_DataParameter, DB_CONNECT_STR_MTS, objDBConn_TestData_C, objDBRcdSet_TestData_C, strSQL_MTS, intRowNr_CurrentScenario, "2",  dictWSDataParameter_KeyName_ColNr("SQL_Populated"), "", "", ""												
											Execute_Insert_SQL DB_CONNECT_STR_MTS, objDBConn_TestData_C, objDBRcdSet_TestData_C, strSQL_MTS
											fn_wait(5)
											gFWbln_ExitIteration = False
											
											objDBRcdSet_TestData_B.MoveNext
										Next 'end of For Iterator = 1 To objDBRcdSet_TestData_B.RecordCount
										
										gFWbln_ExitIteration = "y"
										
										objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "COMPLETE"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "END"
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = "B2BSO_CLEANUP complete"
										PrintMessage "p", "MILESTONE - B2BSO_CLEANUP complete", "B2BSO_CLEANUP complete for row ("  & intRowNr_CurrentScenario &  ")" 
										objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("OverAll_PassOrFail" )).value = "PASS"
										' Function to write timestamp for next process
										fn_capture_current_and_future_time_in_excel objWS_DataParameter, intRowNr_CurrentScenario, "" , Cint_wait_time_ZERO, dictWSDataParameter_KeyName_ColNr("Next_ScenarioFunction_canStart_atOrAfter_TS")
										int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
										int_ctr_no_of_passed_rows = int_ctr_no_of_passed_rows +1
										Exit Do
										
									End If	'end of If objDBRcdSet_TestData_B.State > 0 Then 
								End If 'end of If tsNow > tsNext_ScenarioFunction_canStart_atOrAfter Then
								
								'' --------------------------------------------------------------------
							End Select
						Loop While  (LCase(gFWbln_ExitIteration) = "n" Or gFWbln_ExitIteration = False)
						'
						'
						' ###################### FLOW -Clean Up B2B ENDs ################################################
						
						
						Case Else
							fn_set_error_flags true, "No handling of flow (" & str_ScenarioRow_FlowName & "), moving to the next row"
							PrintMessage "f", "MILESTONE - FLOW (" & str_ScenarioRow_FlowName & " unhandled), moving to the next row", gFWstr_ExitIteration_Error_Reason
							objWS_DataParameter.cells ( intRowNr_CurrentScenario , dictWSDataParameter_KeyName_ColNr("RowCompletionStatus") ).value = "FAIL"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Next_Function" )).value = "FAIL"
							objWS_DataParameter.cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments" )).value = gFWstr_ExitIteration_Error_Reason
							int_ctr_no_of_Eligible_rows = int_ctr_no_of_Eligible_rows -1
					End Select ' end of Select Case LCase(str_ScenarioRow_FlowName)
				End If ' If   strInScope = cY  And str_ScenarioRow_FlowName <> "" And LCase(str_Next_Function) <> "fail" And LCase(str_Next_Function) <> "end" Then
				
				' ####################################################################################################################################################################################
				' #####################################################                 LOGGING STEP 3 - Clear data before moving to next row #########################################################################
				' ####################################################################################################################################################################################
				fnTestExeLog_ClearRuntimeValues
				
				
				'		' Before resetting the flags, write the last error in comments column
				'		If gFWstr_ExitIteration_Error_Reason <> "" Then
				'			strScratch = objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value 
				'			strScratch = strScratch & " : " & gFWstr_ExitIteration_Error_Reason
				'			objWS_DataParameter.Cells(intRowNr_CurrentScenario, dictWSDataParameter_KeyName_ColNr("Comments")).value = strScratch 
				'		End If
				'		
				' Reset error flags
				fn_reset_error_flags
				
				' clear scratch dictionary
				clear_scratch_Dictionary gdict_scratch

		Next 'end of For int_Ctr_No_Of_Times = 1 To temp_no_of_times

	Next ' end of For intRowNr_CurrentScenario	=	int_rgWS_cellReportStatus_rowStart to int_rgWS_cellReportStatus_rowEnd	
	
	objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
	objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
	
	fn_reset_error_flags
	
	i = 2
	objWB_Master.save ' Save the workbook 
	
	' Check if there is a need to restart the application
	If var_App_Restart_Time < now  Then
		PrintMessage "i", "Restarting MTS application", Cint_MTS_Application_Restart_Time & " seconds have passed since the application was last restarted, starting it now..."
		fnMTS_WinClose
		OpenMTS Parameter.Item("Company"), Parameter.Item("Environment")
		var_App_Restart_Time = fn_CurrentTimebySeconds(Cint_MTS_Application_Restart_Time)
	End If

Loop While int_ctr_no_of_Eligible_rows > 0

' Once all the XML's are created, now we need to copy them to gateway in blocks..
Performance_Copy_XML_Files_In_Batches gFWstr_RunFolder, 60, 15



PrintMessage "i", "OverAll_RunStats", "Execution Stats: Total inscope rows ("& int_ctr_total_Eligible_rows & " ) - Passed rows (" & int_ctr_no_of_passed_rows  & ") - Failed rows ("& int_ctr_total_Eligible_rows - int_ctr_no_of_passed_rows & ")"
objWS_DataParameter.range("rgWS_Data_Total_Executed_Rows").formula 		= int_ctr_total_Eligible_rows
objWS_DataParameter.range("rgWS_Data_Total_Passed_Rows").formula 		= int_ctr_no_of_passed_rows
objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula 		= Now

objWB_Master.save ' Save the workbook
Printmessage "i", "MILESTONE - End of Execution", "Finished execution of B2BSO scripts"

' Once the test is complete, move the results to network drive
'Printmessage "i", "MILESTONE - Copy to network drive", "Copying results to network drive ("& Cstr_B2BSO_Final_Result_Location &")"
strNewLocation = replace (strRunLog_Folder,Temp_Execution_Results_Location, Cstr_B2BSO_Final_Result_Location) 
Printmessage "i", "OverAll_RunStats", "Execution Results network drive ("& strNewLocation &")"
copyfolder  gFWstr_RunFolder , Cstr_B2BSO_Final_Result_Location

Printmessage "i", "OverAll_RunStats", "Execution Start Time (" & CDate(objWS_DataParameter.range("rgWS_Data_Exe_Start_Time").formula) & ") & Execution End Time ("& CDate(objWS_DataParameter.range("rgWS_Data_Exe_End_Time").formula)  &")"

' Close application
fnMTS_WinClose


'''''
'''''
'''''' Implementation of AQ check code
'''''str_string = "^AQ_Data_Error_Reason_Code1= ""SO_OUTSTANDING""^AQ_Data_Status1=""OPEN""^AQ_Change_Reason_Code1=""3001 - Create Meter Details - Retrospective"""
'''''
'''''initialize_scratch_Dictionary
'''''clear_scratch_Dictionary gdict_scratch
'''''
'''''fn_Compare_Multiple_AQ_Search_Service_Order_AQ_Screen str_string, "^"
'''''
'''''fn_reset_error_flags
'''''str_string = "^AQ_Data_Error_Reason_Code1= ""SO_OUTSTANDING""^AQ_Data_Status1=""OPEN""^AQ_Change_Reason_Code1=""3001 - Create Meter Details - Retrospective""^AQ_Data_Error_Reason_Code2= ""SO_MTRADDALTS""^AQ_Data_Status2=""OPEN""^AQ_Change_Reason_Code2=""3001 - Create Meter Details - Retrospective"""
'''''fn_Compare_Multiple_AQ_Search_Service_Order_AQ_Screen str_string, "^"
'''''
'''''
