SELECT EXTRACTVALUE(VALUE(TBL_X), 'TransactionAcknowledgement/Event/Code/@description')  AS DESCRIPTION
     , EXTRACTVALUE(VALUE(TBL_X), 'TransactionAcknowledgement/Event/Code') AS CODE
     , EXTRACTVALUE(VALUE(TBL_X), 'TransactionAcknowledgement/@status') AS STATUS
     , EXTRACTVALUE(VALUE(TBL_X), 'TransactionAcknowledgement/Event/Explanation') AS EXPLANATION
     , EXTRACTVALUE(VALUE(TBL_X), 'TransactionAcknowledgement/@initiatingTransactionID') AS INITIATINGTRANSACTIONID 
    , GD.CONTENTS
    FROM GW_DOCUMENT_CONTENTS GD
, TABLE(XMLSEQUENCE(EXTRACT(XMLTYPE(GD.CONTENTS), '*/Acknowledgements/TransactionAcknowledgement'))) TBL_X    
    WHERE GD.DOCUMENT_ID IN (SELECT DOCUMENT_ID FROM GW_MESSAGE
                            WHERE ID IN ( (SELECT MESSAGE_ID FROM GW_TRANSACTION_ACKNOWLEDGEMENT GTA
                                            WHERE GTA.INIT_TRANSACTION_ID IN ('<INIT_TRANSACTION_ID>'))))