   SELECT SH.SERVICE_ORDER_NUMBER AS SQL_RET_SONUM
   FROM   B2B_SERVICE_ORDER_HEADER   SH
     LEFT JOIN B2B_SERVICE_ORDER          SO  ON SO.SERVICE_ORDER_HEADER_ID = SH.ID
     LEFT JOIN B2B_ACTION_TYPE SOAT ON SOAT.ID = SO.ACTION_TYPE_ID 
   WHERE SH.NMI = SUBSTR('<sqltag_nmi>',1,10)
   AND upper(SOAT.ACTION_TYPE_CD) = UPPER('<sqltag_action_type>')
   AND SH.UPDATED_DT > SYSDATE - 1
and rownum < 2