Select Distinct T056.Txt_Nat_Supp_Point As NMI,Trim(FRMP.Txt_Identifier) as FRMP, Trim(LNSP.Txt_Identifier) as LNSP
    From Tvp056servprov T056      
       , TVP054SERVPROVRESP t054
       , TVP024CUSTACCTROLE t024
       , Tvp064lename T064
       , TVP036LEGALENTITY T036        
       , Tvp097propscalert T097
       , Tvp704markpartresp Frmpresp
       , TVP707MARKPARTICIP FRMP
       , Tvp704markpartresp Lnspresp
       , Tvp707markparticip Lnsp
    Where T056.No_Property = T054.No_Property
      And T056.No_Property = T097.No_Property
      And T056.Cd_Company_System = T024.Cd_Company_System
      And T056.Cd_Company_System = T064.Cd_Company_System
      And T056.Cd_Company_System = T097.Cd_Company_System     
      And T056.Cd_Company_System = T054.Cd_Company_System      
      And T054.No_Account = T024.No_Account
      and T024.NO_LEGAL_ENTITY = t064.no_legal_entity
      And T056.Cd_Service_Prov       = 'E'
      And T056.St_Serv_Prov          = 'A'
      And T024.Tp_Cust_Acct_Role     = '3'      
      and nvl(t024.DT_END,sysdate+1) > sysdate
      And T024.Txt_Requestor_Rel = 'Resident'
      And T036.Ind_Legal_Entity = 'I'      
      And T064.Ind_Name              = 'I'     
      and t064.NM_LE_C4              <> 'UNKN'
      and T056.NO_PROPERTY = FRMPRESP.NO_PROPERTY
      and T056.CD_SERVICE_PROV = FRMPRESP.CD_SERVICE_PROV
      and T056.CD_COMPANY_SYSTEM = FRMPRESP.CD_COMPANY_SYSTEM
      and FRMPRESP.DT_END_RESP is null
      and FRMPRESP.CD_MARKET_ROLE = 'FRMP'
      and FRMP.CD_COMPANY_SYSTEM = FRMPRESP.CD_COMPANY_SYSTEM
      and FRMP.CD_MARKET_ROLE = FRMPRESP.CD_MARKET_ROLE
      and FRMP.NO_LEGAL_ENTITY = FRMPRESP.NO_LEGAL_ENTITY
      and FRMP.CD_SERVICE_PROV = FRMPRESP.CD_SERVICE_PROV
      and T056.NO_PROPERTY = LNSPRESP.NO_PROPERTY
      and T056.CD_SERVICE_PROV = LNSPRESP.CD_SERVICE_PROV
      and T056.CD_COMPANY_SYSTEM = LNSPRESP.CD_COMPANY_SYSTEM
      and LNSPRESP.DT_END_RESP is null
      and LNSPRESP.CD_MARKET_ROLE = 'LNSP'
      and LNSP.CD_COMPANY_SYSTEM = LNSPRESP.CD_COMPANY_SYSTEM
      and LNSP.CD_MARKET_ROLE = LNSPRESP.CD_MARKET_ROLE
      and LNSP.NO_LEGAL_ENTITY = LNSPRESP.NO_LEGAL_ENTITY
      And Lnsp.Cd_Service_Prov = Lnspresp.Cd_Service_Prov
      And Trim(T097.Cd_Spec_Cond_Tp) = 'LIFE'
      And T097.Dt_End Is Null
      And T056.Txt_Nat_Supp_Point NOT IN (<NMI_LIST_FROM_KDR>)
      And Rownum < 2
      Order By Dbms_Random.Value
