SELECT   gf.filename
       , gf.FILE_TIME_STAMP 
       , nmid.NMI || nmid.CHECKSUM as NMI 
       , gm.TRANSACTION_GRP
       , tt.TRANSACTION_TYPE
       , investigation_cd
       , mtd2.BEGIN_DT AS REQUEST_BEGIN_DATE
       , mtd2.END_DT AS REQUEST_END_DATE
    FROM gw_message gm
       , gw_document gd
       , gw_file gf
       , gw_transaction gt
       , transaction_type tt
       , b2b_md_transaction_detail mtd2
       , b2b_md_investigation_code mdic
       , transaction_detail mtd
       , nmi_standing_data nmid
   WHERE gf.document_id = gd.ID
     AND gt.message_id = gm.ID
     AND tt.ID = gt.TRANSACTION_TYPE_ID
     AND gd.ID = gm.document_id
     AND mtd.GW_TRANSACTION_ID = gt.ID
     AND mtd.ID = mtd2.TRANSACTION_DETAIL_ID
     AND nmid.ID = mtd2.NMI_STANDING_DATA_ID
     AND MTD2.INVESTIGATION_ID = mdic.id
     AND gd.direction = '<document_Direction>'
     AND gm.TRANSACTION_GRP = '<TRANSACTION_GRP>'
     AND tt.id = 75
     AND investigation_cd in ('<Investigation_Code>') 
     and rownum <10
  order by file_time_stamp desc