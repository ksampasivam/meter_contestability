declare
  queue_options      DBMS_AQ.ENQUEUE_OPTIONS_T;
  message_properties DBMS_AQ.MESSAGE_PROPERTIES_T;
  message_id         RAW(16);
  my_message         SYS.AQ$_JMS_TEXT_MESSAGE;
begin
  my_message := SYS.AQ$_JMS_TEXT_MESSAGE.construct;
  my_message.header.clear_properties();
  my_message.header.set_string_property('CISOVEnableDiagnostics', 'TRUE');
  message_properties.correlation := 'PRE_'||to_char(systimestamp,'dd_mm_yyyy_hh24_mi_ss');
  my_message.set_text('  
<CreateServiceOrderRequest xmlns="http://www.logica.com/CISOV/API">
            <RequestHeader>
                        <Version>0</Version>
                        <CompanyCode>^param_company_code_egCITI^</CompanyCode>
                                <UserName>^param_user_name_eg_RECONECT^</UserName>
            </RequestHeader>
            <RequestBody>
                        <NMI><param_NMI></NMI>
                        <ServiceOrderType><param_CIS_workOrderType></ServiceOrderType>
                        <ServiceProvisionType>E</ServiceProvisionType>
                        <EffectiveDate><param_scheduledate_plus_two_days></EffectiveDate>
                        <ServiceOrderReason>Ret Request - BH</ServiceOrderReason>
                        <WorkCompleteFlag>Y</WorkCompleteFlag>
                        <ResponsibleRetailer><param_Retailer_code></ResponsibleRetailer>
                        <AdditionalData>
                                    <ExtReferenceGroup>
                                                <ExtGroupDesc>Retailer Details</ExtGroupDesc>
                                                <ExtReference>
                                                            <ExtRefDesc>Retailer Name</ExtRefDesc>
                                                            <ExtRefDetails><param_RetailerName></ExtRefDetails>
                                                </ExtReference>
                                    </ExtReferenceGroup>
                                    <ExtReferenceGroup>
                                                <ExtGroupDesc>Retailer Details</ExtGroupDesc>
                                                <ExtReference>
                                                            <ExtRefDesc>Retailer Order Numb</ExtRefDesc>
                                                            <ExtRefDetails><param_ RET_SO_NUM></ExtRefDetails>
                                                </ExtReference>
                                    </ExtReferenceGroup>
                        </AdditionalData>
            </RequestBody>
</CreateServiceOrderRequest>
');
  DBMS_AQ.ENQUEUE(queue_name         => 'WLAQ.CIS_IN_Q',
                  enqueue_options    => queue_options,
                  message_properties => message_properties,
                  payload            => my_message,
                  msgid              => message_id);
  commit;
end;