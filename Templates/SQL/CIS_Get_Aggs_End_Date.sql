select LEAST(nvl(DT_PLANNED,to_date('<Today>','DD/MM/YYYY')-1),TO_DATE ('<Aggs_Start_date>', 'DD/MM/YYYY')+7) AS NEXT_BILL_DATE,
DT_PLANNED as NEXT_READ_DATE
from TVP299SCHEDROUTE
 where  no_route =  '<Route_No>'
   and dt_planned > to_date('<Aggs_start_Date>'  ,'DD/MM/YYYY'  )
   and  st_sched_bc_r_106 =  'A'
   and  rownum  <  2
 order by  dt_planned ASC