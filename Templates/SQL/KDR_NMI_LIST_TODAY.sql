Select replace(rtrim(xmlagg(xmlelement(NMI, ':'||NMI||':,')).extract('//text()'),','),':',chr(39)) as MTS_NMI_LIST
FROM TEST_DATA_KEY_REGISTER  TDR
                WHERE trunc(TDR.CREATED_TS) = TRUNC (SYSDATE)
                and tdr.nmi is not null
                group by trunc(rownum/200)