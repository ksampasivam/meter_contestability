SELECT DISTINCT
    SRACLS.VALUE AS NMI_SIZE
    , 'MDP' AS STATIC_MDP
    , 'RP'  AS STATIC_RP
    , TO_CHAR(R.ENDTIME,'DD/MM/YYYY') AS LAST_READ_DATE
    , MPRP.PARTICIPANT_NAME AS ROLE_RP
    , MPMDP.PARTICIPANT_NAME AS ROLE_MDP
    , MPMPB.PARTICIPANT_NAME AS ROLE_MPB
    , MPMPC.PARTICIPANT_NAME AS ROLE_MPC
    , MPLNSP.PARTICIPANT_NAME AS ROLE_LNSP
    , MPFRMP.PARTICIPANT_NAME AS ROLE_FRMP
    , (SELECT MPAF.PARTICIPANT_NAME
        FROM MARKET_PARTICIPANT MPAF, MARKET_PARTICIPANT_ROLE MPRAF
        WHERE MPAF.ID = MPRAF.MARKET_PARTICIPANT_ID
         AND MPRAF.ROLE_ID = 6       /* 6 = FRMP */
          AND MPAF.ID <> SMRFRMP.MARKET_PARTICIPANT_ID
          AND ROWNUM <=1)                                   AS ROLE_ALTFRMP
    , (SELECT MPAMB.PARTICIPANT_NAME
        FROM MARKET_PARTICIPANT MPAMB, MARKET_PARTICIPANT_ROLE MPRAMB
        WHERE MPAMB.ID = MPRAMB.MARKET_PARTICIPANT_ID
          AND MPRAMB.ROLE_ID = 3       /* 3= MPB */
          AND MPAMB.ID <> SMRMPB.MARKET_PARTICIPANT_ID
          AND ROWNUM <=1)                                   AS ROLE_ALTMPB
    , (SELECT MPAMD.PARTICIPANT_NAME
       FROM MARKET_PARTICIPANT MPAMD, MARKET_PARTICIPANT_ROLE MPRAMD
        WHERE MPAMD.ID = MPRAMD.MARKET_PARTICIPANT_ID
          AND MPRAMD.ROLE_ID = 5       /* 5 = MDP */
          AND MPAMD.ID <> SMRMDP.MARKET_PARTICIPANT_ID
          AND ROWNUM <=1)                                   AS ROLE_ALTMDP
    , (SELECT MPAMC.PARTICIPANT_NAME
        FROM MARKET_PARTICIPANT MPAMC, MARKET_PARTICIPANT_ROLE MPRAMC
        WHERE MPAMC.ID = MPRAMC.MARKET_PARTICIPANT_ID
          AND MPRAMC.ROLE_ID = 4       /* 4 = MPC */
          AND MPAMC.ID <> SMRMPC.MARKET_PARTICIPANT_ID
          AND ROWNUM <=1)                                   AS ROLE_ALTMPC
    , (SELECT MPARP.PARTICIPANT_NAME
        FROM MARKET_PARTICIPANT MPARP, MARKET_PARTICIPANT_ROLE MPRARP
        WHERE MPARP.ID = MPRARP.MARKET_PARTICIPANT_ID
          AND MPRARP.ROLE_ID = 2       /* 2 = RP */
          AND MPARP.ID <> SMRRP.MARKET_PARTICIPANT_ID
          AND ROWNUM <=1)                                   AS ROLE_ALTRP
    , S.NAME AS NMI_STATUS, SS.SERVICEPOINT AS NMI
    , SS. *,   S.*, 'zCol' AS ZCOL
    FROM    SERVICEPOINT_STATUS SS
    JOIN       STATUS S ON S.ID = SS.STATUS_ID
    JOIN SERVICEPOINT_REF_ATTRIBUTE SRACLS ON SRACLS.SERVICEPOINT = SS.SERVICEPOINT
    JOIN SERVICEPOINT_REF_ATTRIBUTE SRAMTR ON SRAMTR.SERVICEPOINT = SS.SERVICEPOINT
    JOIN SERVICEPOINT_MP_ROLE SMRRP ON SMRRP.SERVICEPOINT = SS.SERVICEPOINT
       AND SMRRP.ROLE_ID = 2     /* 2 = RP   */
    JOIN MARKET_PARTICIPANT MPRP ON MPRP.ID = SMRRP.MARKET_PARTICIPANT_ID
       AND EXISTS (SELECT 1 FROM ORGANISATION_PARTICIPANT
                    WHERE PARTICIPANT_ID = MPRP.ID)
    JOIN SERVICEPOINT_MP_ROLE SMRMDP ON SMRMDP.SERVICEPOINT = SS.SERVICEPOINT
       AND SMRMDP.ROLE_ID = 5     /* 5 = MDP  */
    JOIN MARKET_PARTICIPANT MPMDP ON MPMDP.ID = SMRMDP.MARKET_PARTICIPANT_ID
       AND EXISTS (SELECT 1 FROM ORGANISATION_PARTICIPANT
                    WHERE PARTICIPANT_ID = MPMDP.ID)    
    JOIN SERVICEPOINT_MP_ROLE SMRMPB ON SMRMPB.SERVICEPOINT = SS.SERVICEPOINT
      AND SMRMPB.ROLE_ID = 3     /* 3 = MPB  */
    JOIN MARKET_PARTICIPANT MPMPB ON MPMPB.ID = SMRMPB.MARKET_PARTICIPANT_ID
       AND EXISTS (SELECT 1 FROM ORGANISATION_PARTICIPANT
                    WHERE PARTICIPANT_ID = MPMPB.ID)
    JOIN SERVICEPOINT_MP_ROLE SMRMPC ON SMRMPC.SERVICEPOINT = SS.SERVICEPOINT
       AND SMRMPC.ROLE_ID = 4     /* 4 = MPC  */
    JOIN MARKET_PARTICIPANT MPMPC ON MPMPC.ID = SMRMPC.MARKET_PARTICIPANT_ID
       AND EXISTS (SELECT 1 FROM ORGANISATION_PARTICIPANT
                    WHERE PARTICIPANT_ID = MPMPC.ID)
    JOIN SERVICEPOINT_MP_ROLE SMRLNSP ON SMRLNSP.SERVICEPOINT = SS.SERVICEPOINT
       AND SMRLNSP.ROLE_ID = 1     /* 1 = LNSP */
    JOIN MARKET_PARTICIPANT MPLNSP ON MPLNSP.ID = SMRLNSP.MARKET_PARTICIPANT_ID
       AND EXISTS (SELECT 1 FROM ORGANISATION_PARTICIPANT
                    WHERE PARTICIPANT_ID = MPLNSP.ID)
    JOIN SERVICEPOINT_MP_ROLE SMRFRMP ON SMRFRMP.SERVICEPOINT = SS.SERVICEPOINT
       AND SMRFRMP.ROLE_ID = 6      /* 6 = FRMP */
    JOIN MARKET_PARTICIPANT MPFRMP ON MPFRMP.ID = SMRFRMP.MARKET_PARTICIPANT_ID
    JOIN SERVICEPOINT@IEE_LINK.CORP.CHEDA.NET SP ON UPPER(SP.SERVICEPOINTID) = UPPER(SS.SERVICEPOINT)
    JOIN NODELINK@IEE_LINK.CORP.CHEDA.NET NL ON NL.LEFTNODEKEY = SP.NODEKEY
    JOIN SERVICEPOINTCHANNEL@IEE_LINK.CORP.CHEDA.NET SPC ON SPC.NODEKEY = NL.RIGHTNODEKEY
    JOIN READING@IEE_LINK.CORP.CHEDA.NET R ON R.NODEID = SPC.SPCNODEID
    LEFT OUTER JOIN TEST_DATA_KEY_REGISTER  TDK on SS.SERVICEPOINT = TDK.NMI
          and TDK.IS_RETIRED_YN = 'N' AND TDK.IS_IN_USE_YN  = 'Y'
    WHERE 1=1
      AND TDK.NMI IS NULL
      AND S.NAME = 'A'               
      AND SRACLS.ATTRIBUTE_TYPE_ID = 5   
      AND SRACLS.VALUE = 'SMALL'           
      AND SRACLS.END_DT > SYSDATE
      AND SRAMTR.ATTRIBUTE_TYPE_ID = 1  
      AND SRAMTR.VALUE LIKE 'MRIM'         
      AND SRAMTR.END_DT > SYSDATE
      AND SS.END_DT > SYSDATE
      AND SMRRP.END_DT > SYSDATE
      AND SMRMDP.END_DT > SYSDATE
      AND SMRMPB.END_DT > SYSDATE
      AND SMRMPC.END_DT > SYSDATE 
      AND SMRLNSP.END_DT > SYSDATE
      AND SMRFRMP.END_DT > SYSDATE
      AND R.ENDTIME = (SELECT MAX(R1.ENDTIME)
                    FROM READING@IEE_LINK.CORP.CHEDA.NET R1
                    WHERE 1=1
                        AND R1.NODEID = R.NODEID
                        AND R1.ENDTIME > TO_DATE('01/10/2016','dd/mm/yyyy')
                        AND R1.STATUSID > 0
                    )
    AND R.ENDTIME > TO_DATE('01/10/2016','dd/mm/yyyy')
    AND R.STATUSID > 0
    AND R.ENDTIMESECONDS IS NULL
      AND ROWNUM <= <rownum>