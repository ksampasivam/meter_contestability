SELECT 
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//mts:Request/@correlationId', 'xmlns:mts="http://www.itron.com/mts/2008/08"') AS CorrelationId,
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//mts:ProposedStatus', 'xmlns:mts="http://www.itron.com/mts/2008/08"') AS ProposedStatus,
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//mts:Reason', 'xmlns:mts="http://www.itron.com/mts/2008/08"') AS Reason,
EXTRACTVALUE(XMLTYPE(T.USER_DATA),'//mts:EffectiveDate', 'xmlns:mts="http://www.itron.com/mts/2008/08"') AS EffectiveDate
FROM MTS.SQT_INTERFACE_OUT T
WHERE
DBMS_LOB.INSTR(T.USER_DATA.PAYLOAD_DATA.GETCLOBVAL(), '<NMI>') > 0
and DBMS_LOB.INSTR(T.USER_DATA.PAYLOAD_DATA.GETCLOBVAL(), '<ScheduledDate>') > 0
and t.ENQ_TIME = (select max(ENQ_TIME) 
                    from mts.SQT_INTERFACE_OUT sq
                    where sq.ENQ_TIME >= to_date('<ScheduledDate_DD-MMM-YYYY>')
                    and instr(sq.user_data.payload_data.getclobval(), '<NMI>')>0) 
ORDER BY T.ENQ_TIME ASC
