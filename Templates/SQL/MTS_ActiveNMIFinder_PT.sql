SELECT 
      SS.NMI
    , SS.NMI_STATUS
    , SS.RET_SERVICEORDERNUMBER
    , SS.ROLE_MPB
    , SS.ROLE_MDP
    , SS.ROLE_LE 
    , SS.ROLE_LNSP 
    , SS.ROLE_FRMP
    , SS.ROLE_ALTFRMP
    , SS.METERNUMBER
    FROM ETS_B2B_SO_ACTIVE_NMI_FINDER SS 
    LEFT OUTER JOIN TEST_DATA_KEY_REGISTER  TDR ON SS.NMI = TDR.NMI AND (TDR.IS_RETIRED_YN = 'Y' OR TDR.IS_IN_USE_YN  = 'Y' OR TDR.IS_RESERVED_YN ='Y')   AND TDR.CREATED_TS > TRUNC (SYSDATE-1)
    WHERE 1=1
      AND TDR.NMI IS NULL
      AND ROWNUM < 2