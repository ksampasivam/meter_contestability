/* Get CIS Response - USB to MTS */
select t.corrid As CIS_CorrelationID,CISCOR.TRANSACTION_DETAIL_ID As MTS_TRANSACTION_DETAIL_ID, 
transtat.status_id as MTS_status_id, 
st.name as MTS_status_Name, 
st.business_status  as MTS_Business_Status_Name
from mts.sqt_interface_in t, cis_instruction CisCor, transaction_status transtat, status st
where t.corrid in (select to_char(ci.correlation_id) as MTS_CIS_CORRELATION_ID
from cis_instruction ci
where transaction_detail_id = (Select id from transaction_detail td where td.transactionid = 
'<INIT_TRANSACTION_ID>')
and NMI = '<NMI>')
and t.corrid = CISCOR.CORRELATION_ID
and transtat.transaction_detail_id = CISCOR.TRANSACTION_DETAIL_ID
and st.id = transtat.status_id