SELECT SERVICEPOINT,CRCODE, CR_TXN_STATUS, INITIATED_DATE, PROPOSED_DATE, 
  METERSERIALNUMBER,
  MANUFACTURER, 
  MODELCODE,
  METERSTATUS,
  EXTRACTVALUE(VALUE(F), 'Register/RegisterID')        AS REGISTERID,
  EXTRACTVALUE(VALUE(F), 'Register/NetworkTariffCode') AS NETWORKTARIFFCODE,
  EXTRACTVALUE(VALUE(F), 'Register/UnitOfMeasure')     AS UNITOFMEASURE,
  EXTRACTVALUE(VALUE(F), 'Register/Suffix')            AS SUFFIX,
  EXTRACTVALUE(VALUE(F), 'Register/ControlledLoad')    AS CONTROLLEDLOAD,
  EXTRACTVALUE(VALUE(F), 'Register/Status')            AS REGSTATUS,
  EXTRACTVALUE(VALUE(F), 'Register/TimeOfDay')         AS TIMEOFDAY
FROM
  (SELECT VALUE(E) VAL2,CRCODE, CR_TXN_STATUS, INITIATED_DATE, PROPOSED_DATE, 
    SERVICEPOINT ,
    EXTRACTVALUE(VALUE(E), 'Meter/SerialNumber')         AS METERSERIALNUMBER,
    EXTRACTVALUE(VALUE(E), 'Meter/Manufacturer')         AS MANUFACTURER,
    EXTRACTVALUE(VALUE(E), 'Meter/Model')                AS MODELCODE,
    EXTRACTVALUE(VALUE(E), 'Meter/ReadTypeCode')         AS READTYPECODE,
    EXTRACTVALUE(VALUE(E), 'Meter/Status')               AS METERSTATUS
  FROM
    (SELECT VALUE(D) VAL1 ,
      CRC.CRCODE AS CRCODE
    , S.NAME AS CR_TXN_STATUS
    , CCR.INITIATED_DATE AS INITIATED_DATE
    , CCD.PROPOSED_DATE AS PROPOSED_DATE,
      EXTRACTVALUE(VALUE(D), 'Transaction/CATSChangeRequest/NMIStandingData/NMI') AS SERVICEPOINT
    FROM GW_DOCUMENT_CONTENTS GDC,
      CAT_CHANGE_REQUEST CCR ,
      TRANSACTION_STATUS TS ,
      STATUS S ,
      CAT_CHANGE_REASON_CODE CRC ,
      TRANSACTION_DETAIL TD ,
      GW_TRANSACTION GT ,
      GW_MESSAGE GM ,
      CAT_CR_DATA CCD ,
      GW_FILE GF ,
      TABLE(XMLSEQUENCE(EXTRACT(XMLTYPE(GDC.CONTENTS), 'ase:aseXML/Transactions/Transaction', ' xmlns:ase="urn:aseXML:r35"'))) D
    WHERE CCR.NMID                = '<PARAM_NMID>'
    AND CRC.CRCODE                = '<CRCODE>'
    AND CCR.INITIATED_DATE       >= trunc(sysdate  <INITIATED_DATE>)
    AND TS.TRANSACTION_DETAIL_ID  = CCR.TRANSACTION_DETAIL_ID
    AND TS.STATUS_ID              = S.ID
    AND CCR.CHANGE_REASON_CODE_ID = CRC.ID
    AND CCR.TRANSACTION_DETAIL_ID = TD.ID
    AND TD.GW_TRANSACTION_ID      = GT.ID
    AND GT.MESSAGE_ID             = GM.ID
    AND GM.DOCUMENT_ID            = GDC.DOCUMENT_ID
    AND CCR.CR_DATA_ID            = CCD.ID
    AND GM.DOCUMENT_ID            = GF.DOCUMENT_ID
    ) GDC1,
    TABLE(XMLSEQUENCE(EXTRACT(GDC1.VAL1, 'Transaction/CATSChangeRequest/NMIStandingData/MeterRegister/Meter'))) E
  ) GDC2,
  TABLE(XMLSEQUENCE(EXTRACT(GDC2.VAL2, 'Meter/RegisterConfiguration/Register'))) F
WHERE SERVICEPOINT                                = '<PARAM_NMID>'
ORDER BY METERSERIALNUMBER, REGISTERID